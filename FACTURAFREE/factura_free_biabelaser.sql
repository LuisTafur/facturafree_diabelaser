﻿# Host: localhost  (Version 5.5.5-10.4.32-MariaDB)
# Date: 2024-05-02 16:28:25
# Generator: MySQL-Front 6.0  (Build 1.82)


#
# Structure for table "cliente"
#

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipoDocumento` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '',
  `numeroDocumento` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '',
  `nombreRazonSocial` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo_envios` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '-',
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "cliente"
#

INSERT INTO `cliente` VALUES (1,'SIN DOCUMENTO','','PUBLICO VARIOS','--','-','ACTIVO'),(2,'DNI','48745241','VALDERRAMA CERDÁN JHOSE MICHEL','MZ D LT 16 COSMOVISION - PACHACUTEC - VENTANILLA - CALLAO - LIMA - LIMA - LIMA - LIMA','-','ACTIVO'),(3,'DNI','48124556','TAFUR TORRES LUIS CESAR','MZ D LT 16 COSMOVISION - PACHACUTEC - VENTANILLA - CALLAO - LIMA - LIMA - LIMA - LIMA','-','ACTIVO'),(4,'SIN DOCUMENTO','','JUAN CARLOS ','--','-','ACTIVO'),(5,'DNI','48203580','ORELLANA JORGE VIVIAN KATHERINE','-','-','ACTIVO');

#
# Structure for table "compra"
#

DROP TABLE IF EXISTS `compra`;
CREATE TABLE `compra` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idCliente` int(11) unsigned NOT NULL DEFAULT 0,
  `fecha` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `horaEmision` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fechaVencimiento` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `moneda` char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `medioPago` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `totalVentasGravadas` decimal(10,2) NOT NULL DEFAULT 0.00,
  `totalGratuito` decimal(10,2) DEFAULT 0.00,
  `totalExonerado` decimal(10,2) DEFAULT 0.00,
  `igv` decimal(10,2) NOT NULL DEFAULT 0.00,
  `importeTotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `motivo_anulacion` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `icbper` decimal(10,2) DEFAULT 0.00,
  `estado_comprobante` varchar(50) DEFAULT NULL,
  `comprobante_relacionado` varchar(50) DEFAULT NULL,
  `id_usuario` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "compra"
#

INSERT INTO `compra` VALUES ('CM01-00000001',1,'2022-02-24','21:37:41','2022-02-24','PEN','EFECTIVO',8.47,0.00,0.00,1.53,10.00,'',0.00,'GENERADO','F001-312',1),('CM01-00000002',1,'2024-05-02','14:36:40','2024-05-02','PEN','EFECTIVO',76.27,0.00,0.00,13.73,90.00,'',0.00,'GENERADO','B001-65',1);

#
# Structure for table "compradet"
#

DROP TABLE IF EXISTS `compradet`;
CREATE TABLE `compradet` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idcompra` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `item` int(11) NOT NULL DEFAULT 0,
  `cantidad` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tipoUnidad` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tributo` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `montoTributo` decimal(10,2) DEFAULT 0.00,
  `tipoAfectacionTributo` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valorUnitario` decimal(10,2) NOT NULL DEFAULT 0.00,
  `precioUnitarioItem` decimal(10,2) NOT NULL DEFAULT 0.00,
  `valorUnitarioGratuito` decimal(10,2) NOT NULL DEFAULT 0.00,
  `precio` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "compradet"
#

INSERT INTO `compradet` VALUES ('CM01-00000001-001','CM01-00000001',1,1.00,'NIU','2022223155820','ASDASDASDA','IGV',1.53,'10',8.47,10.00,0.00,10.00),('CM01-00000002-001','CM01-00000002',4,1.00,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.05,'10',16.95,20.00,0.00,20.00),('CM01-00000002-002','CM01-00000002',5,1.00,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.58,'10',25.42,30.00,0.00,30.00),('CM01-00000002-003','CM01-00000002',3,1.00,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.05,'10',16.95,20.00,0.00,20.00),('CM01-00000002-004','CM01-00000002',2,1.00,'NIU','2022224234535','PRODUCTO 1','IGV',1.53,'10',8.47,10.00,0.00,10.00),('CM01-00000002-005','CM01-00000002',1,1.00,'NIU','2022223155820','ASDASDASDA','IGV',1.53,'10',8.47,10.00,0.00,10.00);

#
# Structure for table "config"
#

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `ruc` varchar(11) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `razonSocial` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `web` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `impresion` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `usa_cnt_flotante` char(2) NOT NULL DEFAULT 'NO',
  `producto_igv` char(2) NOT NULL DEFAULT 'SI',
  `serv_datos` varchar(100) DEFAULT NULL,
  `correo_aplicacion` varchar(255) NOT NULL DEFAULT '-',
  `pass_aplicacion` varchar(255) NOT NULL DEFAULT '-',
  `logo` varchar(255) DEFAULT 'logo.png',
  `email_contador` varchar(255) DEFAULT '',
  `seriePresupuesto` varchar(4) DEFAULT 'CT01',
  `serieVenta_interna` varchar(4) DEFAULT 'V001',
  `serieCompra` varchar(4) DEFAULT 'CM01',
  `impre_directa` varchar(4) DEFAULT 'NO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "config"
#

INSERT INTO `config` VALUES (1,'10481245562','TAFUR TORRES LUIS CESAR TAFUR TORRES','DIRECCION DE PRUEBA DE SISTEMA DE EMISION ELECTRONICA SISTEMA DE EMISION ELECTRONICA','924899800','mi_correo@gmail.com','https://emiteperu.com','Ticket 80 mm','NO','SI','SERVIDOR 2','cnDW4e8TV0+Ndw38AsX4kKRMlcoEsoNR7IwXE80e42Y=','fihkNbFeccUNXz0BsPl7AeqAslrmktqP','logo.png','IngenieroLuisTafur@gmail.com','CT01','V001','CM01','NO');

#
# Structure for table "presupuesto"
#

DROP TABLE IF EXISTS `presupuesto`;
CREATE TABLE `presupuesto` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idCliente` int(11) unsigned NOT NULL DEFAULT 0,
  `fecha` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `horaEmision` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fechaVencimiento` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `moneda` char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `medioPago` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `totalVentasGravadas` decimal(10,2) NOT NULL DEFAULT 0.00,
  `totalGratuito` decimal(10,2) DEFAULT 0.00,
  `totalExonerado` decimal(10,2) DEFAULT 0.00,
  `igv` decimal(10,2) NOT NULL DEFAULT 0.00,
  `importeTotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `motivo_anulacion` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado_comprobante` varchar(50) DEFAULT 'PENDIENTE',
  `comprobante_relacionado` varchar(50) DEFAULT '',
  `id_usuario` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "presupuesto"
#

INSERT INTO `presupuesto` VALUES ('CT01-00000001',1,'2022-02-24','20:32:32','2022-03-06','PEN','EFECTIVO',42.37,0.00,0.00,7.63,50.00,'','PENDIENTE','',1),('CT01-00000002',2,'2022-02-24','23:08:17','2022-03-06','PEN','EFECTIVO',33.90,0.00,0.00,6.10,40.00,'','PENDIENTE','',1),('CT01-00000003',1,'2024-05-02','14:58:42','2024-05-12','PEN','EFECTIVO',110.17,0.00,0.00,19.83,130.00,'','PENDIENTE','',1);

#
# Structure for table "presupuestodet"
#

DROP TABLE IF EXISTS `presupuestodet`;
CREATE TABLE `presupuestodet` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idPresupuesto` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `item` int(11) NOT NULL DEFAULT 0,
  `cantidad` decimal(10,4) DEFAULT NULL,
  `tipoUnidad` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tributo` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `montoTributo` decimal(10,4) DEFAULT NULL,
  `tipoAfectacionTributo` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valorUnitario` decimal(10,4) DEFAULT NULL,
  `precioUnitarioItem` decimal(10,4) DEFAULT NULL,
  `valorUnitarioGratuito` decimal(10,4) DEFAULT NULL,
  `precio` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "presupuestodet"
#

INSERT INTO `presupuestodet` VALUES ('CT01-00000001-001','CT01-00000001',1,5.0000,'NIU','2022223155820','ASDASDASDA','IGV',7.6270,'10',8.4746,50.0000,0.0000,10.0000),('CT01-00000002-001','CT01-00000002',1,4.0000,'NIU','2022223155820','ASDASDASDA','IGV',6.1016,'10',8.4746,40.0000,0.0000,10.0000),('CT01-00000003-001','CT01-00000003',1,1.0000,'NIU','2022223155820','ASDASDASDA','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('CT01-00000003-002','CT01-00000003',2,5.0000,'NIU','2022224234535','PRODUCTO 1','IGV',7.6270,'10',8.4746,50.0000,0.0000,10.0000),('CT01-00000003-003','CT01-00000003',3,1.0000,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('CT01-00000003-004','CT01-00000003',4,1.0000,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('CT01-00000003-005','CT01-00000003',5,1.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.5763,'10',25.4237,30.0000,0.0000,30.0000);

#
# Structure for table "producto"
#

DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `precio` decimal(11,2) DEFAULT 0.00,
  `incluye_igv` int(1) DEFAULT 0,
  `tipo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'PRODUCTO',
  `afectacion` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'IGV Impuesto General a las Ventas',
  `unidad_medida` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'UNIDAD',
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ACTIVO',
  `icbper` varchar(2) DEFAULT 'NO',
  `subir_online` varchar(4) DEFAULT 'NO',
  `tiene_stock` varchar(4) DEFAULT 'NO',
  `stock_actual` decimal(10,4) DEFAULT 0.0000,
  `afectacion_compra` varchar(100) DEFAULT 'IGV Impuesto General a las Ventas',
  `precio_compra` decimal(10,4) DEFAULT 0.0000,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "producto"
#

INSERT INTO `producto` VALUES (1,'2022223155820','ASDASDASDA',10.00,1,'PRODUCTO','IGV Impuesto General a las Ventas','UNIDAD','ACTIVO','NO','NO','NO',0.0000,'IGV Impuesto General a las Ventas',0.0000),(2,'2022224234535','PRODUCTO 1',10.00,1,'PRODUCTO','IGV Impuesto General a las Ventas','UNIDAD','ACTIVO','NO','NO','SI',129.0000,'IGV Impuesto General a las Ventas',10.0000),(3,'202452143435','PRODUCTO DE PRUEBA 1',20.00,1,'PRODUCTO','IGV Impuesto General a las Ventas','UNIDAD','ACTIVO','NO','NO','SI',97.0000,'IGV Impuesto General a las Ventas',0.0000),(4,'202452143447','PRODUCTO DE PRUEBA 2',20.00,1,'PRODUCTO','IGV Impuesto General a las Ventas','UNIDAD','ACTIVO','NO','NO','NO',0.0000,'IGV Impuesto General a las Ventas',0.0000),(5,'20245214359','PRODUCTO DE PRUEBA 3',30.00,1,'PRODUCTO','IGV Impuesto General a las Ventas','UNIDAD','ACTIVO','NO','NO','NO',0.0000,'IGV Impuesto General a las Ventas',0.0000);

#
# Structure for table "producto_kardex"
#

DROP TABLE IF EXISTS `producto_kardex`;
CREATE TABLE `producto_kardex` (
  `id_producto_kardex` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT 0,
  `fecha` varchar(20) DEFAULT NULL,
  `hora` varchar(10) DEFAULT NULL,
  `cnt_entrada` decimal(10,2) DEFAULT 0.00,
  `cnt_salida` decimal(10,2) DEFAULT 0.00,
  `doc_relacionado` varchar(100) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_producto_kardex`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "producto_kardex"
#

INSERT INTO `producto_kardex` VALUES (1,2,'2022-02-24','23:45:36',100.00,0.00,'2 APERTURA DE STOCK ','APERTURA','ADMIN'),(2,2,'2022-02-24','23:48:7',50.00,0.00,'2 INGRESO EXTRA ','INGRESO EXTRAORDINARIO','ADMIN'),(3,2,'2022-02-24','23:48:11',0.00,20.00,'2 SALIDA EXTRA ','INGRESO EXTRAORDINARIO','ADMIN'),(4,3,'2024-05-02','14:34:35',100.00,0.00,'3 APERTURA DE STOCK ','APERTURA','ADMIN'),(5,3,'2024-05-02','14:36:40',1.00,0.00,'B001-65','COMPRA','ADMIN'),(6,2,'2024-05-02','14:36:40',1.00,0.00,'B001-65','COMPRA','ADMIN'),(7,3,'2024-05-02','14:38:3',0.00,1.00,'V001-00000004','NOTA DE VENTA','ADMIN'),(8,3,'2024-05-02','14:45:19',0.00,1.00,'V001-00000005','NOTA DE VENTA','ADMIN'),(9,3,'2024-05-02','14:46:35',0.00,1.00,'V001-00000006','NOTA DE VENTA','ADMIN'),(10,2,'2024-05-02','15:18:2',0.00,1.00,'V001-00000007','NOTA DE VENTA','ADMIN'),(11,3,'2024-05-02','15:18:2',0.00,1.00,'V001-00000007','NOTA DE VENTA','ADMIN'),(12,2,'2024-05-02','16:24:41',0.00,1.00,'V001-00000008','NOTA DE VENTA','ADMIN');

#
# Structure for table "proveedor"
#

DROP TABLE IF EXISTS `proveedor`;
CREATE TABLE `proveedor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipoDocumento` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '',
  `numeroDocumento` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '',
  `nombreRazonSocial` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo_envios` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT '-',
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "proveedor"
#

INSERT INTO `proveedor` VALUES (1,'RUC','10481245562','TAFUR TORRES LUIS CESAR','-','-','ACTIVO');

#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `contrasena` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `rol` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `estado` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'wA1rmB/TWNs=','wA1rmB/TWNs=','ADMINISTRADOR','ACTIVO');

#
# Structure for table "venta_interna"
#

DROP TABLE IF EXISTS `venta_interna`;
CREATE TABLE `venta_interna` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idCliente` int(11) unsigned NOT NULL DEFAULT 0,
  `fecha` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `horaEmision` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fechaVencimiento` char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `moneda` char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `medioPago` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `totalVentasGravadas` decimal(10,2) NOT NULL DEFAULT 0.00,
  `totalGratuito` decimal(10,2) DEFAULT 0.00,
  `totalExonerado` decimal(10,2) DEFAULT 0.00,
  `igv` decimal(10,2) NOT NULL DEFAULT 0.00,
  `importeTotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `motivo_anulacion` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado_comprobante` varchar(50) DEFAULT 'GENERADO',
  `comprobante_relacionado` varchar(50) DEFAULT '',
  `id_usuario` int(11) DEFAULT 1,
  `montoEfectivoVenta` decimal(10,2) DEFAULT 0.00,
  `montoTarjetaVenta` decimal(10,2) DEFAULT 0.00,
  `montoVueltoVenta` decimal(10,2) DEFAULT 0.00,
  `montoDeudaVenta` decimal(10,2) DEFAULT 0.00,
  `descripcionTarjetaVenta` varchar(255) DEFAULT 'SOLO EFECTIVO',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "venta_interna"
#

INSERT INTO `venta_interna` VALUES ('V001-00000001',1,'2022-02-23','15:58:24','2022-02-23','PEN','EFECTIVO',8.47,0.00,0.00,1.53,10.00,'','GENERADO','',1,10.00,0.00,0.00,0.00,'SOLO EFECTIVO'),('V001-00000002',1,'2022-02-24','21:33:58','2022-02-24','PEN','EFECTIVO',42.37,0.00,0.00,7.63,50.00,'','GENERADO','',1,50.00,0.00,0.00,0.00,'SOLO EFECTIVO'),('V001-00000003',3,'2022-02-24','23:08:47','2022-02-24','PEN','EFECTIVO',8.47,0.00,0.00,1.53,10.00,'','GENERADO','',1,10.00,0.00,0.00,0.00,'SOLO EFECTIVO'),('V001-00000004',4,'2024-05-02','14:38:03','2024-05-02','PEN','EFECTIVO',84.75,0.00,0.00,15.25,100.00,'','GENERADO','',1,100.00,0.00,0.00,0.00,'SOLO EFECTIVO'),('V001-00000005',1,'2024-05-02','14:45:19','2024-05-02','PEN','EFECTIVO',59.32,0.00,0.00,10.68,70.00,'','GENERADO','',1,70.00,0.00,0.00,0.00,'SOLO EFECTIVO'),('V001-00000006',3,'2024-05-02','14:46:35','2024-05-02','PEN','EFECTIVO',59.32,0.00,0.00,10.68,70.00,'','GENERADO','',1,20.00,50.00,0.00,0.00,'YAPE'),('V001-00000007',1,'2024-05-02','15:18:02','2024-05-02','PEN','EFECTIVO',76.27,0.00,0.00,13.73,90.00,'','GENERADO','',1,100.00,0.00,10.00,0.00,'SOLO EFECTIVO'),('V001-00000008',1,'2024-05-02','16:24:41','2024-05-02','PEN','EFECTIVO',42.37,0.00,0.00,7.63,50.00,'','GENERADO','',1,50.00,0.00,0.00,0.00,'SOLO EFECTIVO');

#
# Structure for table "venta_internadet"
#

DROP TABLE IF EXISTS `venta_internadet`;
CREATE TABLE `venta_internadet` (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `idVenta_interna` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `item` int(11) NOT NULL DEFAULT 0,
  `cantidad` decimal(10,4) DEFAULT NULL,
  `tipoUnidad` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `codigo` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcion` longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `tributo` varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `montoTributo` decimal(10,4) DEFAULT NULL,
  `tipoAfectacionTributo` varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valorUnitario` decimal(10,4) DEFAULT NULL,
  `precioUnitarioItem` decimal(10,4) DEFAULT NULL,
  `valorUnitarioGratuito` decimal(10,4) DEFAULT NULL,
  `precio` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

#
# Data for table "venta_internadet"
#

INSERT INTO `venta_internadet` VALUES ('V001-00000001-001','V001-00000001',1,1.0000,'NIU','2022223155820','ASDASDASDA','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000002-001','V001-00000002',1,5.0000,'NIU','2022223155820','ASDASDASDA','IGV',7.6270,'10',8.4746,50.0000,0.0000,10.0000),('V001-00000003-001','V001-00000003',1,1.0000,'NIU','2022223155820','ASDASDASDA','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000004-001','V001-00000004',3,1.0000,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000004-002','V001-00000004',4,1.0000,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000004-003','V001-00000004',5,2.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',9.1526,'10',25.4237,60.0000,0.0000,30.0000),('V001-00000005-001','V001-00000005',3,1.0000,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000005-002','V001-00000005',4,1.0000,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000005-003','V001-00000005',5,1.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.5763,'10',25.4237,30.0000,0.0000,30.0000),('V001-00000006-001','V001-00000006',3,1.0000,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000006-002','V001-00000006',4,1.0000,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000006-003','V001-00000006',5,1.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.5763,'10',25.4237,30.0000,0.0000,30.0000),('V001-00000007-001','V001-00000007',1,1.0000,'NIU','2022223155820','ASDASDASDA','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000007-002','V001-00000007',2,1.0000,'NIU','2022224234535','PRODUCTO 1','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000007-003','V001-00000007',3,1.0000,'NIU','202452143435','PRODUCTO DE PRUEBA 1','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000007-004','V001-00000007',4,1.0000,'NIU','202452143447','PRODUCTO DE PRUEBA 2','IGV',3.0508,'10',16.9492,20.0000,0.0000,20.0000),('V001-00000007-005','V001-00000007',5,1.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.5763,'10',25.4237,30.0000,0.0000,30.0000),('V001-00000008-001','V001-00000008',1,1.0000,'NIU','2022223155820','ASDASDASDA','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000008-002','V001-00000008',2,1.0000,'NIU','2022224234535','PRODUCTO 1','IGV',1.5254,'10',8.4746,10.0000,0.0000,10.0000),('V001-00000008-003','V001-00000008',5,1.0000,'NIU','20245214359','PRODUCTO DE PRUEBA 3','IGV',4.5763,'10',25.4237,30.0000,0.0000,30.0000);

#
# View "compra_view"
#

DROP VIEW IF EXISTS `compra_view`;
CREATE ALGORITHM=UNDEFINED VIEW `compra_view` AS select `compra`.`id` AS `id_compra`,`compra`.`idCliente` AS `id_cliente`,`cliente`.`tipoDocumento` AS `tipoDocumento_cliente`,`cliente`.`numeroDocumento` AS `numeroDocumento_cliente`,`cliente`.`nombreRazonSocial` AS `cliente`,`cliente`.`direccion` AS `direccion_cliente`,`cliente`.`correo_envios` AS `correo_envios`,`compra`.`fecha` AS `fecha_emision`,`compra`.`horaEmision` AS `hora_emision`,`compra`.`fechaVencimiento` AS `fecha_vencimiento`,`compra`.`moneda` AS `moneda`,`compra`.`medioPago` AS `medioPago`,`compra`.`totalVentasGravadas` AS `totalVentasGravadas`,`compra`.`totalGratuito` AS `totalGratuito`,`compra`.`totalExonerado` AS `totalExonerado`,`compra`.`igv` AS `totalIgv`,`compra`.`importeTotal` AS `importeTotal`,`compra`.`motivo_anulacion` AS `motivo_anulacion`,`compra`.`estado_comprobante` AS `estado_comprobante`,`compra`.`comprobante_relacionado` AS `comprobante_relacionado`,`compra`.`id_usuario` AS `id_usuario` from (`compra` join `proveedor` `cliente` on(`compra`.`idCliente` = `cliente`.`id`));

#
# View "presupuesto_view"
#

DROP VIEW IF EXISTS `presupuesto_view`;
CREATE ALGORITHM=UNDEFINED VIEW `presupuesto_view` AS select `presupuesto`.`id` AS `id_presupuesto`,`presupuesto`.`idCliente` AS `id_cliente`,`cliente`.`tipoDocumento` AS `tipoDocumento_cliente`,`cliente`.`numeroDocumento` AS `numeroDocumento_cliente`,`cliente`.`nombreRazonSocial` AS `cliente`,`cliente`.`direccion` AS `direccion_cliente`,`cliente`.`correo_envios` AS `correo_envios`,`presupuesto`.`fecha` AS `fecha_emision`,`presupuesto`.`horaEmision` AS `hora_emision`,`presupuesto`.`fechaVencimiento` AS `fecha_vencimiento`,`presupuesto`.`moneda` AS `moneda`,`presupuesto`.`medioPago` AS `medioPago`,`presupuesto`.`totalVentasGravadas` AS `totalVentasGravadas`,`presupuesto`.`totalGratuito` AS `totalGratuito`,`presupuesto`.`totalExonerado` AS `totalExonerado`,`presupuesto`.`igv` AS `totalIgv`,`presupuesto`.`importeTotal` AS `importeTotal`,`presupuesto`.`motivo_anulacion` AS `motivo_anulacion`,`presupuesto`.`estado_comprobante` AS `estado_comprobante`,`presupuesto`.`comprobante_relacionado` AS `comprobante_relacionado`,`presupuesto`.`id_usuario` AS `id_usuario` from (`presupuesto` join `cliente` on(`presupuesto`.`idCliente` = `cliente`.`id`));

#
# View "venta_interna_view"
#

DROP VIEW IF EXISTS `venta_interna_view`;
CREATE ALGORITHM=UNDEFINED VIEW `venta_interna_view` AS select `venta_interna`.`id` AS `id_venta_interna`,`venta_interna`.`idCliente` AS `id_cliente`,`cliente`.`tipoDocumento` AS `tipoDocumento_cliente`,`cliente`.`numeroDocumento` AS `numeroDocumento_cliente`,`cliente`.`nombreRazonSocial` AS `cliente`,`cliente`.`direccion` AS `direccion_cliente`,`cliente`.`correo_envios` AS `correo_envios`,`venta_interna`.`fecha` AS `fecha_emision`,`venta_interna`.`horaEmision` AS `hora_emision`,`venta_interna`.`fechaVencimiento` AS `fecha_vencimiento`,`venta_interna`.`moneda` AS `moneda`,`venta_interna`.`medioPago` AS `medioPago`,`venta_interna`.`totalVentasGravadas` AS `totalVentasGravadas`,`venta_interna`.`totalGratuito` AS `totalGratuito`,`venta_interna`.`totalExonerado` AS `totalExonerado`,`venta_interna`.`igv` AS `totalIgv`,`venta_interna`.`importeTotal` AS `importeTotal`,`venta_interna`.`motivo_anulacion` AS `motivo_anulacion`,`venta_interna`.`estado_comprobante` AS `estado_comprobante`,`venta_interna`.`comprobante_relacionado` AS `comprobante_relacionado`,`venta_interna`.`id_usuario` AS `id_usuario`,`venta_interna`.`montoEfectivoVenta` AS `montoEfectivoVenta`,`venta_interna`.`montoTarjetaVenta` AS `montoTarjetaVenta`,`venta_interna`.`montoVueltoVenta` AS `montoVueltoVenta`,`venta_interna`.`montoDeudaVenta` AS `montoDeudaVenta`,`venta_interna`.`descripcionTarjetaVenta` AS `descripcionTarjetaVenta` from (`venta_interna` join `cliente` on(`venta_interna`.`idCliente` = `cliente`.`id`));
