/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.modelo;

/**
 *
 * @author USER-1
 */
public class ObVenta_interna {

    private String id;
    private int idCliente;
    private String fecha, horaEmision;
    private String fechaVencimiento, moneda, medioPago;
    private double totalVentasGravadas, totalGratuito, totalExonerado, igv, importeTotal;
    private String motivo_anulacion;
    private String estado_comprobante, comprobante_relacionado;
    private int id_usuario;
    private double montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta;
    private String descripcionTarjetaVenta;

    public ObVenta_interna(String id, int idCliente, String fecha, String horaEmision, String fechaVencimiento, String moneda, String medioPago, double totalVentasGravadas,
            double totalGratuito, double totalExonerado, double igv, double importeTotal, String motivo_anulacion, String estado_comprobante, String comprobante_relacionado,
            int id_usuario, double montoEfectivoVenta, double montoTarjetaVenta, double montoVueltoVenta, double montoDeudaVenta, String descripcionTarjetaVenta) {
        this.id = id;
        this.idCliente = idCliente;
        this.fecha = fecha;
        this.horaEmision = horaEmision;
        this.fechaVencimiento = fechaVencimiento;
        this.moneda = moneda;
        this.medioPago = medioPago;
        this.totalVentasGravadas = totalVentasGravadas;
        this.totalGratuito = totalGratuito;
        this.totalExonerado = totalExonerado;
        this.igv = igv;
        this.importeTotal = importeTotal;
        this.motivo_anulacion = motivo_anulacion;
        this.estado_comprobante = estado_comprobante;
        this.comprobante_relacionado = comprobante_relacionado;
        this.id_usuario = id_usuario;

        this.montoEfectivoVenta = montoEfectivoVenta;
        this.montoTarjetaVenta = montoTarjetaVenta;
        this.montoVueltoVenta = montoVueltoVenta;
        this.montoDeudaVenta = montoDeudaVenta;
        this.descripcionTarjetaVenta = descripcionTarjetaVenta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraEmision() {
        return horaEmision;
    }

    public void setHoraEmision(String horaEmision) {
        this.horaEmision = horaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public double getTotalVentasGravadas() {
        return totalVentasGravadas;
    }

    public void setTotalVentasGravadas(double totalVentasGravadas) {
        this.totalVentasGravadas = totalVentasGravadas;
    }

    public double getTotalGratuito() {
        return totalGratuito;
    }

    public void setTotalGratuito(double totalGratuito) {
        this.totalGratuito = totalGratuito;
    }

    public double getTotalExonerado() {
        return totalExonerado;
    }

    public void setTotalExonerado(double totalExonerado) {
        this.totalExonerado = totalExonerado;
    }

    public double getIgv() {
        return igv;
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }

    public double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getMotivo_anulacion() {
        return motivo_anulacion;
    }

    public void setMotivo_anulacion(String motivo_anulacion) {
        this.motivo_anulacion = motivo_anulacion;
    }

    public String getEstado_comprobante() {
        return estado_comprobante;
    }

    public void setEstado_comprobante(String estado_comprobante) {
        this.estado_comprobante = estado_comprobante;
    }

    public String getComprobante_relacionado() {
        return comprobante_relacionado;
    }

    public void setComprobante_relacionado(String comprobante_relacionado) {
        this.comprobante_relacionado = comprobante_relacionado;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public double getMontoEfectivoVenta() {
        return montoEfectivoVenta;
    }

    public void setMontoEfectivoVenta(double montoEfectivoVenta) {
        this.montoEfectivoVenta = montoEfectivoVenta;
    }

    public double getMontoTarjetaVenta() {
        return montoTarjetaVenta;
    }

    public void setMontoTarjetaVenta(double montoTarjetaVenta) {
        this.montoTarjetaVenta = montoTarjetaVenta;
    }

    public double getMontoVueltoVenta() {
        return montoVueltoVenta;
    }

    public void setMontoVueltoVenta(double montoVueltoVenta) {
        this.montoVueltoVenta = montoVueltoVenta;
    }

    public double getMontoDeudaVenta() {
        return montoDeudaVenta;
    }

    public void setMontoDeudaVenta(double montoDeudaVenta) {
        this.montoDeudaVenta = montoDeudaVenta;
    }

    public String getDescripcionTarjetaVenta() {
        return descripcionTarjetaVenta;
    }

    public void setDescripcionTarjetaVenta(String descripcionTarjetaVenta) {
        this.descripcionTarjetaVenta = descripcionTarjetaVenta;
    }

}
