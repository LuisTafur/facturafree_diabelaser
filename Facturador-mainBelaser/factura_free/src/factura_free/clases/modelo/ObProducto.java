/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.modelo;

/**
 *
 * @author USER-1
 */
public class ObProducto {

    private int id;
    private String codigo, descripcion;
    private double precio;
    private int incluye_igv;
    private String tipo, afectacion, unidad_medida, estado, icbper, subir_online, tiene_stock;
    private double stock_actual;
    private String afectacion_compra;
    private double precio_compra;

    public ObProducto(int id, String codigo, String descripcion, double precio, int incluye_igv, String tipo, String afectacion, String unidad_medida,
            String estado, String icbper, String subir_online, String tiene_stock, double stock_actual, String afectacion_compra, double precio_compra) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.incluye_igv = incluye_igv;
        this.tipo = tipo;
        this.afectacion = afectacion;
        this.unidad_medida = unidad_medida;
        this.estado = estado;
        this.icbper = icbper;
        this.subir_online = subir_online;
        this.tiene_stock = tiene_stock;
        this.stock_actual = stock_actual;
        this.afectacion_compra = afectacion_compra;
        this.precio_compra = precio_compra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getIncluye_igv() {
        return incluye_igv;
    }

    public void setIncluye_igv(int incluye_igv) {
        this.incluye_igv = incluye_igv;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAfectacion() {
        return afectacion;
    }

    public void setAfectacion(String afectacion) {
        this.afectacion = afectacion;
    }

    public String getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(String unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIcbper() {
        return icbper;
    }

    public void setIcbper(String icbper) {
        this.icbper = icbper;
    }

    public String getSubir_online() {
        return subir_online;
    }

    public void setSubir_online(String subir_online) {
        this.subir_online = subir_online;
    }

    public String getTiene_stock() {
        return tiene_stock;
    }

    public void setTiene_stock(String tiene_stock) {
        this.tiene_stock = tiene_stock;
    }

    public double getStock_actual() {
        return stock_actual;
    }

    public void setStock_actual(double stock_actual) {
        this.stock_actual = stock_actual;
    }

    public String getAfectacion_compra() {
        return afectacion_compra;
    }

    public void setAfectacion_compra(String afectacion_compra) {
        this.afectacion_compra = afectacion_compra;
    }

    public double getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(double precio_compra) {
        this.precio_compra = precio_compra;
    }

}
