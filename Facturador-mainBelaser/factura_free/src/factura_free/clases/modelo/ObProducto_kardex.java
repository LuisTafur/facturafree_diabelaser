/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.modelo;

/**
 *
 * @author USER-1
 */
public class ObProducto_kardex {

    private int id_producto_kardex, id_producto;
    private String fecha, hora;
    private double cnt_entrada, cnt_salida;
    private String doc_relacionado, tipo, usuario;

    public ObProducto_kardex(int id_producto_kardex, int id_producto, String fecha, String hora, double cnt_entrada, double cnt_salida, String doc_relacionado,
            String tipo , String usuario) {
        this.id_producto_kardex = id_producto_kardex;
        this.id_producto = id_producto;
        this.fecha = fecha;
        this.hora = hora;
        this.cnt_entrada = cnt_entrada;
        this.cnt_salida = cnt_salida;
        this.doc_relacionado = doc_relacionado;
        this.tipo = tipo;
        this.usuario = usuario;
    }

    public int getId_producto_kardex() {
        return id_producto_kardex;
    }

    public void setId_producto_kardex(int id_producto_kardex) {
        this.id_producto_kardex = id_producto_kardex;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public double getCnt_entrada() {
        return cnt_entrada;
    }

    public void setCnt_entrada(double cnt_entrada) {
        this.cnt_entrada = cnt_entrada;
    }

    public double getCnt_salida() {
        return cnt_salida;
    }

    public void setCnt_salida(double cnt_salida) {
        this.cnt_salida = cnt_salida;
    }

    public String getDoc_relacionado() {
        return doc_relacionado;
    }

    public void setDoc_relacionado(String doc_relacionado) {
        this.doc_relacionado = doc_relacionado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
