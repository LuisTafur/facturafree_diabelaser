/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.modelo;

/**
 *
 * @author USER-1
 */
public class ObConfig {

    private int id;
    private String ruc, razonSocial, direccion, telefono, correo, web, impresion, usa_cnt_flotante, producto_igv, serv_datos, correo_aplicacion, pass_aplicacion, logo; 
    private String email_contador, seriePresupuesto, serieVenta_interna; 
    private String serieCompra, impre_directa;

    public ObConfig(int id, String ruc, String razonSocial, String direccion, String telefono, String correo, String web, String impresion, String usa_cnt_flotante,
            String producto_igv, String serv_datos, String correo_aplicacion, String pass_aplicacion, String logo, String email_contador,
            String seriePresupuesto, String serieVenta_interna, String serieCompra, String impre_directa) {
        this.id = id;
        this.ruc = ruc;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.web = web;
        this.impresion = impresion; 
        this.usa_cnt_flotante = usa_cnt_flotante;
        this.producto_igv = producto_igv;
        this.serv_datos = serv_datos;
        this.correo_aplicacion = correo_aplicacion;
        this.pass_aplicacion = pass_aplicacion; 
        this.logo = logo; 
        this.email_contador = email_contador;
        this.seriePresupuesto = seriePresupuesto;
        this.serieVenta_interna = serieVenta_interna;   
        this.serieCompra = serieCompra;
        this.impre_directa = impre_directa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getImpresion() {
        return impresion;
    }

    public void setImpresion(String impresion) {
        this.impresion = impresion;
    } 

    public String getUsa_cnt_flotante() {
        return usa_cnt_flotante;
    }

    public void setUsa_cnt_flotante(String usa_cnt_flotante) {
        this.usa_cnt_flotante = usa_cnt_flotante;
    }

    public String getProducto_igv() {
        return producto_igv;
    }

    public void setProducto_igv(String producto_igv) {
        this.producto_igv = producto_igv;
    }

    public String getServ_datos() {
        return serv_datos;
    }

    public void setServ_datos(String serv_datos) {
        this.serv_datos = serv_datos;
    }

    public String getCorreo_aplicacion() {
        return correo_aplicacion;
    }

    public void setCorreo_aplicacion(String correo_aplicacion) {
        this.correo_aplicacion = correo_aplicacion;
    }

    public String getPass_aplicacion() {
        return pass_aplicacion;
    }

    public void setPass_aplicacion(String pass_aplicacion) {
        this.pass_aplicacion = pass_aplicacion;
    }
 
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
 
    public String getEmail_contador() {
        return email_contador;
    }

    public void setEmail_contador(String email_contador) {
        this.email_contador = email_contador;
    }

    public String getSeriePresupuesto() {
        return seriePresupuesto;
    }

    public void setSeriePresupuesto(String seriePresupuesto) {
        this.seriePresupuesto = seriePresupuesto;
    }

    public String getSerieVenta_interna() {
        return serieVenta_interna;
    }

    public void setSerieVenta_interna(String serieVenta_interna) {
        this.serieVenta_interna = serieVenta_interna;
    } 

    public String getSerieCompra() {
        return serieCompra;
    }

    public void setSerieCompra(String serieCompra) {
        this.serieCompra = serieCompra;
    }

    public String getImpre_directa() {
        return impre_directa;
    }

    public void setImpre_directa(String impre_directa) {
        this.impre_directa = impre_directa;
    }

}
