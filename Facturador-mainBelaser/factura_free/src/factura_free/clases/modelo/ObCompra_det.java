/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.modelo;

/**
 *
 * @author USER-1
 */
public class ObCompra_det {

    private String id, idcompra;
    private int item;
    private double cantidad;
    private String tipoUnidad, codigo, descripcion, tributo;
    private double montoTributo;
    private String tipoAfectacionTributo;
    private double valorUnitario,  precioUnitarioItem,valorUnitarioGratuito;
    private double precio;

    public ObCompra_det(String id, String idcompra, int item, double cantidad, String tipoUnidad, String codigo, String descripcion, String tributo, double montoTributo, 
            String tipoAfectacionTributo, double valorUnitario, double precioUnitarioItem,double valorUnitarioGratuito, double precio) {
        this.id = id;
        this.idcompra = idcompra;
        this.item = item;
        this.cantidad = cantidad;
        this.tipoUnidad = tipoUnidad;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.tributo = tributo;
        this.montoTributo = montoTributo;
        this.tipoAfectacionTributo = tipoAfectacionTributo;
        this.valorUnitario = valorUnitario; 
        this.precioUnitarioItem = precioUnitarioItem;
        this.valorUnitarioGratuito = valorUnitarioGratuito; 
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdcompra() {
        return idcompra;
    }

    public void setIdcompra(String idcompra) {
        this.idcompra = idcompra;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoUnidad() {
        return tipoUnidad;
    }

    public void setTipoUnidad(String tipoUnidad) {
        this.tipoUnidad = tipoUnidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTributo() {
        return tributo;
    }

    public void setTributo(String tributo) {
        this.tributo = tributo;
    }

    public double getMontoTributo() {
        return montoTributo;
    }

    public void setMontoTributo(double montoTributo) {
        this.montoTributo = montoTributo;
    }

    public String getTipoAfectacionTributo() {
        return tipoAfectacionTributo;
    }

    public void setTipoAfectacionTributo(String tipoAfectacionTributo) {
        this.tipoAfectacionTributo = tipoAfectacionTributo;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    } 

    public double getPrecioUnitarioItem() {
        return precioUnitarioItem;
    }

    public void setPrecioUnitarioItem(double precioUnitarioItem) {
        this.precioUnitarioItem = precioUnitarioItem;
    }

    public double getValorUnitarioGratuito() {
        return valorUnitarioGratuito;
    }

    public void setValorUnitarioGratuito(double valorUnitarioGratuito) {
        this.valorUnitarioGratuito = valorUnitarioGratuito;
    } 

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
