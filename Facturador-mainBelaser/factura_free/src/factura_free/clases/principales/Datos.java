package factura_free.clases.principales;

import factura_free.clases.controlador.ConfigImpl;
import factura_free.clases.principales.alternos.EncriparDatos;
import factura_free.clases.principales.alternos.Rutas;
import factura_free.vista.principal.JFramePrincipal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Datos {

    static EncriparDatos encriptar = new EncriparDatos();

    public static String getHost() {
        String host = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());

        try (Stream<String> lines = Files.lines(filePath)) {
            host = lines.skip(0).findFirst().get().substring(5);
        } catch (Exception e) {
            System.out.println("Error cargando host.");
        }
        return encriptar.Desencriptar(host);
    }

    public static String getPuerto() {
        String puerto = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());

        try (Stream<String> lines = Files.lines(filePath)) {
            puerto = lines.skip(1).findFirst().get().substring(7);
        } catch (Exception e) {
            System.out.println("Error cargando puerto.");
        }
        return encriptar.Desencriptar(puerto);
    }

    public static String getBaseDatos() {
        String baseDatos = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());

        try (Stream<String> lines = Files.lines(filePath)) {
            baseDatos = lines.skip(2).findFirst().get().substring(10);
        } catch (Exception e) {
            System.out.println("Error cargando baseDatos.");
        }
        return encriptar.Desencriptar(baseDatos);
    }

    public static String getUsuario() {
        String baseDatos = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());

        try (Stream<String> lines = Files.lines(filePath)) {
            baseDatos = lines.skip(3).findFirst().get().substring(8);
        } catch (Exception e) {
            System.out.println("Error cargando usuario.");
        }
        return encriptar.Desencriptar(baseDatos);
    }

    public static String getContrasena() {
        String contrasena = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());

        try (Stream<String> lines = Files.lines(filePath)) {
            contrasena = lines.skip(4).findFirst().get().substring(11);
        } catch (Exception e) {
            System.out.println("Error cargando contrasena.");
        }
        return encriptar.Desencriptar(contrasena);
    }

    public static String getUbicacionDistrito() {
        String ubicacion = null;
        Path filePath = Paths.get(Rutas.getRutaConexionBD());
        try (Stream<String> lines = Files.lines(filePath)) {
            ubicacion = lines.skip(5).findFirst().get().substring(10);
        } catch (Exception e) {
            ubicacion = encriptar.Encriptar("FACTURAFREE");
        }
        return encriptar.Desencriptar(ubicacion);
    }

    public static String getRUC() {
        String ruc = JFramePrincipal.lblRucEmpresa.getText();
        return ruc;
    } 

    public static String getUsa_cntFlotante() {
        String ruc = JFramePrincipal.usa_cnt_flotante.getText();
        return ruc;
    }

    public static String getProducto_igv() {
        String ruc = JFramePrincipal.producto_igv.getText();
        return ruc;
    }

    public static String getServ_datos() {
        String ruc = JFramePrincipal.serv_datos.getText();
        return ruc;
    }

    public static String getRazonSocial() {
        String razonSocial = JFramePrincipal.lblRazonSocial.getText();
        return razonSocial;
    }

    public static String getDireccion() {
        String direccion = JFramePrincipal.lblDireccion.getText();
        return direccion;
    }

    public static String getTelefono() {
        String telefono = JFramePrincipal.telefono.getText();
        return telefono;
    }

    public static String getCorreo() {
        String correo = JFramePrincipal.correo.getText();
        return correo;
    }

    public static String getWeb() {
        String web = JFramePrincipal.web.getText();
        return web;
    }

    public static String getImpresion() {
        String impresion = JFramePrincipal.impresion.getText();
        return impresion;
    }

    public static String getEmail_Contador() {
        String email_contador = JFramePrincipal.email_contador.getText();
        return email_contador;
    }  

    public static String getUsar_Impresion_directa() {
        String impre_directa = JFramePrincipal.impre_directa.getText();
        return impre_directa;
    }

    /////CONSULTA DE DATOS POR DNI , DISTINTOS SERVIDORES
    public static String getConsulta_DNI_Serv1(String nrodni) {
        String Consulta_DNI_Serv1 = "https://apifacturacion.com/api-dni.php?dni=" + nrodni;
        return Consulta_DNI_Serv1;
    }

    public static String getConsulta_DNI_Serv2(String nrodni) {
        String Consulta_DNI_Serv2 = "https://api.apis.net.pe/v1/dni?numero=" + nrodni;
        return Consulta_DNI_Serv2;
    } 

    /////CONSULTA DE DATOS POR DNI , DISTINTOS SERVIDORES
    public static String getConsulta_RUC_Serv1(String nroruc) {
        String Consulta_RUC_Serv1 = "https://apifacturacion.com/api.php?rucCliente=" + nroruc;
        return Consulta_RUC_Serv1;
    }

    public static String getConsulta_RUC_Serv2(String nroruc) {
        String Consulta_RUC_Serv2 = "https://api.apis.net.pe/v1/ruc?numero=" + nroruc;
        return Consulta_RUC_Serv2;
    }

    public static String getConsulta_RUC_Serv3(String nroruc) {
        String Consulta_RUC_Serv3 = "http://www.vfpsteambi.solutions/vfpsapiruc/vfpsapiruc.php?ruc=" + nroruc;
        return Consulta_RUC_Serv3;
    } 
 
}
