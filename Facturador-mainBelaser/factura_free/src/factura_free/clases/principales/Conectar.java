package factura_free.clases.principales;

import factura_free.clases.principales.alternos.EncriparDatos;
import factura_free.clases.principales.alternos.Metodos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conectar {

    EncriparDatos encriptar=new EncriparDatos();
    static Connection conexion = null;
    static Connection conexionTest = null;

    public Conectar() {
        String host = Datos.getHost();
        String puerto = Datos.getPuerto();
        String baseDatos = Datos.getBaseDatos();
        String usuario = Datos.getUsuario();
        String contrasena = Datos.getContrasena();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://" + host + ":" + puerto + "/" + baseDatos + "?zeroDateTimeBehavior=convertToNull", usuario, contrasena);
        } catch (Exception ex) {
            System.out.println("Error al conectar a BD: \n" + ex);
            JOptionPane.showMessageDialog(null,ex,"Error al conectar a BD", JOptionPane.ERROR_MESSAGE); 
        }
    }

    public Connection getConexion() {
        return conexion;
    }
 

    public static void getConexionTest(String host, String puerto,
            String baseDatos, String usuario, String contrasena)
            throws InstantiationException, IllegalAccessException, SQLException {

        String Url = "jdbc:mysql://" + host + ":" + puerto + "/" + baseDatos;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexionTest = DriverManager.getConnection(Url, usuario, contrasena);
            Metodos.MensajeInformacion("Conexión exitosa.");
        } catch (Exception e) {
            System.out.println("Error al conectar a BD: \n" + e);
            Metodos.MensajeError("Error al conectar a BD: \n" + e);
        }
    }

    public static void reindexadoBDFacturaFree() {

        Connection conexion = null;//permite hacer el puente entre la BD 
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("CREATE DATABASE IF NOT EXISTS factura_free");
            st.close();
            conexion.close();
        } catch (SQLException ex) {
            System.err.println("error: " + ex.getMessage());
        }
    }
}
