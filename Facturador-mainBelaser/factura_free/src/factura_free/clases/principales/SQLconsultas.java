/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author IngenieroLuisTafurgm
 */
public class SQLconsultas {


    public static ResultSet Consulta(String query) {
        Conectar cc = new Conectar();
        Connection con = null;
        try {
            con = cc.getConexion();
            Statement stmt;
            stmt = con.createStatement();
            ResultSet respuesta = stmt.executeQuery(query);
            return respuesta;
        } catch (Exception e) {
//            Metodos.MensajeError("Error: \n" + e);
        }
        return null;
    }
    
    /**
     * Consultas*
     */
    public static boolean ejecutaConsultas(String consulta) {
        Conectar cc = new Conectar();
        try {
            Statement st = cc.getConexion().createStatement();
            st.executeUpdate(consulta);
            st.close();
            cc.getConexion().close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error en la sentencia Insertar www" + ex.getMessage());
            return false;
        }
    }
    
    /**
     * Graba*
     */
    public static void grabarTabla(String sql) {
        Conectar cc = new Conectar();
        Connection con = null;
        PreparedStatement pst = null;
        try {
            con = cc.getConexion();
            //RECIBE EL PARAMETRO DE LA SENTENCIA SQL
            pst = con.prepareStatement(sql);
            int rs = pst.executeUpdate();//devulve la cantidad de filas añadidas
            pst.close();
            con.close();
        } catch (Exception ex) {
            System.out.println("Error en la sentencia Insertar www" + ex.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
    }

    /**
     * Actualiza*
     */
    public static boolean actualizaTabla(String sql) {
        Conectar cc = new Conectar();
        try {
            Statement st = cc.getConexion().createStatement();
            st.executeUpdate(sql);
            st.close();
            cc.getConexion().close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error en la conexión ACTUALIZAR " + ex);
            return false;
        }
    }

    public static int obtenerEntero(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        int valor = 0;
        Conectar cc = new Conectar(); 
        try {
            con = cc.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                valor = rs.getInt(1);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_entero " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return valor;
    }

    public static String obtenerCadena(String sql) {
        Connection con = null;
        Conectar cc = new Conectar();
        PreparedStatement pst = null;
        String cadena = null;

        try {
            con = cc.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                cadena = rs.getString(1);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_cadena " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return cadena;
    }

    public static double obtenerDouble(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        double valor = 0.0;
        Conectar cc = new Conectar();

        try {
            con = cc.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                valor = rs.getDouble(1);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_double " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return valor;
    }

    public static Date obtenerFecha(String sql) { 
        Connection con = null;
        Conectar cc = new Conectar();
        PreparedStatement pst = null;
        Date fecha = null;

        try {
            con = cc.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                fecha = rs.getDate(1);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_fecha " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return fecha;
    }
    
    
    public static boolean validar(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return true;
            }
            rs.close();
            pst.close();
            con.close();
        } catch (Exception e) {
            System.out.println("Error en la conexión " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }

        return false;
    }
}
