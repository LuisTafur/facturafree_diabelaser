/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.alternos;

import java.awt.Color;
import java.awt.Component; 
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author LuisT
 */
public class ImgTabla extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        if (value instanceof JLabel) {
            JLabel lbl = (JLabel) value;
            return lbl;
        }
        if (value instanceof JButton) {
            JButton btn = (JButton) value;
            return btn;
        }

        if (row % 2 != 0) {
            setBackground(Color.decode("#E1F2FC"));
            setForeground(Color.BLACK);
        } else {
            setBackground(Color.white);
            setForeground(Color.BLACK);
        }
        if (isSelected == true) {
            setBackground(Color.decode("#00838F"));
            setForeground(Color.BLACK);
        }
        
        if (table.getValueAt(row, column).equals("INACTIVO")||table.getValueAt(row, column).equals("AOP")||table.getValueAt(row, column).equals("BAJA")||
                table.getValueAt(row, column).equals("ANULADO")) {
            setBackground(Color.decode("#DE0808"));//rojo
            setForeground(Color.WHITE);
        } else if (table.getValueAt(row, column).equals("ACTIVO")||table.getValueAt(row, column).equals("GENERADO")||table.getValueAt(row, column).equals("PENDIENTE")||table.getValueAt(row, column).equals("FACTURADO")) {
            setBackground(Color.decode("#07A52D"));//verde
            setForeground(Color.WHITE);
        }

        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }

}
