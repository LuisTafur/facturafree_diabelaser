package factura_free.clases.principales.alternos;
 
import factura_free.vista.principal.JFramePrincipal;
import java.io.File;

public class Rutas {

    public static String getRutaConexionBD() {
        return "env.txt";
    }

    public static String getLogo() {
        String rutaSunat = JFramePrincipal.logo.getText();
        return rutaSunat;
    }

    public static String getRutaComporbantePDF(String id) {
        String ruta = "C:\\FACTURAFREE\\archivos\\Repo\\" + id + ".pdf";
        return ruta;
    }

    public static String getRutaQR() {
        String ruta = "C:\\FACTURAFREE\\archivos\\Comprobantes\\QR\\";
        return ruta;
    }

    public static void crearCarpeta(String ruta) {
        File directorio = new File(ruta);
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio creado");
            } else {
                System.out.println("Error al crear directorio");
            }
        }
    }
}
