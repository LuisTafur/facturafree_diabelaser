package factura_free.clases.principales.alternos;

import javax.swing.UIManager;
import factura_free.vista.principal.JFrameSplashScreen;

public class Facturador {

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JFrameSplashScreen ss = new JFrameSplashScreen();//declaro frameLogin
        ss.setVisible(true);//mostrar el frame
    }

}
