/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.alternos;

import com.toedter.calendar.JDateChooser;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author jason
 */
public class Fecha_Date {

    public static String convertirFecha(String fecha) {
        String fechaSunat = "";
        try {
            Date d = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            fechaSunat = sdf.format(d);
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return fechaSunat;
    }

    public String captura_fecha_hoy() {
        //Instanciamos el objeto Calendar
        //en fecha obtenemos la fecha y hora del sistema
        Calendar fecha = new GregorianCalendar();
        //Obtenemos el valor del año, mes, día,
        //hora, minuto y segundo del sistema
        //usando el método get y el parámetro correspondiente
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String mon = "";
        if ((mes + 1) < 10) {
            mon = "0" + (mes + 1);
        } else {
            mon = "" + (mes + 1);
        }
        String day = "";
        if (dia < 10) {
            day = "0" + dia;
        } else {
            day = "" + dia;
        }
        String fecha_actual = anoo + "-" + mon + "-" + day;
        return fecha_actual;
    }

    public String retornaNombremes(int mes) {
        String nombre_mes = "";
        if (mes == 1) {
            nombre_mes = "ENERO";
        } else if (mes == 2) {
            nombre_mes = "FEBRERO";
        } else if (mes == 3) {
            nombre_mes = "MARZO";
        } else if (mes == 4) {
            nombre_mes = "ABRIL";
        } else if (mes == 5) {
            nombre_mes = "MAYO";
        } else if (mes == 6) {
            nombre_mes = "JUNIO";
        } else if (mes == 7) {
            nombre_mes = "JULIO";
        } else if (mes == 8) {
            nombre_mes = "AGOSTO";
        } else if (mes == 9) {
            nombre_mes = "SEPTIEMBRE";
        } else if (mes == 10) {
            nombre_mes = "OCTUBRE";
        } else if (mes == 11) {
            nombre_mes = "NOVIEMBRE";
        } else if (mes == 12) {
            nombre_mes = "DICIEMBRE";
        }
        return nombre_mes;
    }

    public static String retorna_fecha_sistema() {
        String fecha = "";
        java.util.Date date = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fecha = sdf.format(date);
        return fecha;
    }

    public static String retorna_fecha_sistemaSQLite() {
        String fecha = "";
        java.util.Date date = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        fecha = sdf.format(date);
        return fecha;
    }

    public void sumadefecha(JDateChooser jd, int cantidad) {

        String strFormato = "dd/MM/yyyy";
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(strFormato);

        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, cantidad);
        String fecha_suma = sdf.format(c1.getTime());
        jd.setDate(StringADate(fecha_suma));
    }

    public String sumadefecha(int cantidad) {

        String strFormato = "yyyy-MM-dd";
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(strFormato);

        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, cantidad);
        String fecha_suma = sdf.format(c1.getTime());
        return (fecha_suma);
    }

    public void sumadefechaMeses(JDateChooser jd, int cantidad) {

        String strFormato = "dd/MM/yyyy";
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(strFormato);

        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.MONTH, cantidad);
        String fecha_suma = sdf.format(c1.getTime());
        jd.setDate(StringADate(fecha_suma));
    }

    public String retornaDia(int diaActual) {
        String valDia = "";
        if (diaActual <= 9) {
            valDia = "0" + diaActual;
        } else {
            valDia = "" + diaActual;
        }
        return valDia;
    }

    public String retornaMes(int mesActual) {
        String valMes = "";
        if (mesActual <= 9) {
            valMes = "0" + mesActual;
        } else {
            valMes = "" + mesActual;
        }
        return valMes;
    }

    public String sumaFechas(int diaActual, int mesActual, int anioActual) {
        String valMes = "";
        String valDia = "";
        if (mesActual <= 9) {
            valMes = "0" + mesActual;
        } else {
            valMes = "" + mesActual;
        }
        int val_dia = retornarDia(anioActual, mesActual, diaActual);
        if (val_dia <= 9) {
            valDia = "0" + val_dia;
        } else {
            valDia = "" + val_dia;
        }
        return anioActual + "-" + valMes + "-" + valDia;
    }

    public int retornarDia(int anio, int mes, int diaentrada) {
        int diaretorno = 0;
        if (diaentrada == 29 || diaentrada == 30 || diaentrada == 31) {
            if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0))) {
                if (mes == 2) {
                    if (diaentrada == 29) {
                        diaretorno = 29;
                    } else if (diaentrada == 30) {
                        diaretorno = 29;
                    } else if (diaentrada == 31) {
                        diaretorno = 29;
                    }
                } else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
                    if (diaentrada == 29) {
                        diaretorno = 29;
                    } else if (diaentrada == 30) {
                        diaretorno = 30;
                    } else if (diaentrada == 31) {
                        diaretorno = 31;
                    }
                } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                    if (diaentrada == 29) {
                        diaretorno = 29;
                    } else if (diaentrada == 30) {
                        diaretorno = 30;
                    } else if (diaentrada == 31) {
                        diaretorno = 30;
                    }
                }
            } else {
                if (mes == 2) {
                    if (diaentrada == 29) {
                        diaretorno = 28;
                    } else if (diaentrada == 30) {
                        diaretorno = 28;
                    } else if (diaentrada == 31) {
                        diaretorno = 28;
                    }
                } else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
                    if (diaentrada == 29) {
                        diaretorno = 29;
                    } else if (diaentrada == 30) {
                        diaretorno = 30;
                    } else if (diaentrada == 31) {
                        diaretorno = 31;
                    }
                } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                    if (diaentrada == 29) {
                        diaretorno = 29;
                    } else if (diaentrada == 30) {
                        diaretorno = 30;
                    } else if (diaentrada == 31) {
                        diaretorno = 30;
                    }
                }
            }
        } else {
            diaretorno = diaentrada;
        }
        return diaretorno;
    }

    public static Date sumadefechaDias(Date fecha, int cantidad) {
        if (cantidad == 0) {
            return fecha;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_MONTH, cantidad);
        return calendar.getTime();
    }

    public static Date sumadefechaMesesDate(Date fecha, int cantidad) {
        if (cantidad == 0) {
            return fecha;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.MONTH, cantidad);
        return calendar.getTime();
    }

    public String getFecha(JDateChooser jd) {
        SimpleDateFormat Formato = new SimpleDateFormat("dd/MM/yyyy");
        if (jd.getDate() != null) {
            return Formato.format(jd.getDate());
        } else {
            return null;
        }
    }

    public Date StringADate(String fecha) {
        SimpleDateFormat formato_del_Texto = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaE = null;
        try {
            fechaE = formato_del_Texto.parse(fecha);
            return fechaE;
        } catch (ParseException ex) {
            return null;
        }
    }

    public String mostrar_fecha(JDateChooser jd) {
        return "" + leeFecha(getFecha(jd));
    }

    public void capturaymuestrahoradelsistema(JDateChooser jd) {
        //Instanciamos el objeto Calendar
        //en fecha obtenemos la fecha y hora del sistema
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String fecha_actual = dia + "/" + (mes + 1) + "/" + anoo;
        jd.setDate(StringADate(fecha_actual));
    }

    public void capturaymuestrahoradelsistemaPasar(JDateChooser jd, String fecha_actual) {
        //Instanciamos el objeto Calendar 
        jd.setDate(StringADate(fecha_actual));
    }

    public int obtener_anio() {
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        return anoo;
    }

    public double obtener_montoICBPER() {
        double monto_icbper = 0;
        if (obtener_anio() == 2020) {
            monto_icbper = 0.20;
        } else if (obtener_anio() == 2021) {
            monto_icbper = 0.30;
        } else if (obtener_anio() == 2022) {
            monto_icbper = 0.40;
        } else if (obtener_anio() >= 2023) {
            monto_icbper = 0.50;
        }
        return monto_icbper;
    }

    public static String muestrahoradelsistema() {
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String dato = hora + ":" + minuto + ":" + segundo;
        return dato;
    }

    public String retorna_hora_reportes() {
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String dato = hora + "" + minuto + "" + segundo;
        return dato;
    }

    public String retorna_hora_reportesCompletoCodigo() {
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String horaStr = "";
        String minutoStr = "";
        String segundoStr = "";
        horaStr = "" + hora;
        minutoStr = "" + minuto;
        segundoStr = "" + segundo;

        String dato = horaStr + "" + minutoStr + "" + segundoStr;
        return dato;
    }

    public String retorna_hora_reportesCompleto() {
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String horaStr = "";
        String minutoStr = "";
        String segundoStr = "";
        if (hora <= 9) {
            horaStr = "0" + hora;
        } else {
            horaStr = "" + hora;
        }
        if (minuto <= 9) {
            minutoStr = "0" + minuto;
        } else {
            minutoStr = "" + minuto;
        }
        if (segundo <= 9) {
            segundoStr = "0" + segundo;
        } else {
            segundoStr = "" + segundo;
        }
        String dato = horaStr + "" + minutoStr + "" + segundoStr;
        return dato;
    }

    public String generarCodigoFechaHora() {
        return retorna_fecha_reportesBajaCodigo() + "" + retorna_hora_reportesCompletoCodigo();
    }

    public String retorna_fecha_reportesBajaCodigo() {
        String fecha_actual = "";
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String dias = "";
        String meses = "";
        dias = "" + dia;
        meses = "" + (mes + 1);
        fecha_actual = anoo + "" + meses + "" + dias;
        return fecha_actual;
    }

    public String retorna_fecha_reportesBaja() {
        String fecha_actual = "";
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String dias = "";
        String meses = "";
        if (dia <= 9) {
            dias = "0" + dia;
        } else {
            dias = "" + dia;
        }

        if ((mes + 1) <= 9) {
            meses = "0" + (mes + 1);
        } else {
            meses = "" + (mes + 1);
        }
        fecha_actual = anoo + "" + meses + "" + dias;
        return fecha_actual;
    }

    public String retorna_fecha_BD() {
        String fecha_actual = "";
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String dias = "";
        String meses = "";
        if (dia <= 9) {
            dias = "0" + dia;
        } else {
            dias = "" + dia;
        }

        if ((mes + 1) <= 9) {
            meses = "0" + (mes + 1);
        } else {
            meses = "" + (mes + 1);
        }
        fecha_actual = anoo + "-" + meses + "-" + dias;
        return fecha_actual;
    }

    public String retorna_fecha_reportes() {
        String fecha_actual = "";
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        fecha_actual = dia + "" + (mes + 1) + "" + anoo;
        return fecha_actual;
    }

    public String retorna_fecha_del_sistemaOrden() {
        String fecha = "";
        java.util.Date date = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy");
        fecha = sdf.format(date);
        return fecha;
    }

    public String retorna_fecha_del_sistema() {
        String fecha = "";
        java.util.Date date = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd");
        fecha = sdf.format(date);
        return fecha;
    }

    public Date leeFecha(String fecha) {//yyyy-MM-dd
        SimpleDateFormat formatodelTexto = new SimpleDateFormat("dd/MM/yyyy");
        Date fecha1 = null;
        Date fecha2 = null;
        try {
            fecha1 = formatodelTexto.parse(fecha);
            fecha2 = new java.sql.Date(fecha1.getTime());
        } catch (Exception e) {
            System.out.println("" + e);
        }
        return fecha2;
    }

    public String mostrar_fechaAnioMesDia(JDateChooser jd) {
        return "" + leeFechaAnioMesDia(getFechaAnioMesDia(jd));
    }

    public String getFechaAnioMesDia(JDateChooser jd) {
        SimpleDateFormat Formato = new SimpleDateFormat("yyyy/MM/dd");
        if (jd.getDate() != null) {
            return Formato.format(jd.getDate());
        } else {
            return null;
        }
    }

    public Date leeFechaAnioMesDia(String fecha) {//yyyy-MM-dd
        SimpleDateFormat formatodelTexto = new SimpleDateFormat("yyyy/MM/dd");
        Date fecha1 = null;
        Date fecha2 = null;
        try {
            fecha1 = formatodelTexto.parse(fecha);
            fecha2 = new java.sql.Date(fecha1.getTime());
        } catch (Exception e) {
            System.out.println("" + e);
        }
        return fecha2;
    }

}
