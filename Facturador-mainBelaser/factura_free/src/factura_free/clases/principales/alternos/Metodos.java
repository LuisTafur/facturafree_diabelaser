package factura_free.clases.principales.alternos;

import com.toedter.calendar.JDateChooser;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame; 
import java.awt.Window;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar; 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import factura_free.vista.alertas.AlertaBien;
import factura_free.vista.alertas.AlertaError;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.principal.JFramePrincipal;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JTable;

public class Metodos {

    public static void ConfigurarVentana(JFrame jFrame, String tituloVentana) {
        //posiciono el frame al centro de la pantalla
        jFrame.setLocationRelativeTo(null);
        //activa el cambio de tamaño de la ventana
//        jFrame.setResizable(false);
        //asigno titulo a mostrar del frame
        jFrame.setTitle(tituloVentana); 
    }

    public static void Eliminar_archivo(String ruta) {
        try {
            File archi = new File(ruta);
            if (archi.delete()) {
                System.out.println("Archivo Eliminado");
            } else {
                System.out.println("Archivo no se pudo Eliminar");
            }
        } catch (Exception e) {
            System.out.println("ERROR : " + e.getLocalizedMessage());
        }
    }

    public static String leerContenidoArchivo(String ruta) {
        String retorna = "";
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(ruta);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                retorna = retorna + linea + "\n";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return retorna.replace("'", "/////");
    }

    public static int obtener_anio() {
        Calendar fecha = new GregorianCalendar();
        int anio = fecha.get(Calendar.YEAR);
        return anio;
    }
 

    public static void MensajeError(String mensaje) {
        AlertaError alert = new AlertaError("Mensaje", mensaje);
        alert.setVisible(true);
    }

    public static void MensajeInformacion(String mensaje) {
        AlertaBien alert = new AlertaBien("Mensaje", mensaje);
        alert.setVisible(true);
    }

    public static void MensajeAlerta(String mensaje) {
        AlertaInfo alert = new AlertaInfo("Mensaje", mensaje);
        alert.setVisible(true);
    }

    public static void CambiarPanel(JPanel jPanel) {
        Dimension d = JFramePrincipal.jpnlPrincipal.getSize();//capturamos la dimensión del panel actual
        jPanel.setSize(d);//le enviamos el mismo tamaño del panel principal
        jPanel.setLocation(0, 0);//que se inice desde el punto 0,0
        JFramePrincipal.jpnlPrincipal.removeAll();//quitamos los demás complementos
        JFramePrincipal.jpnlPrincipal.add(jPanel, BorderLayout.CENTER);
        JFramePrincipal.jpnlPrincipal.revalidate();
        JFramePrincipal.jpnlPrincipal.repaint();
    }

    public static String ObtenerUltimosXCaracteres(String cadena, int caracteres) {
        String ultimos_caracteres = cadena.substring(cadena.length() - caracteres, cadena.length());//capturo ultimos 8 digitos
        return ultimos_caracteres;// retorno lo últimos 8 digitos
    }

    // dd/MM/yyyy
    public static String CargarFechaActual() {
        String fecha_actual;
        Date fecha = new Date();//se crea objeto de tipo Date
        fecha_actual = new SimpleDateFormat("dd/MM/yyyy").format(fecha);//envio al text field al mismo tiempo se cambia el formato
        return fecha_actual;
    }

    public static Frame FormatoJDialog() {
        JPanel panel = new JPanel();
        Window parentWindow = SwingUtilities.windowForComponent(panel);
        // or pass 'this' if you are inside the panel
        Frame parentFrame = null;
        if (parentWindow instanceof Frame) {
            parentFrame = (Frame) parentWindow;
        }
        return parentFrame;
    }

    public static String ObtenerHora() {
        Calendar calendario = Calendar.getInstance();
        String hh, mm, ss;
        hh = String.format("%02d", (calendario.get(Calendar.HOUR_OF_DAY)));
        mm = String.format("%02d", (calendario.get(Calendar.MINUTE)));
        ss = String.format("%02d", (calendario.get(Calendar.SECOND)));
        String hora_completa = hh + ":" + mm + ":" + ss;
        return hora_completa;
    }

    public static String GenerarCodigoUnico() {
        String codigo = retorna_fechaCodigo() + "" + retorna_horaCodigo();
        return codigo;
    }

    public static String retorna_horaCodigo() {
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String horaStr = "";
        String minutoStr = "";
        String segundoStr = "";
        horaStr = "" + hora;
        minutoStr = "" + minuto;
        segundoStr = "" + segundo;

        String dato = horaStr + "" + minutoStr + "" + segundoStr;
        return dato;
    }

    public static String retorna_fechaCodigo() {
        String fecha_actual = "";
        Calendar fecha = new GregorianCalendar();
        int anoo = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        String dias = "";
        String meses = "";
        dias = "" + dia;
        meses = "" + (mes + 1);
        fecha_actual = anoo + "" + meses + "" + dias;
        return fecha_actual;
    }

    public static void QuitarVariosItem(JTable tblDetalle, DefaultTableModel dtmDetalle) {
        int[] rows = tblDetalle.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            dtmDetalle.removeRow(rows[i] - i);
        }
    }

    public static void LimpiarTabla(DefaultTableModel dtmFactura) {
        //limpiar jtable para que no se dupliquen datos
        for (int i = dtmFactura.getRowCount() - 1; i >= 0; i--) {
            dtmFactura.removeRow(i);
        }
    }

    //convierte a String un JDateChooser
    public static String CapturarDateChooser(JDateChooser jDateChooser) {
        String fecha_final = "error";
        try {
            if (jDateChooser.getDate() == null) {//Si es nulo
                fecha_final = "";
            } else {//si tiene una fecha válida
                String formato_fecha = jDateChooser.getDateFormatString();
                Date fecha = jDateChooser.getDate();
                SimpleDateFormat sdf = new SimpleDateFormat(formato_fecha);
                fecha_final = String.valueOf(sdf.format(fecha));
            }
        } catch (Exception e) {
            Metodos.MensajeError("Error al capturar el JdateChooser 'jDateChooser'\n" + e);
        }
        return fecha_final;
    }

    // ========= Métodos ==========
    public static String validarPersona(String tipoDocumento,
            String numeroDocumento, String nombreRazonSocial, String direccion) {
        String mensaje = "";

        if (nombreRazonSocial.equals("")) {
            mensaje = "Escriba el nombre o razón social.";
        }

        if (tipoDocumento.equals("---SELECCIONE---")) {
            mensaje = "Seleccione el tipo de documento.";
        } else {

            if (tipoDocumento.equals("SIN DOCUMENTO")) {
                if (numeroDocumento.length() != 0) {
                    mensaje = "El " + tipoDocumento + " debe tener 0 dígitos.";
                }
            } else if (tipoDocumento.equals("DNI")) {
                if (numeroDocumento.length() != 8) {
                    mensaje = "El " + tipoDocumento + " debe tener 8 dígitos.";
                }
            } else if (tipoDocumento.equals("RUC")) {
                if (numeroDocumento.length() != 11) {
                    mensaje = "El " + tipoDocumento + " debe tener 11 dígitos.";
                }
            } else if (tipoDocumento.equals("Carnet de extranjería")) {
                if (numeroDocumento.length() != 12) {
                    mensaje = "El " + tipoDocumento + " debe tener 12 dígitos.";
                }
            } else if (tipoDocumento.equals("Pasaporte")) {
                if (numeroDocumento.length() != 12) {
                    mensaje = "El " + tipoDocumento + " debe tener 12 dígitos.";
                }
            }

        }

        return mensaje;
    }

    public static String getFechaJDC(JDateChooser Fecha) {
        String fecha = "";
        try {
            Date d = Fecha.getDate();
            if (d == null) {
                fecha = "-";
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                fecha = sdf.format(d);
            }
        } catch (Exception e) {
        }

        return fecha;
    }

    // YYYY-MM-DD según SUNAT
    public static String fechaFormatoSUNAT(String fecha) {
        String fechaSunat;
        if (fecha.equals("-")) {
            fechaSunat = fecha;
        } else {
            try {
                Date d = new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                fechaSunat = sdf.format(d);
            } catch (Exception e) {
                System.out.println("Error: " + e);
                fechaSunat = "";
            }
        }
        return fechaSunat;
    }

    // máximo 10 decimales SUNAT
    public static double formatoDecimalOperar(String numero) {
        BigDecimal bd = new BigDecimal(Double.parseDouble(numero)).setScale(4, RoundingMode.HALF_EVEN);
        double Gen = bd.doubleValue();
        return Gen;
    }

    // 2 decimales
    public static String formatoDecimalMostrar(double numero) { 
        BigDecimal bd = new BigDecimal(numero).setScale(2, RoundingMode.HALF_EVEN);
        return "" + bd.doubleValue();
    }

    // 2 decimales
    public static String formatoDecimalMostrar4Decimales(double numero) {
        BigDecimal bd = new BigDecimal(numero).setScale(4, RoundingMode.HALF_EVEN);
        return "" + bd.doubleValue(); 
    }

    public static String convertirNumTexto(String numero, String moneda) {
        String moneda_texto;
        ConvertirNumeroTexto NumLetra = new ConvertirNumeroTexto();
        if (moneda.equalsIgnoreCase("PEN")) {
            moneda_texto = "SOLES.";
        } else if (moneda.equalsIgnoreCase("USD")) {
            moneda_texto = "DÓLARES AMERICANOS.";
        } else {
            moneda_texto = "";
        }
        String letritas = (NumLetra.Convertir(numero, band()));
        return letritas + moneda_texto;
    }

    //para el numero en letras
    private static boolean band() {
        if (Math.random() > .5) {
            return true;
        } else {
            return false;
        }
    }

    ///valida campo de simbolos extraños y espacios en blanco
    public static String validarCamposIniciales(String txtDescripcion) {
        String retorna = "";
        boolean texto = true;
        int medida = txtDescripcion.length();
        try {

            if (medida >= 1) {
                String vacio_1 = txtDescripcion.substring(0, 1);
                if (vacio_1.equalsIgnoreCase(" ")) {
                    retorna = (txtDescripcion.substring(1, medida));
                    texto = false;
                }
            }
            if (medida >= 2) {
                String vacio_2 = txtDescripcion.substring(0, 2);
                if (vacio_2.equalsIgnoreCase("  ")) {
                    retorna = (txtDescripcion.substring(2, medida));
                    texto = false;
                }
            }
            if (medida >= 3) {
                String vacio_3 = txtDescripcion.substring(0, 3);
                if (vacio_3.equalsIgnoreCase("   ")) {
                    retorna = (txtDescripcion.substring(3, medida));
                    texto = false;
                }
            }
            if (medida >= 4) {
                String vacio_4 = txtDescripcion.substring(0, 4);
                if (vacio_4.equalsIgnoreCase("    ")) {
                    retorna = (txtDescripcion.substring(4, medida));
                    texto = false;
                }
            }
            if (medida >= 5) {
                String vacio_5 = txtDescripcion.substring(0, 5);
                if (vacio_5.equalsIgnoreCase("     ")) {
                    retorna = (txtDescripcion.substring(5, medida));
                    texto = false;
                }
            }
            if (medida >= 6) {
                String vacio_6 = txtDescripcion.substring(0, 6);
                if (vacio_6.equalsIgnoreCase("      ")) {
                    retorna = (txtDescripcion.substring(6, medida));
                    texto = false;
                }
            }
            if (medida >= 7) {
                String vacio_7 = txtDescripcion.substring(0, 7);
                if (vacio_7.equalsIgnoreCase("       ")) {
                    retorna = (txtDescripcion.substring(6, medida));
                    texto = false;
                }
            }
            if (medida >= 8) {
                String vacio_8 = txtDescripcion.substring(0, 8);
                if (vacio_8.equalsIgnoreCase("        ")) {
                    retorna = (txtDescripcion.substring(6, medida));
                    texto = false;
                }
            }
            if (medida >= 9) {
                String vacio_9 = txtDescripcion.substring(0, 9);
                if (vacio_9.equalsIgnoreCase("         ")) {
                    retorna = (txtDescripcion.substring(6, medida));
                    texto = false;
                }
            }
            if (texto) {
                retorna = txtDescripcion;
            }
        } catch (Exception e) {
            System.err.println("error en medida: " + e.getMessage());
        }
        return retorna.toUpperCase().replace("'", "").replace("|", "/").replace("%", "").replace("\"", "").replace("?", "").replace("¡", "").replace("!", "")
                .replace("//", "/").replace("#", "").replace("´", " ").replace("\n", "");
    }
}
