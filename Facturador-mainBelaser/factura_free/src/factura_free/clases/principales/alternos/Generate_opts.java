/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.alternos;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author IngenieroLuisTafurgm
 */
public class Generate_opts {

    public static String generarCodigoHash(String data) {
        String valorhash = "";
        MessageDigest md = null;

        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Generate_opts.class.getName()).log(Level.SEVERE, null, ex);
        }

        byte[] digest = null;
        try {
            digest = md.digest(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Generate_opts.class.getName()).log(Level.SEVERE, null, ex);
        }
        String encodedDigestBase64 = DatatypeConverter.printBase64Binary(digest);
        valorhash = encodedDigestBase64.substring(16, 44);

        return valorhash;
    }
}
