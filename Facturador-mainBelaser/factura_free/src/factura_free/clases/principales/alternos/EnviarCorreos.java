/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.alternos;

import factura_free.clases.principales.Datos; 
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author jason
 */
public class EnviarCorreos {

    String usuarioCorreo;
    String password;

    String rutaArchivo;
    String nombreArchivo;

    String destinatario;
    String asunto;
    String mensaje;

    public EnviarCorreos(String usuarioCorreo, String password, String rutaArchivo, String nombreArchivo, String destinatario, String asunto, String mensaje) {
        this.usuarioCorreo = usuarioCorreo;
        this.password = password;
        this.rutaArchivo = rutaArchivo;
        this.nombreArchivo = nombreArchivo;
        this.destinatario = destinatario;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public EnviarCorreos(String usuarioCorre, String password, String destinatario, String mensaje) {
        this(usuarioCorre, password, "", "", destinatario, "", mensaje);
    }

    public EnviarCorreos(String usuarioCorre, String password, String destinatario, String asunto,
            String mensaje) {
        this(usuarioCorre, password, "", "", destinatario, asunto, mensaje);
    }
 
    public String sendMail() {
        String respuesta = "NO";
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", usuarioCorreo);
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props, null);
            BodyPart texto = new MimeBodyPart();
            texto.setText(mensaje);

            BodyPart adjunto = new MimeBodyPart();
            if (!rutaArchivo.equals("")) {
                adjunto.setDataHandler(
                        new DataHandler(new FileDataSource(rutaArchivo)));
                adjunto.setFileName(nombreArchivo);
            }

            String CadHTML = "<p style=\"margin-top: 0; color: #74787E; font-size: 14px; line-height: 1.5em;\">\n"
                    + "Hola, somos <strong>" + Datos.getRazonSocial() + "</strong>\n"
                    + "Le enviamos la representación en formato PDF de su comprobante generado..<br>\n"
                    + "<br>"
                    + " Desarrollado por <a style=\"color: #74787E;\" href=\"https://emiteperu.com\" target=\"_blank\"> EmitePerú</a>, especialistas en desarrollo de sistemas de Facturación Electrónica. "
                    + "</p>";

            texto.setContent(CadHTML, "text/html");

            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            if (!rutaArchivo.equals("")) {
                multiParte.addBodyPart(adjunto);
            }
            try {

                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(usuarioCorreo));
                message.addRecipient(
                        Message.RecipientType.TO,
                        new InternetAddress(destinatario));
                message.setSubject(asunto);
                message.setContent(multiParte);

                Transport t = session.getTransport("smtp");
                t.connect(usuarioCorreo, password);
                t.sendMessage(message, message.getAllRecipients());
                t.close();
                respuesta = "SI";
            } catch (Exception e) {
                System.err.println("" + e.getMessage());
                Metodos.MensajeError("Revise su conexion a Internet y/o Correo");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

}
