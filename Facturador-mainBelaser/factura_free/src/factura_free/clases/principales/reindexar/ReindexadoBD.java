/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.reindexar;

import factura_free.clases.controlador.ClienteImpl;
import factura_free.clases.controlador.ConfigImpl;
import factura_free.clases.controlador.UsuarioImpl;
import factura_free.clases.modelo.ObCliente;
import factura_free.clases.modelo.ObConfig;
import factura_free.clases.modelo.ObUsuario;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.principales.alternos.EncriparDatos;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author USER-1
 */
public class ReindexadoBD {

    public static void reindexarBase_de_Datos() {
        crearTablas();
        nuevos_campos();
        crearVistas();
    }

    public static void crearTablas() {

        String config = "CREATE TABLE IF NOT EXISTS config ( "
                + " id int(1) unsigned NOT NULL AUTO_INCREMENT, "
                + " ruc varchar(11) COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " razonSocial longtext COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " direccion longtext COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " telefono varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " correo varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " web varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " impresion varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " usa_cnt_flotante char(2) NOT NULL DEFAULT 'NO', "
                + " producto_igv char(2) NOT NULL DEFAULT 'SI', "
                + " serv_datos varchar(20) NOT NULL DEFAULT 'SERVIDOR 1', "
                + " correo_aplicacion varchar(255) NOT NULL DEFAULT '-', "
                + " pass_aplicacion varchar(255) NOT NULL DEFAULT '-', "
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(config);

        String usuario = "CREATE TABLE IF NOT EXISTS usuario ( "
                + " id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                + " usuario varchar(100) COLLATE utf8_spanish2_ci NOT NULL, "
                + " contrasena varchar(100) COLLATE utf8_spanish2_ci NOT NULL, "
                + " rol varchar(100) COLLATE utf8_spanish2_ci NOT NULL, "
                + " estado varchar(10) COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO', "
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(usuario);
        int cntUsuarios = UsuarioImpl.obtenerEntero("Select count(id) from usuario");
        if (cntUsuarios == 0) {
            try {
                ObUsuario usuarioObj = new ObUsuario(0, "ADMIN", "ADMIN", "ADMINISTRADOR", "ACTIVO");
                UsuarioImpl.Registrar(usuarioObj);
            } catch (SQLException ex) {
                Logger.getLogger(ReindexadoBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String producto = "CREATE TABLE IF NOT EXISTS producto ( "
                + " id int(11) NOT NULL AUTO_INCREMENT, "
                + " codigo varchar(30) COLLATE utf8_spanish2_ci NOT NULL, "
                + " descripcion varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '', "
                + " precio decimal(11,2) DEFAULT 0.00, "
                + " incluye_igv int(1) DEFAULT 0, "
                + " tipo varchar(50) COLLATE utf8_spanish2_ci DEFAULT 'PRODUCTO', "
                + " afectacion varchar(255) COLLATE utf8_spanish2_ci DEFAULT 'IGV Impuesto General a las Ventas', "
                + " unidad_medida varchar(255) COLLATE utf8_spanish2_ci DEFAULT 'UNIDAD', "
                + " estado varchar(10) COLLATE utf8_spanish2_ci NOT NULL DEFAULT 'ACTIVO', "
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(producto);

        String cliente = "CREATE TABLE IF NOT EXISTS cliente ( "
                + " id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                + " tipoDocumento varchar(50) COLLATE utf8_spanish2_ci DEFAULT '', "
                + " numeroDocumento varchar(20) COLLATE utf8_spanish2_ci DEFAULT '', "
                + " nombreRazonSocial text COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " direccion text COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " correo_envios varchar(255) COLLATE utf8_spanish2_ci DEFAULT '-', "
                + " estado varchar(10) COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO', "
                + " PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(cliente);

        String proveedor = "CREATE TABLE IF NOT EXISTS proveedor ( "
                + " id int(11) unsigned NOT NULL AUTO_INCREMENT, "
                + " tipoDocumento varchar(50) COLLATE utf8_spanish2_ci DEFAULT '', "
                + " numeroDocumento varchar(20) COLLATE utf8_spanish2_ci DEFAULT '', "
                + " nombreRazonSocial text COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " direccion text COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + " correo_envios varchar(255) COLLATE utf8_spanish2_ci DEFAULT '-', "
                + " estado varchar(10) COLLATE utf8_spanish2_ci DEFAULT 'ACTIVO', "
                + " PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(proveedor);

        int cntClientes = ClienteImpl.obtenerEntero("Select count(id) from cliente");
        if (cntClientes == 0) {
            try {
                ObCliente clienteObj = new ObCliente(0, "SIN DOCUMENTO", "", "PUBLICO VARIOS", "--", "", "ACTIVO");
                ClienteImpl.Registrar(clienteObj);
            } catch (SQLException ex) {
                Logger.getLogger(ReindexadoBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String presupuesto = "CREATE TABLE IF NOT EXISTS presupuesto ("
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  idCliente int(11) unsigned NOT NULL DEFAULT 0,"
                + "  fecha char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  horaEmision varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  fechaVencimiento char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  moneda char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  medioPago varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  totalVentasGravadas decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  totalGratuito decimal(10,2) DEFAULT 0.00,"
                + "  totalExonerado decimal(10,2) DEFAULT 0.00,"
                + "  igv decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  importeTotal decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  motivo_anulacion varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL," 
                + "  PRIMARY KEY (id)"
                + ");";
        SQLconsultas.actualizaTabla(presupuesto);

        String presupuestodet = "CREATE TABLE IF NOT EXISTS presupuestodet ( "
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  idPresupuesto varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  item int(11) NOT NULL DEFAULT 0, "
                + "  cantidad decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  tipoUnidad varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  codigo varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  descripcion longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  tributo varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  montoTributo decimal(10,2) DEFAULT 0.00, "
                + "  tipoAfectacionTributo varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  valorUnitario decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  precioUnitarioItem decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  valorUnitarioGratuito decimal(10,2) NOT NULL DEFAULT 0.00, " 
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(presupuestodet);

        String venta_interna = "CREATE TABLE IF NOT EXISTS venta_interna ("
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  idCliente int(11) unsigned NOT NULL DEFAULT 0,"
                + "  fecha char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  horaEmision varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  fechaVencimiento char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  moneda char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  medioPago varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  totalVentasGravadas decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  totalGratuito decimal(10,2) DEFAULT 0.00,"
                + "  totalExonerado decimal(10,2) DEFAULT 0.00,"
                + "  igv decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  importeTotal decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  motivo_anulacion varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL," 
                + "  PRIMARY KEY (id)"
                + ");";
        SQLconsultas.actualizaTabla(venta_interna);

        String venta_internadet = "CREATE TABLE IF NOT EXISTS venta_internadet ( "
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  idVenta_interna varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  item int(11) NOT NULL DEFAULT 0, "
                + "  cantidad decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  tipoUnidad varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  codigo varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  descripcion longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  tributo varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  montoTributo decimal(10,2) DEFAULT 0.00, "
                + "  tipoAfectacionTributo varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  valorUnitario decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  precioUnitarioItem decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  valorUnitarioGratuito decimal(10,2) NOT NULL DEFAULT 0.00, " 
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(venta_internadet);

        //////KARDEX
        String producto_kardex = "CREATE TABLE IF NOT EXISTS  producto_kardex ("
                + "  id_producto_kardex int(11) NOT NULL AUTO_INCREMENT,"
                + "  id_producto int(11) DEFAULT 0,"
                + "  fecha varchar(20) DEFAULT NULL,"
                + "  hora varchar(10) DEFAULT NULL,"
                + "  cnt_entrada decimal(10,2) DEFAULT 0.00,"
                + "  cnt_salida decimal(10,2) DEFAULT 0.00,"
                + "  doc_relacionado varchar(20) DEFAULT NULL,"
                + "  tipo varchar(255) DEFAULT NULL,"
                + "  usuario varchar(255) DEFAULT NULL,"
                + "  PRIMARY KEY (id_producto_kardex)"
                + ");";
        SQLconsultas.actualizaTabla(producto_kardex);

        String compra = "CREATE TABLE IF NOT EXISTS compra ("
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  idCliente int(11) unsigned NOT NULL DEFAULT 0,"
                + "  fecha char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  horaEmision varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  fechaVencimiento char(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  moneda char(3) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,"
                + "  medioPago varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL,"
                + "  totalVentasGravadas decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  totalGratuito decimal(10,2) DEFAULT 0.00,"
                + "  totalExonerado decimal(10,2) DEFAULT 0.00,"
                + "  igv decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  importeTotal decimal(10,2) NOT NULL DEFAULT 0.00,"
                + "  motivo_anulacion varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL," 
                + "  estado_comprobante varchar(50) DEFAULT NULL,"
                + "  comprobante_relacionado varchar(50) DEFAULT NULL,"
                + "  id_usuario int(11) unsigned NOT NULL DEFAULT 0,"
                + "  PRIMARY KEY (id)"
                + ");";
        SQLconsultas.actualizaTabla(compra);

        String compradet = "CREATE TABLE IF NOT EXISTS compradet ( "
                + "  id varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  idcompra varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  item int(11) NOT NULL DEFAULT 0, "
                + "  cantidad decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  tipoUnidad varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  codigo varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  descripcion longtext CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL, "
                + "  tributo varchar(10) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  montoTributo decimal(10,2) DEFAULT 0.00, "
                + "  tipoAfectacionTributo varchar(5) CHARACTER SET utf8 COLLATE utf8_spanish2_ci DEFAULT NULL, "
                + "  valorUnitario decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  precioUnitarioItem decimal(10,2) NOT NULL DEFAULT 0.00, "
                + "  valorUnitarioGratuito decimal(10,2) NOT NULL DEFAULT 0.00, " 
                + "  precio decimal(10,2) DEFAULT 0.00, "
                + "  PRIMARY KEY (id) "
                + ");";
        SQLconsultas.actualizaTabla(compradet);

    }

    public static void nuevos_campos() {
        /////agregamos campo para bajas en facturas, notas para facturas y notas debito relacionadas a facturas 
        if (!SQLconsultas.validar("select icbper from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD icbper varchar(2) NULL DEFAULT 'NO' AFTER estado ;");
        }

        //////LEEREMOS LOS XML DE FIRMA Y RESPUESTA PARA LA BD//////   
        if (!SQLconsultas.validar("select logo from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD logo varchar(255) NULL DEFAULT 'logo.png' AFTER logo ;");
        }

        if (!SQLconsultas.validar("select email_contador from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD email_contador varchar(255) NULL DEFAULT '' AFTER avanzados ;");
        }

        if (!SQLconsultas.validar("select seriePresupuesto from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD seriePresupuesto varchar(4) NULL DEFAULT 'CT01' AFTER email_contador ;");
        }

        if (!SQLconsultas.validar("select serieVenta_interna from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD serieVenta_interna varchar(4) NULL DEFAULT 'V001' AFTER seriePresupuesto ;");
        }

        ////MODIFICACION DE DETALLE DE COMPROBANTES   
        if (!SQLconsultas.validar("select precio from presupuestodet")) {
            SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet ADD precio decimal(10,2) NULL DEFAULT '0.0' AFTER valorUnitarioGratuito;");
        }
        SQLconsultas.actualizaTabla("UPDATE presupuestodet set precio=(precioUnitarioItem/cantidad) where precio='0';");

        if (!SQLconsultas.validar("select precio from venta_internadet")) {
            SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet ADD precio decimal(10,2) NULL DEFAULT '0.0' AFTER valorUnitarioGratuito;");
        }
        SQLconsultas.actualizaTabla("UPDATE venta_internadet set precio=(precioUnitarioItem/cantidad) where precio='0';");

        ////alteramos los montos de los detalles  
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE cantidad cantidad decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE montoTributo montoTributo decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE valorUnitario valorUnitario decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE precioUnitarioItem precioUnitarioItem decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE valorUnitarioGratuito valorUnitarioGratuito decimal(10,4);"); 
        SQLconsultas.actualizaTabla("ALTER TABLE presupuestodet CHANGE precio precio decimal(10,4);");

        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE cantidad cantidad decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE montoTributo montoTributo decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE valorUnitario valorUnitario decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE precioUnitarioItem precioUnitarioItem decimal(10,4);");
        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE valorUnitarioGratuito valorUnitarioGratuito decimal(10,4);"); 
        SQLconsultas.actualizaTabla("ALTER TABLE venta_internadet CHANGE precio precio decimal(10,4);");

//////AGREGAMOS EL IS DEL USUARIO RESPONSABLE DE LA VENTA 
        if (!SQLconsultas.validar("select id_usuario from presupuesto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE presupuesto ADD id_usuario int(11) NULL DEFAULT '1' AFTER comprobante_relacionado ;");
        }

        if (!SQLconsultas.validar("select id_usuario from venta_interna")) {
            SQLconsultas.actualizaTabla("ALTER TABLE venta_interna ADD id_usuario int(11) NULL DEFAULT '1' AFTER comprobante_relacionado ;");
        }

        if (!SQLconsultas.validar("select subir_online from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD subir_online varchar(4) NULL DEFAULT 'NO' AFTER icbper ;");
        }

        if (!SQLconsultas.validar("select tiene_stock from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD tiene_stock varchar(4) NULL DEFAULT 'NO' AFTER subir_online ;");
        }

        if (!SQLconsultas.validar("select stock_actual from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD stock_actual decimal(10,4) NULL DEFAULT '0' AFTER tiene_stock ;");
        }

        if (!SQLconsultas.validar("select afectacion_compra from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD afectacion_compra varchar(100) NULL DEFAULT 'IGV Impuesto General a las Ventas' AFTER stock_actual;");
        }

        if (!SQLconsultas.validar("select precio_compra from producto")) {
            SQLconsultas.actualizaTabla("ALTER TABLE producto ADD precio_compra decimal(10,4) NULL DEFAULT '0' AFTER afectacion_compra ;");
        }

        SQLconsultas.actualizaTabla("ALTER TABLE config CHANGE serv_datos serv_datos varchar(100);");
 
        if (!SQLconsultas.validar("select serieCompra from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD serieCompra varchar(4) NULL DEFAULT 'CM01' AFTER serieVenta_interna ;");
        } 

        if (!SQLconsultas.validar("select impre_directa from config")) {
            SQLconsultas.actualizaTabla("ALTER TABLE config ADD impre_directa varchar(4) NULL DEFAULT 'NO' AFTER serieCompra ;");
        }

        int cntConfig = ConfigImpl.obtenerEntero("Select count(id) from config");
        if (cntConfig == 0) {
            try {
                ObConfig configObj = new ObConfig(0, "10101010101", "EMPRESA DEMO", "--", "000000000", "mi_correo@gmail.com", "https://emiteperu.com", "Ticket 80 mm",
                        "NO", "SI", EncriparDatos.Encriptar("SERVIDOR 1"), "facturafree2021@gmail.com",
                        "oermjtryssuobrfc", "logo.png", "", "CT01", "V001",  "CM01", "NO");
                ConfigImpl.Registrar(configObj);
            } catch (SQLException ex) {
                Logger.getLogger(ReindexadoBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void crearVistas() {

        //////PRESUPUESTO
        String view_presupuesto_view = "DROP VIEW IF EXISTS presupuesto_view;";
        SQLconsultas.actualizaTabla(view_presupuesto_view);

        String presupuesto_view = "CREATE VIEW presupuesto_view AS "
                + " select "
                + " presupuesto.id AS id_presupuesto,"
                + " presupuesto.idCliente AS id_cliente,"
                + " cliente.tipoDocumento AS tipoDocumento_cliente,"
                + " cliente.numeroDocumento AS numeroDocumento_cliente,"
                + " cliente.nombreRazonSocial AS cliente,"
                + " cliente.direccion AS direccion_cliente,"
                + " cliente.correo_envios AS correo_envios,"
                + " presupuesto.fecha AS fecha_emision,"
                + " presupuesto.horaEmision AS hora_emision,"
                + " presupuesto.fechaVencimiento AS fecha_vencimiento,"
                + " presupuesto.moneda AS moneda,"
                + " presupuesto.medioPago AS medioPago,"
                + " presupuesto.totalVentasGravadas AS totalVentasGravadas,"
                + " presupuesto.totalGratuito AS totalGratuito,"
                + " presupuesto.totalExonerado AS totalExonerado,"
                + " presupuesto.igv AS totalIgv,"
                + " presupuesto.importeTotal AS importeTotal,"
                + " presupuesto.motivo_anulacion AS motivo_anulacion ," 
                + " presupuesto.estado_comprobante AS estado_comprobante ,"
                + " presupuesto.comprobante_relacionado AS comprobante_relacionado ,"
                + " presupuesto.id_usuario AS id_usuario "
                + " from (presupuesto join cliente on "
                + " (presupuesto.idCliente = cliente.id));";
        SQLconsultas.actualizaTabla(presupuesto_view);

        //////venta_interna
        String view_venta_interna_view = "DROP VIEW IF EXISTS venta_interna_view;";
        SQLconsultas.actualizaTabla(view_venta_interna_view);

        String venta_interna_view = "CREATE VIEW venta_interna_view AS "
                + " select "
                + " venta_interna.id AS id_venta_interna,"
                + " venta_interna.idCliente AS id_cliente,"
                + " cliente.tipoDocumento AS tipoDocumento_cliente,"
                + " cliente.numeroDocumento AS numeroDocumento_cliente,"
                + " cliente.nombreRazonSocial AS cliente,"
                + " cliente.direccion AS direccion_cliente,"
                + " cliente.correo_envios AS correo_envios,"
                + " venta_interna.fecha AS fecha_emision,"
                + " venta_interna.horaEmision AS hora_emision,"
                + " venta_interna.fechaVencimiento AS fecha_vencimiento,"
                + " venta_interna.moneda AS moneda,"
                + " venta_interna.medioPago AS medioPago,"
                + " venta_interna.totalVentasGravadas AS totalVentasGravadas,"
                + " venta_interna.totalGratuito AS totalGratuito,"
                + " venta_interna.totalExonerado AS totalExonerado,"
                + " venta_interna.igv AS totalIgv,"
                + " venta_interna.importeTotal AS importeTotal,"
                + " venta_interna.motivo_anulacion AS motivo_anulacion ," 
                + " venta_interna.estado_comprobante AS estado_comprobante ,"
                + " venta_interna.comprobante_relacionado AS comprobante_relacionado ,"
                + " venta_interna.id_usuario AS id_usuario ,"
                + " venta_interna.montoEfectivoVenta AS montoEfectivoVenta ,"
                + " venta_interna.montoTarjetaVenta AS montoTarjetaVenta ,"
                + " venta_interna.montoVueltoVenta AS montoVueltoVenta ,"
                + " venta_interna.montoDeudaVenta AS montoDeudaVenta ,"
                + " venta_interna.descripcionTarjetaVenta AS descripcionTarjetaVenta "
                + " from (venta_interna join cliente on "
                + " (venta_interna.idCliente = cliente.id));";
        SQLconsultas.actualizaTabla(venta_interna_view);

        //////compra
        String view_compra_view = "DROP VIEW IF EXISTS compra_view;";
        SQLconsultas.actualizaTabla(view_compra_view);

        String compra_view = "CREATE VIEW compra_view AS "
                + " select compra.id AS id_compra, "
                + " compra.idCliente AS id_cliente, "
                + " cliente.tipoDocumento AS tipoDocumento_cliente, "
                + " cliente.numeroDocumento AS numeroDocumento_cliente, "
                + " cliente.nombreRazonSocial AS cliente, "
                + " cliente.direccion AS direccion_cliente, "
                + " cliente.correo_envios AS correo_envios,"
                + " compra.fecha AS fecha_emision, "
                + " compra.horaEmision AS hora_emision,"
                + " compra.fechaVencimiento AS fecha_vencimiento, "
                + " compra.moneda AS moneda,"
                + " compra.medioPago AS medioPago, "
                + " compra.totalVentasGravadas AS totalVentasGravadas, "
                + " compra.totalGratuito AS totalGratuito, "
                + " compra.totalExonerado AS totalExonerado,"
                + " compra.igv AS totalIgv, "
                + " compra.importeTotal AS importeTotal,"
                + " compra.motivo_anulacion AS motivo_anulacion, " 
                + " compra.estado_comprobante AS estado_comprobante, "
                + " compra.comprobante_relacionado AS comprobante_relacionado, "
                + " compra.id_usuario AS id_usuario "
                + " from compra as compra inner join proveedor as cliente on compra.idCliente = cliente.id;";
        SQLconsultas.actualizaTabla(compra_view);
    }
}
