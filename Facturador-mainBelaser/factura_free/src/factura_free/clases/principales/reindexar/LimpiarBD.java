/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.reindexar;

import factura_free.clases.controlador.ClienteImpl;
import factura_free.clases.modelo.ObCliente;
import factura_free.clases.principales.SQLconsultas;
import java.sql.SQLException;

/**
 *
 * @author USER-1
 */
public class LimpiarBD {

    public static void limpiarDBTruncate() throws SQLException { 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE producto;"); 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE producto_kardex;");    
        SQLconsultas.actualizaTabla("TRUNCATE TABLE cliente;");
        ObCliente cliente = new ObCliente(0, "SIN DOCUMENTO", "", "PUBLICO VARIOS", "--", "", "ACTIVO");
        ClienteImpl.Registrar(cliente);
 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE compra;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE compradet;");
 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE presupuesto;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE presupuestodet;");

        SQLconsultas.actualizaTabla("TRUNCATE TABLE venta_interna;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE venta_internadet;");

        SQLconsultas.actualizaTabla("TRUNCATE TABLE proveedor;");
    }

    public static void limpiarDBTruncateProductos() throws SQLException { 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE producto;"); 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE producto_kardex;");  
    }

    public static void limpiarDBTruncateClientes() throws SQLException { 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE cliente;");
        ObCliente cliente = new ObCliente(0, "SIN DOCUMENTO", "", "PUBLICO VARIOS", "--", "", "ACTIVO");
        ClienteImpl.Registrar(cliente);
    }

    public static void limpiarDBTruncateVentas() throws SQLException {  
 
        SQLconsultas.actualizaTabla("TRUNCATE TABLE compra;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE compradet;");

        SQLconsultas.actualizaTabla("TRUNCATE TABLE proveedor;");
        
        SQLconsultas.actualizaTabla("TRUNCATE TABLE presupuesto;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE presupuestodet;");

        SQLconsultas.actualizaTabla("TRUNCATE TABLE venta_interna;");
        SQLconsultas.actualizaTabla("TRUNCATE TABLE venta_internadet;");
         
    }
}
