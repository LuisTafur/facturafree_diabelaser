/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.consulta_datos;

import factura_free.clases.principales.Datos;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author USER-1
 */
public class Control_ConsultaDatos {

    private static String servidor_consulta = Datos.getServ_datos();
    private static String rpta = "NO";

    public static String consultaDatos(JTextField txtNombresCompleto, JTextField txtDireccion, JTextField txtNumeroDocumento, JComboBox cboTipodocumento) {
        ////SERVIDORES DE CONSULTA
        if (servidor_consulta.equalsIgnoreCase("SERVIDOR 1")) {
            rpta = Consulta_Serv1.ConsultaServidor1(txtNombresCompleto, txtDireccion, txtNumeroDocumento, cboTipodocumento);
        } else if (servidor_consulta.equalsIgnoreCase("SERVIDOR 2")) {
            rpta = Consulta_Serv2.ConsultaServidor2(txtNombresCompleto, txtDireccion, txtNumeroDocumento, cboTipodocumento);
        }  
        return rpta;
    }
}
