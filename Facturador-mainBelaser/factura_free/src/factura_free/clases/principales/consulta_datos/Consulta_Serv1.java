/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.consulta_datos;

import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.consulta_datos.serv.Consulta_DNI_Serv1;
import factura_free.clases.principales.consulta_datos.serv.Consulta_RUC_Serv1;
import factura_free.vista.alertas.AlertaInfo;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 *
 * @author USER-1
 */
public class Consulta_Serv1 {

    public static String ConsultaServidor1(JTextField txtNombresCompleto, JTextField txtDireccion, JTextField txtNumeroDocumento, JComboBox cboTipodocumento) {

        String rpt = "";
        txtNombresCompleto.setText("");
        txtDireccion.setText("");
        String tipoDoc = cboTipodocumento.getSelectedItem().toString();
        if (tipoDoc.equalsIgnoreCase("RUC") || tipoDoc.equalsIgnoreCase("DNI")) {
            int longitud = txtNumeroDocumento.getText().length();
            if (tipoDoc.equalsIgnoreCase("RUC")) {
                int longiDoc = 11;
                if (longitud > longiDoc) {
                    Metodos.MensajeAlerta("El número de documento no es válido. Vuelva a intentarlo.");
                } else {
                    try {
                        rpt = Consulta_RUC_Serv1.consultarServicioRUC(txtNumeroDocumento.getText(), txtNombresCompleto, txtDireccion);
                    } catch (Exception ex) {
                        rpt = "NO";
                    }
                }
            } else if (tipoDoc.equalsIgnoreCase("DNI")) {
                int longiDoc = 8;
                if (longitud > longiDoc) {
                    Metodos.MensajeAlerta("El número de documento no es válido. Vuelva a intentarlo.");
                } else {
                    try {
                        rpt = Consulta_DNI_Serv1.consultarServicioDNI(txtNumeroDocumento.getText(), txtNombresCompleto);
                    } catch (Exception ex) {
                        rpt = "NO";
                    }
                }
            }
        } else {
            rpt = "NO";
            AlertaInfo alert = new AlertaInfo("Mensaje", "Verifique el Documento de Identidad Ingresado");
            alert.setVisible(true);
        }
        return rpt;
    }
}
