/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.consulta_datos.serv;

import factura_free.clases.principales.Datos;
import factura_free.vista.alertas.AlertaError; 
import java.io.IOException;
import javax.swing.JTextField;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author SERVIDOR
 */
public class Consulta_RUC_Serv1 {

    MediaType mediaType = MediaType.get("application/json; charset=utf-8");
    public static OkHttpClient client = new OkHttpClient();

    public static String consultarServicioRUC(String nroruc, JTextField txtNombreR, JTextField txtdireccion) throws Exception {
        String valor = "NO";
        try {
            String response = post(Datos.getConsulta_RUC_Serv1(nroruc));
            if (response == null) {
                valor = "NO";
            } else {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(response);
                try {
                    JSONObject jsonObject = (JSONObject) obj;
                    String estado = (String) jsonObject.get("estado");
                    String condicion = (String) jsonObject.get("condicion");

                    if (estado.equalsIgnoreCase("ACTIVO")) {
                        if (condicion.equalsIgnoreCase("HABIDO")) {
                            String ruc = (String) jsonObject.get("ruc");
                            String razon_social = (String) jsonObject.get("razon_social");
                            String direccion = (String) jsonObject.get("direccion");
//                            String ubigeo = (String) jsonObject.get("ubigeo");
                            String departamento = (String) jsonObject.get("departamento");
                            String provincia = (String) jsonObject.get("provincia");
                            String distrito = (String) jsonObject.get("distrito");

                            if (ruc.length() != 0 && razon_social.length() != 0) {
                                System.err.println("razon_social: " + razon_social);
                                System.err.println("direccion: " + direccion);

                                String direccion_completa = "-";
                                if (direccion.equalsIgnoreCase("-") || direccion.equalsIgnoreCase("") || direccion.equalsIgnoreCase(" ") || direccion.equalsIgnoreCase("null") || direccion == null) {
                                    direccion_completa = direccion;
                                } else {
                                    direccion_completa = direccion + " - " + distrito + " - " + provincia + " - " + departamento;
                                }

                                txtNombreR.setText(razon_social.toUpperCase().replace(",", "").replace("\\u00C1", "Á").replace("\\u00C9", "É").replace("\\u00cd", "Í").replace("\\u00d3", "Ó").replace("\\u00da", "Ú"));
                                txtdireccion.setText(direccion_completa.replace(",", "").replace("\\u00C1", "Á").replace("\\u00C9", "É").replace("\\u00cd", "Í").replace("\\u00d3", "Ó").replace("\\u00da", "Ú"));
                                valor = "SI";
                            } else {
                                valor = "NO";
                            }
                        } else {
                            valor = "NO";
                            AlertaError alert = new AlertaError("Mensaje", "El RUC consultado se encuentra como NO HABIDO");
                            alert.setVisible(true);
                        }
                    } else {
                        valor = "NO";
                        AlertaError alert = new AlertaError("Mensaje", "El RUC consultado se encuentra INACTIVO");
                        alert.setVisible(true);
                    }

                } catch (Exception e) {
                    valor = "NO";
                }
            }
        } catch (Exception e) {
            valor = "NO";
        }
        return valor;
    }

    public static String post(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

}
