/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.consulta_datos.serv;

import factura_free.clases.principales.Datos;
import java.io.IOException;
import javax.swing.JTextField;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author SERVIDOR
 */
public class Consulta_DNI_Serv2 {

    MediaType mediaType = MediaType.get("application/json; charset=utf-8");
    public static OkHttpClient client = new OkHttpClient();

    public static String consultarServicioDNI(String nrodni, JTextField txtNombre) throws Exception {
        String valor = "NO";
        try {
            String response = post(Datos.getConsulta_DNI_Serv2(nrodni));
            if (response == null) {
                valor = "NO";
            } else {
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(response);
                try {
                    JSONObject jsonObject = (JSONObject) obj;
                    String nombre = (String) jsonObject.get("nombre");
                    System.err.println("nombre: " + nombre);
                    if (nombre.length() != 0) {
                        txtNombre.setText(nombre.replace(",", "").replace("\\u00C1", "Á").replace("\\u00C9", "É").replace("\\u00cd", "Í").replace("\\u00d3", "Ó").replace("\\u00da", "Ú"));
                        valor = "SI";
                    } else {
                        valor = "NO";
                    }
                } catch (Exception e) {
                    valor = "NO";
                }
            }
        } catch (Exception e) {
            valor = "NO";
        }
        return valor;
    }

    public static String post(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
