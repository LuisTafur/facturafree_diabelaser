/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales;
 
import java.sql.Connection; 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;

/**
 *
 * @author IngenieroLuisTafurgm
 */
public class SQLconsultasProcedure {
 
    
    /**
     * Consultas*
     */ 
    public boolean ejecutaConsultas(String consulta) {
        Conectar cc = new Conectar();
        try {
            CallableStatement st = cc.getConexion().prepareCall(consulta);
            st.close(); 
            cc.getConexion().close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error en la sentencia Insertar www" + ex.getMessage());
            return false;
        }
    }
    
    /**
     * Graba*
     */ 
    public void grabarTabla(String sql) {
        Connection con = null;
        Conectar cc = new Conectar();
        CallableStatement pst = null;
        try {  
            //RECIBE EL PARAMETRO DE LA SENTENCIA SQL
            con = cc.getConexion(); 
            pst = con.prepareCall(sql);
            pst.executeUpdate(sql);
            pst.close(); 
            con.close();
        } catch (Exception ex) {
            System.out.println("Error en la sentencia Insertar 1111111111" + ex.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
    }
 

    /**
     * Actualiza*
     */
    public boolean actualizaTabla(String sql) {
        Connection con = null;
        Conectar cc = new Conectar();
        CallableStatement pst = null;
        try {
            con = cc.getConexion(); 
            pst = con.prepareCall(sql);
            pst.executeUpdate(sql);
            pst.close(); 
            cc.getConexion().close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error en la conexión ACTUALIZAR " + ex);
            return false;
        }
    }

    public int obtenerEntero(String sql) {
        Connection con = null;
        CallableStatement pst = null;
        int valor = 0;
        Conectar cc = new Conectar();

        try { 
            con = cc.getConexion();
            pst = con.prepareCall(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                valor = rs.getInt(1);
            }
            rs.close(); 
            pst.close(); 
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_entero " + e.getMessage());
        } finally {
            try { 
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return valor;
    }

    public String obtenerCadena(String sql) {
        Connection con = null;
        Conectar cc = new Conectar();
        CallableStatement pst = null;
        String cadena = null;

        try { 
            con = cc.getConexion();
            pst = con.prepareCall(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                cadena = rs.getString(1);
            }
            rs.close(); 
            pst.close(); 
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_cadena " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return cadena;
    }

    public double obtenerDouble(String sql) {
        Connection con = null;
        CallableStatement pst = null;
        double valor = 0.0;
        Conectar cc = new Conectar();

        try { 
            con = cc.getConexion();
            pst = con.prepareCall(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) { // mientras aya informacion almacenarl en el listado
                valor = rs.getDouble(1);
            }
            rs.close(); 
            pst.close(); 
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en obtener_double " + e.getMessage());
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return valor;
    }
 
}
