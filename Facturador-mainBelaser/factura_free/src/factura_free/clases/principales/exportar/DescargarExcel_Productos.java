/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.exportar;

import factura_free.clases.controlador.ProductoImpl; 
import factura_free.clases.modelo.ObProducto; 
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author USER
 */
public class DescargarExcel_Productos {

    public static ArrayList<ObProducto> lista_ObProducto = null;

    public static void crearExcel() {
        Workbook book = new XSSFWorkbook();
        //titulo
        Sheet sheet = book.createSheet("LISTADO DE PRODUCTOS");
        Row row1Boleta = sheet.createRow(0);
        row1Boleta.createCell(0).setCellValue("#");
        row1Boleta.createCell(1).setCellValue("CODIGO");
        row1Boleta.createCell(2).setCellValue("DESCRIPCIÓN");
        row1Boleta.createCell(3).setCellValue("PRECIO");
        row1Boleta.createCell(4).setCellValue("INCLUYE IGV");
        row1Boleta.createCell(5).setCellValue("TIPO");
        row1Boleta.createCell(6).setCellValue("AFECTACIÓN VENTA");
        row1Boleta.createCell(7).setCellValue("UNIDAD DE MEDIDA");
        row1Boleta.createCell(8).setCellValue("TIENE IMPUESTO A LA BOLSA");
        row1Boleta.createCell(9).setCellValue("TIENE STOCK");
        row1Boleta.createCell(9).setCellValue("STOCK ACTUAL");
        row1Boleta.createCell(9).setCellValue("AFECTACIÓN COMPRA");
        row1Boleta.createCell(9).setCellValue("ESTADO");

        int numFila = 0;
        lista_ObProducto = (ArrayList<ObProducto>) ProductoImpl.listarProductos("Select * from producto");
        for (int i = 0; i < lista_ObProducto.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObProducto.get(i).getId());
            rowMat.createCell(1).setCellValue(lista_ObProducto.get(i).getCodigo());
            rowMat.createCell(2).setCellValue(lista_ObProducto.get(i).getDescripcion());
            rowMat.createCell(3).setCellValue(lista_ObProducto.get(i).getPrecio());
            rowMat.createCell(4).setCellValue(lista_ObProducto.get(i).getIncluye_igv());
            rowMat.createCell(5).setCellValue(lista_ObProducto.get(i).getTipo());
            rowMat.createCell(6).setCellValue(lista_ObProducto.get(i).getAfectacion());
            rowMat.createCell(7).setCellValue(lista_ObProducto.get(i).getUnidad_medida());
            rowMat.createCell(8).setCellValue(lista_ObProducto.get(i).getIcbper());
            rowMat.createCell(9).setCellValue(lista_ObProducto.get(i).getTiene_stock());
            rowMat.createCell(10).setCellValue(lista_ObProducto.get(i).getStock_actual());
            rowMat.createCell(11).setCellValue(lista_ObProducto.get(i).getAfectacion_compra());
            rowMat.createCell(12).setCellValue(lista_ObProducto.get(i).getEstado());
        }

        File directorioQr = new File("C:\\FACTURAFREE\\archivos\\excel");
        if (!directorioQr.exists()) {
            directorioQr.mkdir();
        }

        Metodos.MensajeInformacion("Reporte generado de forma exitosa.");
        try {
            String rutaCont = "C:\\FACTURAFREE\\archivos\\excel\\Productos.xlsx";
            if (new File(rutaCont).exists()) {
                new File(rutaCont).delete();
            }
            FileOutputStream fileout = new FileOutputStream(rutaCont);
            book.write(fileout);
            fileout.close();

            ///abrir excel
            File archivo = new File(rutaCont);
            Desktop.getDesktop().open(archivo);

        } catch (IOException ex) {
            Metodos.MensajeError("TIENE UN REPORTE APERTURADO. CIÉRRELO SI DESEA GENERAR UNO NUEVO");
            System.err.println("Error de generación: " + ex.getMessage());
        }
    }
}
