/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.exportar;

import factura_free.clases.controlador.Venta_internaImpl;
import factura_free.clases.modelo.ObVenta_internaView;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author USER
 */
public class DescargarExcel_Ventas_internas {

    public static ArrayList<ObVenta_internaView> lista_ObVenta_internaView = null;

    public static void crearExcel(String fecha_inicial, String fecha_final, int idCliente, String cliente) {
        Workbook book = new XSSFWorkbook();
        //titulo
        Sheet sheet = book.createSheet("REPORTE DE NOTAS DE VENTA");

        Row row1Cabecera = sheet.createRow(0);
        row1Cabecera.createCell(0).setCellValue("RAZÓN SOCIAL");
        row1Cabecera.createCell(1).setCellValue(Datos.getRazonSocial());
        Row row1CabeceraCliente = sheet.createRow(1);
        row1CabeceraCliente.createCell(0).setCellValue("CLIENTE:");
        if (idCliente != 0) {
            row1CabeceraCliente.createCell(1).setCellValue(cliente.toUpperCase());
        } else {
            row1CabeceraCliente.createCell(1).setCellValue("TODOS");
        }

        Row row1Boleta = sheet.createRow(3);
        row1Boleta.createCell(0).setCellValue("COMPROBANTE");
        row1Boleta.createCell(1).setCellValue("FEC. EMISIÓN");
        row1Boleta.createCell(2).setCellValue("DOCUMENTO");
        row1Boleta.createCell(3).setCellValue("CLIENTE");
        row1Boleta.createCell(4).setCellValue("MONEDA");
        row1Boleta.createCell(5).setCellValue("TOTAL GRAVADO");
        row1Boleta.createCell(6).setCellValue("TOTAL EXONERADO");
        row1Boleta.createCell(7).setCellValue("TOTAL GRATUITO");
        row1Boleta.createCell(8).setCellValue("TOTAL IGV");
        row1Boleta.createCell(9).setCellValue("IMPORTE TOTAL");
        row1Boleta.createCell(10).setCellValue("TOTAL EFECTIVO");
        row1Boleta.createCell(11).setCellValue("TOTAL PAG.DIVERSO");
        row1Boleta.createCell(12).setCellValue("FORMA DE PAGO");
        row1Boleta.createCell(13).setCellValue("MOTIVO DE ANULACIÓN");
        row1Boleta.createCell(14).setCellValue("ESTADO");

        double importeTotalVenta_internaSoles = 0;
        double importeTotalVenta_internaDolares = 0;

        String ConsultaVenta_interna = "";
        if (idCliente == 0) {
            ConsultaVenta_interna = "Select * from Venta_interna_view where fecha_emision>='" + fecha_inicial + "' and fecha_emision<='" + fecha_final + "'";
        } else {
            ConsultaVenta_interna = "Select * from Venta_interna_view where id_cliente='" + idCliente + "' and fecha_emision>='" + fecha_inicial + "' and fecha_emision<='" + fecha_final + "'";
        }

        int numFila = 3;
        lista_ObVenta_internaView = (ArrayList<ObVenta_internaView>) Venta_internaImpl.listarVenta_internas(ConsultaVenta_interna);
        for (int i = 0; i < lista_ObVenta_internaView.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObVenta_internaView.get(i).getId());
            rowMat.createCell(1).setCellValue(lista_ObVenta_internaView.get(i).getFecha());
            if (lista_ObVenta_internaView.get(i).getTipoDocumento_cliente().equalsIgnoreCase("SIN DOCUMENTO")) {
                rowMat.createCell(2).setCellValue("00000000");
            } else {
                rowMat.createCell(2).setCellValue(lista_ObVenta_internaView.get(i).getNumeroDocumento_cliente());
            }
            rowMat.createCell(3).setCellValue(lista_ObVenta_internaView.get(i).getCliente());
            rowMat.createCell(4).setCellValue(lista_ObVenta_internaView.get(i).getMoneda());
            rowMat.createCell(5).setCellValue(lista_ObVenta_internaView.get(i).getTotalVentasGravadas());
            rowMat.createCell(6).setCellValue(lista_ObVenta_internaView.get(i).getTotalExonerado());
            rowMat.createCell(7).setCellValue(lista_ObVenta_internaView.get(i).getTotalGratuito());
            rowMat.createCell(8).setCellValue(lista_ObVenta_internaView.get(i).getIgv());

            if (lista_ObVenta_internaView.get(i).getMotivo_anulacion().length() == 0) {
                if (lista_ObVenta_internaView.get(i).getMoneda().equalsIgnoreCase("PEN")) {
                    importeTotalVenta_internaSoles = importeTotalVenta_internaSoles + lista_ObVenta_internaView.get(i).getImporteTotal();
                } else {
                    importeTotalVenta_internaDolares = importeTotalVenta_internaDolares + lista_ObVenta_internaView.get(i).getImporteTotal();
                }
            }

            rowMat.createCell(9).setCellValue(lista_ObVenta_internaView.get(i).getImporteTotal());
            rowMat.createCell(10).setCellValue(lista_ObVenta_internaView.get(i).getMontoEfectivoVenta());
            rowMat.createCell(11).setCellValue(lista_ObVenta_internaView.get(i).getMontoTarjetaVenta());
            rowMat.createCell(12).setCellValue(lista_ObVenta_internaView.get(i).getDescripcionTarjetaVenta());
            rowMat.createCell(13).setCellValue(lista_ObVenta_internaView.get(i).getMotivo_anulacion());
            if (lista_ObVenta_internaView.get(i).getMotivo_anulacion().length() == 0) {
                if (lista_ObVenta_internaView.get(i).getComprobante_relacionado().length() == 0) {
                    rowMat.createCell(14).setCellValue("ACTIVO");
                } else {
                    rowMat.createCell(14).setCellValue("PROCESADO - (" + lista_ObVenta_internaView.get(i).getComprobante_relacionado() + ")");
                }
            } else {
                rowMat.createCell(14).setCellValue("ANULADO");
            }
        }

        numFila = numFila + 1;
        Row rowMatTotVenta_internaSoles = sheet.createRow(numFila);
        rowMatTotVenta_internaSoles.createCell(8).setCellValue("TOTAL NOTAS DE VENTA SOLES:");
        rowMatTotVenta_internaSoles.createCell(9).setCellValue(importeTotalVenta_internaSoles);

        numFila = numFila + 1;
        Row rowMatTotVenta_internaDolares = sheet.createRow(numFila);
        rowMatTotVenta_internaDolares.createCell(8).setCellValue("TOTAL NOTAS DE VENTA DOLARES:");
        rowMatTotVenta_internaDolares.createCell(9).setCellValue(importeTotalVenta_internaDolares);

        File directorioQr = new File("C:\\FACTURAFREE\\archivos\\excel");
        if (!directorioQr.exists()) {
            directorioQr.mkdir();
        }

        Metodos.MensajeInformacion("Reporte generado de forma exitosa.");
        try {
            String rutaCont = "C:\\FACTURAFREE\\archivos\\excel\\Ventas_Internas_desde_" + fecha_inicial + "_hasta_" + fecha_final + ".xlsx";
            if (new File(rutaCont).exists()) {
                new File(rutaCont).delete();
            }
            FileOutputStream fileout = new FileOutputStream(rutaCont);
            book.write(fileout);
            fileout.close();

            ///abrir excel
            File archivo = new File(rutaCont);
            Desktop.getDesktop().open(archivo);

        } catch (IOException ex) {
            Metodos.MensajeError("TIENE UN REPORTE APERTURADO. CIÉRRELO SI DESEA GENERAR UNO NUEVO");
            System.err.println("Error de generación: " + ex.getMessage());
        }
    }
}
