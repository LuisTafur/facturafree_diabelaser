/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.exportar;

import factura_free.clases.controlador.ClienteImpl; 
import factura_free.clases.modelo.ObCliente; 
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author USER
 */
public class DescargarExcel_Clientes {

    public static ArrayList<ObCliente> lista_ObCliente = null;

    public static void crearExcel() {
        Workbook book = new XSSFWorkbook();
        //titulo
        Sheet sheet = book.createSheet("LISTADO DE CLIENTES");
        Row row1Boleta = sheet.createRow(0);
        row1Boleta.createCell(0).setCellValue("#");
        row1Boleta.createCell(1).setCellValue("TIPO DOC.");
        row1Boleta.createCell(2).setCellValue("DOCUMENTO");
        row1Boleta.createCell(3).setCellValue("CLIENTE");
        row1Boleta.createCell(4).setCellValue("DIRECCIÓN");
        row1Boleta.createCell(5).setCellValue("CORREO");
        row1Boleta.createCell(6).setCellValue("ESTADO");

        int numFila = 0;
        lista_ObCliente = (ArrayList<ObCliente>) ClienteImpl.listarClientes("Select * from cliente");
        for (int i = 0; i < lista_ObCliente.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObCliente.get(i).getId());
            rowMat.createCell(1).setCellValue(lista_ObCliente.get(i).getTipoDocumento());
            if (lista_ObCliente.get(i).getTipoDocumento().equalsIgnoreCase("SIN DOCUMENTO")) {
                rowMat.createCell(2).setCellValue("00000000");
            } else {
                rowMat.createCell(2).setCellValue(lista_ObCliente.get(i).getNumeroDocumento());
            }
            rowMat.createCell(3).setCellValue(lista_ObCliente.get(i).getNombreRazonSocial());
            rowMat.createCell(4).setCellValue(lista_ObCliente.get(i).getDireccion());
            rowMat.createCell(5).setCellValue(lista_ObCliente.get(i).getCorreo_envios());
            rowMat.createCell(6).setCellValue(lista_ObCliente.get(i).getEstado());
        }

        File directorioQr = new File("C:\\FACTURAFREE\\archivos\\excel");
        if (!directorioQr.exists()) {
            directorioQr.mkdir();
        }

        Metodos.MensajeInformacion("Reporte generado de forma exitosa.");
        try {
            String rutaCont = "C:\\FACTURAFREE\\archivos\\excel\\Clientes.xlsx";
            if (new File(rutaCont).exists()) {
                new File(rutaCont).delete();
            }
            FileOutputStream fileout = new FileOutputStream(rutaCont);
            book.write(fileout);
            fileout.close();

            ///abrir excel
            File archivo = new File(rutaCont);
            Desktop.getDesktop().open(archivo);

        } catch (IOException ex) {
            Metodos.MensajeError("TIENE UN REPORTE APERTURADO. CIÉRRELO SI DESEA GENERAR UNO NUEVO");
            System.err.println("Error de generación: " + ex.getMessage());
        }
    }
}
