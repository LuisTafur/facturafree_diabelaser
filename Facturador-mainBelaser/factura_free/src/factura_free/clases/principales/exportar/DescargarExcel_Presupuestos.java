/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.exportar;

import factura_free.clases.controlador.PresupuestoImpl;
import factura_free.clases.modelo.ObPresupuestoView;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author USER
 */
public class DescargarExcel_Presupuestos {

    public static ArrayList<ObPresupuestoView> lista_ObPresupuestoView = null;
    public static ArrayList<ObPresupuestoView> lista_ObPresupuestoViewPorVencer = null;

    public static void crearExcel(String fecha_inicial, String fecha_final, int idCliente, String cliente) {
        Workbook book = new XSSFWorkbook();
        //titulo
        Sheet sheet = book.createSheet("REPORTE DE PRESUPUESTOS");

        Row row1Cabecera = sheet.createRow(0);
        row1Cabecera.createCell(0).setCellValue("RAZÓN SOCIAL");
        row1Cabecera.createCell(1).setCellValue(Datos.getRazonSocial());
        Row row1CabeceraCliente = sheet.createRow(1);
        row1CabeceraCliente.createCell(0).setCellValue("CLIENTE:");
        if (idCliente != 0) {
            row1CabeceraCliente.createCell(1).setCellValue(cliente.toUpperCase());
        } else {
            row1CabeceraCliente.createCell(1).setCellValue("TODOS");
        }

        Row row1Boleta = sheet.createRow(3);
        row1Boleta.createCell(0).setCellValue("COMPROBANTE");
        row1Boleta.createCell(1).setCellValue("FEC. EMISIÓN");
        row1Boleta.createCell(2).setCellValue("FEC. VENCIMIENTO");
        row1Boleta.createCell(3).setCellValue("DOCUMENTO");
        row1Boleta.createCell(4).setCellValue("CLIENTE");
        row1Boleta.createCell(5).setCellValue("MONEDA");
        row1Boleta.createCell(6).setCellValue("IMPORTE TOTAL");
        row1Boleta.createCell(7).setCellValue("MOTIVO DE ANULACIÓN");
        row1Boleta.createCell(8).setCellValue("ESTADO");

        int numFila = 3;
        numFila = numFila + 1;
        Row rowMatPresupuestosGeneral = sheet.createRow(numFila);
        rowMatPresupuestosGeneral.createCell(0).setCellValue("LISTADO GENERAL");
        String ConsultaPresupuestos = "";
        if (idCliente == 0) {
            ConsultaPresupuestos = "Select * from Presupuesto_view where fecha_emision>='" + fecha_inicial + "' and fecha_emision<='" + fecha_final + "'";
        } else {
            ConsultaPresupuestos = "Select * from Presupuesto_view where id_cliente='" + idCliente + "' and fecha_emision>='" + fecha_inicial + "' and fecha_emision<='" + fecha_final + "'";
        }

        lista_ObPresupuestoView = (ArrayList<ObPresupuestoView>) PresupuestoImpl.listarPresupuestos(ConsultaPresupuestos);
        for (int i = 0; i < lista_ObPresupuestoView.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObPresupuestoView.get(i).getId());
            rowMat.createCell(1).setCellValue(lista_ObPresupuestoView.get(i).getFecha());
            rowMat.createCell(2).setCellValue(lista_ObPresupuestoView.get(i).getFechaVencimiento());
            if (lista_ObPresupuestoView.get(i).getTipoDocumento_cliente().equalsIgnoreCase("SIN DOCUMENTO")) {
                rowMat.createCell(3).setCellValue("00000000");
            } else {
                rowMat.createCell(3).setCellValue(lista_ObPresupuestoView.get(i).getNumeroDocumento_cliente());
            }
            rowMat.createCell(4).setCellValue(lista_ObPresupuestoView.get(i).getCliente());
            rowMat.createCell(5).setCellValue(lista_ObPresupuestoView.get(i).getMoneda());
            rowMat.createCell(6).setCellValue(lista_ObPresupuestoView.get(i).getImporteTotal());
            rowMat.createCell(7).setCellValue(lista_ObPresupuestoView.get(i).getMotivo_anulacion());
            if (lista_ObPresupuestoView.get(i).getMotivo_anulacion().length() == 0) {
                if (lista_ObPresupuestoView.get(i).getComprobante_relacionado().length() == 0) {
                    rowMat.createCell(8).setCellValue("ACTIVO");
                } else {
                    rowMat.createCell(8).setCellValue("PROCESADO - (" + lista_ObPresupuestoView.get(i).getComprobante_relacionado() + ")");
                }
            } else {
                rowMat.createCell(8).setCellValue("ANULADO");
            }
        }

        numFila = numFila + 2;
        Row rowMatPresupuestosPorVencer = sheet.createRow(numFila);
        rowMatPresupuestosPorVencer.createCell(0).setCellValue("LISTADO DE PRESUPUESTOS POR VENCER");
        String ConsultaPresupuestosVencer = "";
        if (idCliente == 0) {
            ConsultaPresupuestosVencer = "Select * from Presupuesto_view where fecha_vencimiento>='" + fecha_inicial + "' and fecha_vencimiento<='" + fecha_final + "'";
        } else {
            ConsultaPresupuestosVencer = "Select * from Presupuesto_view where id_cliente='" + idCliente + "' and fecha_vencimiento>='" + fecha_inicial + "' and fecha_vencimiento<='" + fecha_final + "'";
        }

        lista_ObPresupuestoViewPorVencer = (ArrayList<ObPresupuestoView>) PresupuestoImpl.listarPresupuestos(ConsultaPresupuestosVencer);
        for (int i = 0; i < lista_ObPresupuestoViewPorVencer.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getId());
            rowMat.createCell(1).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getFecha());
            rowMat.createCell(2).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getFechaVencimiento());
            if (lista_ObPresupuestoViewPorVencer.get(i).getTipoDocumento_cliente().equalsIgnoreCase("SIN DOCUMENTO")) {
                rowMat.createCell(3).setCellValue("00000000");
            } else {
                rowMat.createCell(3).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getNumeroDocumento_cliente());
            }
            rowMat.createCell(4).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getCliente());
            rowMat.createCell(5).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getMoneda());
            rowMat.createCell(6).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getImporteTotal());
            rowMat.createCell(7).setCellValue(lista_ObPresupuestoViewPorVencer.get(i).getMotivo_anulacion());
            if (lista_ObPresupuestoViewPorVencer.get(i).getMotivo_anulacion().length() == 0) {
                rowMat.createCell(8).setCellValue("");
            } else {
                rowMat.createCell(8).setCellValue("ANULADO");
            }
        }

        File directorioQr = new File("C:\\FACTURAFREE\\archivos\\excel");
        if (!directorioQr.exists()) {
            directorioQr.mkdir();
        }

        Metodos.MensajeInformacion("Reporte generado dforma exitose a.");
        try {
            String rutaCont = "C:\\FACTURAFREE\\archivos\\excel\\Presupuestos_desde_" + fecha_inicial + "_hasta_" + fecha_final + ".xlsx";
            if (new File(rutaCont).exists()) {
                new File(rutaCont).delete();
            }
            FileOutputStream fileout = new FileOutputStream(rutaCont);
            book.write(fileout);
            fileout.close();

            ///abrir excel
            File archivo = new File(rutaCont);
            Desktop.getDesktop().open(archivo);

        } catch (IOException ex) {
            Metodos.MensajeError("TIENE UN REPORTE APERTURADO. CIÉRRELO SI DESEA GENERAR UNO NUEVO");
            System.err.println("Error de generación: " + ex.getMessage());
        }
    }
}
