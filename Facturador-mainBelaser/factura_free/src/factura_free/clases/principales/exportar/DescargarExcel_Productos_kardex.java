/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.principales.exportar;

import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto_kardex;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author USER
 */
public class DescargarExcel_Productos_kardex {

    public static ArrayList<ObProducto_kardex> lista_ObProducto_kardex = null;

    public static void crearExcel(int id_producto, String producto, String fechaInicial, String fechaFinal) {
        Workbook book = new XSSFWorkbook();
        //titulo
        Sheet sheet = book.createSheet("LISTADO DE KARDEX POR PRODUCTO");

        Row row1Cabecera = sheet.createRow(0);
        row1Cabecera.createCell(0).setCellValue("RAZÓN SOCIAL");
        row1Cabecera.createCell(1).setCellValue(Datos.getRazonSocial());
        Row row1CabeceraCliente = sheet.createRow(1);
        row1CabeceraCliente.createCell(0).setCellValue("PRODUCTO:");
        row1CabeceraCliente.createCell(1).setCellValue(producto.toUpperCase());

        Row row1Boleta = sheet.createRow(3);
        row1Boleta.createCell(0).setCellValue("FECHA");
        row1Boleta.createCell(1).setCellValue("ENTRADA");
        row1Boleta.createCell(2).setCellValue("SALIDA");
        row1Boleta.createCell(3).setCellValue("DOCUMENTO");
        row1Boleta.createCell(4).setCellValue("TIPO");
        row1Boleta.createCell(5).setCellValue("USUARIO");

        int numFila = 3;
        String consulta = "select * from producto_kardex where fecha>='" + fechaInicial + "' and fecha<='" + fechaFinal + "' "
                + " and id_producto='" + id_producto + "' order by fecha;";
        lista_ObProducto_kardex = (ArrayList<ObProducto_kardex>) ProductoImpl.listarProductos_kardex(consulta);
        for (int i = 0; i < lista_ObProducto_kardex.size(); i++) {
            numFila++;
            Row rowMat = sheet.createRow(numFila);
            rowMat.createCell(0).setCellValue(lista_ObProducto_kardex.get(i).getFecha() + " / " + lista_ObProducto_kardex.get(i).getHora());
            rowMat.createCell(1).setCellValue(lista_ObProducto_kardex.get(i).getCnt_entrada());
            rowMat.createCell(2).setCellValue(lista_ObProducto_kardex.get(i).getCnt_salida());
            rowMat.createCell(3).setCellValue(lista_ObProducto_kardex.get(i).getDoc_relacionado());
            rowMat.createCell(4).setCellValue(lista_ObProducto_kardex.get(i).getTipo());
            rowMat.createCell(5).setCellValue(lista_ObProducto_kardex.get(i).getUsuario());
        }

        File directorioQr = new File("C:\\FACTURAFREE\\archivos\\excel");
        if (!directorioQr.exists()) {
            directorioQr.mkdir();
        }

        Metodos.MensajeInformacion("Reporte generado de forma exitosa.");
        try {
            String rutaCont = "C:\\FACTURAFREE\\archivos\\excel\\" + producto + "_kardex.xlsx";
            if (new File(rutaCont).exists()) {
                new File(rutaCont).delete();
            }
            FileOutputStream fileout = new FileOutputStream(rutaCont);
            book.write(fileout);
            fileout.close();

            ///abrir excel
            File archivo = new File(rutaCont);
            Desktop.getDesktop().open(archivo);

        } catch (IOException ex) {
            Metodos.MensajeError("TIENE UN REPORTE APERTURADO. CIÉRRELO SI DESEA GENERAR UNO NUEVO");
            System.err.println("Error de generación: " + ex.getMessage());
        }
    }
}
