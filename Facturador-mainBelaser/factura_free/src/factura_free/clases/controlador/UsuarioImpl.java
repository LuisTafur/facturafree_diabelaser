package factura_free.clases.controlador;

import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.modelo.ObUsuario;
import factura_free.clases.principales.alternos.EncriparDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioImpl extends SQLconsultas {

    public static ObUsuario usuario;

    public static void Registrar(ObUsuario obj) throws SQLException {
        String sql = "insert into usuario (usuario, contrasena, rol, estado) "
                + "values ('" + EncriparDatos.Encriptar(obj.getUsuario()) + "','" + EncriparDatos.Encriptar(obj.getContrasena()) + "','" + obj.getRol() + "','" + obj.getEstado() + "');";
        grabarTabla(sql);
        Metodos.MensajeInformacion("Usuario Registrado Exitosamente.");
    }

    public static void Actualizar(ObUsuario obj) throws SQLException {
        String sql = "update usuario set usuario = '" + EncriparDatos.Encriptar(obj.getUsuario()) + "', contrasena = '" + EncriparDatos.Encriptar(obj.getContrasena()) + "', "
                + "rol = '" + obj.getRol() + "', estado = '" + obj.getEstado() + "' where id = '" + obj.getId() + "';";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Usuario Actualizado Exitosamente.");
    }

    public static void eliminar(int id) {
        String sql = "UPDATE usuario SET estado='INACTIVO' WHERE id='" + id + "'";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Usuario Eliminado Exitosamente.");
    }

    public static List<ObUsuario> listarUsuarios(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObUsuario> listaArray = new ArrayList<ObUsuario>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String usuarios = rs.getString(2);
                String contrasena = rs.getString(3);
                String rol = rs.getString(4);
                String estado = rs.getString(5);
                listaArray.add(new ObUsuario(id, EncriparDatos.Desencriptar(usuarios), contrasena, rol, estado));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObUsuario" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static ObUsuario buscarUsuario(int id_usuario) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement("select * from usuario where id='" + id_usuario + "' ");

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String usuarios = rs.getString(2);
                String contrasena = rs.getString(3);
                String rol = rs.getString(4);
                String estado = rs.getString(5);
                usuario = new ObUsuario(id, EncriparDatos.Desencriptar(usuarios), contrasena, rol, estado);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en buscar_por_id " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return usuario;
    }

    public static ObUsuario getUsuario() {
        return usuario;
    }

    public static void setUsuario(ObUsuario usuario) {
        UsuarioImpl.usuario = usuario;
    }

    public static boolean validaInicioSesion(String usuario, String password) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            String sql = "select * from usuario where "
                    + " usuario='" + EncriparDatos.Encriptar(usuario) + "' and "
                    + " contrasena='" + EncriparDatos.Encriptar(password) + "' "
                    + " and estado='ACTIVO'";
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {//recorre haver si SI HUVO REGISTRO con ese ID Y ESA CLAVE 
                return true;   //retorna lo q encontro en la BD
            }
            rs.close();
        } catch (Exception e) {
            System.out.println("Error en la conexión " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return false;
    }
 
}
