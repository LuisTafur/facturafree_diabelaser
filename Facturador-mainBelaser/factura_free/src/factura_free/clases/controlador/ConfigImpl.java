/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.controlador;

import factura_free.clases.modelo.ObConfig;
import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.principales.alternos.EncriparDatos;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.alternos.Rutas;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author USER-1
 */
public class ConfigImpl extends SQLconsultas {

    public static ObConfig config;

    public static ObConfig buscarConfig(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String ruc = rs.getString(2);
                String razonSocial = rs.getString(3);
                String direccion = rs.getString(4);
                String telefono = rs.getString(5);
                String correo = rs.getString(6);
                String web = rs.getString(7);
                String impresion = rs.getString(8); 
                String usa_cnt_flotante = rs.getString(9);
                String producto_igv = rs.getString(10);
                String serv_datos = rs.getString(11);
                String correo_aplicacion = rs.getString(12);
                String pass_aplicacion = rs.getString(13); 
                String logo = rs.getString(14); 
                String email_contador = rs.getString(15);
                String seriePresupuesto = rs.getString(16);
                String serieVenta_interna = rs.getString(17);   
                String serieCompra = rs.getString(18); 
                String impre_directa = rs.getString(19);
                config = new ObConfig(id, ruc, razonSocial, direccion, telefono, correo, web, impresion, usa_cnt_flotante, producto_igv, EncriparDatos.Desencriptar(serv_datos),
                        EncriparDatos.Desencriptar(correo_aplicacion), EncriparDatos.Desencriptar(pass_aplicacion), logo, email_contador, seriePresupuesto,
                        serieVenta_interna, serieCompra, impre_directa);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en buscar_por_id " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return config;
    }

    public static void actualizarFoto(String logo) {
        String consulta = "UPDATE  config  SET  logo ='" + logo + "' WHERE id ='1'";
        actualizaTabla(consulta);
    }

    public static void Registrar(ObConfig obj) throws SQLException {
        String sql = "insert into config (ruc, razonSocial, direccion, telefono, correo, web, impresion, usa_cnt_flotante, producto_igv, serv_datos, correo_aplicacion, "
                + "pass_aplicacion,logo,email_contador, seriePresupuesto, serieVenta_interna,serieCompra,impre_directa) "
                + "values ('" + obj.getRuc() + "','" + obj.getRazonSocial() + "','" + obj.getDireccion() + "','" + obj.getTelefono() + "','" + obj.getCorreo() + "',"
                + "'" + obj.getWeb() + "','" + obj.getImpresion() + "',"
                + "'" + obj.getUsa_cnt_flotante() + "','" + obj.getProducto_igv() + "','" + obj.getServ_datos() + "','" + EncriparDatos.Encriptar(obj.getCorreo_aplicacion()) + "',"
                + "'" + EncriparDatos.Encriptar(obj.getPass_aplicacion()) + "','" + obj.getLogo() + "',"
                + "'" + obj.getEmail_contador() + "','" + obj.getSeriePresupuesto() + "','" + obj.getSerieVenta_interna() + "','" + obj.getSerieCompra() + "',"
                + "'" + obj.getImpre_directa()+ "');";
        grabarTabla(sql);
    }

    public static void Actualizar(ObConfig obj) {
        String sql = "update config set ruc = '" + obj.getRuc() + "', razonSocial = '" + Metodos.validarCamposIniciales(obj.getRazonSocial()) + "',"
                + " direccion = '" + Metodos.validarCamposIniciales(obj.getDireccion()) + "', usa_cnt_flotante = '" + obj.getUsa_cnt_flotante() + "', "
                + " producto_igv = '" + obj.getProducto_igv() + "', serv_datos = '" + EncriparDatos.Encriptar(obj.getServ_datos()) + "', "
                + " correo_aplicacion = '" + EncriparDatos.Encriptar(obj.getCorreo_aplicacion()) + "', "
                + " pass_aplicacion = '" + EncriparDatos.Encriptar(obj.getPass_aplicacion()) + "', "
                + " logo = '" + obj.getLogo() + "', email_contador = '" + obj.getEmail_contador() + "',"
                + " seriePresupuesto = '" + obj.getSeriePresupuesto() + "', serieVenta_interna = '" + obj.getSerieVenta_interna() + "',"
                + " serieCompra = '" + obj.getSerieCompra() + "', impre_directa = '" + obj.getImpre_directa() + "' "
                + " where id = '" + obj.getId() + "';";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Config Actualizado Exitosamente.");
    }

    public static void ActualizarENV(String host, String puerto, String baseDatos,
            String usuario, String contrasena, String ubicacion) {
        try {
            FileOutputStream fos = new FileOutputStream(Rutas.getRutaConexionBD());
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            bw.write("host=" + host + "\n"
                    + "puerto=" + puerto + "\n"
                    + "baseDatos=" + baseDatos + "\n"
                    + "usuario=" + usuario + "\n"
                    + "contrasena=" + contrasena + "\n"
                    + "ubicacion=" + ubicacion + "\n");

            bw.close();
        } catch (Exception e) {
            Metodos.MensajeError("Error: " + e);
        }
    }

    public static ObConfig getConfig() {
        return config;
    }

    public static void setConfig(ObConfig config) {
        ConfigImpl.config = config;
    }

    public static void actualizarDatosConfig(String tabla, String id, String campo_actualizar, String dato_actualizar) {
        String sql = " update " + tabla + " set " + campo_actualizar + " = '" + dato_actualizar + "' where id = '" + id + "';";
        actualizaTabla(sql);
    }

    public static String obtenerDatosConfiguracion(String id, String campo) {
        String datos = obtenerCadena("Select " + campo + " config where id = '" + id + "';");
        return datos;
    }
}
