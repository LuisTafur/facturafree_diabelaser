package factura_free.clases.controlador;

import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.modelo.ObCliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteImpl extends SQLconsultas {

    public static ObCliente cliente;

    public static void Registrar(ObCliente obj) throws SQLException {
        String direccion = obj.getDireccion();
        String correo_envios = obj.getCorreo_envios();
        if (direccion.equalsIgnoreCase("")) {
            direccion = "-";
        }
        if (correo_envios.equalsIgnoreCase("")) {
            correo_envios = "-";
        }
        String sql = "insert into cliente (tipoDocumento,numeroDocumento, nombreRazonSocial,direccion, correo_envios, estado) "
                + "values ('" + obj.getTipoDocumento() + "','" + obj.getNumeroDocumento() + "', '" + Metodos.validarCamposIniciales(obj.getNombreRazonSocial()) + "',"
                + "'" + Metodos.validarCamposIniciales(direccion) + "','" + correo_envios + "','" + obj.getEstado() + "');";
        grabarTabla(sql);
        Metodos.MensajeInformacion("Cliente Registrado Exitosamente.");
    }

    public static void Actualizar(ObCliente obj) throws SQLException {
        String direccion = obj.getDireccion();
        String correo_envios = obj.getCorreo_envios();
        if (direccion.equalsIgnoreCase("")) {
            direccion = "-";
        } 
        if (correo_envios.equalsIgnoreCase("")) {
            correo_envios = "-";
        }
        String sql = "update cliente set tipoDocumento = '" + obj.getTipoDocumento() + "', numeroDocumento = '" + obj.getNumeroDocumento() + "', "
                + "nombreRazonSocial = '" + Metodos.validarCamposIniciales(obj.getNombreRazonSocial()) + "', direccion = '" + Metodos.validarCamposIniciales(direccion) + "', "
                + "correo_envios = '" + correo_envios + "', estado = '" + obj.getEstado() + "' where id = '" + obj.getId() + "';";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Cliente Actualizado Exitosamente.");
    }

    public static void eliminar(int id) {
        String sql = "UPDATE cliente SET estado='INACTIVO' WHERE id='" + id + "'";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Cliente Eliminado Exitosamente.");
    }

    public static void actualizarCorreo(int id, String correo_envios) {
        String sql = "UPDATE cliente SET correo_envios='" + correo_envios + "' WHERE id='" + id + "'";
        actualizaTabla(sql);
    }

    public static void actualizarDireccion(int id, String direccion) {
        String sql = "UPDATE cliente SET direccion='" + direccion + "' WHERE id='" + id + "'";
        actualizaTabla(sql);
    }

    public static String obtenerDatosCliente(String campoSeleccionar, String campoBuscar, String datoIgualFiltro) {
        String dato_cliente = obtenerCadena("Select " + campoSeleccionar + " from cliente where " + campoBuscar + "='" + datoIgualFiltro + "'");
        return dato_cliente;
    }

    public static List<ObCliente> listarClientes(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObCliente> listaArray = new ArrayList<ObCliente>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String tipoDocumento = rs.getString(2);
                String numeroDocumento = rs.getString(3);
                String nombreRazonSocial = rs.getString(4);
                String direccion = rs.getString(5);
                String correo_envios = rs.getString(6);
                String estado = rs.getString(7);
                listaArray.add(new ObCliente(id, tipoDocumento, numeroDocumento, nombreRazonSocial, direccion, correo_envios, estado));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObCliente" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static ObCliente buscarCliente(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String tipoDocumento = rs.getString(2);
                String numeroDocumento = rs.getString(3);
                String nombreRazonSocial = rs.getString(4);
                String direccion = rs.getString(5);
                String correo_envios = rs.getString(6);
                String estado = rs.getString(7);
                cliente = new ObCliente(id, tipoDocumento, numeroDocumento, nombreRazonSocial, direccion, correo_envios, estado);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en buscar_por_id " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return cliente;
    }

    public static ObCliente getCliente() {
        return cliente;
    }

    public static void setCliente(ObCliente cliente) {
        ClienteImpl.cliente = cliente;
    }

}
