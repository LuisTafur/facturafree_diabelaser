package factura_free.clases.controlador;

import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.modelo.ObProducto;
import factura_free.clases.modelo.ObProducto_kardex; 
import factura_free.clases.principales.alternos.Fecha_Date; 
import factura_free.vista.principal.JFramePrincipal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductoImpl extends SQLconsultas {

    public static ObProducto producto;
    public static java.util.Timer timerProductoOnline = null;
 

    public static void Registrar(ObProducto obj) throws SQLException {
        String sql = "insert into producto (codigo, descripcion, precio, incluye_igv, tipo, afectacion, unidad_medida, estado, icbper, subir_online, tiene_stock,"
                + " stock_actual, afectacion_compra, precio_compra) "
                + "values ('" + obj.getCodigo() + "','" + Metodos.validarCamposIniciales(obj.getDescripcion()) + "','" + obj.getPrecio() + "','" + obj.getIncluye_igv() + "','" + obj.getTipo() + "',"
                + "'" + obj.getAfectacion() + "','" + obj.getUnidad_medida() + "','" + obj.getEstado() + "','" + obj.getIcbper() + "','NO','" + obj.getTiene_stock() + "',"
                + "'" + obj.getStock_actual() + "','" + obj.getAfectacion_compra() + "','" + obj.getPrecio_compra() + "');";
        grabarTabla(sql);
        Metodos.MensajeInformacion("Producto Registrado Exitosamente."); 
    }

    public static void Registrar_Kardex(int id_producto, double cnt_entrada, double cnt_salida, String doc_relacionado, String tipo, String fecha) throws SQLException {
        String tiene_stock = obtenerCadena("select tiene_stock from producto where id='" + id_producto + "'");
        if (tiene_stock.equalsIgnoreCase("SI")) {
            String sql = "insert into producto_kardex (id_producto, fecha, hora, cnt_entrada, cnt_salida, doc_relacionado, tipo, usuario) "
                    + "values ('" + id_producto + "','" + fecha + "','" + Fecha_Date.muestrahoradelsistema() + "','" + cnt_entrada + "',"
                    + "'" + cnt_salida + "', '" + doc_relacionado + "', '" + tipo + "', '" + JFramePrincipal.usuario + "');";
            grabarTabla(sql);
            obtenerStockActual(id_producto);
        }
    }

    public static void eliminar_Kardex(String doc_relacionado) {
        String sql = "DELETE FROM producto_kardex WHERE doc_relacionado='" + doc_relacionado + "'";
        actualizaTabla(sql);
    }

    public static void obtenerStockActual(int id_producto) {
        double stockActual = obtenerDouble("Select sum(cnt_entrada-cnt_salida) from producto_kardex where id_producto='" + id_producto + "'");
        actualizarCampoProducto(id_producto, "stock_actual", ("" + stockActual), 0);
    }

    public static List<ObProducto_kardex> listarProductos_kardex(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObProducto_kardex> listaArray = new ArrayList<ObProducto_kardex>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id_producto_kardex = rs.getInt(1);
                int id_producto = rs.getInt(2);
                String fecha = rs.getString(3);
                String hora = rs.getString(4);
                double cnt_entrada = rs.getDouble(5);
                double cnt_salida = rs.getDouble(6);
                String doc_relacionado = rs.getString(7);
                String tipo = rs.getString(8);
                String usuario = rs.getString(9);
                listaArray.add(new ObProducto_kardex(id_producto_kardex, id_producto, fecha, hora, cnt_entrada, cnt_salida, doc_relacionado, tipo, usuario));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObProducto_kardex" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static String obtenerTieneICBPER(int id) {
        String icbper = obtenerCadena("Select icbper from producto where id='" + id + "'");
        return icbper;
    }

    public static void Actualizar(ObProducto obj) throws SQLException {
        String sql = "update producto set codigo = '" + obj.getCodigo() + "', descripcion = '" + Metodos.validarCamposIniciales(obj.getDescripcion()) + "', precio = '" + obj.getPrecio() + "',  "
                + " incluye_igv = '" + obj.getIncluye_igv() + "', tipo = '" + obj.getTipo() + "', afectacion = '" + obj.getAfectacion() + "', "
                + " unidad_medida = '" + obj.getUnidad_medida() + "', estado = '" + obj.getEstado() + "', icbper = '" + obj.getIcbper() + "',subir_online='NO',"
                + " tiene_stock = '" + obj.getTiene_stock() + "', stock_actual = '" + obj.getStock_actual() + "', afectacion_compra = '" + obj.getAfectacion_compra() + "',"
                + " precio_compra = '" + obj.getPrecio_compra() + "' "
                + " where id = '" + obj.getId() + "';";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Producto Actualizado Exitosamente."); 
    }

    public static void actualizarCampoProducto(int id, String campo, String valorCampo, int verMsm) {
        String sql = "UPDATE producto SET " + campo + "='" + valorCampo + "' WHERE id='" + id + "'";
        actualizaTabla(sql);
        if (verMsm == 1) {
            Metodos.MensajeInformacion("Producto Editado Exitosamente.");
        }
    }

    public static void actualizarTodosCampoProducto(String campo, String valorCampo) {
        String sql = "UPDATE producto SET " + campo + "='" + valorCampo + "'";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Producto Editado Exitosamente.");
    }

    public static void eliminar(int id) {
        String sql = "UPDATE producto SET estado='INACTIVO' WHERE id='" + id + "'";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Producto Eliminado Exitosamente."); 
    }

    public static int obtenerCantidadId_codigo(String codigo) {
        int id_producto = obtenerEntero("Select count(id) from producto where codigo='" + codigo + "' and estado='ACTIVO'");
        return id_producto;
    }

    public static int obtenerId_Descripcion(String descripcion) {
        int id_producto = obtenerEntero("Select id from producto where descripcion='" + descripcion + "' and estado='ACTIVO'");
        return id_producto;
    }

    public static int obtenerCnt_penSubir_Online_Producto() {
        int id_producto = obtenerEntero("Select count(id) from producto where subir_online='NO' and estado='ACTIVO';");
        return id_producto;
    }

    public static int obtenerMin_penSubir_Online_Producto() {
        int id_producto = obtenerEntero("Select min(id) from producto where subir_online='NO' and estado='ACTIVO'");
        return id_producto;
    }

    public static int obtenerId_codigo(String codigo) {
        int id_producto = obtenerEntero("Select id from producto where codigo='" + codigo + "' and estado='ACTIVO'");
        return id_producto;
    }

    public static int obtenerIncluye_igvID(int id) {
        int incluye_igv = obtenerEntero("Select incluye_igv from producto where id='" + id + "' and estado='ACTIVO'");
        return incluye_igv;
    }

    public static List<ObProducto> listarProductos(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObProducto> listaArray = new ArrayList<ObProducto>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String codigo = rs.getString(2);
                String descripcion = rs.getString(3);
                double precio = rs.getDouble(4);
                int incluye_igv = rs.getInt(5);
                String tipo = rs.getString(6);
                String afectacion = rs.getString(7);
                String unidad_medida = rs.getString(8);
                String estado = rs.getString(9);
                String icbper = rs.getString(10);
                String subir_online = rs.getString(11);
                String tiene_stock = rs.getString(12);
                double stock_actual = rs.getDouble(13);
                String afectacion_compra = rs.getString(14);
                double precio_compra = rs.getDouble(15);
                listaArray.add(new ObProducto(id, codigo, descripcion, precio, incluye_igv, tipo, afectacion, unidad_medida, estado, icbper, subir_online,
                        tiene_stock, stock_actual, afectacion_compra, precio_compra));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObProducto" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static ObProducto buscarProducto(int id_producto) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement("select * from producto where id='" + id_producto + "' ");

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                int id = rs.getInt(1);
                String codigo = rs.getString(2);
                String descripcion = rs.getString(3);
                double precio = rs.getDouble(4);
                int incluye_igv = rs.getInt(5);
                String tipo = rs.getString(6);
                String afectacion = rs.getString(7);
                String unidad_medida = rs.getString(8);
                String estado = rs.getString(9);
                String icbper = rs.getString(10);
                String subir_online = rs.getString(11);
                String tiene_stock = rs.getString(12);
                double stock_actual = rs.getDouble(13);
                String afectacion_compra = rs.getString(14);
                double precio_compra = rs.getDouble(15);
                producto = new ObProducto(id, codigo, Metodos.validarCamposIniciales(descripcion), precio, incluye_igv, tipo, afectacion, unidad_medida, estado,
                        icbper, subir_online, tiene_stock, stock_actual, afectacion_compra, precio_compra);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en buscar_por_id " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return producto;
    }

    public static ObProducto getProducto() {
        return producto;
    }

    public static void setProducto(ObProducto producto) {
        ProductoImpl.producto = producto;
    }

}
