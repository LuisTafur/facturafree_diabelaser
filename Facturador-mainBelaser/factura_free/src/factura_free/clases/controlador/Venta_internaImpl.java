package factura_free.clases.controlador;

import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.modelo.ObVenta_interna;
import factura_free.clases.modelo.ObVenta_internaView;
import factura_free.clases.modelo.ObVenta_interna_det;
import factura_free.clases.principales.Catalogos;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.GenerarQR;
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Rutas;
import factura_free.vista.modales.impresion.Fview_comprobante;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.view.JRViewer;

public class Venta_internaImpl extends SQLconsultas {

    public static ObVenta_internaView venta_interna;

    public static void Registrar(ObVenta_interna obj) throws SQLException {
        String sql = "insert into venta_interna (id, idCliente, fecha, horaEmision, fechaVencimiento, moneda, medioPago, totalVentasGravadas, totalGratuito, totalExonerado, "
                + "igv, importeTotal,  motivo_anulacion , estado_comprobante, comprobante_relacionado, id_usuario, montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, "
                + "montoDeudaVenta, descripcionTarjetaVenta) "
                + "values ('" + obj.getId() + "','" + obj.getIdCliente() + "','" + obj.getFecha() + "','" + obj.getHoraEmision() + "','" + obj.getFechaVencimiento() + "',"
                + "'" + obj.getMoneda() + "','" + obj.getMedioPago() + "','" + obj.getTotalVentasGravadas() + "','" + obj.getTotalGratuito() + "','" + obj.getTotalExonerado() + "',"
                + "'" + obj.getIgv() + "','" + obj.getImporteTotal() + "', '" + obj.getMotivo_anulacion() + "', '" + obj.getEstado_comprobante() + "',"
                + "'" + obj.getComprobante_relacionado() + "','" + obj.getId_usuario() + "','" + obj.getMontoEfectivoVenta() + "','" + obj.getMontoTarjetaVenta() + "',"
                + "'" + obj.getMontoVueltoVenta() + "','" + obj.getMontoDeudaVenta() + "','" + obj.getDescripcionTarjetaVenta() + "');";
        grabarTabla(sql);
        Metodos.MensajeInformacion("Venta_interna Registrado Exitosamente.");
    }

    public static void Actualizar(ObVenta_interna obj) throws SQLException {
        String sql = "update venta_interna set idCliente = '" + obj.getIdCliente() + "', fecha = '" + obj.getFecha() + "', horaEmision = '" + obj.getHoraEmision() + "',  "
                + " fechaVencimiento = '" + obj.getFechaVencimiento() + "', moneda = '" + obj.getMoneda() + "', medioPago = '" + obj.getMedioPago() + "', "
                + " totalVentasGravadas = '" + obj.getTotalVentasGravadas() + "', totalGratuito = '" + obj.getTotalGratuito() + "', igv = '" + obj.getIgv() + "',"
                + " totalExonerado = '" + obj.getTotalExonerado() + "', importeTotal = '" + obj.getImporteTotal() + "',  motivo_anulacion= '" + obj.getMotivo_anulacion() + "', "
                + " id_usuario= '" + obj.getId_usuario() + "', montoEfectivoVenta= '" + obj.getMontoEfectivoVenta() + "', montoTarjetaVenta= '" + obj.getMontoTarjetaVenta() + "', "
                + " montoVueltoVenta= '" + obj.getMontoVueltoVenta() + "', montoDeudaVenta= '" + obj.getMontoDeudaVenta() + "', descripcionTarjetaVenta= '" + obj.getDescripcionTarjetaVenta() + "'"
                + " where id = '" + obj.getId() + "';";
        actualizaTabla(sql);
        Metodos.MensajeInformacion("Venta_interna Actualizado Exitosamente.");
    }

    public static List<ObVenta_internaView> listarVenta_internas(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObVenta_internaView> listaArray = new ArrayList<ObVenta_internaView>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                String id = rs.getString(1);
                int id_cliente = rs.getInt(2);
                String tipoDocumento_cliente = rs.getString(3);
                String numeroDocumento_cliente = rs.getString(4);
                String cliente = rs.getString(5);
                String direccion_cliente = rs.getString(6);
                String correo_envios = rs.getString(7);
                String fecha = rs.getString(8);
                String horaEmision = rs.getString(9);
                String fechaVencimiento = rs.getString(10);
                String moneda = rs.getString(11);
                String medioPago = rs.getString(12);
                double totalVentasGravadas = rs.getDouble(13);
                double totalGratuito = rs.getDouble(14);
                double totalExonerado = rs.getDouble(15);
                double igv = rs.getDouble(16);
                double importeTotal = rs.getDouble(17);
                String motivo_anulacion = rs.getString(18);
                String estado_comprobante = rs.getString(19);
                String comprobante_relacionado = rs.getString(20);
                int id_usuario = rs.getInt(21);

                double montoEfectivoVenta = rs.getDouble(22);
                double montoTarjetaVenta = rs.getDouble(23);
                double montoVueltoVenta = rs.getDouble(24);
                double montoDeudaVenta = rs.getDouble(25);
                String descripcionTarjetaVenta = rs.getString(26);
                listaArray.add(new ObVenta_internaView(id, id_cliente, tipoDocumento_cliente, numeroDocumento_cliente, cliente, direccion_cliente, correo_envios, fecha,
                        horaEmision, fechaVencimiento, moneda, medioPago, totalVentasGravadas, totalGratuito, totalExonerado, igv, importeTotal, motivo_anulacion,
                        estado_comprobante, comprobante_relacionado, id_usuario, montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta,
                        descripcionTarjetaVenta));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObVenta_interna" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static void registrarVenta_internaDet(ObVenta_interna_det obj) throws SQLException {
        String sql = "insert into venta_internadet (id, idVenta_interna, item, cantidad, tipoUnidad, codigo, descripcion, tributo, montoTributo, tipoAfectacionTributo, valorUnitario,"
                + " precioUnitarioItem,valorUnitarioGratuito, precio) "
                + "values ('" + obj.getId() + "','" + obj.getIdVenta_interna() + "','" + obj.getItem() + "','" + obj.getCantidad() + "','" + obj.getTipoUnidad() + "',"
                + "'" + obj.getCodigo() + "','" + Metodos.validarCamposIniciales(obj.getDescripcion()) + "','" + obj.getTributo() + "','" + obj.getMontoTributo() + "','" + obj.getTipoAfectacionTributo() + "',"
                + "'" + obj.getValorUnitario() + "', '" + obj.getPrecioUnitarioItem() + "', '" + obj.getValorUnitarioGratuito() + "',"
                + "'" + obj.getPrecio() + "');";
        grabarTabla(sql);
    }

    public static void eliminarVenta_internaDet(String idVenta_interna) {
        String eliminarDocumento = "delete from venta_internadet where idVenta_interna='" + idVenta_interna + "'";
        actualizaTabla(eliminarDocumento);
    }

    public static String obtenerDatosVenta_int(String campoSeleccionar, String campoBuscar, String datoIgualFiltro) {
        String dato_venta_interna = obtenerCadena("Select " + campoSeleccionar + " from venta_interna where " + campoBuscar + "='" + datoIgualFiltro + "'");
        return dato_venta_interna;
    }

    public static void actualizarDatosVenta_int(String campoActualizar, String datoActualizar, String id) {
        String sql = "UPDATE venta_interna SET " + campoActualizar + "='" + datoActualizar + "' WHERE id='" + id + "'";
        actualizaTabla(sql);
    }

    public static void anularVenta_internas_Det(String id) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement("select * from venta_internadet where idVenta_interna='" + id + "'");

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                String idVenta_interna = rs.getString(2);
                int item = rs.getInt(3);
                double cantidad = rs.getDouble(4);

                ////KARDEX 
                ProductoImpl.Registrar_Kardex(item, cantidad, 0, idVenta_interna, "ANULA NOTA DE VENTA", Fecha_Date.retorna_fecha_sistema());
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar anularVenta_internas_Det" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
    }

    public static List<ObVenta_interna_det> listarVenta_internas_Det(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        ArrayList<ObVenta_interna_det> listaArray = new ArrayList<ObVenta_interna_det>();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);

            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                String id = rs.getString(1);
                String idVenta_interna = rs.getString(2);
                int item = rs.getInt(3);
                double cantidad = rs.getDouble(4);
                String tipoUnidad = rs.getString(5);
                String codigo = rs.getString(6);
                String descripcion = rs.getString(7);
                String tributo = rs.getString(8);
                double montoTributo = rs.getDouble(9);
                String tipoAfectacionTributo = rs.getString(10);
                double valorUnitario = rs.getDouble(11);
                double precioUnitarioItem = rs.getDouble(12);
                double valorUnitarioGratuito = rs.getDouble(13);
                double precio = rs.getDouble(14);
                listaArray.add(new ObVenta_interna_det(id, idVenta_interna, item, cantidad, tipoUnidad, codigo, descripcion, tributo, montoTributo, tipoAfectacionTributo, valorUnitario,
                        precioUnitarioItem, valorUnitarioGratuito, precio));
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en listar ObVenta_interna_det" + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return listaArray;
    }

    public static ObVenta_internaView buscarVenta_interna(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();//si hay consulta 
            while (rs.next()) {
                String id = rs.getString(1);
                int id_cliente = rs.getInt(2);
                String tipoDocumento_cliente = rs.getString(3);
                String numeroDocumento_cliente = rs.getString(4);
                String cliente = rs.getString(5);
                String direccion_cliente = rs.getString(6);
                String correo_envios = rs.getString(7);
                String fecha = rs.getString(8);
                String horaEmision = rs.getString(9);
                String fechaVencimiento = rs.getString(10);
                String moneda = rs.getString(11);
                String medioPago = rs.getString(12);
                double totalVentasGravadas = rs.getDouble(13);
                double totalGratuito = rs.getDouble(14);
                double totalExonerado = rs.getDouble(15);
                double igv = rs.getDouble(16);
                double importeTotal = rs.getDouble(17);
                String motivo_anulacion = rs.getString(18);
                String estado_comprobante = rs.getString(19);
                String comprobante_relacionado = rs.getString(20);
                int id_usuario = rs.getInt(21);

                double montoEfectivoVenta = rs.getDouble(22);
                double montoTarjetaVenta = rs.getDouble(23);
                double montoVueltoVenta = rs.getDouble(24);
                double montoDeudaVenta = rs.getDouble(25);
                String descripcionTarjetaVenta = rs.getString(26);
                venta_interna = new ObVenta_internaView(id, id_cliente, tipoDocumento_cliente, numeroDocumento_cliente, cliente, direccion_cliente, correo_envios, fecha, horaEmision,
                        fechaVencimiento, moneda, medioPago, totalVentasGravadas, totalGratuito, totalExonerado, igv, importeTotal, motivo_anulacion, estado_comprobante,
                        comprobante_relacionado, id_usuario, montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta, descripcionTarjetaVenta);
            }
            rs.close();
            pst.close();
            con.close();
        } catch (SQLException e) {
            System.out.println("Error en buscar_por_id " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }
        return venta_interna;
    }

    public static ObVenta_internaView getVenta_interna() {
        return venta_interna;
    }

    public static void setVenta_interna(ObVenta_internaView venta_interna) {
        Venta_internaImpl.venta_interna = venta_interna;
    }

    public static void generarReporteVenta_interna(String id, String formato, int opcion) {
        // enviar a reporte
        String numeroComprobante = id;
        String tipoDocumento = "";
        String numeroDocumento = "";
        String nombreRazonSocial = "";
        String direccion = "";
        String correo_cliente = "";
        String fecha = "";
        String hora = "";
        String fechaVencimiento = "";
        String moneda = "";
        // totales
        String totalVentasGravadas = "";
        String totalGratuito = "";
        String totalExonerado = "";
        String igv = "";
        String importeTotal = "";
        // otros
        String montoEnTexto = "";
        // totales vuelto/deuda
        String montoEfectivoVenta = "";
        String montoTarjetaVenta = "";
        String vuelto_deuda = "";
        String monto_vuelto_deuda = "";
        String descripcionTarjetaVenta = "";
        // ========= Datos de nuestra empresa =============
        String ruc = Datos.getRUC();
        String razonSocial = Datos.getRazonSocial();
        String direccionLegal = Datos.getDireccion();
        String telefono = Datos.getTelefono();
        String correo = Datos.getCorreo();
        String web = Datos.getWeb();
        String simbolo = "S/.";
        // ================================================
        String datosCabecera = direccionLegal + "\n"
                + "Teléfono: " + telefono + "\n"
                + "Correo: " + correo + "\n"
                + "Web: " + web + "\n";
        String nombre_archivo = ruc + "-" + id;
        String qr = Rutas.getRutaQR() + nombre_archivo + ".jpg";
        String logo = "C:\\FACTURAFREE\\archivos\\Logo\\" + Rutas.getLogo();
        try {
            buscarVenta_interna("select * from venta_interna_view where id_venta_interna = '" + id + "' ;");
            tipoDocumento = getVenta_interna().getTipoDocumento_cliente();
            if (tipoDocumento.equals("SIN DOCUMENTO")) {
                tipoDocumento = "NDC:";
            } else if (tipoDocumento.equals("Carnet de extranjería")) {
                tipoDocumento = "CARNET EX:";
            } else if (tipoDocumento.equals("Pasaporte")) {
                tipoDocumento = "PASS:";
            } else {
                tipoDocumento = tipoDocumento + ":";
            }
            numeroDocumento = getVenta_interna().getNumeroDocumento_cliente();
            nombreRazonSocial = getVenta_interna().getCliente();
            direccion = getVenta_interna().getDireccion_cliente();
            correo_cliente = getVenta_interna().getCorreo_envios();
            fecha = getVenta_interna().getFecha();
            hora = getVenta_interna().getHoraEmision();
            fechaVencimiento = getVenta_interna().getFechaVencimiento();
            moneda = getVenta_interna().getMoneda();
            if (moneda.equals("PEN")) {
                simbolo = "S/ ";
            } else {
                simbolo = "US$ ";
            }

            totalVentasGravadas = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getTotalVentasGravadas());
            totalGratuito = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getTotalGratuito());
            totalExonerado = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getTotalExonerado());
            igv = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getIgv());
            importeTotal = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getImporteTotal());

            montoEfectivoVenta = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getMontoEfectivoVenta());
            montoTarjetaVenta = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getMontoTarjetaVenta());
            descripcionTarjetaVenta = getVenta_interna().getDescripcionTarjetaVenta();
            // enviamos el monto en texto
            montoEnTexto = Metodos.convertirNumTexto(String.valueOf(Metodos.formatoDecimalMostrar(getVenta_interna().getImporteTotal())), moneda);

        } catch (Exception e) {
        }
        //parámetros de envio al reporte
        Map parametro = new HashMap();
        parametro.put("numeroComprobante", numeroComprobante);
        parametro.put("tipoDocumento", tipoDocumento);
        parametro.put("numeroDocumento", numeroDocumento);
        parametro.put("nombreRazonSocial", nombreRazonSocial);
        parametro.put("direccion", direccion);
        parametro.put("fecha", fecha);
        parametro.put("fechaVencimiento", fechaVencimiento);

        parametro.put("totalVentasGravadas", dosDecimaes(totalVentasGravadas));
        parametro.put("totalGratuito", dosDecimaes(totalGratuito));
        parametro.put("totalExonerado", dosDecimaes(totalExonerado));
        parametro.put("igv", dosDecimaes(igv));
        parametro.put("importeTotal", dosDecimaes(importeTotal));

        parametro.put("montoEfectivoVenta", dosDecimaes(montoEfectivoVenta));
        parametro.put("montoTarjetaVenta", dosDecimaes(montoTarjetaVenta));
        if (getVenta_interna().getMontoVueltoVenta() > 0 && getVenta_interna().getMontoDeudaVenta() == 0) {
            monto_vuelto_deuda = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getMontoVueltoVenta());
            vuelto_deuda = "VUELTO:";
        } else if (getVenta_interna().getMontoDeudaVenta() > 0 && getVenta_interna().getMontoVueltoVenta() == 0) {
            monto_vuelto_deuda = simbolo + Metodos.formatoDecimalMostrar(getVenta_interna().getMontoDeudaVenta());
            vuelto_deuda = "DEUDA:";
        } else if (getVenta_interna().getMontoDeudaVenta() == 0 && getVenta_interna().getMontoVueltoVenta() == 0) {
            monto_vuelto_deuda = simbolo + "0.00";
            vuelto_deuda = "VUELTO:";
        }

        parametro.put("monto_vuelto_deuda", dosDecimaes(monto_vuelto_deuda));
        parametro.put("vuelto_deuda", vuelto_deuda);
        parametro.put("descripcionTarjetaVenta", descripcionTarjetaVenta);

        parametro.put("montoEnTexto", montoEnTexto);
        parametro.put("ruc", ruc);
        parametro.put("razonSocial", razonSocial);
        parametro.put("datosCabecera", datosCabecera);
        parametro.put("logo", logo);

        if (formato.equalsIgnoreCase("Ticket 80 mm")) {
            parametro.put("telefono", telefono);
            parametro.put("web", web);
            parametro.put("horaEmision", hora);
            parametro.put("direccionLegal", direccionLegal);
        }
        // parametro para buscar el detalle
        parametro.put("id", id);
        try {
            Connection con = null;
            Conectar conectar = new Conectar();
            con = conectar.getConexion();

            /////VERIFICAMOS EL FORMATO DESEADO
            JasperPrint jp = null;
            if (formato.equalsIgnoreCase("Ticket 80 mm")) {
                jp = JasperFillManager.fillReport("C:\\FACTURAFREE\\repo\\TICKET\\Venta_interna\\Venta_interna-Ticket80mm.jasper", parametro, con);
            } else if (formato.equalsIgnoreCase("A4")) {
                jp = JasperFillManager.fillReport("C:\\FACTURAFREE\\repo\\A4\\Venta_interna\\Venta_interna-A4.jasper", parametro, con);
            }

            con.close();
            /////IMPRIME EN IMPRESORA POR DEFECTO
            if (opcion == 0) {
                System.err.println("imprimió");
                JasperPrintManager.printReport(jp, false);
            } else if (opcion == 1) {
                //APLICAR UNA PREVISUALIZACIÓN DEL REPORTE
                Fview_comprobante view = new Fview_comprobante(null, true);

                JRViewer jRViewer = new JRViewer(jp);
                view.PanelView.removeAll();
                //para el tamaño de l reporte se agrega un BorderLayout
                view.PanelView.setLayout(new BorderLayout());
                view.PanelView.add(jRViewer, BorderLayout.CENTER);
                //SE CREA EL ZOOM
                if (formato.equalsIgnoreCase("Ticket 80 mm")) {
                    jRViewer.setZoomRatio((float) 1.5);
                } else {
                    jRViewer.setZoomRatio((float) 0.6);
                }
                jRViewer.setVisible(true);
                //SE AGREGA EL REPORTE AL PANEL 
                view.PanelView.repaint();
                view.PanelView.revalidate();
                view.setVisible(true);
                view.setLocationRelativeTo(null);

            } else if (opcion == 2) {
                ///SELECCIONA UNA IMPRESORA (TRUE -para ver la ventana de seleccion de impresoras)
                JasperPrintManager.printReport(jp, true);

            } else if (opcion == 3) {
                ///CREAMOS UNA COPIA EN EL ESCRITORIO Y ABRIMOS SU UBICACION  
                String rutaRepo = Rutas.getRutaComporbantePDF(nombre_archivo);
                JasperExportManager.exportReportToPdfFile(jp, rutaRepo);
                File filses = new File(rutaRepo);
                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.open(filses);
                } catch (IOException ex) {
                    System.err.println("ERROR EXPORTAR: " + ex.getMessage());
                }

            } else if (opcion == 4) {
                if (correo_cliente.equalsIgnoreCase("") || correo_cliente.equalsIgnoreCase("-") || correo_cliente.equalsIgnoreCase("--")) {
                    Metodos.MensajeError("Asigne un correo válido para poder realizar el envío Solicitado");
                    return;
                }
                /////GESTIONAMOS EL ENVÍO DE CORREO AL CLIENTE DEL ARCHIVO PDF
                String rutaArchivo = Rutas.getRutaComporbantePDF(nombre_archivo);
                JasperExportManager.exportReportToPdfFile(jp, rutaArchivo);

                File archivoPDF = new File(rutaArchivo);
                if (archivoPDF.exists()) {
                    String mensajeCoti = nombreRazonSocial + " - Gracias por su Preferencia " + id;
                    ComprobantesImpl.Enviar_CorreoGeneral(rutaArchivo, (nombre_archivo + ".pdf"), correo_cliente, (razonSocial + " NOTA DE VENTA - " + id), mensajeCoti);
                    archivoPDF.delete();
                }
            }

            File arcQR = new File(qr);
            if (arcQR.exists()) {
                arcQR.delete();
            }
        } catch (Exception e) {
            System.out.println("Error creando PDF: " + id + " " + e);
            Metodos.MensajeError("Error creando PDF: " + id + " " + e);
        }
    }

    public static String dosDecimaes(String valor) {
        String[] parts_valor = valor.split("\\.");
        String valorDecimal = parts_valor[1];
        if (valorDecimal.length() == 1) {
            int valorInt = Integer.parseInt(valorDecimal);
            if (valorInt <= 9) {
                valorDecimal = valorDecimal + "0";
            }
        }
        return parts_valor[0] + "." + valorDecimal;
    }
}
