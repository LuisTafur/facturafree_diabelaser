/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.clases.controlador;

import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.SQLconsultas;
import factura_free.clases.principales.alternos.EnviarCorreos;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.vista.principal.JFramePrincipal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author USER-1
 */
public class ComprobantesImpl extends SQLconsultas {


    public static void comprobantes_impresion(String tipoDocumento, String id_documento, String papel, int valor, String fecha_emision) { 
        if (tipoDocumento.equalsIgnoreCase("Presupuesto")) {
            PresupuestoImpl.generarReportePresupuesto(id_documento, papel, valor);
        } else if (tipoDocumento.equalsIgnoreCase("Venta_interna")) {
            Venta_internaImpl.generarReporteVenta_interna(id_documento, papel, valor);
        } else if (tipoDocumento.equalsIgnoreCase("Compra")) {
            CompraImpl.generarReporteCompra(id_documento, papel, valor);
        }
    }

    public static String obtenerProcesoProducto() {
        String procesProducto = obtenerCadena("Select procesProducto from config;");
        return procesProducto;
    }

    public static String obtenerProcesoSincronizacion() {
        String process_upload = obtenerCadena("Select process_upload from config;");
        return process_upload;
    }


    public static boolean existeComprobante(String sql) {
        Connection con = null;
        PreparedStatement pst = null;
        Conectar conectar = new Conectar();
        try {
            con = conectar.getConexion();
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                return true;
            }
            rs.close();
            pst.close();
            con.close();
        } catch (Exception e) {
            System.out.println("Error en la conexión " + e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                System.out.println("Error al cerrar ");
            }
        }

        return false;
    }
     

    public static void Enviar_CorreoGeneral(String archivo_adjunto, String nom_archivo_adjunto, String para_destino_cliente, String asunto_envio,
            String contenido_mensaje) {
        String pass_aplicacion = JFramePrincipal.pass_aplicacion.getText();
        String correo_aplicacion = JFramePrincipal.correo_aplicacion.getText();

        EnviarCorreos e = new EnviarCorreos(correo_aplicacion, pass_aplicacion, archivo_adjunto,
                nom_archivo_adjunto, para_destino_cliente, asunto_envio, contenido_mensaje);

        if (e.sendMail().equalsIgnoreCase("SI")) {
            Metodos.MensajeInformacion("Envio de Correo Exitoso");
        } else {
            JOptionPane.showMessageDialog(null, "Revise su conexion a Internet y su Configuración de Correo.");
        }
    }
}
