/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 */
package factura_free.vista.modales;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.vista.alertas.AlertaError;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Pago_Venta_int extends javax.swing.JDialog {

    Fecha_Date fec = new Fecha_Date();
    java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Time Time = new java.sql.Time(lnMilisegundos);

    public static int respuesta = 0;
    public static double montoEfectivo = 0, montoTarjeta, montoVuelto = 0, montoDeuda = 0;
    public static String descripcionTarjeta = "SOLO EFECTIVO";

    public Pago_Venta_int(java.awt.Frame parent, boolean modal, String documento, Date fecha, double total) {
        super(parent, modal);
        initComponents();

        respuesta = 0;
        montoEfectivo = 0;
        montoTarjeta = 0;
        montoVuelto = 0;
        montoDeuda = 0;

        txtFecha.setDate(fecha);
        txtDocumento.setText(documento);
        txtMontoTotal.setText("" + total);
        txtMontoEfectivo.setText("" + total);

        this.setLocationRelativeTo(null);
        this.setTitle("PAGO DE VENTAS");

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());

        txtMontoEfectivo.requestFocus();
//        fec.capturaymuestrahoradelsistema(txtFecha);

        txtMontoEfectivo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resultado();
            }
        });

        txtMontoPagoTarjeta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resultado();
            }
        });

        KeyDeCampos();
    }

    public void KeyDeCampos() {
        jPanel1.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        btnRegistrar.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtMontoDeuda.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtMontoPagoTarjeta.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        cboTarjeta.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtMontoEfectivo.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtFecha.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtDocumento.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        txtMontoTotal.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent eve) {
            }

            public void keyPressed(KeyEvent eve) {
                //BUSCA CLIENTE
                if (eve.getKeyCode() == KeyEvent.VK_F1) {
                    resultado();
                    Procesar();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    public void resultado() {
        double resp = 0;
        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
                respuesta = 0;
                JOptionPane.showMessageDialog(null, "Recuerde que sin seleccionar una tarjeta de crédito, debe pagar la totalidad en Efectivo.\n"
                        + "Ingrese un Monto de Pago Válido.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                txtMontoEfectivo.requestFocusInWindow();
                return;
            }

            if (txtMontoPagoTarjeta.getText().equalsIgnoreCase("")) {
                txtMontoPagoTarjeta.setText("0");
            }
        } else {
            if (txtMontoPagoTarjeta.getText().equalsIgnoreCase("")) {
                respuesta = 0;
                JOptionPane.showMessageDialog(null, "Recuerde que ha seleccionado Pagar por Tarjeta.\n"
                        + "Ingrese un Monto de Pago Válido.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                txtMontoPagoTarjeta.requestFocusInWindow();
                return;
            }

            if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
                txtMontoEfectivo.setText("0");
            }
        }
        if (leeMontoPension() > leeMontoPago()) {
            resp = leeMontoPension() - leeMontoPago();
            jLabel4.setText("Deuda Pendiente *");
            txtMontoDeuda.setText("" + resp);
            Procesar();
        } else {
            resp = leeMontoPago() - leeMontoPension();
            jLabel4.setText("Vuelto *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
            Procesar();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btnRegistrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtFecha = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cboTarjeta = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        txtDocumento = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtMontoTotal = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtMontoDeuda = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtMontoPagoTarjeta = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtMontoEfectivo = new org.edisoncor.gui.textField.TextFieldRectBackground();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setForeground(new java.awt.Color(102, 102, 102));
        jLabel2.setText("Total *");

        btnRegistrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnRegistrar.setText("PROCESAR PAGO");
        btnRegistrar.setToolTipText("Realizar Nuevo Registro");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("Monto de Pago Efectivo *");

        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText("Vuelto *");

        txtFecha.setEnabled(false);

        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("Fecha");

        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Núm. de Documento *");

        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Forma de Pago *");

        cboTarjeta.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboTarjeta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SOLO EFECTIVO", "VISA", "MASTERCARD", "DEPÓSITO BCP", "DEPÓSITO BBVA", "YAPE", "PLIN" }));
        cboTarjeta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTarjeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTarjetaActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Monto de Pago Tarjeta *");

        txtDocumento.setEditable(false);
        txtDocumento.setAnchoDeBorde(1.0F);
        txtDocumento.setDescripcion("Número");
        txtDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtMontoTotal.setEditable(false);
        txtMontoTotal.setBackground(new java.awt.Color(1, 150, 51));
        txtMontoTotal.setForeground(new java.awt.Color(255, 255, 255));
        txtMontoTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoTotal.setAnchoDeBorde(1.0F);
        txtMontoTotal.setColorDeBorde(new java.awt.Color(1, 150, 51));
        txtMontoTotal.setColorDeTextoBackground(new java.awt.Color(255, 255, 255));
        txtMontoTotal.setDescripcion("Total");
        txtMontoTotal.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N

        txtMontoDeuda.setEditable(false);
        txtMontoDeuda.setBackground(new java.awt.Color(1, 150, 51));
        txtMontoDeuda.setForeground(new java.awt.Color(255, 255, 255));
        txtMontoDeuda.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoDeuda.setAnchoDeBorde(1.0F);
        txtMontoDeuda.setColorDeBorde(new java.awt.Color(1, 150, 51));
        txtMontoDeuda.setColorDeTextoBackground(new java.awt.Color(255, 255, 255));
        txtMontoDeuda.setDescripcion("Total");
        txtMontoDeuda.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N

        txtMontoPagoTarjeta.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoPagoTarjeta.setAnchoDeBorde(1.0F);
        txtMontoPagoTarjeta.setDescripcion("Ingrese un Monto");
        txtMontoPagoTarjeta.setFont(new java.awt.Font("Arial Narrow", 1, 16)); // NOI18N
        txtMontoPagoTarjeta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoPagoTarjetaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMontoPagoTarjetaKeyTyped(evt);
            }
        });

        txtMontoEfectivo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMontoEfectivo.setAnchoDeBorde(1.0F);
        txtMontoEfectivo.setDescripcion("Ingrese un Monto");
        txtMontoEfectivo.setFont(new java.awt.Font("Arial Narrow", 1, 16)); // NOI18N
        txtMontoEfectivo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoEfectivoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMontoEfectivoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnRegistrar, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMontoPagoTarjeta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cboTarjeta, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMontoEfectivo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMontoTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMontoDeuda, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE))
                        .addGap(70, 70, 70))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoEfectivo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(4, 4, 4)
                .addComponent(cboTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoPagoTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMontoDeuda, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPlantillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlantillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaActionPerformed

    private void txtPlantillaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlantillaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaKeyReleased

    private void txtCodigoBarraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoBarraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraActionPerformed

    private void txtCodigoBarraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBarraKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraKeyReleased
 

    public double leeMontoEfectivo() {
        return Double.parseDouble(txtMontoEfectivo.getText());
    }

    public double leeMontoTarjeta() {
        return Double.parseDouble(txtMontoPagoTarjeta.getText());
    }

    public double leeMontoPension() {
        return Double.parseDouble(txtMontoTotal.getText());
    }

    public double leeMontoPago() {
        double efectivo = 0;
        if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
            efectivo = 0;
        } else {
            efectivo = Double.parseDouble(txtMontoEfectivo.getText());
        }
        double tarjeta = 0;
        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            tarjeta = 0;
        } else {
            tarjeta = Double.parseDouble(txtMontoPagoTarjeta.getText());
        }
        double totalAbonado = efectivo + tarjeta;
        return redondearDecimales(totalAbonado, 2);
    } 

    public String leeFecha() {
        return fec.mostrar_fecha(txtFecha);
    }

    public String leeDocumento() {
        return txtDocumento.getText().toUpperCase();
    }

    public void Procesar() {

        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
                respuesta = 0;
                JOptionPane.showMessageDialog(null, "Recuerde que sin seleccionar una tarjeta de crédito, debe pagar la totalidad en Efectivo.\n"
                        + "Ingrese un Monto de Pago Válido.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                txtMontoEfectivo.requestFocusInWindow();
                return;
            }

            if (txtMontoPagoTarjeta.getText().equalsIgnoreCase("")) {
                txtMontoPagoTarjeta.setText("0");
            }
        } else {
            if (txtMontoPagoTarjeta.getText().equalsIgnoreCase("")) {
                respuesta = 0;
                JOptionPane.showMessageDialog(null, "Recuerde que ha seleccionado Pagar por Tarjeta.\n"
                        + "Ingrese un Monto de Pago Válido.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                txtMontoPagoTarjeta.requestFocusInWindow();
                return;
            }

            if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
                txtMontoEfectivo.setText("0");
            }
        }

        int opcion = JOptionPane.showConfirmDialog(null, "Desea Continuar con el Registro del Pago?\n"
                + "Recuerde verificar los montos antes de poder realizar el Registro del Pago", "Mensaje", JOptionPane.OK_CANCEL_OPTION);
        if (opcion == JOptionPane.OK_OPTION) {
            try {

                if (leeMontoPension() > leeMontoPago()) {
                    montoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());
                    montoTarjeta = Double.parseDouble(txtMontoPagoTarjeta.getText());
                    montoVuelto = 0;
                    montoDeuda = Double.parseDouble(txtMontoDeuda.getText());
                    descripcionTarjeta = cboTarjeta.getSelectedItem().toString().toUpperCase();
                    respuesta = 1;
                    this.dispose();

                } else if (leeMontoPago() > leeMontoPension()) {

                    montoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());
                    montoTarjeta = Double.parseDouble(txtMontoPagoTarjeta.getText());
                    montoDeuda = 0;
                    montoVuelto = Double.parseDouble(txtMontoDeuda.getText());
                    descripcionTarjeta = cboTarjeta.getSelectedItem().toString().toUpperCase();
                    respuesta = 1;
                    this.dispose();
                } else if (leeMontoPension() == leeMontoPago()) {

                    montoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());
                    montoTarjeta = Double.parseDouble(txtMontoPagoTarjeta.getText());
                    montoDeuda = 0;
                    montoVuelto = 0;
                    descripcionTarjeta = cboTarjeta.getSelectedItem().toString().toUpperCase();
                    respuesta = 1;
                    this.dispose();
                }
            } catch (Exception e) {
                System.err.println("error al registrar area " + e.getLocalizedMessage());
            }
        } else {
            AlertaError alerta = new AlertaError("Informe", "Operación Cancelada.");
            alerta.setVisible(true);
        }
    }
    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed

        resultado();

    }//GEN-LAST:event_btnRegistrarActionPerformed
    public void abrirarchivo(String archivo) {

        try {
            File objetofile = new File(archivo);
            Desktop.getDesktop().open(objetofile);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtMontoEfectivoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoEfectivoKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && txtMontoEfectivo.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMontoEfectivoKeyTyped

    private void cboTarjetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTarjetaActionPerformed

        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            txtMontoPagoTarjeta.setText("0"); 
            txtMontoEfectivo.requestFocusInWindow();
        } else {
            txtMontoPagoTarjeta.requestFocusInWindow();
            //OBTENEMOS LOS DATOS DE LOS CAMPOS
            double totalPagar = Double.parseDouble(txtMontoTotal.getText());

            double totalPagadoEfectivo = 0;
            if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
                totalPagadoEfectivo = 0;
            } else {
                totalPagadoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());
            }

            //validamos la diferencia para tarjetas
            if (totalPagadoEfectivo != 0) {
                //validamos si el monto es menor al que esta en efectivo
                if (totalPagadoEfectivo < totalPagar) {
                    double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                    txtMontoPagoTarjeta.setText("" + redondearDecimales(montoTarjetaDiferencia, 2));
                } else {
                    cboTarjeta.setSelectedIndex(0);
                    txtMontoPagoTarjeta.setText("0"); 
                    txtMontoEfectivo.requestFocusInWindow();
                    JOptionPane.showMessageDialog(null, "El monto en Efectivo cubre todo el Pago.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                txtMontoPagoTarjeta.setText("" + redondearDecimales(montoTarjetaDiferencia, 2));
            }
        }

        double resp = 0;
        if (leeMontoPension() > leeMontoPago()) {
            resp = leeMontoPension() - leeMontoPago();
            jLabel4.setText("Deuda Pendiente *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        } else {
            resp = leeMontoPago() - leeMontoPension();
            jLabel4.setText("Vuelto *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        }
    }//GEN-LAST:event_cboTarjetaActionPerformed

    private void txtMontoPagoTarjetaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoPagoTarjetaKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && txtMontoPagoTarjeta.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMontoPagoTarjetaKeyTyped

    public static double redondearDecimales(double respuestaInicial, int numeroDecimales) {
        double parteEntera, resultado;

        DecimalFormatSymbols separadoresPersonalizados = new DecimalFormatSymbols();
        separadoresPersonalizados.setDecimalSeparator('.');

        resultado = respuestaInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;

        DecimalFormat formato1 = new DecimalFormat("#.00", separadoresPersonalizados);
        return (Double.parseDouble(formato1.format(resultado)));
    }
    private void txtMontoEfectivoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoEfectivoKeyReleased

        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            txtMontoPagoTarjeta.setEnabled(false);
            txtMontoPagoTarjeta.setText("0");
        } else {
            txtMontoPagoTarjeta.setEnabled(true);
            txtMontoPagoTarjeta.setText("");
            txtMontoPagoTarjeta.requestFocusInWindow();
        }
        double totalPagadoEfectivo = 0;
        if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
            totalPagadoEfectivo = 0;
        } else {
            totalPagadoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());
        }

        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            txtMontoPagoTarjeta.setText("0");
            txtMontoEfectivo.requestFocusInWindow();
        } else {
            txtMontoPagoTarjeta.requestFocusInWindow();
            //OBTENEMOS LOS DATOS DE LOS CAMPOS
            double totalPagar = Double.parseDouble(txtMontoTotal.getText());

            //validamos la diferencia para tarjetas
            if (totalPagadoEfectivo != 0) {
                //validamos si el monto es menor al que esta en efectivo
                if (totalPagadoEfectivo < totalPagar) {
                    double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                    txtMontoPagoTarjeta.setText("" + redondearDecimales(montoTarjetaDiferencia, 2));
                } else {
                    cboTarjeta.setSelectedIndex(0);
                    txtMontoPagoTarjeta.setText("0");
                    txtMontoEfectivo.requestFocusInWindow();
                    JOptionPane.showMessageDialog(null, "El monto en Efectivo cubre todo el Pago.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                txtMontoPagoTarjeta.setText("" + redondearDecimales(montoTarjetaDiferencia, 2));
            }
        }

        double resp = 0;
        if (leeMontoPension() > leeMontoPago()) {
            resp = leeMontoPension() - leeMontoPago();
            jLabel4.setText("Deuda Pendiente *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        } else {
            resp = leeMontoPago() - leeMontoPension();
            jLabel4.setText("Vuelto *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        }
    }//GEN-LAST:event_txtMontoEfectivoKeyReleased

    private void txtMontoPagoTarjetaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoPagoTarjetaKeyReleased

        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            txtMontoPagoTarjeta.setEnabled(false);
            txtMontoPagoTarjeta.setText("0");
        } else {
            txtMontoPagoTarjeta.setEnabled(true);
            txtMontoPagoTarjeta.setText("");
            txtMontoPagoTarjeta.requestFocusInWindow();
        }

        if (txtMontoEfectivo.getText().equalsIgnoreCase("")) {
            txtMontoEfectivo.setText("0");
        }
        
        if (cboTarjeta.getSelectedItem().toString().equalsIgnoreCase("SOLO EFECTIVO")) {
            txtMontoPagoTarjeta.setText("0");
            txtMontoEfectivo.requestFocusInWindow();
        } else {
            txtMontoPagoTarjeta.requestFocusInWindow();
            //OBTENEMOS LOS DATOS DE LOS CAMPOS
            double totalPagar = Double.parseDouble(txtMontoTotal.getText());
            double totalPagadoEfectivo = Double.parseDouble(txtMontoEfectivo.getText());

            //validamos la diferencia para tarjetas
            if (totalPagadoEfectivo != 0) {
                //validamos si el monto es menor al que esta en efectivo
                if (totalPagadoEfectivo < totalPagar) {
                    double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                    txtMontoPagoTarjeta.setText("" + montoTarjetaDiferencia);
                } else {
                    cboTarjeta.setSelectedIndex(0);
                    txtMontoPagoTarjeta.setText("0");
                    txtMontoEfectivo.requestFocusInWindow();
                    JOptionPane.showMessageDialog(null, "El monto en Efectivo cubre todo el Pago.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                double montoTarjetaDiferencia = totalPagar - totalPagadoEfectivo;
                txtMontoPagoTarjeta.setText("" + montoTarjetaDiferencia);
            }
        }

        double resp = 0;
        if (leeMontoPension() > leeMontoPago()) {
            resp = leeMontoPension() - leeMontoPago();
            jLabel4.setText("Deuda Pendiente *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        } else {
            resp = leeMontoPago() - leeMontoPension();
            jLabel4.setText("Vuelto *");
            txtMontoDeuda.setText("" + redondearDecimales(resp, 2));
        }
    }//GEN-LAST:event_txtMontoPagoTarjetaKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

        this.respuesta = 0;
    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        this.respuesta = 0;
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pago_Venta_int.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pago_Venta_int.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pago_Venta_int.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pago_Venta_int.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Pago_Venta_int dialog = new Pago_Venta_int(new javax.swing.JFrame(), true, "", null, 0);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JComboBox cboTarjeta;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDocumento;
    public static com.toedter.calendar.JDateChooser txtFecha;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtMontoDeuda;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtMontoEfectivo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtMontoPagoTarjeta;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtMontoTotal;
    // End of variables declaration//GEN-END:variables
}
