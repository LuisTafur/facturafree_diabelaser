package factura_free.vista.modales.busquedas;

import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto;
import factura_free.clases.principales.Catalogos;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.modales.JDialogProducto;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

public class JDialogBuscarProducto extends javax.swing.JDialog {

    DefaultTableModel modelo = null;
    ArrayList<ObProducto> lista_ObProducto = null;
    public static int id = 0;
    String tipoDocumento = "";

    public JDialogBuscarProducto(java.awt.Frame parent, boolean modal, String valor_de_Busqueda, String tipoDocumento) {
        super(parent, modal);
        initComponents();
        this.id = 0;
        this.tipoDocumento = tipoDocumento;
        tblDetalle.setDefaultRenderer(Object.class, new ImgTabla());
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());

        txtBusqueda.setText(valor_de_Busqueda);
        configurarVentana();
        mostrar_Tabla();
        cargarProductos();

        txtBusqueda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarProductos();
            }
        });

    }

    private void configurarVentana() {
        //posiciono el frame al centro de la pantalla
        this.setLocationRelativeTo(null);
        //desactiva el cambio de tamaño de la ventana
        this.setResizable(false);
        //asigno titulo a mostrar del frame
        this.setTitle("LISTA DE PRODUCTOS");
        txtBusqueda.requestFocusInWindow();
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn(""); 
        modelo.addColumn("PRODUCTO");
        modelo.addColumn("UNID.M");
        modelo.addColumn("PRECIO");
        modelo.addColumn("IGV");
        modelo.addColumn("STOCK");
        tblDetalle.setRowHeight(23);
        tblDetalle.setModel(modelo);
        tblDetalle.setBackground(Color.WHITE);
        tblDetalle.setAutoResizeMode(tblDetalle.AUTO_RESIZE_OFF);

        tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(0); 
        tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(310);
        tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(77);
        tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(65);
        tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(40);
        tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(65);
    }

    private void cargarProductos() {
        Metodos.LimpiarTabla(modelo);
        String valorfiltroNotas = "";
        if (tipoDocumento.equalsIgnoreCase("07")) {
            valorfiltroNotas = " and tipo='CONCEPTOS NOTAS DE CRÉDITO'";
        } else if (tipoDocumento.equalsIgnoreCase("08")) {
            valorfiltroNotas = " and tipo='CONCEPTOS NOTAS DE DÉBITO'";
        } else if (tipoDocumento.equalsIgnoreCase("")) {
            valorfiltroNotas = " and tipo!='CONCEPTOS NOTAS DE CRÉDITO' and tipo!='CONCEPTOS NOTAS DE DÉBITO'";
        }
        ///mostramos la lista de los 100 primeros productos
        String consulta = "select * from producto where CONCAT(codigo,'-',descripcion) like '%" + txtBusqueda.getText() + "%' " + valorfiltroNotas + " "
                + " and estado='ACTIVO' order by descripcion  limit 0," + 50 + ";";
        lista_ObProducto = (ArrayList<ObProducto>) ProductoImpl.listarProductos(consulta);

        Object datos[] = new Object[7];
        for (int i = 0; i < lista_ObProducto.size(); i++) {
            datos[0] = (lista_ObProducto.get(i).getId()) + ""; 
            datos[1] = (lista_ObProducto.get(i).getDescripcion()) + "";
            datos[2] = (lista_ObProducto.get(i).getUnidad_medida()) + "";
            datos[3] = (lista_ObProducto.get(i).getPrecio()) + "";
            datos[4] = Catalogos.tipoTributo("", lista_ObProducto.get(i).getAfectacion(), "", "")[3];
            if (lista_ObProducto.get(i).getTiene_stock().equalsIgnoreCase("SI")) {
                datos[5] = (lista_ObProducto.get(i).getStock_actual()) + "";
            } else {
                datos[5] = "ILIMITADO";
            }

            modelo.addRow(datos);
        }
    }

    public void seleccionarProducto() {
        //capturo número documento para buscarlo más adelante
        int fila = tblDetalle.getSelectedRow();
        id = Integer.parseInt(tblDetalle.getValueAt(fila, 0).toString());
        dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        txtBusqueda = new org.edisoncor.gui.textField.TextFieldRectBackground();
        btnNuevoCliente = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalle.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblDetalle);

        jPanel3.add(jScrollPane2);

        txtBusqueda.setAnchoDeBorde(1.0F);
        txtBusqueda.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusqueda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyTyped(evt);
            }
        });

        btnNuevoCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btnNuevoCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(".:: Buscar Productos ::.", jPanel2);

        jPanel1.add(jTabbedPane1);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked
        if (evt.getClickCount() == 2) {
            seleccionarProducto();
        }
    }//GEN-LAST:event_tblDetalleMouseClicked

    private void txtBusquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyTyped

    }//GEN-LAST:event_txtBusquedaKeyTyped

    private void btnNuevoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoClienteActionPerformed

        String valorND = "";
        if (tipoDocumento.equalsIgnoreCase("08")) {
            valorND = "CONCEPTOS NOTAS DE DÉBITO";
        } else if (tipoDocumento.equalsIgnoreCase("07")) {
            valorND = "CONCEPTOS NOTAS DE CRÉDITO";
        }
        new JDialogProducto(null, true, 0, 0, valorND).setVisible(true);
        if (JDialogProducto.procesado == 1) {
            id = ProductoImpl.obtenerEntero("Select max(id) from producto;");
            dispose();
        } else {
            AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
            alert.setVisible(true);
        }
    }//GEN-LAST:event_btnNuevoClienteActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogBuscarProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogBuscarProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogBuscarProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogBuscarProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>  
        //</editor-fold> 
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogBuscarProducto dialog = new JDialogBuscarProducto(new javax.swing.JFrame(), true, "", "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevoCliente;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblDetalle;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
