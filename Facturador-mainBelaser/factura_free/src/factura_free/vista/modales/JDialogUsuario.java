package factura_free.vista.modales;

import factura_free.clases.principales.alternos.FormatoTextField;
import factura_free.clases.principales.alternos.Metodos;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import factura_free.clases.controlador.UsuarioImpl;
import factura_free.clases.modelo.ObUsuario;
import javax.swing.ImageIcon;

public class JDialogUsuario extends javax.swing.JDialog {

    FormatoTextField JTF = new FormatoTextField();
    public static int id_usuario = 0, procesado = 0;

    public JDialogUsuario(java.awt.Frame parent, boolean modal, int id_usuario) {
        super(parent, modal);
        initComponents();
        procesado = 0;
        this.id_usuario = id_usuario;
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        ConfigurarVentana();
        if (id_usuario != 0) {
            cargarUsuario();
        }
    }

    void ConfigurarVentana() {
        //posiciono el frame al centro de la pantalla
        this.setLocationRelativeTo(null);
        //desactiva el cambio de tamaño de la ventana
        this.setResizable(false);
        //asigno titulo a mostrar del frame
        if (id_usuario == 0) {
            this.setTitle("REGISTRO DE USUARIO");
            btnRegistrar.setText("Registrar");
        } else {
            this.setTitle("EDITAR USUARIO");
            btnRegistrar.setText("Actualizar");
        }
        //configurar formatos de JTField
        JTF.modelo_1TF(txtUsuario, 2);
        JTF.modelo_1TF(txtContrasena, 2);
        JTF.modelo_1TF(txtRContrasena, 2);
    }

    private void cargarUsuario() {
        UsuarioImpl.buscarUsuario(id_usuario);
        String usuario = UsuarioImpl.getUsuario().getUsuario();
        String contrasena = UsuarioImpl.getUsuario().getContrasena();
        String contrasenaRep = "" + UsuarioImpl.getUsuario().getContrasena();
        String rol = "" + UsuarioImpl.getUsuario().getRol();
        String estado = "" + UsuarioImpl.getUsuario().getEstado();

        txtUsuario.setText(usuario);
        txtContrasena.setText(contrasena);
        txtRContrasena.setText(contrasenaRep);
        cboRol.setSelectedItem(rol);
        cbxEstado.setSelectedItem(estado);
    }

    boolean ExisteUsuario(String usuario) {
        boolean existe = false;
        try {
            existe = UsuarioImpl.validar("select * from usuario where usuario = '" + usuario + "' ;");
        } catch (Exception e) {
        }
        return existe;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbxEstado = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        cboRol = new javax.swing.JComboBox<>();
        txtUsuario = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel7 = new javax.swing.JLabel();
        txtContrasena = new org.edisoncor.gui.passwordField.PasswordFieldRectBackground();
        jLabel8 = new javax.swing.JLabel();
        txtRContrasena = new org.edisoncor.gui.passwordField.PasswordFieldRectBackground();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        cbxEstado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ACTIVO", "INACTIVO" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        cboRol.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "USUARIO", "ADMINISTRADOR" }));
        cboRol.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        txtUsuario.setAnchoDeBorde(1.0F);
        txtUsuario.setDescripcion("Ingrese una descripción");
        txtUsuario.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Usuario:");

        txtContrasena.setAnchoDeBorde(1.0F);
        txtContrasena.setDescripcion("***************************");
        txtContrasena.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Contraseña:");

        txtRContrasena.setAnchoDeBorde(1.0F);
        txtRContrasena.setDescripcion("***************************");
        txtRContrasena.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Repetir contraseña:");

        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("Rol:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtContrasena, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRContrasena, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 129, Short.MAX_VALUE))
                            .addComponent(txtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboRol, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cboRol, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed

        String usuario = txtUsuario.getText().toUpperCase();
        String contrasena = txtContrasena.getText().toUpperCase();
        String rcontrasena = txtRContrasena.getText().toUpperCase();
        String rol = cboRol.getSelectedItem().toString().toUpperCase();
        String estado = cbxEstado.getSelectedItem().toString().toUpperCase();

        if (usuario.equals("")) {
            Metodos.MensajeAlerta("Ingrese una Descripción Válida.");
            txtUsuario.requestFocusInWindow();
            return;
        }
        if (contrasena.equals("")) {
            Metodos.MensajeAlerta("Ingrese una Descripción Válida.");
            txtContrasena.requestFocusInWindow();
            return;
        }
        if (rcontrasena.equals("")) {
            Metodos.MensajeAlerta("Ingrese una Descripción Válida.");
            txtRContrasena.requestFocusInWindow();
            return;
        }
        
        if(!contrasena.equals(rcontrasena)){
            Metodos.MensajeAlerta("Las contraseñas no coincide.");
            txtContrasena.requestFocusInWindow();
            return;
        }

        ///Si el ID del usuario es 0 REGISTRAMOS
        if (id_usuario == 0) {
            if (ExisteUsuario(usuario) == true) {
                Metodos.MensajeAlerta("El Usuario " + usuario + " ya existe.");
                txtUsuario.requestFocusInWindow();
            } else {
                try {
                    ObUsuario obj = new ObUsuario(0, usuario, contrasena, rol, estado);
                    UsuarioImpl.Registrar(obj);
                    procesado = 1;
                    dispose();
                } catch (SQLException ex) {
                    Logger.getLogger(JDialogUsuario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ///Si el ID del usuario es DIFERENTE DE 0 ACTUALIZAMOS
        } else if (id_usuario != 0) {
            try {
                try {
                    ObUsuario obj = new ObUsuario(id_usuario, usuario, contrasena, rol, estado);
                    UsuarioImpl.Actualizar(obj);
                    procesado = 1;
                    dispose();
                } catch (Exception e) {
                    Metodos.MensajeError("Error actualizando usuario: " + e);
                }
            } catch (Exception e) {
            }
        }

    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUsuarioActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>     
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogUsuario dialog = new JDialogUsuario(new javax.swing.JFrame(), true, 0);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JComboBox<String> cboRol;
    private javax.swing.JComboBox<String> cbxEstado;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private org.edisoncor.gui.passwordField.PasswordFieldRectBackground txtContrasena;
    private org.edisoncor.gui.passwordField.PasswordFieldRectBackground txtRContrasena;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtUsuario;
    // End of variables declaration//GEN-END:variables
}
