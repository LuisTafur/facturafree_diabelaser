/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 */
package factura_free.vista.modales.auxiliares;

import factura_free.clases.principales.alternos.FormatoTextField;
import factura_free.vista.alertas.AlertaInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

public class Form_CambioNumTPV extends javax.swing.JDialog {

    FormatoTextField JTF = new FormatoTextField();
    public static double valorCambio = 0;
    public static int procesado = 0;

    public Form_CambioNumTPV(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        valorCambio = 0;
        procesado = 0;
        JTF.modelo_1TF(txtCantidad, 2);
        txtCantidad.requestFocusInWindow();

        this.setLocationRelativeTo(null);
        this.setTitle("EDITAR CANTIDAD Y/O MONTOS");

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());

        txtCantidad.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                proceso();
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        LTABLA = new javax.swing.JLabel();
        lblIdEmpresa = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtCantidad = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnD10 = new javax.swing.JButton();
        btnD11 = new javax.swing.JButton();
        btnD12 = new javax.swing.JButton();
        btnD13 = new javax.swing.JButton();
        btnD14 = new javax.swing.JButton();
        btnD15 = new javax.swing.JButton();
        btnD16 = new javax.swing.JButton();
        btnD17 = new javax.swing.JButton();
        btnD18 = new javax.swing.JButton();
        btnD19 = new javax.swing.JButton();
        btnD20 = new javax.swing.JButton();
        btnLimpiar3 = new javax.swing.JButton();
        btnLimpiar4 = new javax.swing.JButton();
        btnLimpiar5 = new javax.swing.JButton();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        LTABLA.setText("jLabel1");
        jPanel4.add(LTABLA);

        lblIdEmpresa.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        txtCantidad.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantidad.setAnchoDeBorde(1.0F);
        txtCantidad.setDescripcion("valor");
        txtCantidad.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadKeyTyped(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(new java.awt.GridLayout(4, 0));

        btnD10.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD10.setForeground(new java.awt.Color(51, 51, 51));
        btnD10.setText("1");
        btnD10.setAutoscrolls(true);
        btnD10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD10.setDefaultCapable(false);
        btnD10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD10ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD10);

        btnD11.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD11.setForeground(new java.awt.Color(51, 51, 51));
        btnD11.setText("2");
        btnD11.setAutoscrolls(true);
        btnD11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD11.setDefaultCapable(false);
        btnD11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD11ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD11);

        btnD12.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD12.setForeground(new java.awt.Color(51, 51, 51));
        btnD12.setText("3");
        btnD12.setAutoscrolls(true);
        btnD12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD12.setDefaultCapable(false);
        btnD12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD12ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD12);

        btnD13.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD13.setForeground(new java.awt.Color(51, 51, 51));
        btnD13.setText("4");
        btnD13.setAutoscrolls(true);
        btnD13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD13.setDefaultCapable(false);
        btnD13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD13ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD13);

        btnD14.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD14.setForeground(new java.awt.Color(51, 51, 51));
        btnD14.setText("5");
        btnD14.setAutoscrolls(true);
        btnD14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD14.setDefaultCapable(false);
        btnD14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD14ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD14);

        btnD15.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD15.setForeground(new java.awt.Color(51, 51, 51));
        btnD15.setText("6");
        btnD15.setAutoscrolls(true);
        btnD15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD15.setDefaultCapable(false);
        btnD15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD15ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD15);

        btnD16.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD16.setForeground(new java.awt.Color(51, 51, 51));
        btnD16.setText("7");
        btnD16.setAutoscrolls(true);
        btnD16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD16.setDefaultCapable(false);
        btnD16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD16ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD16);

        btnD17.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD17.setForeground(new java.awt.Color(51, 51, 51));
        btnD17.setText("8");
        btnD17.setAutoscrolls(true);
        btnD17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD17.setDefaultCapable(false);
        btnD17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD17ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD17);

        btnD18.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD18.setForeground(new java.awt.Color(51, 51, 51));
        btnD18.setText("9");
        btnD18.setAutoscrolls(true);
        btnD18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD18.setDefaultCapable(false);
        btnD18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD18ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD18);

        btnD19.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        btnD19.setForeground(new java.awt.Color(51, 51, 51));
        btnD19.setText("0");
        btnD19.setAutoscrolls(true);
        btnD19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD19.setDefaultCapable(false);
        btnD19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD19ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD19);

        btnD20.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnD20.setForeground(new java.awt.Color(51, 51, 51));
        btnD20.setText(".");
        btnD20.setAutoscrolls(true);
        btnD20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnD20.setDefaultCapable(false);
        btnD20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnD20ActionPerformed(evt);
            }
        });
        jPanel5.add(btnD20);

        btnLimpiar3.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        btnLimpiar3.setForeground(new java.awt.Color(51, 51, 51));
        btnLimpiar3.setText("CLEAR");
        btnLimpiar3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpiar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiar3ActionPerformed(evt);
            }
        });
        jPanel5.add(btnLimpiar3);

        jPanel2.add(jPanel5);

        btnLimpiar4.setBackground(new java.awt.Color(0, 102, 0));
        btnLimpiar4.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        btnLimpiar4.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar4.setText("PROCESAR");
        btnLimpiar4.setBorderPainted(false);
        btnLimpiar4.setContentAreaFilled(false);
        btnLimpiar4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpiar4.setOpaque(true);
        btnLimpiar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiar4ActionPerformed(evt);
            }
        });

        btnLimpiar5.setBackground(new java.awt.Color(204, 0, 0));
        btnLimpiar5.setFont(new java.awt.Font("Arial Narrow", 1, 20)); // NOI18N
        btnLimpiar5.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar5.setText("CNL");
        btnLimpiar5.setBorderPainted(false);
        btnLimpiar5.setContentAreaFilled(false);
        btnLimpiar5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLimpiar5.setOpaque(true);
        btnLimpiar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiar5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnLimpiar5, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpiar4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLimpiar4, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimpiar5, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPlantillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlantillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaActionPerformed

    private void txtPlantillaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlantillaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaKeyReleased

    private void txtCodigoBarraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoBarraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraActionPerformed

    private void txtCodigoBarraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBarraKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraKeyReleased

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed

    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtPrecioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyReleased

    }//GEN-LAST:event_txtPrecioKeyReleased

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadKeyReleased

    private void txtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && txtCantidad.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCantidadKeyTyped

    private void btnD10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD10ActionPerformed
        String btncero = txtCantidad.getText() + btnD10.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD10ActionPerformed

    private void btnD11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD11ActionPerformed
        String btncero = txtCantidad.getText() + btnD11.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD11ActionPerformed

    private void btnD12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD12ActionPerformed
        String btncero = txtCantidad.getText() + btnD12.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD12ActionPerformed

    private void btnD13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD13ActionPerformed
        String btncero = txtCantidad.getText() + btnD13.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD13ActionPerformed

    private void btnD14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD14ActionPerformed
        String btncero = txtCantidad.getText() + btnD14.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD14ActionPerformed

    private void btnD15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD15ActionPerformed
        String btncero = txtCantidad.getText() + btnD15.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD15ActionPerformed

    private void btnD16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD16ActionPerformed
        String btncero = txtCantidad.getText() + btnD16.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD16ActionPerformed

    private void btnD17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD17ActionPerformed
        String btncero = txtCantidad.getText() + btnD17.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD17ActionPerformed

    private void btnD18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD18ActionPerformed
        String btncero = txtCantidad.getText() + btnD18.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD18ActionPerformed

    private void btnD19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD19ActionPerformed
        String btncero = txtCantidad.getText() + btnD19.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD19ActionPerformed

    private void btnLimpiar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiar3ActionPerformed
        txtCantidad.setText("");
        valorCambio = 0;
        procesado = 0;
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnLimpiar3ActionPerformed

    private void btnLimpiar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiar4ActionPerformed
        proceso();
    }//GEN-LAST:event_btnLimpiar4ActionPerformed

    private void btnD20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnD20ActionPerformed
        String btncero = txtCantidad.getText() + btnD20.getText();
        txtCantidad.setText(btncero);
        txtCantidad.requestFocusInWindow();
    }//GEN-LAST:event_btnD20ActionPerformed
    public void proceso() {
        try {
            if (txtCantidad.getText().equalsIgnoreCase("")) {
                txtCantidad.setText("1.0");
                valorCambio = Double.parseDouble(txtCantidad.getText());
                if (valorCambio <= 0) {
                    valorCambio = 0;
                    procesado = 0;
                    txtCantidad.requestFocusInWindow();
                    AlertaInfo alerta = new AlertaInfo("Informe", "Ingrese una Cantidad Válida.");
                    alerta.setVisible(true);
                } else {
                    procesado = 1;
                    dispose();
                }
            } else {
                valorCambio = Double.parseDouble(txtCantidad.getText());
                if (valorCambio <= 0) {
                    valorCambio = 0;
                    procesado = 0;
                    txtCantidad.requestFocusInWindow();
                    AlertaInfo alerta = new AlertaInfo("Informe", "Ingrese una Cantidad Válida.");
                    alerta.setVisible(true);
                } else {
                    procesado = 1;
                    dispose();
                }
            }
        } catch (Exception e) {
            valorCambio = 0;
            procesado = 0;
            txtCantidad.requestFocusInWindow();
        }
    }
    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        valorCambio = 0;
        procesado = 0;
        dispose();
    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        valorCambio = 0;
        procesado = 0;
        dispose();
    }//GEN-LAST:event_formWindowClosing

    private void btnLimpiar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiar5ActionPerformed
        valorCambio = 0;
        procesado = 0;
        dispose();
    }//GEN-LAST:event_btnLimpiar5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Form_CambioNumTPV.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Form_CambioNumTPV.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Form_CambioNumTPV.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Form_CambioNumTPV.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>


        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Form_CambioNumTPV dialog = new Form_CambioNumTPV(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LTABLA;
    private javax.swing.JButton btnD10;
    private javax.swing.JButton btnD11;
    private javax.swing.JButton btnD12;
    private javax.swing.JButton btnD13;
    private javax.swing.JButton btnD14;
    private javax.swing.JButton btnD15;
    private javax.swing.JButton btnD16;
    private javax.swing.JButton btnD17;
    private javax.swing.JButton btnD18;
    private javax.swing.JButton btnD19;
    private javax.swing.JButton btnD20;
    private javax.swing.JButton btnLimpiar3;
    private javax.swing.JButton btnLimpiar4;
    private javax.swing.JButton btnLimpiar5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel lblIdEmpresa;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCantidad;
    // End of variables declaration//GEN-END:variables
}
