/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package factura_free.vista.modales.auxiliares;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.exportar.DescargarExcel_Presupuestos;
import factura_free.clases.principales.exportar.DescargarExcel_Ventas_internas;
import factura_free.vista.modales.busquedas.JDialogBuscarCliente;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author proxc
 */
public class Formulario_GenerarRepoFechasCliente extends javax.swing.JDialog {

    Fecha_Date fec = new Fecha_Date();
    String tipoRepo = "";

    public Formulario_GenerarRepoFechasCliente(java.awt.Frame parent, boolean modal, String tipoRepo) {
        super(parent, modal);
        initComponents();
        this.tipoRepo = tipoRepo;

        fec.capturaymuestrahoradelsistema(txtFechaInicial);
        fec.capturaymuestrahoradelsistema(txtFechaFinal);
        lblIdCliente.setText("0");

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        this.setLocationRelativeTo(null);
        if (tipoRepo.equalsIgnoreCase("Ventas")) {
            this.setTitle("Generar Reporte de Ventas");
        } else if (tipoRepo.equalsIgnoreCase("Ventas_internas")) {
            this.setTitle("Generar Reporte de Notas de Venta");
        } else if (tipoRepo.equalsIgnoreCase("Presupuestos")) {
            this.setTitle("Generar Reporte de Presupuestos");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIdCliente = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnImpresionDefecto = new javax.swing.JButton();
        txtFechaInicial = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtFechaFinal = new com.toedter.calendar.JDateChooser();
        btnBuscarCliente = new javax.swing.JButton();
        txtNombresCompleto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel13 = new javax.swing.JLabel();

        lblIdCliente.setText("jLabel8");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnImpresionDefecto.setBackground(new java.awt.Color(0, 153, 0));
        btnImpresionDefecto.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        btnImpresionDefecto.setForeground(new java.awt.Color(255, 255, 255));
        btnImpresionDefecto.setText("Generar Reporte");
        btnImpresionDefecto.setBorderPainted(false);
        btnImpresionDefecto.setContentAreaFilled(false);
        btnImpresionDefecto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnImpresionDefecto.setOpaque(true);
        btnImpresionDefecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImpresionDefectoActionPerformed(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Fecha Inicial *");

        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setText("Fecha Final *");

        btnBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar_16px.png"))); // NOI18N
        btnBuscarCliente.setToolTipText("Buscar un Cliente");
        btnBuscarCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });

        txtNombresCompleto.setAnchoDeBorde(1.0F);
        txtNombresCompleto.setDescripcion("Ingrese una descripción");
        txtNombresCompleto.setEnabled(false);
        txtNombresCompleto.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        jLabel13.setForeground(new java.awt.Color(102, 102, 102));
        jLabel13.setText("Cliente *");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnImpresionDefecto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 83, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtFechaFinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(txtNombresCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel12)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFechaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombresCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImpresionDefecto, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txt_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_emailActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void btnImpresionDefectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImpresionDefectoActionPerformed

        if (lblIdCliente.getText().equalsIgnoreCase("") || lblIdCliente.getText().equalsIgnoreCase("0")) {
            Metodos.MensajeError("Seleccione un Cliente Válido para su Consulta.");
        } else {

            int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                    "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                    new Object[]{" Generar Reporte en Excel ", " Cancelar Operación "}, " Cancelar Operación ");

            /////GENERAR ARCHIVOS PLANOS
            if (seleccion == 0) {
                if (tipoRepo.equalsIgnoreCase("Ventas_internas")) {
                    DescargarExcel_Ventas_internas.crearExcel(fec.mostrar_fecha(txtFechaInicial), fec.mostrar_fecha(txtFechaFinal), Integer.parseInt(lblIdCliente.getText()), txtNombresCompleto.getText().toUpperCase());
                } else if (tipoRepo.equalsIgnoreCase("Presupuestos")) {
                    DescargarExcel_Presupuestos.crearExcel(fec.mostrar_fecha(txtFechaInicial), fec.mostrar_fecha(txtFechaFinal), Integer.parseInt(lblIdCliente.getText()), txtNombresCompleto.getText().toUpperCase());
                }
            } else if (seleccion == 1) {
                Metodos.MensajeAlerta("Cliente no Seleccionado. Operación Cancelada.");
            }
        }

    }//GEN-LAST:event_btnImpresionDefectoActionPerformed

    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        new JDialogBuscarCliente(null, true, "TODOS").setVisible(true);
        if (JDialogBuscarCliente.id != 0) {
            lblIdCliente.setText("" + JDialogBuscarCliente.id);
            txtNombresCompleto.setText(JDialogBuscarCliente.cliente);
            Metodos.MensajeInformacion("Cliente Seleccionado Exitosamente.");
        } else {
            lblIdCliente.setText("0");
            Metodos.MensajeAlerta("Cliente no Seleccionado. Operación Cancelada.");
        }
    }//GEN-LAST:event_btnBuscarClienteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Formulario_GenerarRepoFechasCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Formulario_GenerarRepoFechasCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Formulario_GenerarRepoFechasCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Formulario_GenerarRepoFechasCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold> 
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>  
        //</editor-fold>
        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Formulario_GenerarRepoFechasCliente dialog = new Formulario_GenerarRepoFechasCliente(new javax.swing.JFrame(), true, "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnImpresionDefecto;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblIdCliente;
    private com.toedter.calendar.JDateChooser txtFechaFinal;
    private com.toedter.calendar.JDateChooser txtFechaInicial;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNombresCompleto;
    // End of variables declaration//GEN-END:variables
}
