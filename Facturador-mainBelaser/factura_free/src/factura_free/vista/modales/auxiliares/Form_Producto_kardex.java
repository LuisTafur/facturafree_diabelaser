/*
 * To change this license header, choose License Headers in Project Properties. 
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 */
package factura_free.vista.modales.auxiliares;

import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto_kardex;
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.exportar.DescargarExcel_Productos_kardex;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

public class Form_Producto_kardex extends javax.swing.JDialog {

    public static int id_producto = 0;
    public static String descripcion = "", stock = "";
    DefaultTableModel modelo = null;
    Fecha_Date fec = new Fecha_Date();
    ArrayList<ObProducto_kardex> lista_ObProducto_kardex = null;

    public Form_Producto_kardex(java.awt.Frame parent, boolean modal, int id_producto, String descripcion, String stock) {
        super(parent, modal);
        initComponents();
        this.id_producto = id_producto;
        this.descripcion = descripcion;
        this.stock = stock;
        fec.capturaymuestrahoradelsistema(txtFechaInicial);
        fec.capturaymuestrahoradelsistema(txtFechaFinal);

        txtDescripcion.setText(descripcion);
        txtStock_actual.setText(stock);
        this.setTitle("VER KARDEX DE PRODUCTO");
        mostrar_Tabla();

        tblDetalle.setDefaultRenderer(Object.class, new ImgTabla());
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        this.setLocationRelativeTo(null);

        txtFechaInicial.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarProductos();
            }
        });

        txtFechaFinal.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarProductos();
            }
        });
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn("FECHA");
        modelo.addColumn("ENTRADA");
        modelo.addColumn("SALIDA");
        modelo.addColumn("DOCUMENTO");
        modelo.addColumn("TIPO");
        modelo.addColumn("USUARIO");
        tblDetalle.setRowHeight(23);
        tblDetalle.setModel(modelo);
        tblDetalle.setBackground(Color.WHITE);
        tblDetalle.setAutoResizeMode(tblDetalle.AUTO_RESIZE_OFF);

        tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(120);
        tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(65);
        tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(65);
        tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(130);
        tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(120);
        tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(80);
        cargarProductos();
    }

    public String leeFechaFinal() {
        return fec.mostrar_fecha(txtFechaFinal);
    }

    public String leeFechaInicial() {
        return fec.mostrar_fecha(txtFechaInicial);
    }

    private void cargarProductos() {
        Metodos.LimpiarTabla(modelo);
        String consulta = "select * from producto_kardex where fecha>='" + leeFechaInicial() + "' and fecha<='" + leeFechaFinal() + "' "
                + " and id_producto='" + id_producto + "' order by id_producto_kardex desc;";

        lista_ObProducto_kardex = (ArrayList<ObProducto_kardex>) ProductoImpl.listarProductos_kardex(consulta);

        Object datos[] = new Object[6];
        for (int i = 0; i < lista_ObProducto_kardex.size(); i++) {
            datos[0] = (lista_ObProducto_kardex.get(i).getFecha()) + " / " + (lista_ObProducto_kardex.get(i).getHora());
            datos[1] = (lista_ObProducto_kardex.get(i).getCnt_entrada()) + "";
            datos[2] = (lista_ObProducto_kardex.get(i).getCnt_salida()) + "";
            datos[3] = (lista_ObProducto_kardex.get(i).getDoc_relacionado()) + "";
            datos[4] = (lista_ObProducto_kardex.get(i).getTipo()) + "";
            datos[5] = (lista_ObProducto_kardex.get(i).getUsuario()) + "";
            modelo.addRow(datos);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        LTABLA = new javax.swing.JLabel();
        lblIdEmpresa = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtDescripcion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtStock_actual = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        txtFechaInicial = new com.toedter.calendar.JDateChooser();
        txtFechaFinal = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        LTABLA.setText("jLabel1");
        jPanel4.add(LTABLA);

        lblIdEmpresa.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("Producto");

        txtDescripcion.setEditable(false);
        txtDescripcion.setAnchoDeBorde(1.0F);
        txtDescripcion.setDescripcion("Ingrese una descripción");
        txtDescripcion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtStock_actual.setEditable(false);
        txtStock_actual.setAnchoDeBorde(1.0F);
        txtStock_actual.setDescripcion("Ingrese un Monto");
        txtStock_actual.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtStock_actual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStock_actualActionPerformed(evt);
            }
        });
        txtStock_actual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStock_actualKeyTyped(evt);
            }
        });

        jLabel17.setForeground(new java.awt.Color(102, 102, 102));
        jLabel17.setText("Stock Actual");

        tblDetalle.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblDetalle);

        txtFechaInicial.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtFechaFinal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        jButton1.setText("Exportar Datos");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtFechaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStock_actual, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtStock_actual, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtFechaInicial, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(txtFechaFinal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPlantillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlantillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaActionPerformed

    private void txtPlantillaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlantillaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaKeyReleased

    private void txtCodigoBarraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoBarraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraActionPerformed

    private void txtCodigoBarraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBarraKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraKeyReleased

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed

    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtPrecioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyReleased

    }//GEN-LAST:event_txtPrecioKeyReleased

    private void txtNuevoPrecioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevoPrecioKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNuevoPrecioKeyReleased

    private void txtStock_actualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStock_actualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStock_actualActionPerformed

    private void txtStock_actualKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStock_actualKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStock_actualKeyTyped

    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked

    }//GEN-LAST:event_tblDetalleMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        DescargarExcel_Productos_kardex.crearExcel(id_producto,descripcion,fec.mostrar_fecha(txtFechaInicial), fec.mostrar_fecha(txtFechaFinal));
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Form_Producto_kardex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Form_Producto_kardex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Form_Producto_kardex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Form_Producto_kardex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold  
        //</editor-fold>  
        //</editor-fold>
        //</editor-fold  

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Form_Producto_kardex dialog = new Form_Producto_kardex(new javax.swing.JFrame(), true, 0, "", "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LTABLA;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblIdEmpresa;
    private javax.swing.JTable tblDetalle;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDescripcion;
    private com.toedter.calendar.JDateChooser txtFechaFinal;
    private com.toedter.calendar.JDateChooser txtFechaInicial;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtStock_actual;
    // End of variables declaration//GEN-END:variables
}
