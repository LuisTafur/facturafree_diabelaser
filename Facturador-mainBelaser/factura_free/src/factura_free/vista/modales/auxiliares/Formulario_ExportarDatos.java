/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package factura_free.vista.modales.auxiliares;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.exportar.DescargarExcel_Clientes;
import factura_free.clases.principales.exportar.DescargarExcel_Productos;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author proxc
 */
public class Formulario_ExportarDatos extends javax.swing.JDialog {

    Fecha_Date fec = new Fecha_Date();
    String tipoRepo = "";

    public Formulario_ExportarDatos(java.awt.Frame parent, boolean modal, String tipoRepo) {
        super(parent, modal);
        initComponents();
        this.tipoRepo = tipoRepo;

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        this.setLocationRelativeTo(null);
        if (tipoRepo.equalsIgnoreCase("Clientes")) {
            this.setTitle("Exportar Datos de Clientes");
        } else if (tipoRepo.equalsIgnoreCase("Productos")) {
            this.setTitle("Exportar Datos de Productos");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnImpresionDefecto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnImpresionDefecto.setBackground(new java.awt.Color(0, 153, 0));
        btnImpresionDefecto.setFont(new java.awt.Font("Arial Narrow", 1, 14)); // NOI18N
        btnImpresionDefecto.setForeground(new java.awt.Color(255, 255, 255));
        btnImpresionDefecto.setText("Generar Reporte");
        btnImpresionDefecto.setBorderPainted(false);
        btnImpresionDefecto.setContentAreaFilled(false);
        btnImpresionDefecto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnImpresionDefecto.setOpaque(true);
        btnImpresionDefecto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImpresionDefectoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnImpresionDefecto, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnImpresionDefecto, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void txt_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_emailActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void btnImpresionDefectoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImpresionDefectoActionPerformed

        int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Exportar Datos en Excel ", " Cancelar Operación "}, " Cancelar Operación ");

        /////GENERAR ARCHIVOS PLANOS
        if (seleccion == 0) {
            if (tipoRepo.equalsIgnoreCase("Clientes")) {
                DescargarExcel_Clientes.crearExcel();
            } else if (tipoRepo.equalsIgnoreCase("Productos")) {
                DescargarExcel_Productos.crearExcel();
            }
        } else if (seleccion == 1) {

            Metodos.MensajeAlerta("Cliente no Seleccionado. Operación Cancelada.");
        }

    }//GEN-LAST:event_btnImpresionDefectoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Formulario_ExportarDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Formulario_ExportarDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Formulario_ExportarDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Formulario_ExportarDatos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold> 
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>  
        //</editor-fold>
        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Formulario_ExportarDatos dialog = new Formulario_ExportarDatos(new javax.swing.JFrame(), true, "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnImpresionDefecto;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
