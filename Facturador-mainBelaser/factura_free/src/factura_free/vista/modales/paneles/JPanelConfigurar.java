package factura_free.vista.modales.paneles;

/**
 * facturafree2021@gmail.com oermjtryssuobrfc
 *
 * @author USER-1
 */
import factura_free.clases.controlador.ConfigImpl;
import factura_free.clases.modelo.ObConfig;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.CopiarArchivo;
import factura_free.clases.principales.alternos.EncriparDatos;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.consulta_datos.Control_ConsultaDatos;
import factura_free.clases.principales.reindexar.LimpiarBD;
import factura_free.vista.principal.JFramePrincipal;
import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class JPanelConfigurar extends javax.swing.JPanel {

    EncriparDatos encriptar = new EncriparDatos();

    public JPanelConfigurar() {
        initComponents();
        cargarConfig();
    }

    private void cargarConfig() {
        try {
            ConfigImpl.buscarConfig("select * from config where id = '1';");

            String ruc = ConfigImpl.getConfig().getRuc();
            String razonSocial = ConfigImpl.getConfig().getRazonSocial();
            String direccion = ConfigImpl.getConfig().getDireccion();
            String telefono = ConfigImpl.getConfig().getTelefono();
            String correo = ConfigImpl.getConfig().getCorreo();
            String web = ConfigImpl.getConfig().getWeb(); 
            String impresion = ConfigImpl.getConfig().getImpresion();
            String usa_cnt_flotante = ConfigImpl.getConfig().getUsa_cnt_flotante();
            String producto_igv = ConfigImpl.getConfig().getProducto_igv();
            String serv_datos = ConfigImpl.getConfig().getServ_datos();
            String correo_aplicacion = ConfigImpl.getConfig().getCorreo_aplicacion();
            String pass_aplicacion = ConfigImpl.getConfig().getPass_aplicacion(); 
            String logo = ConfigImpl.getConfig().getLogo(); 
            String email_contador = ConfigImpl.getConfig().getEmail_contador();
            String seriePresupuesto = ConfigImpl.getConfig().getSeriePresupuesto();
                String serieVenta_interna = ConfigImpl.getConfig().getSerieVenta_interna();   
            String serieCompra = ConfigImpl.getConfig().getSerieCompra(); 
            String impre_directa = ConfigImpl.getConfig().getImpre_directa();

            txtNumeroDocumento.setText(ruc);
            txtNombresCompleto.setText(razonSocial);
            txtDireccion.setText(direccion);
            txtTelefono.setText(telefono);
            txtCorreo.setText(correo);
            txtWeb.setText(web); 
            cboTipoImpresion.setSelectedItem(impresion);
            cboCantidadFlotante.setSelectedItem(usa_cnt_flotante);
            cboProductoIGV.setSelectedItem(producto_igv);
            cboServidorDatos.setSelectedItem(serv_datos);
            txtCorreoAplicacion.setText(correo_aplicacion);
            txtPasswordAplicacion.setText(pass_aplicacion);

            txtUbicacionDistrito.setText(Datos.getUbicacionDistrito()); 
            txtCorreoContador.setText(email_contador);
            txtSeriePresupuesto.setText(seriePresupuesto);
            txtSerieVenta_interna.setText(serieVenta_interna);
            txtSerieCompras.setText(serieCompra);  

            txtRuta.setText(logo);  

            cboImpresionDirecta.setSelectedItem(impre_directa);

            mostrarFotoPoDefecto(logo);
        } catch (Exception e) {
            System.out.println("Error cargando datos: \n" + e);
            Metodos.MensajeError("Error cargando datos: \n" + e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cboTipodocumento = new javax.swing.JComboBox<>();
        fcBuscarFoto = new javax.swing.JFileChooser();
        txtRuta = new javax.swing.JLabel();
        cboTipoImpresion = new javax.swing.JComboBox<>();
        jLabel30 = new javax.swing.JLabel();
        cboProductoIGV = new javax.swing.JComboBox<>();
        jLabel34 = new javax.swing.JLabel();
        cboServidorDatos = new javax.swing.JComboBox<>();
        jLabel33 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jbtnGuardar = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        txtNumeroDocumento = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel16 = new javax.swing.JLabel();
        txtNombresCompleto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel17 = new javax.swing.JLabel();
        txtDireccion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel18 = new javax.swing.JLabel();
        txtTelefono = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel19 = new javax.swing.JLabel();
        txtCorreo = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel20 = new javax.swing.JLabel();
        txtWeb = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel21 = new javax.swing.JLabel();
        btnConsulta = new javax.swing.JButton();
        lblFoto = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnFoto = new javax.swing.JButton();
        btnRestaurarFoto = new javax.swing.JButton();
        txtCorreoContador = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel41 = new javax.swing.JLabel();
        txtSeriePresupuesto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel42 = new javax.swing.JLabel();
        txtSerieVenta_interna = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel43 = new javax.swing.JLabel();
        txtSerieCompras = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel59 = new javax.swing.JLabel();
        cboImpresionDirecta = new javax.swing.JComboBox<>();
        jLabel61 = new javax.swing.JLabel();
        cboCantidadFlotante = new javax.swing.JComboBox<>();
        jLabel32 = new javax.swing.JLabel();
        txtCorreoAplicacion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel29 = new javax.swing.JLabel();
        txtPasswordAplicacion = new javax.swing.JPasswordField();
        jLabel36 = new javax.swing.JLabel();
        txtUbicacionDistrito = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel38 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        cboTipodocumento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RUC" }));
        cboTipodocumento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        txtRuta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtRuta.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        cboTipoImpresion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ticket 80 mm", "A4" }));
        cboTipoImpresion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel30.setForeground(new java.awt.Color(102, 102, 102));
        jLabel30.setText("Impresión por Defecto *");

        cboProductoIGV.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SI", "NO" }));
        cboProductoIGV.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel34.setForeground(new java.awt.Color(102, 102, 102));
        jLabel34.setText("Productos Incluyen IGV?");

        cboServidorDatos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SERVIDOR 1", "SERVIDOR 2" }));
        cboServidorDatos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboServidorDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboServidorDatosActionPerformed(evt);
            }
        });

        jLabel33.setForeground(new java.awt.Color(102, 102, 102));
        jLabel33.setText("Consulta de Datos *");

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridLayout(1, 0));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jbtnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        jbtnGuardar.setText("Guardar");
        jbtnGuardar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(903, Short.MAX_VALUE)
                .addComponent(jbtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jbtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel4, java.awt.BorderLayout.PAGE_END);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.GridLayout(1, 0));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        txtNumeroDocumento.setAnchoDeBorde(1.0F);
        txtNumeroDocumento.setDescripcion("Ingrese una descripción");
        txtNumeroDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel16.setForeground(new java.awt.Color(102, 102, 102));
        jLabel16.setText("RUC *");

        txtNombresCompleto.setAnchoDeBorde(1.0F);
        txtNombresCompleto.setDescripcion("Ingrese una descripción");
        txtNombresCompleto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel17.setForeground(new java.awt.Color(102, 102, 102));
        jLabel17.setText("Razón Social *");

        txtDireccion.setAnchoDeBorde(1.0F);
        txtDireccion.setDescripcion("Ingrese una descripción");
        txtDireccion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel18.setForeground(new java.awt.Color(102, 102, 102));
        jLabel18.setText("Dirección *");

        txtTelefono.setAnchoDeBorde(1.0F);
        txtTelefono.setDescripcion("Ingrese una descripción");
        txtTelefono.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel19.setForeground(new java.awt.Color(102, 102, 102));
        jLabel19.setText("Teléfono");

        txtCorreo.setAnchoDeBorde(1.0F);
        txtCorreo.setDescripcion("Ingrese una descripción");
        txtCorreo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel20.setForeground(new java.awt.Color(102, 102, 102));
        jLabel20.setText("Correo");

        txtWeb.setAnchoDeBorde(1.0F);
        txtWeb.setDescripcion("Ingrese una descripción");
        txtWeb.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel21.setForeground(new java.awt.Color(102, 102, 102));
        jLabel21.setText("Página Web");

        btnConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnConsulta.setToolTipText("Consulta Reniec o Sunat dependiendo de su Tipo de Documento Seleccionado");
        btnConsulta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaActionPerformed(evt);
            }
        });

        lblFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFoto.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        lblFoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblFotoMouseClicked(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("Foto ");

        btnFoto.setBackground(new java.awt.Color(255, 255, 255));
        btnFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnFoto.setText("Subir foto");
        btnFoto.setToolTipText("Opcion de Cargar foto");
        btnFoto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFotoActionPerformed(evt);
            }
        });

        btnRestaurarFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/eliminar_16px.png"))); // NOI18N
        btnRestaurarFoto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRestaurarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestaurarFotoActionPerformed(evt);
            }
        });

        txtCorreoContador.setAnchoDeBorde(1.0F);
        txtCorreoContador.setDescripcion("Ingrese una descripción");
        txtCorreoContador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel41.setForeground(new java.awt.Color(102, 102, 102));
        jLabel41.setText("Correo Contador");

        txtSeriePresupuesto.setAnchoDeBorde(1.0F);
        txtSeriePresupuesto.setDescripcion("Ingrese una descripción");
        txtSeriePresupuesto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel42.setForeground(new java.awt.Color(102, 102, 102));
        jLabel42.setText("Serie de Presupuestos *");

        txtSerieVenta_interna.setAnchoDeBorde(1.0F);
        txtSerieVenta_interna.setDescripcion("Ingrese una descripción");
        txtSerieVenta_interna.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel43.setForeground(new java.awt.Color(102, 102, 102));
        jLabel43.setText("Serie de Notas de Venta *");

        txtSerieCompras.setAnchoDeBorde(1.0F);
        txtSerieCompras.setDescripcion("Ingrese una descripción");
        txtSerieCompras.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel59.setForeground(new java.awt.Color(102, 102, 102));
        jLabel59.setText("Serie de Compras *");

        cboImpresionDirecta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SI", "NO" }));
        cboImpresionDirecta.setToolTipText("");
        cboImpresionDirecta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel61.setForeground(new java.awt.Color(102, 102, 102));
        jLabel61.setText("Impresión Directa?");

        cboCantidadFlotante.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SI", "NO" }));
        cboCantidadFlotante.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel32.setForeground(new java.awt.Color(102, 102, 102));
        jLabel32.setText("Usar Cantidad Flotante *");

        txtCorreoAplicacion.setAnchoDeBorde(1.0F);
        txtCorreoAplicacion.setDescripcion("Ingrese una descripción");
        txtCorreoAplicacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel29.setForeground(new java.awt.Color(102, 102, 102));
        jLabel29.setText("Correo de Aplicación *");

        jLabel36.setForeground(new java.awt.Color(102, 102, 102));
        jLabel36.setText("Contraseña de Aplicación *");

        txtUbicacionDistrito.setAnchoDeBorde(1.0F);
        txtUbicacionDistrito.setDescripcion("Ingrese una descripción");
        txtUbicacionDistrito.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel38.setForeground(new java.awt.Color(102, 102, 102));
        jLabel38.setText("Ubicación de Establecimiento (Referencia para Nombre de Backup)");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jButton3.setText("Limpiar Base de Datos");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombresCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCorreo, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSeriePresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel61, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtSerieVenta_interna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                                .addComponent(txtWeb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCorreoContador, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCorreoAplicacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel38)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtUbicacionDistrito, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel5Layout.createSequentialGroup()
                                                .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(51, 51, 51))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(txtSerieCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(cboCantidadFlotante, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(4, 4, 4)))
                                        .addComponent(cboImpresionDirecta, 0, 214, Short.MAX_VALUE)))
                                .addGap(218, 218, 218)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(66, 66, 66))
                                    .addComponent(txtPasswordAplicacion, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnFoto)
                        .addGap(6, 6, 6)
                        .addComponent(btnRestaurarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNombresCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtWeb, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorreoContador, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                            .addComponent(jLabel29)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtCorreoAplicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel42)
                                            .addComponent(jLabel43))
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                            .addComponent(jLabel59)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(txtSerieCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtSeriePresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(txtSerieVenta_interna, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                            .addComponent(jLabel32)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(cboCantidadFlotante, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(cboImpresionDirecta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanel5Layout.createSequentialGroup()
                                            .addComponent(jLabel61)
                                            .addGap(36, 36, 36))))
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(36, 36, 36)))
                            .addComponent(txtPasswordAplicacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUbicacionDistrito, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(7, 7, 7)
                        .addComponent(lblFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRestaurarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab(".:: Empresa ::.", jPanel5);

        jPanel7.add(jTabbedPane1);

        jPanel2.add(jPanel7, java.awt.BorderLayout.CENTER);

        add(jPanel2);
    }// </editor-fold>//GEN-END:initComponents

    private void cargarEnv() {
        String host = Datos.getHost();
        String puerto = Datos.getPuerto();
        String baseDatos = Datos.getBaseDatos();
        String usuario = Datos.getUsuario();
        String contrasena = Datos.getContrasena();
        String ubicacion = txtUbicacionDistrito.getText().toUpperCase();

        if (ubicacion.equalsIgnoreCase("") || ubicacion.length() == 0) {
            ubicacion = "FACTURAFREE";
        }

        ConfigImpl.ActualizarENV(encriptar.Encriptar(host), encriptar.Encriptar(puerto), encriptar.Encriptar(baseDatos), encriptar.Encriptar(usuario),
                encriptar.Encriptar(contrasena), encriptar.Encriptar(ubicacion));
    }

    private void jbtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnGuardarActionPerformed
        String ruc = txtNumeroDocumento.getText();
        String razonSocial = txtNombresCompleto.getText();
        String direccion = txtDireccion.getText();
        String telefono = txtTelefono.getText();
        String correo = txtCorreo.getText();
        String web = txtWeb.getText(); 
        String impresion = cboTipoImpresion.getSelectedItem().toString();
        String usa_cnt_flotante = cboCantidadFlotante.getSelectedItem().toString();
        String producto_igv = cboProductoIGV.getSelectedItem().toString();
        String serv_datos = cboServidorDatos.getSelectedItem().toString();
        String correo_aplicacion = txtCorreoAplicacion.getText();
        String pass_aplicacion = txtPasswordAplicacion.getText(); 
        String logo = txtRuta.getText(); 
        String email_contador = txtCorreoContador.getText();
        String seriePresupuesto = txtSeriePresupuesto.getText().toUpperCase();
        String serieVenta_interna = txtSerieVenta_interna.getText().toUpperCase(); 
        String serieCompra = txtSerieCompras.getText().toUpperCase();  

        String impre_directa = cboImpresionDirecta.getSelectedItem().toString();

        cargarEnv();

        if (correo_aplicacion.equalsIgnoreCase("") || correo_aplicacion.length() == 0) {
            txtCorreoAplicacion.setText("emiteperugratis@gmail.com");
            correo_aplicacion = "emiteperugratis@gmail.com";
        }
        if (pass_aplicacion.equalsIgnoreCase("") || pass_aplicacion.length() == 0) {
            txtPasswordAplicacion.setText("xaffjuboefesxvzs");
            pass_aplicacion = "xaffjuboefesxvzs";
        }

        if (ruc.equalsIgnoreCase("") || ruc.length() != 11) {
            Metodos.MensajeError("INGRESE UN NÚMERO DE RUC VÁLIDO");
            txtNumeroDocumento.requestFocusInWindow();
            return;
        }
        if (razonSocial.equalsIgnoreCase("")) {
            Metodos.MensajeError("INGRESE UNA RAZÓN SOCIAL VALIDA");
            txtNombresCompleto.requestFocusInWindow();
            return;
        }
        if (direccion.equalsIgnoreCase("")) {
            Metodos.MensajeError("INGRESE UNA DIRECCIÓN VALIDA");
            txtDireccion.requestFocusInWindow();
            return;
        } 
        if (seriePresupuesto.equalsIgnoreCase("") || seriePresupuesto.length() != 4) {
            Metodos.MensajeError("INGRESE UNA SERIE VALIDA");
            txtSeriePresupuesto.requestFocusInWindow();
            return;
        }
        if (serieVenta_interna.equalsIgnoreCase("") || serieVenta_interna.length() != 4) {
            Metodos.MensajeError("INGRESE UNA SERIE VALIDA");
            txtSeriePresupuesto.requestFocusInWindow();
            return;
        } 
        if (seriePresupuesto.contains("F") || seriePresupuesto.contains("B")) {
            Metodos.MensajeError("La serie NO PUEDE INCLUIR LA LETRA (F) NI (B)");
            txtSeriePresupuesto.requestFocusInWindow();
            return;
        }
        if (serieVenta_interna.contains("F") || serieVenta_interna.contains("B")) {
            Metodos.MensajeError("La serie NO PUEDE INCLUIR LA LETRA (F) NI (B)");
            txtSerieVenta_interna.requestFocusInWindow();
            return;
        }
        if (serieCompra.equalsIgnoreCase("") || serieCompra.length() != 4) {
            txtSerieCompras.setText("CM01");
            serieCompra = "CM01";
        }
 

        try {
            JFramePrincipal.logo.setText(logo);
            ObConfig Obj = new ObConfig(1, ruc, razonSocial, direccion, telefono, correo, web, impresion, usa_cnt_flotante, producto_igv,
                    serv_datos, correo_aplicacion, pass_aplicacion, logo, email_contador, seriePresupuesto, serieVenta_interna, serieCompra, impre_directa);
            ConfigImpl.Actualizar(Obj);
            JOptionPane.showMessageDialog(null, "Presione ACEPTAR para poder culminar con la configuración necesaria.\n"
                    + "El sistema se va a cerrar para tomar los nuevos cambios y procesarlos para su gestión total.", "Mensaje de Configuración", JOptionPane.INFORMATION_MESSAGE);
            System.exit(-1);
        } catch (Exception e) {
            System.out.println("Error guardando cambios:\n" + e);
            Metodos.MensajeError("Error guardando cambios:\n" + e);
        }
    }//GEN-LAST:event_jbtnGuardarActionPerformed

    private void btnConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaActionPerformed

        JOptionPane.showMessageDialog(null, "Se ha soliciatado realizar la consulta de RUC, esta función puede tomar unos segundos.\n"
                + "PRESIONE ACEPTAR para continuar con el proceso de consulta solicitada.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        ////CONSULTA AL SERVIDOR 1 
        String rpt = Control_ConsultaDatos.consultaDatos(txtNombresCompleto, txtDireccion, txtNumeroDocumento, cboTipodocumento);
        if (!rpt.equalsIgnoreCase("SI")) {
            Metodos.MensajeError("Intente nuevamente");
        }
    }//GEN-LAST:event_btnConsultaActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Limpiar Toda la BD ", " Limpiar Procesos de Venta ", " Limpiar Clientes ", " Limpiar Productos ", " Cancelar "}, " Enviar a SUNAT ");

        if (seleccion == 0) {
            int opcion = JOptionPane.showConfirmDialog(null, "Deseas Eliminar toda la información de CLIENTES, PRODUCTOS Y COMPROBANTES DE TU BASE DE DATOS?.\n"
                    + "Recuerde que al realizar este proceso TODOS LOS DATOS REGISTRADOS HASTA EL MOMENTO NO SE PODRÁN RECUPERAR.",
                    "Mensaje", JOptionPane.OK_CANCEL_OPTION);

            if (opcion == JOptionPane.OK_OPTION) {
                try {
                    LimpiarBD.limpiarDBTruncate();
                    Metodos.MensajeInformacion("BASE DE DATOS EN BLANCO");
                } catch (Exception ex) {
                    Logger.getLogger(JPanelConfigurar.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Metodos.MensajeAlerta("Operación Cancelada");
            }
        } else if (seleccion == 1) {
            int opcion = JOptionPane.showConfirmDialog(null, "Deseas Eliminar toda la información de VENTAS, PRESUPUESTOS Y COMPROBANTES DE TU BASE DE DATOS?.\n"
                    + "Recuerde que al realizar este proceso TODOS LOS DATOS REGISTRADOS HASTA EL MOMENTO NO SE PODRÁN RECUPERAR.",
                    "Mensaje", JOptionPane.OK_CANCEL_OPTION);

            if (opcion == JOptionPane.OK_OPTION) {
                try {
                    LimpiarBD.limpiarDBTruncateVentas();
                    Metodos.MensajeInformacion("BASE DE VENTAS EN BLANCO");
                } catch (Exception ex) {
                    Logger.getLogger(JPanelConfigurar.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Metodos.MensajeAlerta("Operación Cancelada");
            }
        } else if (seleccion == 2) {
            int opcion = JOptionPane.showConfirmDialog(null, "Deseas Eliminar toda la información de CLIENTES DE TU BASE DE DATOS?.\n"
                    + "Recuerde que al realizar este proceso TODOS LOS DATOS REGISTRADOS HASTA EL MOMENTO NO SE PODRÁN RECUPERAR.",
                    "Mensaje", JOptionPane.OK_CANCEL_OPTION);

            if (opcion == JOptionPane.OK_OPTION) {
                try {
                    LimpiarBD.limpiarDBTruncateClientes();
                    Metodos.MensajeInformacion("BASE DE CLIENTES EN BLANCO");
                } catch (Exception ex) {
                    Logger.getLogger(JPanelConfigurar.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Metodos.MensajeAlerta("Operación Cancelada");
            }
        } else if (seleccion == 3) {
            int opcion = JOptionPane.showConfirmDialog(null, "Deseas Eliminar toda la información de PRODUCTOS DE TU BASE DE DATOS?.\n"
                    + "Recuerde que al realizar este proceso TODOS LOS DATOS REGISTRADOS HASTA EL MOMENTO NO SE PODRÁN RECUPERAR.",
                    "Mensaje", JOptionPane.OK_CANCEL_OPTION);

            if (opcion == JOptionPane.OK_OPTION) {
                try {
                    LimpiarBD.limpiarDBTruncateProductos();
                    Metodos.MensajeInformacion("BASE DE PRODUCTOS EN BLANCO");
                } catch (Exception ex) {
                    Logger.getLogger(JPanelConfigurar.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Metodos.MensajeAlerta("Operación Cancelada");
            }
        } else if (seleccion == 4) {
            Metodos.MensajeAlerta("Operación Cancelada");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void cboServidorDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboServidorDatosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboServidorDatosActionPerformed

    private void lblFotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblFotoMouseClicked

    }//GEN-LAST:event_lblFotoMouseClicked

    private void btnFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFotoActionPerformed
        //busca la foto y lo graba en el paquete Imagen

        int resp;
        FileNameExtensionFilter f = new FileNameExtensionFilter(".png", ".PNG");
        fcBuscarFoto.setFileFilter(f);
        resp = fcBuscarFoto.showOpenDialog(this);

        String foto = "", origen = "", destino = "";
        if (resp == JFileChooser.APPROVE_OPTION) {

            origen = fcBuscarFoto.getSelectedFile().toString();
            foto = fcBuscarFoto.getName(fcBuscarFoto.getSelectedFile());

            if (!txtRuta.getText().equalsIgnoreCase("my_log_2770.png")) {
                Metodos.Eliminar_archivo("C:\\FACTURAFREE\\archivos\\Logo\\" + txtRuta.getText());
            }

            /////////////////////////////////////////////////////////////////////////////
            destino = "C:\\FACTURAFREE\\archivos\\Logo\\" + fcBuscarFoto.getName(fcBuscarFoto.getSelectedFile());
            /////////////////////////////////////////////////////////////////////////////

            ImageIcon fot = new ImageIcon(origen);
            Icon icono = new ImageIcon(fot.getImage().getScaledInstance(128, 128, Image.SCALE_DEFAULT));//lo de 140 y 170 es para q la imagen salga ese tamaño
            lblFoto.setIcon(icono);
            this.repaint();
            txtRuta.setText(foto);///foto contiene el nombre del archivo q se guardara en la BD
            /////////////////////////////////////////////////////////////////////////////

            JFramePrincipal.logo.setText(foto);

            ConfigImpl.actualizarFoto(foto);
            CopiarArchivo.getInstance().copiar(origen, destino);
            return;
        } else if (resp == JFileChooser.CANCEL_OPTION) {
            JOptionPane.showMessageDialog(null, "Operación Cancelada");
            return;
        }// TODO add your handling code here:
    }//GEN-LAST:event_btnFotoActionPerformed

    private void btnRestaurarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestaurarFotoActionPerformed
        if (!txtRuta.getText().equalsIgnoreCase("my_log_2770.png")) {
            Metodos.Eliminar_archivo("C:\\FACTURAFREE\\archivos\\Logo\\" + txtRuta.getText());
        }
        ConfigImpl.actualizarFoto("my_log_2770.png");
        mostrarFotoPoDefecto("my_log_2770.png");
        txtRuta.setText("my_log_2770.png");
        JFramePrincipal.logo.setText("my_log_2770.png");
    }//GEN-LAST:event_btnRestaurarFotoActionPerformed

    public void mostrarFotoPoDefecto(String f) {
        //para MOSTRAR LA IMAGEN COMO ICONO DENTRO DEL LABEL
        ImageIcon fot = new ImageIcon("C:\\FACTURAFREE\\archivos\\Logo\\" + f);
        Icon icono = new ImageIcon(fot.getImage().getScaledInstance(151, 151, Image.SCALE_DEFAULT));//lo de 140 y 170 es para q la imagen salga ese tamaño
        lblFoto.setIcon(icono);
        this.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsulta;
    public static javax.swing.JButton btnFoto;
    private javax.swing.JButton btnRestaurarFoto;
    private javax.swing.JComboBox<String> cboCantidadFlotante;
    private javax.swing.JComboBox<String> cboImpresionDirecta;
    private javax.swing.JComboBox<String> cboProductoIGV;
    private javax.swing.JComboBox<String> cboServidorDatos;
    private javax.swing.JComboBox<String> cboTipoImpresion;
    private javax.swing.JComboBox<String> cboTipodocumento;
    private javax.swing.JFileChooser fcBuscarFoto;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton jbtnGuardar;
    private javax.swing.JLabel lblFoto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreoAplicacion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreoContador;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDireccion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNombresCompleto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNumeroDocumento;
    private javax.swing.JPasswordField txtPasswordAplicacion;
    private javax.swing.JLabel txtRuta;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtSerieCompras;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtSeriePresupuesto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtSerieVenta_interna;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtTelefono;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtUbicacionDistrito;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtWeb;
    // End of variables declaration//GEN-END:variables
}
