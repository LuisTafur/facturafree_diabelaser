/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factura_free.vista.modales.paneles.importar;

import factura_free.clases.controlador.ClienteImpl;
import factura_free.clases.controlador.ComprobantesImpl;
import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.CopiarCarpetas;
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.alternos.export_excel;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author USER-1
 */
public class JPanelImportarProductos extends javax.swing.JPanel {

    private JFileChooser FileChooser = new JFileChooser();
    DefaultTableModel modelo = null;//creamos el modelo de la tabla
    java.util.Timer timer = null;
    Vector columna = new Vector();
    Vector filas = new Vector();

    int valorProceso = 0;

    public JPanelImportarProductos() {
        initComponents();
        mostrar_Tabla();
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn("#");
        modelo.addColumn("CODIGO");
        modelo.addColumn("DESCRIPCIÓN");
        modelo.addColumn("PRECIO VENTA");
        modelo.addColumn("INCLUYE IGV");
        modelo.addColumn("TIPO");
        modelo.addColumn("AFECTACIÓN VENTA");
        modelo.addColumn("UNIDAD DE MEDIDA");
        modelo.addColumn("TIENE IMPUESTO A LA BOLSA");
        modelo.addColumn("TIENE STOCK");
        modelo.addColumn("STOCK");
        modelo.addColumn("AFECTACIÓN COMPRA");
        modelo.addColumn("PRECIO COMPRA");

        tblImportar.setRowHeight(18);
        tblImportar.setModel(modelo);

        tblImportar.setAutoResizeMode(tblImportar.AUTO_RESIZE_ALL_COLUMNS);

    }

    /////LIMPIAR TABLA
    private void Limpiar_Tabla() {
        for (int i = 0; i < tblImportar.getRowCount() - 1; i++) {
            modelo.removeRow(i);
            i -= 1;
        }
    }

    public void CrearTabla(File file) throws IOException {
        Workbook workbook = null;
        try {
            workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet(0);

            columna.clear();

            for (int i = 0; i < sheet.getColumns(); i++) {
                Cell cell = sheet.getCell(i, 0);
                columna.add(cell.getContents());
            }
            filas.clear();

            for (int j = 1; j < sheet.getRows(); j++) {
                Vector d = new Vector();
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    d.add(cell.getContents());
                }
                d.add("\n");
                filas.add(d);
            }

        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    public class Tiempo implements Runnable {

        @Override
        public void run() {
            barra.repaint();

            int totalRow = tblImportar.getRowCount();
            if (totalRow > 0) {

                int opcion = JOptionPane.showConfirmDialog(null, "Desea continuar con el registro masivo de Clientes? .", "Mensaje", JOptionPane.OK_CANCEL_OPTION);
                if (opcion == JOptionPane.OK_OPTION) {

                    //REGISTRAMOS LOS DATOS MASIVAMENTE
                    int num = 0;
                    barra.setMaximum(totalRow - 1);
                    for (int i = 0; i < (totalRow); i++) {
                        barra.setValue(i);
                        if (tblImportar.getValueAt(i, 0) != null) {

                            String descripcion = tblImportar.getValueAt(i, 2).toString().replace("'", "").replace("|", "").replace("%", "").replace("\"", "").replace("?", "").replace("¡", "").replace("!", "")
                                    .replace("*", "").replace(";", "").replace("%", "").replace("//", "/").replace("#", "").replace("´", " ").replace("\n", "");
                            String codigo = "-";
                            try {
                                codigo = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 1).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/"));

                            } catch (Exception e) {
                                codigo = "-";
                            }
                            if (codigo.equalsIgnoreCase("-") || codigo.equalsIgnoreCase("_")) {
                                codigo = (Metodos.GenerarCodigoUnico());
                            }

                            if (descripcion.length() != 0 && descripcion != null) {
                                num++;
                                String consulta = "Select id from producto where descripcion='" + descripcion + "' and codigo='" + codigo + "' and estado='ACTIVO'";
//                                System.err.println("descripcion: " + descripcion);
                                if (!ProductoImpl.validar(consulta)) {
//                                    ////REGISTRAMOS LOS DATOS DEL PRODUCTO 
                                    try {

                                        double precio_venta = Double.parseDouble((tblImportar.getValueAt(i, 3).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/"));
                                        String incluye_igv = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 4).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", ""));
                                        String tipo = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 5).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", ""));
                                        String afectacion_venta = (tblImportar.getValueAt(i, 6).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", "");
                                        String unidad_medida = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 7).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", ""));
                                        String icbper = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 8).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", ""));

                                        String tiene_stock = Metodos.validarCamposIniciales((tblImportar.getValueAt(i, 9).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", ""));
                                        double stock_actual = Double.parseDouble((tblImportar.getValueAt(i, 10).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/"));
                                        String afectacion_compra = (tblImportar.getValueAt(i, 11).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/").replace(".0", "").replace(".00", "");
                                        double precio_compra = Double.parseDouble((tblImportar.getValueAt(i, 12).toString()).replace("'", "").replace("\"", "").replace("|", "").replace("\n", "").replace("//", "/"));

                                        ObProducto obj = new ObProducto(0, codigo, descripcion, precio_venta, Integer.parseInt(incluye_igv), tipo, afectacion_venta, unidad_medida,
                                                "ACTIVO", icbper, "NO", tiene_stock, stock_actual, afectacion_compra, precio_compra);
                                        ProductoImpl.Registrar(obj);

                                        /////SI TIENE CONTROL DE STOCK - GENERA SU HISTORIAL DE KARDEX 
                                        if (tiene_stock.equalsIgnoreCase("SI")) {
                                            int id_maxProducto = ProductoImpl.obtenerId_Descripcion(descripcion);
                                            ProductoImpl.Registrar_Kardex(id_maxProducto, stock_actual, 0, (id_maxProducto + " APERTURA DE STOCK "), "APERTURA",Fecha_Date.retorna_fecha_sistema());
                                        }

                                    } catch (Exception ex) {
                                        Logger.getLogger(JPanelImportarProductos.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                        }
                    }

                    lblHoraFin.setText("TERMINADO A LAS  " + (ClienteImpl.obtenerCadena("SELECT TIME_FORMAT(NOW(), '%r') AS Tiempo;")));
                    valorProceso = 0;

                    Metodos.MensajeAlerta("Proceso Solicitado realizado con Exito");

                    JOptionPane.showMessageDialog(null, num + " Registros realizados Exitosamente.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                    mostrar_Tabla();
                    Limpiar_Tabla();
                    txtBusqueda.setText("");
                    barra.setValue(0);
                } else {
                    Metodos.MensajeInformacion("Operación Cancelada");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Importe la Hoja de Excell para poder Realizar el Registro Masivo.", "Mensajes", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton6 = new javax.swing.JButton();
        barra = new javax.swing.JProgressBar();
        lblHoraInicio = new javax.swing.JLabel();
        lblHoraFin = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        txtBusqueda = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblImportar = new javax.swing.JTable();
        lblHoraInicio1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jButton6.setForeground(new java.awt.Color(51, 51, 51));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/servers_16.png"))); // NOI18N
        jButton6.setText("Generar Formato de Importación");
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        barra.setBackground(new java.awt.Color(255, 255, 255));
        barra.setForeground(new java.awt.Color(65, 158, 247));
        barra.setAutoscrolls(true);
        barra.setBorderPainted(false);
        barra.setRequestFocusEnabled(false);
        barra.setStringPainted(true);
        barra.setVerifyInputWhenFocusTarget(false);

        lblHoraInicio.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHoraInicio.setForeground(new java.awt.Color(51, 51, 51));
        lblHoraInicio.setText("SOLICITADO A LAS ");

        lblHoraFin.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHoraFin.setForeground(new java.awt.Color(51, 51, 51));
        lblHoraFin.setText("TERMINADO A LAS ");

        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(new java.awt.GridLayout(1, 0));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(102, 102, 102));
        jLabel2.setText("Búsqueda");
        jPanel5.add(jLabel2);

        jPanel4.add(jPanel5, java.awt.BorderLayout.PAGE_START);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        jPanel11.setLayout(new java.awt.BorderLayout());

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.GridLayout(1, 0));

        jButton4.setForeground(new java.awt.Color(51, 51, 51));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jButton4.setText("Importar Archivo");
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel12.add(jButton4);

        jButton5.setForeground(new java.awt.Color(51, 51, 51));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        jButton5.setText("Registrar Datos Masivamente");
        jButton5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel12.add(jButton5);

        jPanel11.add(jPanel12, java.awt.BorderLayout.LINE_END);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(new java.awt.GridLayout(1, 0));

        txtBusqueda.setEditable(false);
        txtBusqueda.setAnchoDeBorde(1.0F);
        txtBusqueda.setDescripcion("Ruta de Archivo");
        txtBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel13.add(txtBusqueda);

        jPanel11.add(jPanel13, java.awt.BorderLayout.CENTER);

        jPanel6.add(jPanel11);

        jPanel4.add(jPanel6, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new java.awt.GridLayout(1, 0));

        tblImportar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblImportar.setFillsViewportHeight(true);
        tblImportar.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane2.setViewportView(tblImportar);

        jPanel8.add(jScrollPane2);

        lblHoraInicio1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHoraInicio1.setForeground(new java.awt.Color(51, 51, 51));
        lblHoraInicio1.setText("IMPORTACIÓN DE PRODUCTOS ***");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblHoraInicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 119, Short.MAX_VALUE))
                            .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHoraInicio1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed

        try {
            // TODO add your handling code here:
            FileChooser.showDialog(null, "Generar");

            File file = FileChooser.getSelectedFile();
            String fl = file.toString();

            File original = new File("C:\\FACTURAFREE\\archivos\\Importar\\Productos");
            File copia = new File(fl);

            //LLAMAMOS A LAS CLASES PARA PODER UTILIZAR EL COPIADO DE ARCHIVOS
            CopiarCarpetas cop_carpeta = new CopiarCarpetas();
            cop_carpeta.copiar_directorios(original, copia);

            JOptionPane.showMessageDialog(null, "Archivos Generados Exitosamente en :\n"
                    + fl, "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            System.out.println("ERROR EN : " + e.getMessage());
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        mostrar_Tabla();
        Limpiar_Tabla();

        try {
            //
            // TODO add your handling code here:
            FileChooser.showDialog(null, "Importar Hoja ");
            File file = FileChooser.getSelectedFile();
            if (file.getName().endsWith("xls") || file.getName().endsWith("xlsx")) {
                int numero = tblImportar.getColumnCount();
                System.out.println("nnumero : " + numero);
                if (numero != 13) {
                    Metodos.MensajeError("Verifique su hoja de Excel, se detecta errores en su importación.");
                    jButton4.requestFocusInWindow();
                    Limpiar_Tabla();
                } else {
                    String fl = file.toString();
                    txtBusqueda.setText(fl);
                    export_excel ex = new export_excel();
                    ex.Importar(file, tblImportar);
                }
            } else {
                Metodos.MensajeError("Seleccione un archivo excel...");
            }
        } catch (Exception e) {
            System.out.println("error en  : " + e.getMessage());
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

        if (valorProceso == 0) {
            valorProceso = 1;
            lblHoraInicio.setText("SOLICITADO A LAS " + (ComprobantesImpl.obtenerCadena("SELECT TIME_FORMAT(NOW(), '%r') AS Tiempo;")));

            new Thread(new Tiempo()).start();
        } else {
            Metodos.MensajeAlerta("Operación en Curso, espere que culmite la Tarea Actual.");
        }
    }//GEN-LAST:event_jButton5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblHoraFin;
    private javax.swing.JLabel lblHoraInicio;
    private javax.swing.JLabel lblHoraInicio1;
    private javax.swing.JTable tblImportar;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
