package factura_free.vista.modales.paneles;

import factura_free.clases.controlador.Venta_internaImpl;
import factura_free.vista.modales.busquedas.JDialogBuscarProducto;
import factura_free.clases.principales.Catalogos;
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Metodos;
import javax.swing.table.DefaultTableModel;
import factura_free.clases.controlador.ClienteImpl;
import factura_free.clases.controlador.ComprobantesImpl;
import factura_free.clases.controlador.ConfigImpl;
import factura_free.clases.controlador.PresupuestoImpl;
import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObVenta_interna;
import factura_free.clases.modelo.ObVenta_interna_det;
import factura_free.clases.modelo.ObCliente;
import factura_free.clases.modelo.ObPresupuesto_det;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.consulta_datos.Control_ConsultaDatos;
import factura_free.vista.alertas.AlertaBien;
import factura_free.vista.alertas.AlertaError;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.mantenimiento.JPanelVenta_internas;
import factura_free.vista.modales.JDialogCliente;
import factura_free.vista.modales.JDialogProducto;
import factura_free.vista.modales.Pago_Venta_int;
import factura_free.vista.modales.auxiliares.Form_CambioNumTPV;
import factura_free.vista.modales.auxiliares.Form_CampoProducto;
import factura_free.vista.modales.busquedas.JDialogBuscarCliente;
import factura_free.vista.modales.impresion.Imp_Comprobantes;
import factura_free.vista.principal.JFramePrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class JPanelVenta_interna extends javax.swing.JPanel {

    ArrayList<ObVenta_interna_det> lista_ObVenta_interna_det = null;
    ArrayList<ObPresupuesto_det> lista_ObPresupuesto_det = null;
    Fecha_Date fec = new Fecha_Date();
    DefaultTableModel dtmDetalle;

    public String idVenta_internaEdit = "", idVenta_internaRecuperar = "", idPresupuesto = "";
    public int verVenta_interna = 0;
    public static String numero = "";

    public double montoEfectivoVenta = 0, montoTarjetaVenta, montoVueltoVenta = 0, montoDeudaVenta = 0;
    public String descripcionTarjetaVenta = "SOLO EFECTIVO";

    public JPanelVenta_interna(String idVenta_internaEdit, int verVenta_interna, String idVenta_internaRecuperar, String idPresupuesto) {
        initComponents();
        this.idVenta_internaEdit = idVenta_internaEdit;
        this.idVenta_internaRecuperar = idVenta_internaRecuperar;
        this.verVenta_interna = verVenta_interna;
        this.idPresupuesto = idPresupuesto;
        this.numero = "";

        ///LLENAMOS LAS UNIDADES DE MEDIDA DESDE EL CATÁLOGO
        Catalogos.ListaUnidadesMedida(cboUnidadMedida, "codigo");

        ////VERIFICAMOS SI USA O NO USA LA CANTIDAD FLOTANTE
        if (Datos.getUsa_cntFlotante().equalsIgnoreCase("SI")) {
            rbtCantidadFlotante.setSelected(true);
        } else {
            rbtCantidadFlotante.setSelected(false);
        }

        if (idVenta_internaEdit.equalsIgnoreCase("")) {
            cargarIdVenta_interna();
            lblIdProducto.setText("0");
            lblIdCliente.setText("0");

            fec.capturaymuestrahoradelsistema(txtFechaEmision);
            fec.capturaymuestrahoradelsistema(txtFechaVencimiento);

            if (!idPresupuesto.equalsIgnoreCase("")) {
                cargarPresupuesto(idPresupuesto);
            } else {
                consultarCliente(1);
            }
        } else {
            ///SI EL ID ESTÁ LISTO PARA EDITAR, CARGAMOS TODOS LOS DATOS
            cargarVenta_interna(idVenta_internaEdit);
            if (verVenta_interna == 1) {
                txtFechaEmision.setEnabled(false);
                txtFechaVencimiento.setEnabled(false);
                cboTipodocumento.setEnabled(false);
                txtNumeroDocumento.setEnabled(false);
                btnConsultarDocumento.setEnabled(false);
                btnBuscarCliente.setEnabled(false);
                btnNuevoCliente.setEnabled(false);
                cboMedioPago.setEnabled(false);
                cboMoneda.setEnabled(false);
                txtNombresCompleto.setEnabled(false);
                txtDireccion.setEnabled(false);
                txtDescripcion.setEnabled(false);
                btnBuscarProductoServicio.setEnabled(false);
                btnNuevoProducto.setEnabled(false);
                btnQuitar.setEnabled(false);
                btnCrearArchivosPlanos.setEnabled(false);
                txtCorreo.setEnabled(false);
            }
        }

        if (idVenta_internaRecuperar != "") {
            ////SI LA VENTA SE DESEA RECUPERAR SE JALA TODOS LOS DATOS DE LA BOLETA RELACIONADA Y SE GENERA UN NUEVO CORRELATIVO
            cargarVenta_interna(idVenta_internaRecuperar);
            cargarIdVenta_interna();
            Metodos.MensajeInformacion("Venta Electrónica recuperar de forma Exitosa.");
        }

        txtNumeroDocumento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                consultarDatos();
            }
        });

        txtDescripcion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (txtDescripcion.getText().equalsIgnoreCase("")) {
                    buscarProducto("");
                } else {
                    int cntProdEnc = ProductoImpl.obtenerCantidadId_codigo(txtDescripcion.getText());
                    if (cntProdEnc == 0) {
                        txtDescripcion.setText("");
                        lblIdProducto.setText("0");
                        txtDescripcion.requestFocusInWindow();
                        Metodos.MensajeInformacion("El Producto no Existe");

                    } else if (cntProdEnc == 1) {
                        int id_producto = ProductoImpl.obtenerId_codigo(txtDescripcion.getText());
                        if (id_producto != 0) {
                            lblIdProducto.setText("" + id_producto);
                            cargarProducto(id_producto, 1.0);
                        } else {
                            txtDescripcion.setText("");
                            lblIdProducto.setText("0");
                            txtDescripcion.requestFocusInWindow();
                            Metodos.MensajeInformacion("El Producto no Existe");
                        }
                    } else if (cntProdEnc > 1) {
                        buscarProducto(txtDescripcion.getText().toUpperCase());
                    }
                }
            }
        });

        tblDetalle.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent Mouse_evt) {
                if (Mouse_evt.getClickCount() == 2) {
                    int fila = tblDetalle.getSelectedRow();
                    int columna = tblDetalle.getSelectedColumn();
                    String id_producto = String.valueOf(tblDetalle.getValueAt(fila, 0));
                    String afectacion = String.valueOf(tblDetalle.getValueAt(fila, 6));

                    //////CAMBIA LA CANTIDAD
                    if (verVenta_interna == 0) {
                        if (columna == 1) {
                            new Form_CambioNumTPV(null, true).setVisible(true);
                            if (Form_CambioNumTPV.procesado == 0) {
                                AlertaInfo alerta = new AlertaInfo("Informe", "Cambio de Cantidad no Válida.");
                                alerta.setVisible(true);
                            } else {
                                double cantidad_actual = Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(fila, 1)));
//                                double precio_actual = (Double.parseDouble(String.valueOf(tblDetalle.getValueAt(fila, 5))));
                                double tributo_actual = (Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(fila, 7))) / cantidad_actual);
                                double importe_actual = (Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(fila, 8))) / cantidad_actual);

                                double nueva_cantidad = Form_CambioNumTPV.valorCambio;
                                tblDetalle.setValueAt(Metodos.formatoDecimalMostrar(nueva_cantidad), fila, 1);

//                                tblDetalle.setValueAt(Metodos.formatoDecimalMostrar((precio_actual)), fila, 5);
                                tblDetalle.setValueAt(Metodos.formatoDecimalMostrar4Decimales((tributo_actual * nueva_cantidad)), fila, 7);
                                tblDetalle.setValueAt(Metodos.formatoDecimalMostrar((importe_actual * nueva_cantidad)), fila, 8);

                                actualizarTotales();
                                AlertaBien alerta = new AlertaBien("Informe", "Actualización Exitosa.");
                                alerta.setVisible(true);
                            }
                        }
                    }

                    /////EDITAR CODIGO DE PRODUCTO
                    if (verVenta_interna == 0) {
                        if (columna == 3) {
                            new Form_CampoProducto(null, true, Integer.parseInt(id_producto), "codigo", String.valueOf(tblDetalle.getValueAt(fila, 3))).setVisible(true);
                            if (Form_CampoProducto.valor == 0) {
                                AlertaInfo alerta = new AlertaInfo("Informe", "Edición no Realizada.");
                                alerta.setVisible(true);
                            } else {
                                String codigo = Form_CampoProducto.codigoProducto;
                                tblDetalle.setValueAt(codigo, fila, 3);
                            }
                        }
                    }

                    /////EDITAR CODIGO DE PRODUCTO
                    if (verVenta_interna == 0) {
                        if (columna == 4) {
                            new Form_CampoProducto(null, true, Integer.parseInt(id_producto), "descripcion", String.valueOf(tblDetalle.getValueAt(fila, 4))).setVisible(true);
                            if (Form_CampoProducto.valor == 0) {
                                AlertaInfo alerta = new AlertaInfo("Informe", "Edición no Realizada.");
                                alerta.setVisible(true);
                            } else {
                                String codigo = Form_CampoProducto.descripcionProducto;
                                tblDetalle.setValueAt(codigo, fila, 4);
                            }
                        }
                    }

                    //////CAMBIA EL IMPORTE TOTAL
                    if (verVenta_interna == 0) {
                        if (columna == 8) {
                            if (afectacion.equalsIgnoreCase("GRA")) {
                                AlertaError alerta = new AlertaError("Informe", "Un producto GRATUTIO no puede ser editado.");
                                alerta.setVisible(true);
                            } else {
                                new Form_CambioNumTPV(null, true).setVisible(true);
                                if (Form_CambioNumTPV.procesado == 0) {
                                    AlertaInfo alerta = new AlertaInfo("Informe", "Cambio de Precio Unitario no Válida.");
                                    alerta.setVisible(true);
                                } else {
                                    double cantidad_actual = Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(fila, 1)));
                                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar(Form_CambioNumTPV.valorCambio), fila, 8);

                                    double nuevo_precioUnitario = 0.0;
                                    double montoTributo = 0.0;
                                    ////CALCULAMOS EL MONTO DE TRIBUTO IGV
                                    if (afectacion.equalsIgnoreCase("IGV")) {
                                        nuevo_precioUnitario = ((Form_CambioNumTPV.valorCambio / cantidad_actual) / 1.18);
                                        montoTributo = ((((Form_CambioNumTPV.valorCambio / cantidad_actual) / 1.18) * 0.18) * cantidad_actual);
                                    } else {
                                        nuevo_precioUnitario = ((Form_CambioNumTPV.valorCambio / cantidad_actual));
                                    }
                                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar4Decimales(montoTributo), fila, 7);

                                    BigDecimal bd_precioUnitario = new BigDecimal(nuevo_precioUnitario).setScale(4, RoundingMode.HALF_EVEN);
                                    double Gen_precioUnitario = bd_precioUnitario.doubleValue();
                                    tblDetalle.setValueAt("" + Gen_precioUnitario, fila, 5);
                                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar4Decimales((Form_CambioNumTPV.valorCambio / cantidad_actual)), fila, 9);
//                               
                                    actualizarTotales();
                                    AlertaBien alerta = new AlertaBien("Informe", "Actualización Exitosa.");
                                    alerta.setVisible(true);
                                }
                            }
                        }
                    }

                }
            }
        }
        );
    }

    private void cargarPresupuesto(String id_Presupuesto) {
        PresupuestoImpl.buscarPresupuesto("select * from Presupuesto_view where id_Presupuesto='" + id_Presupuesto + "' ");

        // mostrando datos del cliente 
        String numeroDocumento = PresupuestoImpl.getPresupuesto().getNumeroDocumento_cliente();
        if (numeroDocumento.length() == 8 || numeroDocumento.length() == 0 || numeroDocumento.length() == 11) {
            String id_cliente = "" + PresupuestoImpl.getPresupuesto().getIdCliente();
            String tipoDocumento = PresupuestoImpl.getPresupuesto().getTipoDocumento_cliente();
            String nombreRazonSocial = PresupuestoImpl.getPresupuesto().getCliente();
            String direccion = PresupuestoImpl.getPresupuesto().getDireccion_cliente();
            String correo_envio = PresupuestoImpl.getPresupuesto().getCorreo_envios();
            cboTipodocumento.setSelectedItem(tipoDocumento);
            txtNumeroDocumento.setText(numeroDocumento);
            txtNombresCompleto.setText(nombreRazonSocial);
            txtDireccion.setText(direccion);
            lblIdCliente.setText(id_cliente);
            if (correo_envio.equalsIgnoreCase("-")) {
                correo_envio = "";
            }
            txtCorreo.setText(correo_envio);
        } else {
            consultarCliente(1);
            Metodos.MensajeError("ASIGNE UN NÚMERO DE RUC PARA SU FACTURACIÓN.");
        }

        ///cargamos datos del comprobante
        String medioPago = PresupuestoImpl.getPresupuesto().getMedioPago();
        String moneda = PresupuestoImpl.getPresupuesto().getMoneda();
//        String fecha = PresupuestoImpl.getPresupuesto().getFecha();

        cboMedioPago.setSelectedItem(medioPago);
        cboMoneda.setSelectedItem(moneda);
//        txtFechaEmision.setDate(fec.StringADate(fec.convertirFecha(fecha)));

        lista_ObPresupuesto_det(id_Presupuesto);
    }

    private void lista_ObPresupuesto_det(String id_Presupuesto) {
        lista_ObPresupuesto_det = (ArrayList<ObPresupuesto_det>) PresupuestoImpl.listarPresupuestos_Det("select * from presupuestodet where idPresupuesto='" + id_Presupuesto + "'");
        dtmDetalle = (DefaultTableModel) tblDetalle.getModel();
        Object[] fila = new Object[11];
        for (int i = 0; i < lista_ObPresupuesto_det.size(); i++) {

            int item = lista_ObPresupuesto_det.get(i).getItem();
            double cantidad = lista_ObPresupuesto_det.get(i).getCantidad();
            String tipoUnidad = lista_ObPresupuesto_det.get(i).getTipoUnidad();
            String codigo = lista_ObPresupuesto_det.get(i).getCodigo();
            String descripcion = lista_ObPresupuesto_det.get(i).getDescripcion();
            String tributo = lista_ObPresupuesto_det.get(i).getTributo();
            double precioUnitario = 0;
            if (tributo.equalsIgnoreCase("GRA")) {
                precioUnitario = lista_ObPresupuesto_det.get(i).getValorUnitarioGratuito();
            } else {
                precioUnitario = lista_ObPresupuesto_det.get(i).getValorUnitario();
            }
            double montoTributo = lista_ObPresupuesto_det.get(i).getMontoTributo();
            double precioUnitarioItem = lista_ObPresupuesto_det.get(i).getPrecioUnitarioItem();
            double precio = lista_ObPresupuesto_det.get(i).getPrecio();

            fila[0] = item;
            fila[1] = Metodos.formatoDecimalMostrar(cantidad);
            fila[2] = tipoUnidad;
            fila[3] = codigo;
            fila[4] = descripcion;
            fila[5] = Metodos.formatoDecimalMostrar4Decimales(precioUnitario);
            fila[6] = tributo;
            fila[7] = Metodos.formatoDecimalMostrar4Decimales(montoTributo);
            fila[8] = Metodos.formatoDecimalMostrar(precioUnitarioItem);
            fila[9] = Metodos.formatoDecimalMostrar(precio);

            dtmDetalle.addRow(fila);
        }
        tblDetalle.setModel(dtmDetalle);
        actualizarTotales();

        lblIdProducto.setText("0");
        // activo botones
        btnQuitar.setEnabled(true);
        btnCrearArchivosPlanos.setEnabled(true);

    }

    private void consultarCliente(int id_cliente) {
        ClienteImpl.buscarCliente("select * from cliente where id='" + id_cliente + "' ");
        String tipoDocumento = ClienteImpl.getCliente().getTipoDocumento();
        String numeroDocumento = ClienteImpl.getCliente().getNumeroDocumento();
        String nombreRazonSocial = ClienteImpl.getCliente().getNombreRazonSocial();
        String direccion = ClienteImpl.getCliente().getDireccion();
        String correo_envio = ClienteImpl.getCliente().getCorreo_envios();
        // mostrando datos
        lblIdCliente.setText("" + id_cliente);
        cboTipodocumento.setSelectedItem(tipoDocumento);
        txtNumeroDocumento.setText(numeroDocumento);
        txtNombresCompleto.setText(nombreRazonSocial);
        txtDireccion.setText(direccion);
        if (correo_envio.equalsIgnoreCase("-")) {
            correo_envio = "";
        }
        txtCorreo.setText(correo_envio);
    }

    private void cargarVenta_interna(String id_venta_interna) {
        Venta_internaImpl.buscarVenta_interna("select * from venta_interna_view where id_venta_interna='" + id_venta_interna + "' ");
        // mostrando datos del cliente
        String id_cliente = "" + Venta_internaImpl.getVenta_interna().getIdCliente();
        String tipoDocumento = Venta_internaImpl.getVenta_interna().getTipoDocumento_cliente();
        String numeroDocumento = Venta_internaImpl.getVenta_interna().getNumeroDocumento_cliente();
        String nombreRazonSocial = Venta_internaImpl.getVenta_interna().getCliente();
        String direccion = Venta_internaImpl.getVenta_interna().getDireccion_cliente();
        String correo_envio = Venta_internaImpl.getVenta_interna().getCorreo_envios();
        cboTipodocumento.setSelectedItem(tipoDocumento);
        txtNumeroDocumento.setText(numeroDocumento);
        txtNombresCompleto.setText(nombreRazonSocial);
        txtDireccion.setText(direccion);
        lblIdCliente.setText(id_cliente);
        if (correo_envio.equalsIgnoreCase("-")) {
            correo_envio = "";
        }
        txtCorreo.setText(correo_envio);

        ///cargamos datos del comprobante
        String medioPago = Venta_internaImpl.getVenta_interna().getMedioPago();
        String moneda = Venta_internaImpl.getVenta_interna().getMoneda();
        String fecha = Venta_internaImpl.getVenta_interna().getFecha();
        String fecha_vencimiento = Venta_internaImpl.getVenta_interna().getFechaVencimiento();

        txtIdComprobante.setText(id_venta_interna);
        cboMedioPago.setSelectedItem(medioPago);
        cboMoneda.setSelectedItem(moneda);
        txtFechaEmision.setDate(fec.StringADate(fec.convertirFecha(fecha)));
        txtFechaVencimiento.setDate(fec.StringADate(fec.convertirFecha(fecha_vencimiento)));

        lista_ObVenta_interna_det(id_venta_interna);
    }

    private void lista_ObVenta_interna_det(String id_venta_interna) {
        lista_ObVenta_interna_det = (ArrayList<ObVenta_interna_det>) Venta_internaImpl.listarVenta_internas_Det("select * from venta_internadet where idVenta_interna='" + id_venta_interna + "'");
        dtmDetalle = (DefaultTableModel) tblDetalle.getModel();
        Object[] fila = new Object[11];
        for (int i = 0; i < lista_ObVenta_interna_det.size(); i++) {

            int item = lista_ObVenta_interna_det.get(i).getItem();
            double cantidad = lista_ObVenta_interna_det.get(i).getCantidad();
            String tipoUnidad = lista_ObVenta_interna_det.get(i).getTipoUnidad();
            String codigo = lista_ObVenta_interna_det.get(i).getCodigo();
            String descripcion = lista_ObVenta_interna_det.get(i).getDescripcion();
            String tributo = lista_ObVenta_interna_det.get(i).getTributo();
            double precioUnitario = 0;
            if (tributo.equalsIgnoreCase("GRA")) {
                precioUnitario = lista_ObVenta_interna_det.get(i).getValorUnitarioGratuito();
            } else {
                precioUnitario = lista_ObVenta_interna_det.get(i).getValorUnitario();
            }
            double montoTributo = lista_ObVenta_interna_det.get(i).getMontoTributo();
            double precioUnitarioItem = lista_ObVenta_interna_det.get(i).getPrecioUnitarioItem();
            double precio = lista_ObVenta_interna_det.get(i).getPrecio();

            fila[0] = item;
            fila[1] = Metodos.formatoDecimalMostrar(cantidad);
            fila[2] = tipoUnidad;
            fila[3] = codigo;
            fila[4] = descripcion;
            fila[5] = Metodos.formatoDecimalMostrar4Decimales(precioUnitario);
            fila[6] = tributo;
            fila[7] = Metodos.formatoDecimalMostrar4Decimales(montoTributo);
            fila[8] = Metodos.formatoDecimalMostrar(precioUnitarioItem);
            fila[9] = Metodos.formatoDecimalMostrar(precio);

            dtmDetalle.addRow(fila);
        }
        tblDetalle.setModel(dtmDetalle);
        actualizarTotales();

        lblIdProducto.setText("0");
        // activo botones
        btnQuitar.setEnabled(true);
        btnCrearArchivosPlanos.setEnabled(true);

    }

    private void cargarIdVenta_interna() {
        ///////////OBTIENE LA SERIE CONFIGURADA
        String nombreVenta_interna = Venta_internaImpl.obtenerCadena("Select serieVenta_interna from config");
        try {
            /////GENERA NUEVO COMPROBANTE
            String Id = Venta_internaImpl.obtenerCadena("Select max(id) from venta_interna");
            if (Id != null) {
                String numero_venta_interna = Metodos.ObtenerUltimosXCaracteres(Id, 8);
                int numero = Integer.parseInt(numero_venta_interna) + 1;
                String numeroVenta_interna = nombreVenta_interna + "-" + String.format("%08d", numero);
                txtIdComprobante.setText(numeroVenta_interna);
            } else {
                nombreVenta_interna = nombreVenta_interna + "-00000001";
                txtIdComprobante.setText(nombreVenta_interna);
            }
        } catch (Exception e) {
            System.out.println("Error generando id de venta_interna: \n" + e);
            Metodos.MensajeError("Error generando id de venta_interna: \n" + e);
        }
    }

    private void cargarProducto(int idProducto, double cantidadCarga) {
        ProductoImpl.buscarProducto(idProducto);
        String codigo = ProductoImpl.getProducto().getCodigo();
        String descripcion = ProductoImpl.getProducto().getDescripcion();
        String afectacion = ProductoImpl.getProducto().getAfectacion();
        String precio = "" + ProductoImpl.getProducto().getPrecio();
        String unidad_medida = ProductoImpl.getProducto().getUnidad_medida();

        lblIdProducto.setText("" + idProducto);
        txtCodigo.setText(codigo);
        txtDescripcion.setText(descripcion);
        cboPrecioUnitario.removeAllItems();
        cboPrecioUnitario.addItem(precio);
        cboAfectacion.setSelectedItem(afectacion);
        cboUnidadMedida.setSelectedItem(Catalogos.unidadMedida("", unidad_medida)[0]);

        agregarDetalle(cantidadCarga);
    }

    private void agregarDetalle(double cantidadCarga) {
        // capturando

        int filas = tblDetalle.getRowCount();
        if (filas > 0) {//si no elije ninguna fila 
            int totalRow = tblDetalle.getRowCount();
            totalRow -= 1;
            int encontro = 0;
            for (int i = totalRow; i >= 0; i--) {
                int idproducto = Integer.parseInt(String.valueOf(tblDetalle.getValueAt(i, 0)));
                if (idproducto == Integer.parseInt(lblIdProducto.getText())) {
                    double cantidad_actual = Double.parseDouble(String.valueOf(tblDetalle.getValueAt(i, 1)));

                    double tributo_actual = (Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(i, 7))) / cantidad_actual);
                    double importe_actual = (Metodos.formatoDecimalOperar(String.valueOf(tblDetalle.getValueAt(i, 8))) / cantidad_actual);
                    double nueva_cantidad = cantidad_actual + 1;

                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar(nueva_cantidad), i, 1);

                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar4Decimales((tributo_actual * nueva_cantidad)), i, 7);
                    tblDetalle.setValueAt(Metodos.formatoDecimalMostrar((importe_actual * nueva_cantidad)), i, 8);

                    encontro = 1;
                }
            }
            ///SI DENTRO DEL DETALLE DE LA VENTA NO ENCUENTRA EL PRODUCTO
            if (encontro == 0) {
                agregarProducto(cantidadCarga);
            }
            /////SI ES EL PRIMER PRODUCTO AGREGAR AL DETALLE DE LA VENTA
        } else {
            agregarProducto(cantidadCarga);
        }

        // ACTUALIZAMOS LOS TOTALES 
        actualizarTotales();
        txtDescripcion.setText("");
        lblIdProducto.setText("0");
    }

    public void agregarProducto(double cantidadCarga) {

        ////VERIFICAMOS SI USA O NO USA LA CANTIDAD FLOTANTE
        if (Datos.getUsa_cntFlotante().equalsIgnoreCase("SI")) {
            new Form_CambioNumTPV(null, true).setVisible(true);
            if (Form_CambioNumTPV.procesado != 0) {
                txtCantidad.setText("" + Form_CambioNumTPV.valorCambio);
            } else {
                txtDescripcion.setText("");
                lblIdProducto.setText("0");
                Metodos.MensajeAlerta("Proceso de Seleccón de Producto Cancelada.");
                return;
            }
        } else {
            txtCantidad.setText("" + cantidadCarga);
        }

        int incluye_igv = ProductoImpl.obtenerIncluye_igvID(Integer.parseInt(lblIdProducto.getText()));
        double cantidad = Metodos.formatoDecimalOperar(txtCantidad.getText());
        String tipoUnidad = cboUnidadMedida.getSelectedItem().toString();
        double precioUnitario = Double.parseDouble((cboPrecioUnitario.getSelectedItem().toString()));

        String codigo = txtCodigo.getText();
        String descripcion = txtDescripcion.getText();
        String tributo = Catalogos.tipoTributo("", cboAfectacion.getSelectedItem().toString(), "", "")[3];
        // calculando
        //String precioTotal = Metodos.FormatoDecimalOperar(Double.parseDouble(cantidad) * Double.parseDouble(precioUnitario));
        double precioTotal = 0.00;
        double montoTributo = 0.00;
        double importeTotal = 0;

        switch (tributo) {
            case "IGV":
                precioTotal = cantidad * precioUnitario;
                montoTributo = precioTotal * 0.18;
                break;
            case "GRA":
                precioTotal = 0.00;
                montoTributo = 0.00;
                break;
            case "EXO":
                precioTotal = cantidad * precioUnitario;
                montoTributo = 0.00;
                break;
            default:
                break;
        }

        if (!tributo.equalsIgnoreCase("GRA")) {
            ////VERIFICAMOS SI EL PRODUCTO NCLUYE EL IGV DENTRO DEL PRECIO
            if (incluye_igv == 1) {
                double nuevo_precioUnitario = ((precioUnitario / 1.18));

                ////CALCULAMOS EL MONTO DE TRIBUTO IGV
                if (tributo.equalsIgnoreCase("IGV")) {
                    montoTributo = ((precioUnitario * cantidad / 1.18) * 0.18);
                }
                importeTotal = ((precioUnitario * cantidad));
                precioUnitario = nuevo_precioUnitario;
            } else {
                importeTotal = precioTotal + montoTributo;
            }
        }

        dtmDetalle = (DefaultTableModel) tblDetalle.getModel();
        Object[] fila = new Object[11];
        fila[0] = lblIdProducto.getText();
        fila[1] = Metodos.formatoDecimalMostrar(cantidad);
        fila[2] = tipoUnidad;
        fila[3] = codigo;
        fila[4] = descripcion;
        BigDecimal bd = new BigDecimal(precioUnitario).setScale(4, RoundingMode.HALF_EVEN);
        double Gen_precioUnitario = bd.doubleValue();
        fila[5] = Gen_precioUnitario;
        fila[6] = tributo;
        fila[7] = Metodos.formatoDecimalMostrar4Decimales(montoTributo);
        fila[8] = Metodos.formatoDecimalMostrar(importeTotal);
        precioUnitario = Double.parseDouble((cboPrecioUnitario.getSelectedItem().toString()));
        fila[9] = Metodos.formatoDecimalMostrar((precioUnitario));

        dtmDetalle.addRow(fila);
        tblDetalle.setModel(dtmDetalle);
        //se limpia los campos
        txtCantidad.setText("");
        cboUnidadMedida.setSelectedIndex(0);
        txtCodigo.setText("");
        txtDescripcion.setText("");
        cboPrecioUnitario.setSelectedItem("");
        cboAfectacion.setSelectedIndex(0);
        // activo botones
        btnQuitar.setEnabled(true);
        btnCrearArchivosPlanos.setEnabled(true);
        lblIdProducto.setText("0");
    }

    private void actualizarTotales() {
        double importe = 0.00;
        double importeTributo = 0.00;
        double importeTotal = 0.00;
        double gratuito = 0.00;
        double exonerado = 0.00;

        int numero_filas = tblDetalle.getRowCount();
        for (int i = 0; i < numero_filas; i++) {
            double cantidadFila = Double.parseDouble(tblDetalle.getModel().getValueAt(i, 1).toString());
            double precioUnitarioFila = Double.parseDouble(tblDetalle.getModel().getValueAt(i, 5).toString());
            String tributoFila = tblDetalle.getModel().getValueAt(i, 6).toString();
            double importeTributoFila = Double.parseDouble(tblDetalle.getModel().getValueAt(i, 7).toString());
            //capturo importeTotal con decimales
            double importeDecimal = Double.parseDouble(tblDetalle.getModel().getValueAt(i, 8).toString());
            // importes depende del tributo 
            switch (tributoFila) {
                case "GRA":
                    gratuito = gratuito + (cantidadFila * precioUnitarioFila);
                    break;
                case "EXO":
                    exonerado = exonerado + (cantidadFila * precioUnitarioFila);
                    break;
                case "IGV":
                    importe = importe + (importeDecimal - importeTributoFila);
                    importeTributo = importeTributo + importeTributoFila;
                    importeTotal = importe + importeTributo;
                    break;
                // inafecto
                default:
                    break;
            }
        }

        //actualizando totales
        txtImporte.setText(Metodos.formatoDecimalMostrar(importe + exonerado));
        txtIgv.setText(Metodos.formatoDecimalMostrar(importeTributo));
        txtImporteTotal.setText(Metodos.formatoDecimalMostrar(importeTotal + exonerado));
        txtTotalGratuito.setText(Metodos.formatoDecimalMostrar(gratuito));
        txtTotalExonerado.setText(Metodos.formatoDecimalMostrar(exonerado));

        // monto en texto
        String moneda = cboMoneda.getSelectedItem().toString();
        String montoEnTexto = Metodos.convertirNumTexto(Metodos.formatoDecimalMostrar(Double.parseDouble(txtImporteTotal.getText())), moneda);
        jlblMontoEnTexto.setText(montoEnTexto);
    }

    private void registrarCliente() {

        String tipoDocumento = cboTipodocumento.getSelectedItem().toString();
        String numeroDocumento = txtNumeroDocumento.getText();
        String nombreRazonSocial = txtNombresCompleto.getText().toUpperCase();
        String direccion = txtDireccion.getText().toUpperCase();
        String correo = txtCorreo.getText();
        String estado = "ACTIVO";
        try {
            ObCliente obj = new ObCliente(0, tipoDocumento, numeroDocumento, nombreRazonSocial, direccion, correo, estado);
            ClienteImpl.Registrar(obj);
            consultaClienteDOCUMENTO(numeroDocumento);
        } catch (Exception e) {
            Metodos.MensajeAlerta("Error en el Registro del Cliente.");
        }
    }

    public String leeFechaEmision() {
        return fec.mostrar_fecha(txtFechaEmision);
    }

    public String leeFechaVencimiento() {
        return fec.mostrar_fecha(txtFechaVencimiento);
    }

    private void procesoVenta_interna(double montoEfectivoVenta, double montoTarjetaVenta, double montoVueltoVenta, double montoDeudaVenta, String descripcionTarjetaVenta) {
        //capturamos los datos a enviar desde el frame
        String id_cpe = txtIdComprobante.getText();
        int idCliente = Integer.parseInt(lblIdCliente.getText());
        String fecha = leeFechaEmision();
        String horaEmision = Metodos.ObtenerHora();
        String fechaVencimiento = leeFechaVencimiento();
        String moneda = cboMoneda.getSelectedItem().toString();
        String medioPago = cboMedioPago.getSelectedItem().toString();
        double totalVentasGravadas = Double.parseDouble(txtImporte.getText());
        double totalGratuito = Double.parseDouble(txtTotalGratuito.getText());
        double totalVentasExoneradas = Double.parseDouble(txtTotalExonerado.getText());
        double igv = Double.parseDouble(txtIgv.getText());
        double totalImporteVenta = Double.parseDouble(txtImporteTotal.getText());

        ///SI EL ID ES NULO - REGISTRA
        if (idVenta_internaEdit.equalsIgnoreCase("")) {
            ObVenta_interna obj = new ObVenta_interna(id_cpe, idCliente, fecha, horaEmision, fechaVencimiento, moneda, medioPago, (totalVentasGravadas - totalVentasExoneradas), totalGratuito,
                    totalVentasExoneradas, igv, totalImporteVenta, "", "GENERADO", "", JFramePrincipal.id_usuario,montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta, 
                    descripcionTarjetaVenta);
            try {
                Venta_internaImpl.Registrar(obj);
            } catch (Exception e) {
                System.out.println("Error registrando venta_interna a la base de datos: \n" + e);
                Metodos.MensajeError("Error registrando venta_interna a la base de datos: \n" + e);
            }

            ///SI EL ID ES DIFERENTE DE NULO - ACTUALIZA
        } else if (!idVenta_internaEdit.equalsIgnoreCase("")) {
            ObVenta_interna obj = new ObVenta_interna(id_cpe, idCliente, fecha, horaEmision, fechaVencimiento, moneda, medioPago, (totalVentasGravadas - totalVentasExoneradas), totalGratuito,
                    totalVentasExoneradas, igv, totalImporteVenta, "", "GENERADO", "", JFramePrincipal.id_usuario,montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta, 
                    descripcionTarjetaVenta);
            try {
                Venta_internaImpl.Actualizar(obj);
            } catch (Exception e) {
                System.out.println("Error registrando venta_interna a la base de datos: \n" + e);
                Metodos.MensajeError("Error registrando venta_interna a la base de datos: \n" + e);
            }
        }
    }

    private void registrarVenta_internaDet() {
        ///SI EL ID ES DIFERENTE DE NULO - ACTUALIZA
        if (!idVenta_internaEdit.equalsIgnoreCase("")) {
            Venta_internaImpl.eliminarVenta_internaDet(idVenta_internaEdit);
            ProductoImpl.eliminar_Kardex(idVenta_internaEdit);
        }
        String id_cpe = txtIdComprobante.getText();
        try {
            int numero = 1;//contador para IdVenta_internaDet
            for (int i = 0; i < tblDetalle.getRowCount(); i++) {
                int item = Integer.parseInt(tblDetalle.getValueAt(i, 0).toString());
                double cantidad = Double.parseDouble(tblDetalle.getValueAt(i, 1).toString());
                String tipoUnidad = tblDetalle.getValueAt(i, 2).toString();
                String codigo = tblDetalle.getValueAt(i, 3).toString();
                String descripcion = tblDetalle.getValueAt(i, 4).toString();
                double precioUnitario = Double.parseDouble(tblDetalle.getValueAt(i, 5).toString());
                String tributo = tblDetalle.getValueAt(i, 6).toString();
                double montoTributo = Double.parseDouble(tblDetalle.getValueAt(i, 7).toString());
                double precioTotal = Double.parseDouble(tblDetalle.getValueAt(i, 8).toString());

                double valorUnitarioGratuito = 0;
                String tipoAfectacionTributo;
                switch (tributo) {
                    case "GRA":
                        tipoAfectacionTributo = "21";
                        valorUnitarioGratuito = Metodos.formatoDecimalOperar("" + (precioUnitario));
                        precioUnitario = 0;
                        break;
                    case "EXO":
                        tipoAfectacionTributo = "20";
                        break;
                    case "IGV":
                        tipoAfectacionTributo = "10";
                        break;
                    default:
                        //INA
                        tipoAfectacionTributo = "30";
                        break;
                }

                String IdVenta_interna = id_cpe + "-" + String.format("%03d", (numero++));
                double precio = Double.parseDouble(tblDetalle.getValueAt(i, 9).toString());

                ObVenta_interna_det obj = new ObVenta_interna_det(IdVenta_interna, id_cpe, item, cantidad, tipoUnidad, codigo, descripcion, tributo, montoTributo, tipoAfectacionTributo,
                        precioUnitario, precioTotal, valorUnitarioGratuito, precio);
                Venta_internaImpl.registrarVenta_internaDet(obj);

                ////KARDEX 
                ProductoImpl.Registrar_Kardex(item, 0, cantidad, id_cpe, "NOTA DE VENTA", leeFechaEmision());
            }
        } catch (Exception e) {
            System.out.println("Error registrando detalle de venta_interna en base de datos: \n" + e);
            Metodos.MensajeError("Error registrando detalle de venta_interna en base de datos: \n" + e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCodigo = new javax.swing.JTextField();
        cboAfectacion = new javax.swing.JComboBox<>();
        txtCantidad = new javax.swing.JTextField();
        lblIdCliente = new javax.swing.JLabel();
        cboPrecioUnitario = new javax.swing.JComboBox<>();
        lblIdProducto = new javax.swing.JLabel();
        txtFechaVencimiento = new com.toedter.calendar.JDateChooser();
        jLabel21 = new javax.swing.JLabel();
        txtTotalExonerado = new javax.swing.JTextField();
        rbtCantidadFlotante = new javax.swing.JCheckBox();
        cboMedioPago = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        cboMoneda = new javax.swing.JComboBox<>();
        txtTotalGratuito = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtDireccion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnBuscarCliente = new javax.swing.JButton();
        cboTipodocumento = new javax.swing.JComboBox<>();
        btnConsultarDocumento = new javax.swing.JButton();
        btnNuevoCliente = new javax.swing.JButton();
        txtNombresCompleto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtNumeroDocumento = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel17 = new javax.swing.JLabel();
        txtCorreo = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        btnNuevoProducto = new javax.swing.JButton();
        btnQuitar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cboUnidadMedida = new javax.swing.JComboBox<>();
        btnBuscarProductoServicio = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtDescripcion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel5 = new javax.swing.JLabel();
        btnCrearArchivosPlanos = new javax.swing.JButton();
        txtIdComprobante = new javax.swing.JTextField();
        txtFechaEmision = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        jlblMontoEnTexto = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtImporte = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtIgv = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtImporteTotal = new javax.swing.JTextField();

        txtCodigo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cboAfectacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "IGV Impuesto General a las Ventas", "Exonerado", "Gratuito" }));
        cboAfectacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboAfectacionActionPerformed(evt);
            }
        });

        txtCantidad.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadKeyTyped(evt);
            }
        });

        lblIdCliente.setText("jLabel8");

        cboPrecioUnitario.setEditable(true);
        cboPrecioUnitario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboPrecioUnitario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cboPrecioUnitarioKeyTyped(evt);
            }
        });

        lblIdProducto.setText("jLabel8");

        txtFechaVencimiento.setEnabled(false);
        txtFechaVencimiento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel21.setText("Operaciones Exoneradas:");

        txtTotalExonerado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtTotalExonerado.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalExonerado.setText("0.00");
        txtTotalExonerado.setEnabled(false);

        rbtCantidadFlotante.setBackground(new java.awt.Color(255, 255, 255));
        rbtCantidadFlotante.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rbtCantidadFlotante.setForeground(new java.awt.Color(204, 0, 0));
        rbtCantidadFlotante.setText("Usar Cantidad Flotante?");
        rbtCantidadFlotante.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtCantidadFlotanteMouseClicked(evt);
            }
        });

        cboMedioPago.setMaximumRowCount(9);
        cboMedioPago.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "EFECTIVO", "DEPÓSITO EN CUENTA", "TARJETA DE DÉBITO", "TARJETA DE CRÉDITO", "OTROS" }));
        cboMedioPago.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel15.setText("Moneda");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Medio de Pago");

        cboMoneda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PEN", "USD" }));
        cboMoneda.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboMoneda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboMonedaActionPerformed(evt);
            }
        });

        txtTotalGratuito.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtTotalGratuito.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalGratuito.setText("0.00");
        txtTotalGratuito.setEnabled(false);
        txtTotalGratuito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalGratuitoActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setText("Operaciones Gratuitas:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Dirección:");

        txtDireccion.setAnchoDeBorde(1.0F);
        txtDireccion.setDescripcion("Ingrese una descripción");
        txtDireccion.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("NOTA DE VENTA  N°");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cabecera:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Tipo Doc.:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Razón Social:");

        btnBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar_16px.png"))); // NOI18N
        btnBuscarCliente.setToolTipText("Buscar un Cliente");
        btnBuscarCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });

        cboTipodocumento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SIN DOCUMENTO", "DNI", "RUC" }));
        cboTipodocumento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTipodocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipodocumentoActionPerformed(evt);
            }
        });

        btnConsultarDocumento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnConsultarDocumento.setToolTipText("Consultar Número de Documento");
        btnConsultarDocumento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnConsultarDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarDocumentoActionPerformed(evt);
            }
        });

        btnNuevoCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevoCliente.setToolTipText("Registrar Nuevo Cliente");
        btnNuevoCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoClienteActionPerformed(evt);
            }
        });

        txtNombresCompleto.setAnchoDeBorde(1.0F);
        txtNombresCompleto.setDescripcion("Ingrese una descripción");
        txtNombresCompleto.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        txtNumeroDocumento.setAnchoDeBorde(1.0F);
        txtNumeroDocumento.setDescripcion("Ingrese un Número de Documento");
        txtNumeroDocumento.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtNumeroDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNumeroDocumentoKeyReleased(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setText("Correo :");

        txtCorreo.setAnchoDeBorde(1.0F);
        txtCorreo.setDescripcion("Ingrese una descripción");
        txtCorreo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cboTipodocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCorreo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txtNombresCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsultarDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cboTipodocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel17)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel3)
                    .addComponent(txtNombresCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detalle:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item", "Cantidad", "U.M.", "Código", "Descripción", "P.U.", "Tributo", "Monto Tributo", "Importe", "Precio"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.setRowHeight(23);
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblDetalle);
        if (tblDetalle.getColumnModel().getColumnCount() > 0) {
            tblDetalle.getColumnModel().getColumn(0).setMinWidth(50);
            tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(50);
            tblDetalle.getColumnModel().getColumn(0).setMaxWidth(50);
            tblDetalle.getColumnModel().getColumn(1).setMinWidth(70);
            tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(70);
            tblDetalle.getColumnModel().getColumn(1).setMaxWidth(70);
            tblDetalle.getColumnModel().getColumn(2).setMinWidth(80);
            tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(80);
            tblDetalle.getColumnModel().getColumn(2).setMaxWidth(80);
            tblDetalle.getColumnModel().getColumn(3).setMinWidth(100);
            tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblDetalle.getColumnModel().getColumn(3).setMaxWidth(100);
            tblDetalle.getColumnModel().getColumn(4).setMinWidth(300);
            tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(300);
            tblDetalle.getColumnModel().getColumn(4).setMaxWidth(300);
            tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(100);
            tblDetalle.getColumnModel().getColumn(6).setPreferredWidth(85);
            tblDetalle.getColumnModel().getColumn(7).setPreferredWidth(140);
            tblDetalle.getColumnModel().getColumn(8).setPreferredWidth(100);
            tblDetalle.getColumnModel().getColumn(9).setPreferredWidth(80);
            tblDetalle.getColumnModel().getColumn(9).setMaxWidth(80);
        }

        btnNuevoProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevoProducto.setToolTipText("Registrar Nuevo Producto");
        btnNuevoProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevoProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoProductoActionPerformed(evt);
            }
        });

        btnQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/EliminarTabla_40px.png"))); // NOI18N
        btnQuitar.setToolTipText("Quitar Producto del Listado");
        btnQuitar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnQuitar.setEnabled(false);
        btnQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Descripción:");

        cboUnidadMedida.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboUnidadMedida.setEnabled(false);

        btnBuscarProductoServicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar_16px.png"))); // NOI18N
        btnBuscarProductoServicio.setToolTipText("Buscar un Producto");
        btnBuscarProductoServicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBuscarProductoServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoServicioActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(204, 0, 0));
        jLabel8.setText("Doble Clik sobre la CANTIDAD o IMPORTE para poder editar el campo.");

        txtDescripcion.setAnchoDeBorde(1.0F);
        txtDescripcion.setDescripcion("Búsqueda por Código de Barra");
        txtDescripcion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 909, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(10, 10, 10)
                        .addComponent(txtDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboUnidadMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscarProductoServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnQuitar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscarProductoServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(btnQuitar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnNuevoProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cboUnidadMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("EMISIÓN : ");

        btnCrearArchivosPlanos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCrearArchivosPlanos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnCrearArchivosPlanos.setText("Registrar Comprobante");
        btnCrearArchivosPlanos.setEnabled(false);
        btnCrearArchivosPlanos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearArchivosPlanosActionPerformed(evt);
            }
        });

        txtIdComprobante.setEditable(false);
        txtIdComprobante.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        txtFechaEmision.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        jButton1.setToolTipText("Ver Listado de Facturas");
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel13.setText("SON:");

        jlblMontoEnTexto.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jlblMontoEnTexto.setText("?");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel10.setText("Importe:");

        txtImporte.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtImporte.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporte.setText("0.00");
        txtImporte.setEnabled(false);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setText("IGV:");

        txtIgv.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtIgv.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtIgv.setText("0.00");
        txtIgv.setEnabled(false);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel12.setText("Importe Total:");

        txtImporteTotal.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotal.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotal.setText("0.00");
        txtImporteTotal.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIdComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(101, 101, 101)
                                        .addComponent(jLabel12))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlblMontoEnTexto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(35, 35, 35)
                        .addComponent(btnCrearArchivosPlanos)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtIdComprobante, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFechaEmision, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblMontoEnTexto)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnCrearArchivosPlanos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarActionPerformed
        int fila = tblDetalle.getSelectedRow();
        if (fila >= 0) {//si hay fila seleccionada
            Metodos.QuitarVariosItem(tblDetalle, dtmDetalle);
            actualizarTotales();
            //verifica que haya datos en el jtable
            if (tblDetalle.getRowCount() == 0) {
                //desactiva botones
                btnCrearArchivosPlanos.setEnabled(false);
                btnQuitar.setEnabled(false);
                //limpia los campos totales
                txtImporte.setText("0.00");
                txtIgv.setText("0.00");
                txtImporteTotal.setText("0.00");
                jlblMontoEnTexto.setText("?");
            }
            lblIdProducto.setText("0");
        } else {//no hay fila seleccionada
            lblIdProducto.setText("0");
            Metodos.MensajeAlerta("Seleccione un detalle");
        }
    }//GEN-LAST:event_btnQuitarActionPerformed

    private void btnCrearArchivosPlanosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearArchivosPlanosActionPerformed

        if (txtFechaEmision.getDate() == null) {
            txtFechaEmision.requestFocus();
            Metodos.MensajeError("Seleccione una Fecha válida para su Registro.");
            return;
        }

        if (!txtCorreo.getText().equalsIgnoreCase("") && !lblIdCliente.getText().equalsIgnoreCase("1")) {
            /////SI DETECTA CORREO VÁLIDO ACTUALIZA
            ClienteImpl.actualizarCorreo(Integer.parseInt(lblIdCliente.getText()), txtCorreo.getText());
        }

        int filas = tblDetalle.getRowCount();
        if (filas <= 0) {//si no elije ninguna fila  
            btnBuscarProductoServicio.requestFocusInWindow();
            Metodos.MensajeError("Ingrese como Mínimo un Detalle de Producto.");
            return;
        }

        if (txtNombresCompleto.getText().equalsIgnoreCase("")) {
            txtNumeroDocumento.requestFocus();
            Metodos.MensajeError("Ingrese un Cliente Válido");
            return;
        }

        if (txtNumeroDocumento.getText().length() == 0) {
            String clienteVal = Metodos.validarCamposIniciales(txtNombresCompleto.getText().toUpperCase());
            if (!ClienteImpl.validar("Select * from cliente where nombreRazonSocial='" + clienteVal + "' and tipoDocumento='SIN DOCUMENTO'  and estado='ACTIVO'")) {
                registrarCliente();
                int id = ClienteImpl.obtenerEntero("Select max(id) from cliente;");
                lblIdCliente.setText("" + id);
            } else {
                int id_clienteRegistro = ClienteImpl.obtenerEntero("Select id from cliente where nombreRazonSocial='" + clienteVal + "' and tipoDocumento='SIN DOCUMENTO' and estado='ACTIVO'");
                lblIdCliente.setText("" + id_clienteRegistro);
            }

        } else {
            if (txtNumeroDocumento.getText().length() != 8) {
                if (txtNumeroDocumento.getText().length() != 11) {
                    Metodos.MensajeError("Verifique el Tipo de Documento ingresado");
                    txtNumeroDocumento.requestFocus();
                    return;
                }
            }
        }

        if (!txtDireccion.getText().equalsIgnoreCase("") || !txtDireccion.getText().equalsIgnoreCase("-") || !txtDireccion.getText().equalsIgnoreCase("--")) {
            /////SI DETECTA CORREO VÁLIDO ACTUALIZA
            ClienteImpl.actualizarDireccion(Integer.parseInt(lblIdCliente.getText()), txtDireccion.getText().toUpperCase());
        }

        if (lblIdCliente.getText().equalsIgnoreCase("") || lblIdCliente.getText().equalsIgnoreCase("0")) {
            txtNumeroDocumento.requestFocus();
            Metodos.MensajeError("Ingrese y/o Seleccione un Cliente Válido");
            return;
        }

        //////PAGO DE VENTAS 
        new Pago_Venta_int(null, true, txtIdComprobante.getText(), txtFechaEmision.getDate(), Double.parseDouble(txtImporteTotal.getText())).setVisible(true);
        if (Pago_Venta_int.respuesta == 0) {
            AlertaError alert = new AlertaError("Mensaje", "Proceso de Pago Cancelado. Continúe Editando.");
            alert.setVisible(true);
            return;
        } else {

            montoEfectivoVenta = Pago_Venta_int.montoEfectivo;
            montoTarjetaVenta = Pago_Venta_int.montoTarjeta;
            montoVueltoVenta = Pago_Venta_int.montoVuelto;
            montoDeudaVenta = Pago_Venta_int.montoDeuda;
            descripcionTarjetaVenta = Pago_Venta_int.descripcionTarjeta;

            int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                    "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                    new Object[]{" Registrar Documento ", " Cancelar Registro "}, " Cancelar Registro ");
            if (seleccion == 0) {
                //crea comprobante y ap
                procesoVenta_interna(montoEfectivoVenta, montoTarjetaVenta, montoVueltoVenta, montoDeudaVenta, descripcionTarjetaVenta);
                registrarVenta_internaDet();

                if (Datos.getUsar_Impresion_directa().equalsIgnoreCase("SI")) {
                    ComprobantesImpl.comprobantes_impresion("Venta_interna", txtIdComprobante.getText(), Datos.getImpresion(), 0, leeFechaEmision());
                } else {
                    ////LLAMAMOS AL MODAL DE IMPRESIÓN 
                    new Imp_Comprobantes(null, true, txtIdComprobante.getText(), "Venta_interna").setVisible(true);
                }

                if (!idPresupuesto.equalsIgnoreCase("")) {
                    PresupuestoImpl.actualizarDatosPresupuesto("comprobante_relacionado", txtIdComprobante.getText(), idPresupuesto);
                    PresupuestoImpl.actualizarDatosPresupuesto("estado_comprobante", "GENERADO", idPresupuesto);
                }

                JPanelVenta_interna jpfn = new JPanelVenta_interna("", 0, "", "");
                Metodos.CambiarPanel(jpfn);

            } else if (seleccion == 1) {
                Metodos.MensajeAlerta("Continúe con el Registro del Comprobante.");
            }
        }

    }//GEN-LAST:event_btnCrearArchivosPlanosActionPerformed

    private void txtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyTyped

    }//GEN-LAST:event_txtCantidadKeyTyped

    private void cboPrecioUnitarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cboPrecioUnitarioKeyTyped

    }//GEN-LAST:event_cboPrecioUnitarioKeyTyped

    private void btnBuscarProductoServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoServicioActionPerformed
        buscarProducto("");
    }//GEN-LAST:event_btnBuscarProductoServicioActionPerformed

    public void buscarProducto(String valor_busqueda) {
        lblIdProducto.setText("0");
        new JDialogBuscarProducto(null, true, valor_busqueda, "").setVisible(true);;
        if (JDialogBuscarProducto.id != 0) {
            cargarProducto(JDialogBuscarProducto.id, 1.0);
        } else {
            txtDescripcion.setText("");
            lblIdProducto.setText("0");
            Metodos.MensajeAlerta("Producto/Servicio no Seleccionado. Operación Cancelada.");
        }
    }

    public int consultaClienteDOCUMENTO(String numeroDoc) {
        int valor = 0;
        try {
            String consulta = "select * from cliente where numeroDocumento='" + numeroDoc + "';";
            if (ClienteImpl.validar(consulta)) {
                ClienteImpl.buscarCliente(consulta);
                int id_cliente = ClienteImpl.getCliente().getId();
                String tipoDocumento = ClienteImpl.getCliente().getTipoDocumento();
                String numeroDocumento = ClienteImpl.getCliente().getNumeroDocumento();
                String nombreRazonSocial = ClienteImpl.getCliente().getNombreRazonSocial();
                String direccion = ClienteImpl.getCliente().getDireccion();
                String correo = ClienteImpl.getCliente().getCorreo_envios();
                // mostrando datos
                lblIdCliente.setText("" + id_cliente);
                cboTipodocumento.setSelectedItem(tipoDocumento);
                txtNumeroDocumento.setText(numeroDocumento);
                txtNombresCompleto.setText(nombreRazonSocial);
                txtDireccion.setText(direccion);
                if (correo.equalsIgnoreCase("-")) {
                    correo = "";
                }
                txtCorreo.setText(correo);
                valor = 1;
            } else {
                valor = 0;
            }
        } catch (Exception e) {
            valor = 0;
        }
        return valor;
    }
    private void cboAfectacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboAfectacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboAfectacionActionPerformed

    private void cboTipodocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipodocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboTipodocumentoActionPerformed

    private void txtTotalGratuitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalGratuitoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalGratuitoActionPerformed

    public void consultarDatos() {
        String numeroDoc = txtNumeroDocumento.getText();
        if (numeroDoc.length() == 11 || numeroDoc.length() == 8) {

            if (numeroDoc.length() == 11) {
                cboTipodocumento.setSelectedIndex(2);
            } else if (numeroDoc.length() == 8) {
                cboTipodocumento.setSelectedIndex(1);
            }

            int pregunta = JOptionPane.showConfirmDialog(null, "¿Desea realizar la Búsqueda del Cliente Ingresado?\n"
                    + "El número de Documento es: " + txtNumeroDocumento.getText(), "Mensaje", JOptionPane.OK_CANCEL_OPTION);
            if (pregunta == JOptionPane.OK_OPTION) {
                ///para buscar primero limpiamos los campos restantes al num. de documento
                lblIdCliente.setText("0");
                txtNombresCompleto.setText("");
                txtDireccion.setText("");
                txtCorreo.setText("");

                ////VALIDAMOS SI EL CLIENTE YA EXISTE
                if (consultaClienteDOCUMENTO(txtNumeroDocumento.getText()) == 1) {
                    Metodos.MensajeInformacion("Cliente Encontrado");
                } else {
                    String rpt = Control_ConsultaDatos.consultaDatos(txtNombresCompleto, txtDireccion, txtNumeroDocumento, cboTipodocumento);
                    if (!rpt.equalsIgnoreCase("SI")) {
                        Metodos.MensajeError("Intente nuevamente realizar la búsqueda de Documento");
                    } else {
                        if (consultaClienteDOCUMENTO(numeroDoc) == 1) {
                            Metodos.MensajeInformacion("Cliente Encontrado");
                        } else {
                            if (txtNombresCompleto.getText().equalsIgnoreCase("")) {
                                Metodos.MensajeError("Intente nuevamente realizar la búsqueda de Documento");
                            } else {
                                int preguntaRegistro = JOptionPane.showConfirmDialog(null, "¿Desea realizar el Registro del Cliente Consultado?\n"
                                        + "Cliente: " + txtNombresCompleto.getText(), "Mensaje", JOptionPane.OK_CANCEL_OPTION);
                                if (preguntaRegistro == JOptionPane.OK_OPTION) {
                                    registrarCliente();
                                } else {
                                    lblIdCliente.setText("0");
                                    txtNumeroDocumento.setText("");
                                    txtNombresCompleto.setText("");
                                    txtDireccion.setText("");
                                    txtCorreo.setText("");
                                    Metodos.MensajeAlerta("Operación Cancelada.");
                                }
                            }

                        }
                    }
                }
            }
        } else if (numeroDoc.length() == 0) {
            cboTipodocumento.setSelectedIndex(0);
            consultarCliente(1);
        } else {
            Metodos.MensajeError("Verifique el Documento de Identidad Ingresado");
        }

    }
    private void btnConsultarDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarDocumentoActionPerformed
        consultarDatos();
    }//GEN-LAST:event_btnConsultarDocumentoActionPerformed

    private void btnNuevoProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoProductoActionPerformed

        new JDialogProducto(null, true, 0, 0, "").setVisible(true);
        if (JDialogProducto.procesado == 1) {
            int id = ProductoImpl.obtenerEntero("Select max(id) from producto;");
            cargarProducto(id, 1.0);
        } else {
            AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
            alert.setVisible(true);
        }
    }//GEN-LAST:event_btnNuevoProductoActionPerformed

    private void btnNuevoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoClienteActionPerformed

        new JDialogCliente(null, true, 0, "FACTURA").setVisible(true);
        if (JDialogCliente.procesado == 1) {
            int id = ClienteImpl.obtenerEntero("Select max(id) from cliente;");
            consultarCliente(id);
        } else {
            AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
            alert.setVisible(true);
        }
    }//GEN-LAST:event_btnNuevoClienteActionPerformed

    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        new JDialogBuscarCliente(null, true, "TODOS").setVisible(true);
        if (JDialogBuscarCliente.id != 0) {
            consultarCliente(JDialogBuscarCliente.id);
            Metodos.MensajeInformacion("Cliente Seleccionado Exitosamente.");
        } else {
            lblIdCliente.setText("0");
            Metodos.MensajeAlerta("Cliente no Seleccionado. Operación Cancelada.");
        }
    }//GEN-LAST:event_btnBuscarClienteActionPerformed

    private void cboMonedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboMonedaActionPerformed
        actualizarTotales();
    }//GEN-LAST:event_cboMonedaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JPanelVenta_internas jpc = new JPanelVenta_internas();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtNumeroDocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumeroDocumentoKeyReleased

        int longitud = txtNumeroDocumento.getText().length();
        if (longitud == 11 || longitud == 8) {
            if (consultaClienteDOCUMENTO(txtNumeroDocumento.getText()) == 1) {
                Metodos.MensajeInformacion("Cliente Encontrado");
            } else {
                lblIdCliente.setText("0");
                cboTipodocumento.setSelectedIndex(0);
                txtNombresCompleto.setText("");
                txtDireccion.setText("");
                txtCorreo.setText("");
            }
        } else {
            lblIdCliente.setText("0");
            cboTipodocumento.setSelectedIndex(0);
            txtNombresCompleto.setText("");
            txtDireccion.setText("");
            txtCorreo.setText("");
            if (longitud == 0) {
                consultarCliente(1);
            }
        }
    }//GEN-LAST:event_txtNumeroDocumentoKeyReleased

    private void rbtCantidadFlotanteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtCantidadFlotanteMouseClicked
        if (rbtCantidadFlotante.isSelected()) {
            ConfigImpl.actualizarDatosConfig("config", "1", "usa_cnt_flotante", "SI");
            JFramePrincipal.usa_cnt_flotante.setText("SI");
        } else {
            ConfigImpl.actualizarDatosConfig("config", "1", "usa_cnt_flotante", "NO");
            JFramePrincipal.usa_cnt_flotante.setText("NO");
        }
    }//GEN-LAST:event_rbtCantidadFlotanteMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnBuscarProductoServicio;
    private javax.swing.JButton btnConsultarDocumento;
    private javax.swing.JButton btnCrearArchivosPlanos;
    public static javax.swing.JButton btnNuevoCliente;
    public static javax.swing.JButton btnNuevoProducto;
    private javax.swing.JButton btnQuitar;
    public static javax.swing.JComboBox<String> cboAfectacion;
    public static javax.swing.JComboBox<String> cboMedioPago;
    public static javax.swing.JComboBox<String> cboMoneda;
    public static javax.swing.JComboBox<String> cboPrecioUnitario;
    private javax.swing.JComboBox<String> cboTipodocumento;
    public static javax.swing.JComboBox<String> cboUnidadMedida;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jlblMontoEnTexto;
    private javax.swing.JLabel lblIdCliente;
    private javax.swing.JLabel lblIdProducto;
    private javax.swing.JCheckBox rbtCantidadFlotante;
    private javax.swing.JTable tblDetalle;
    public static javax.swing.JTextField txtCantidad;
    public static javax.swing.JTextField txtCodigo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDescripcion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDireccion;
    private com.toedter.calendar.JDateChooser txtFechaEmision;
    private com.toedter.calendar.JDateChooser txtFechaVencimiento;
    private javax.swing.JTextField txtIdComprobante;
    private javax.swing.JTextField txtIgv;
    private javax.swing.JTextField txtImporte;
    private javax.swing.JTextField txtImporteTotal;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNombresCompleto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNumeroDocumento;
    private javax.swing.JTextField txtTotalExonerado;
    private javax.swing.JTextField txtTotalGratuito;
    // End of variables declaration//GEN-END:variables
}
