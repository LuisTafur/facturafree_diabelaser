package factura_free.vista.modales.paneles;
 
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon; 

public class JPanelInicio extends javax.swing.JPanel {

    public static String rol;

    public JPanelInicio(String user_rol) {
        initComponents();
        rol = user_rol;

                
    }
//
    @Override
    public void paint(Graphics g) {
        Dimension d = getSize();
        ImageIcon imagen = new ImageIcon(getClass().getResource("/img/COMPACTFREE.png"));
        g.drawImage(imagen.getImage(), 0, 0, d.width, d.height, null);
        setOpaque(false);
        super.paint(g);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGap(0, 932, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 516, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
