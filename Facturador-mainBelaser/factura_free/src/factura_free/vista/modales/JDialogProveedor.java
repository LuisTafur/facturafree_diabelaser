package factura_free.vista.modales;

import factura_free.clases.principales.alternos.FormatoTextField;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.ProveedorImpl;
import factura_free.clases.modelo.ObProveedor;
import factura_free.clases.principales.consulta_datos.Control_ConsultaDatos;
import factura_free.vista.alertas.AlertaInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;  
import javax.swing.ImageIcon;

public class JDialogProveedor extends javax.swing.JDialog {

    FormatoTextField JTF = new FormatoTextField();
    public static int id_proveedor = 0, procesado = 0;
    private String tipoRegistro = "";

    public JDialogProveedor(java.awt.Frame parent, boolean modal, int id_proveedor) {
        super(parent, modal);
        initComponents();
        configurarVentana();
        procesado = 0;
        this.tipoRegistro = tipoRegistro;
        this.id_proveedor = id_proveedor;
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        if (id_proveedor != 0) {
            cargarProveedor();
        }  

        txtNumeroDocumento.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                consultarDatos();
            }
        });
    }

    private void configurarVentana() {
        //posiciono el frame al centro de la pantalla
        this.setLocationRelativeTo(null);
        //desactiva el cambio de tamaño de la ventana
        this.setResizable(false);
        //asigno titulo a mostrar del frame 
        if (id_proveedor == 0) {
            this.setTitle("REGISTRO DE PROVEEDORES");
            btnRegistrar.setText("Registrar");
        } else {
            this.setTitle("EDITAR PROVEEDOR");
            btnRegistrar.setText("Actualizar");
        }
        //configurar formatos de JTField
        JTF.modelo_1TF(txtNumeroDocumento, 2);
        JTF.modelo_1TF(txtNombresCompleto, 2);
        JTF.modelo_1TF(txtDireccion, 2);
    }

    private void cargarProveedor() {
        ProveedorImpl.buscarProveedor("select * from proveedor where id='" + id_proveedor + "' ");
        String tipoDocumento = ProveedorImpl.getProveedor().getTipoDocumento();
        String numeroDocumento = ProveedorImpl.getProveedor().getNumeroDocumento();
        String nombreRazonSocial = ProveedorImpl.getProveedor().getNombreRazonSocial();
        String direccion = ProveedorImpl.getProveedor().getDireccion();
        String correo = ProveedorImpl.getProveedor().getCorreo_envios();
        cboTipodocumento.setSelectedItem(tipoDocumento);
        txtNumeroDocumento.setText(numeroDocumento);
        txtNombresCompleto.setText(nombreRazonSocial);
        txtDireccion.setText(direccion);
        txtCorreo.setText(correo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbxEstado = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        cboTipodocumento = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnConsulta = new javax.swing.JButton();
        txtDireccion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtNombresCompleto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtNumeroDocumento = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtCorreo = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel11 = new javax.swing.JLabel();

        cbxEstado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ACTIVO", "INACTIVO" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        cboTipodocumento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SIN DOCUMENTO", "DNI", "RUC" }));
        cboTipodocumento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Tipo de Documento *");

        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Número de Documento *");

        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("Nombre / Razón Social *");

        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Dirección");

        btnConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnConsulta.setToolTipText("Consulta Reniec o Sunat dependiendo de su Tipo de Documento Seleccionado");
        btnConsulta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaActionPerformed(evt);
            }
        });

        txtDireccion.setAnchoDeBorde(1.0F);
        txtDireccion.setDescripcion("Ingrese una descripción");
        txtDireccion.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        txtNombresCompleto.setAnchoDeBorde(1.0F);
        txtNombresCompleto.setDescripcion("Ingrese una descripción");
        txtNombresCompleto.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        txtNumeroDocumento.setAnchoDeBorde(1.0F);
        txtNumeroDocumento.setDescripcion("Ingrese una descripción");
        txtNumeroDocumento.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        txtCorreo.setAnchoDeBorde(1.0F);
        txtCorreo.setDescripcion("Ingrese una descripción");
        txtCorreo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Correo");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtNombresCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 127, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cboTipodocumento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCorreo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(0, 0, Short.MAX_VALUE)))))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btnConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboTipodocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtCorreo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombresCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    boolean ExisteProveedor(String numeroDocumento, String nombreRazonSocial) {
        boolean existe = false;
        try {
            String consulta = "select * from proveedor where numeroDocumento = '" + numeroDocumento + "' and "
                    + " nombreRazonSocial='" + nombreRazonSocial + "' and tipoDocumento!='DOC.TRIB.NO.DOM.SIN.RUC' ;";
            existe = ProveedorImpl.validar(consulta);
        } catch (Exception e) {
        }
        return existe;
    }
    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed

        String tipoDocumento = cboTipodocumento.getSelectedItem().toString();
        String numeroDocumento = txtNumeroDocumento.getText().toUpperCase();
        String nombreRazonSocial = txtNombresCompleto.getText().toUpperCase();
        String direccion = txtDireccion.getText().toUpperCase();
        String estado = cbxEstado.getSelectedItem().toString();
        String correo = txtCorreo.getText();

        String mensaje = Metodos.validarPersona(tipoDocumento, numeroDocumento,
                nombreRazonSocial, direccion);

        if (mensaje.equals("")) {

            ///Si el ID del proveedors es 0 REGISTRAMOS
            if (id_proveedor == 0) {

                if (cboTipodocumento.getSelectedItem().toString().equalsIgnoreCase("DOC.TRIB.NO.DOM.SIN.RUC")) {
                    String consulta = "Select nombreRazonSocial from proveedor where nombreRazonSocial='" + nombreRazonSocial + "' and "
                            + " estado='ACTIVO' AND numeroDocumento='" + numeroDocumento + "'  ";
                    if (ProveedorImpl.validar(consulta)) {
                        Metodos.MensajeError("El Proveedor ingresado ya existe.");
                        txtNumeroDocumento.setText("");
                        txtNumeroDocumento.requestFocusInWindow();
                        return;
                    }
                } else {
                    String consulta = "Select nombreRazonSocial from proveedor where nombreRazonSocial='" + nombreRazonSocial + "' and "
                            + " estado='ACTIVO'  AND numeroDocumento='" + numeroDocumento + "'   ";
                    if (ProveedorImpl.validar(consulta)) {
                        Metodos.MensajeError("El Proveedor ingresado ya existe.");
                        txtNumeroDocumento.setText("");
                        txtNumeroDocumento.requestFocusInWindow();
                        return;
                    }
                }

                try {
                    ObProveedor obj = new ObProveedor(0, tipoDocumento, numeroDocumento, nombreRazonSocial, direccion, correo, estado);
                    ProveedorImpl.Registrar(obj);
                    procesado = 1;
                    dispose();
                } catch (Exception e) {
                    Metodos.MensajeError("Error actualizando proveedor:" + e);
                }
                ///Si el ID del proveedors es DIFERENTE DE 0 ACTUALIZAMOS
            } else if (id_proveedor != 0) {

                if (cboTipodocumento.getSelectedItem().toString().equalsIgnoreCase("DOC.TRIB.NO.DOM.SIN.RUC")) {
                    String consulta = "Select nombreRazonSocial from proveedor where id='" + id_proveedor + "'  and "
                            + " nombreRazonSocial='" + nombreRazonSocial + "' and estado='1' "
                            + " AND  numeroDocumento='" + numeroDocumento + "'  ";
                    if (!ProveedorImpl.validar(consulta)) {

                        String consultaCli = "Select nombreRazonSocial from proveedor where "
                                + " nombreRazonSocial='" + nombreRazonSocial + "' and estado='1' "
                                + "  AND numeroDocumento='" + numeroDocumento + "'   ";
                        if (ProveedorImpl.validar(consultaCli)) {
                            Metodos.MensajeError("El Proveedor ingresado ya existe.");
                            txtNumeroDocumento.setText("");
                            txtNumeroDocumento.requestFocusInWindow();
                            return;
                        }
                    }
                } else {
                    String consulta = "Select nombreRazonSocial from proveedor where id='" + id_proveedor + "'  and "
                            + " nombreRazonSocial='" + nombreRazonSocial + "' and estado='1'"
                            + " AND numeroDocumento='" + numeroDocumento + "'   ";
                    if (!ProveedorImpl.validar(consulta)) {

                        String consultaCli = "Select nombreRazonSocial from proveedor where "
                                + " nombreRazonSocial='" + nombreRazonSocial + "' and estado='1' "
                                + "  AND numeroDocumento='" + numeroDocumento + "'   ";
                        if (ProveedorImpl.validar(consultaCli)) {
                            Metodos.MensajeError("El Proveedor ingresado ya existe.");
                            txtNumeroDocumento.setText("");
                            txtNumeroDocumento.requestFocusInWindow();
                            return;
                        }
                    }
                }

                try {
                    ObProveedor obj = new ObProveedor(id_proveedor, tipoDocumento, numeroDocumento, nombreRazonSocial, direccion, correo, estado);
                    ProveedorImpl.Actualizar(obj);
                    procesado = 1;
                    dispose();
                } catch (Exception e) {
                    Metodos.MensajeError("Error registrando proveedor:" + e);
                }
            }
        } else {
            Metodos.MensajeAlerta(mensaje);
        }
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    public void consultarDatos() {

        String numeroDoc = txtNumeroDocumento.getText();
        if (numeroDoc.length() == 8 || numeroDoc.length() == 11) {
            if (numeroDoc.length() == 8) {
                cboTipodocumento.setSelectedIndex(1);
            } else if (numeroDoc.length() == 11) {
                cboTipodocumento.setSelectedIndex(2);
            }
            ////CONSULTA AL SERVIDOR 1
            String rpt = Control_ConsultaDatos.consultaDatos(txtNombresCompleto, txtDireccion, txtNumeroDocumento, cboTipodocumento);
            if (!rpt.equalsIgnoreCase("SI")) {
                AlertaInfo alert = new AlertaInfo("Mensaje", "Intente nuevamente");
                alert.setVisible(true);
            }
        } else {
            AlertaInfo alert = new AlertaInfo("Mensaje", "Verifique el Documento de Identidad Ingresado");
            alert.setVisible(true);
        }
    }
    private void btnConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaActionPerformed
        consultarDatos();
    }//GEN-LAST:event_btnConsultaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogProveedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogProveedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogProveedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogProveedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogProveedor dialog = new JDialogProveedor(new javax.swing.JFrame(), true, 0);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConsulta;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JComboBox<String> cboTipodocumento;
    private javax.swing.JComboBox<String> cbxEstado;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDireccion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNombresCompleto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNumeroDocumento;
    // End of variables declaration//GEN-END:variables
}
