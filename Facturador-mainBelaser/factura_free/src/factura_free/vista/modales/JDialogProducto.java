package factura_free.vista.modales;

import factura_free.clases.principales.alternos.FormatoTextField;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto;
import factura_free.clases.principales.Catalogos; 
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.vista.modales.busquedas.JDialogBuscarProducto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

public class JDialogProducto extends javax.swing.JDialog {

    ArrayList<String> ListaUnidadesMedida = new ArrayList<>();
    FormatoTextField JTF = new FormatoTextField();
    public static int id_producto = 0, procesado = 0, copiar = 0;
    public String concepto_debito = "";

    public JDialogProducto(java.awt.Frame parent, boolean modal, int id_producto, int copiar, String concepto_debito) {
        super(parent, modal);
        initComponents();
        procesado = 0;
        this.id_producto = id_producto;
        this.concepto_debito = concepto_debito;
        this.copiar = copiar;

        ///LLENAMOS LAS UNIDADES DE MEDIDA DESDE EL CATÁLOGO
        Catalogos.ListaUnidadesMedida(cboUnidadMedida, "descripcion");
        rbtIncluyeIgv.setSelected(true);

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());

        btnProductoSimilar.setEnabled(true);
        if (concepto_debito.equalsIgnoreCase("CONCEPTOS NOTAS DE DÉBITO")) {
            cboTipo.setEnabled(false);
            cboTipo.setSelectedItem("CONCEPTOS NOTAS DE DÉBITO");
            btnProductoSimilar.setEnabled(false);
            cboIcbper.setSelectedItem("NO");
            cboIcbper.setEnabled(false);
        } else if (concepto_debito.equalsIgnoreCase("CONCEPTOS NOTAS DE CRÉDITO")) {
            cboTipo.setEnabled(false);
            cboTipo.setSelectedItem("CONCEPTOS NOTAS DE CRÉDITO");
            btnProductoSimilar.setEnabled(false);
            cboIcbper.setSelectedItem("NO");
            cboIcbper.setEnabled(false);
        }

        configurarVentana();
        if (id_producto != 0) {
            cargarProducto(id_producto);
            ///SI EL REGISTRO VIENE DE UN PRODUCTO COPIADO EL ID DE BUSQUEDA QUEDA EN CERO PARA UN REGISTRAR Y NO ACUALIZAR
//            if (copiar == 1) {
//                if (Datos.getUsa_stock().equalsIgnoreCase("NO")) {
//                    txtStock_actual.setEnabled(false);
//                } else {
//                    txtStock_actual.setEnabled(true);
//                }
//            }
        }
    }

    private void configurarVentana() {
        //posiciono el frame al centro de la pantalla
        this.setLocationRelativeTo(null);
        //desactiva el cambio de tamaño de la ventana
        this.setResizable(false);
        //asigno titulo a mostrar del frame 
        if (id_producto == 0) {
            this.setTitle("REGISTRO DE PRODUCTOS");
            btnRegistrar.setText("Registrar");
        } else {
            this.setTitle("EDITAR PRODUCTO");
            btnRegistrar.setText("Actualizar");
        }
        //configurar formatos de JTField
        JTF.modelo_1TF(txtCodigo, 2);
        JTF.modelo_1TF(txtDescripcion, 2);
        JTF.modelo_1TF(txtPrecio, 2);
    }

    private void cargarProducto(int id_producto) {
        ProductoImpl.buscarProducto(id_producto);
        String codigo = ProductoImpl.getProducto().getCodigo();
        String descripcion = ProductoImpl.getProducto().getDescripcion();
        String precio1 = "" + ProductoImpl.getProducto().getPrecio();
        int incluye_igv = ProductoImpl.getProducto().getIncluye_igv();
        String tipo = ProductoImpl.getProducto().getTipo();
        String afectacion = ProductoImpl.getProducto().getAfectacion();
        String unidad_medida = ProductoImpl.getProducto().getUnidad_medida();
        String estado = ProductoImpl.getProducto().getEstado();
        String icbper = ProductoImpl.getProducto().getIcbper();
        String tiene_stock = ProductoImpl.getProducto().getTiene_stock();
        String stock_actual = "" + ProductoImpl.getProducto().getStock_actual();
        String afectacion_compra = ProductoImpl.getProducto().getAfectacion_compra();
        String precio_compra = "" + ProductoImpl.getProducto().getPrecio_compra();

        if (copiar == 1) {
            txtCodigo.setText("");
        } else {
            txtCodigo.setText(codigo);
        }
        txtDescripcion.setText(descripcion);
        txtPrecio.setText(precio1);
        rbtIncluyeIgv.setSelected(false);
        if (incluye_igv == 1) {
            rbtIncluyeIgv.setSelected(true);
        }

        cboIcbper.setSelectedItem(icbper);
        cbxEstado.setSelectedItem(estado);
        cboAfectacion.setSelectedItem(afectacion);
        cboTipo.setSelectedItem(tipo);
        cboUnidadMedida.setSelectedItem(unidad_medida);

        rbtIncluyeIgv.setEnabled(true);
        if (cboAfectacion.getSelectedIndex() == 2) {
            rbtIncluyeIgv.setEnabled(false);
        }

        ////SECCION DE STOCK
        cboTiene_stock.setSelectedItem(tiene_stock);
        cboAfectacionCompra.setSelectedItem(afectacion_compra);
        txtStock_actual.setText(stock_actual);
        txtPrecioCompra.setText(precio_compra);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cbxEstado = new javax.swing.JComboBox<>();
        cboIcbper = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        rbtIncluyeIgv = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtCodigo = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtDescripcion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtPrecio = new org.edisoncor.gui.textField.TextFieldRectBackground();
        cboTipo = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        cboAfectacion = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        btnProductoSimilar = new javax.swing.JButton();
        cboUnidadMedida = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        cboAfectacionCompra = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        cboTiene_stock = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        txtPrecioCompra = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel16 = new javax.swing.JLabel();
        txtStock_actual = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel17 = new javax.swing.JLabel();

        cbxEstado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ACTIVO", "INACTIVO" }));

        cboIcbper.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NO", "SI" }));
        cboIcbper.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("ICBPER");

        rbtIncluyeIgv.setBackground(new java.awt.Color(255, 255, 255));
        rbtIncluyeIgv.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        rbtIncluyeIgv.setText("¿INCLUYE IGV?");
        rbtIncluyeIgv.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rbtIncluyeIgv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtIncluyeIgvActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Código de Producto ");

        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Descripción (*)");

        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Precio Venta (*)");

        txtCodigo.setAnchoDeBorde(1.0F);
        txtCodigo.setDescripcion("Ingrese una descripción");
        txtCodigo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoActionPerformed(evt);
            }
        });

        txtDescripcion.setAnchoDeBorde(1.0F);
        txtDescripcion.setDescripcion("Ingrese una descripción");
        txtDescripcion.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });

        txtPrecio.setAnchoDeBorde(1.0F);
        txtPrecio.setDescripcion("Ingrese un Monto");
        txtPrecio.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioActionPerformed(evt);
            }
        });
        txtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioKeyTyped(evt);
            }
        });

        cboTipo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PRODUCTO", "SERVICIO" }));
        cboTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoActionPerformed(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Tipo (*)");

        cboAfectacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "IGV Impuesto General a las Ventas", "Exonerado", "Gratuito" }));
        cboAfectacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboAfectacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboAfectacionActionPerformed(evt);
            }
        });

        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setText("Afectación Venta (*)");

        btnProductoSimilar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar_16px.png"))); // NOI18N
        btnProductoSimilar.setText("Buscar Productos Similares");
        btnProductoSimilar.setToolTipText("Buscar productos Similares");
        btnProductoSimilar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnProductoSimilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductoSimilarActionPerformed(evt);
            }
        });

        cboUnidadMedida.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel13.setForeground(new java.awt.Color(102, 102, 102));
        jLabel13.setText("Unidad de Medida");

        cboAfectacionCompra.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "IGV Impuesto General a las Ventas", "Exonerado", "Gratuito" }));
        cboAfectacionCompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboAfectacionCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboAfectacionCompraActionPerformed(evt);
            }
        });

        jLabel14.setForeground(new java.awt.Color(102, 102, 102));
        jLabel14.setText("Afectación Compra (*)");

        cboTiene_stock.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cboTiene_stock.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NO", "SI" }));
        cboTiene_stock.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTiene_stock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTiene_stockActionPerformed(evt);
            }
        });

        jLabel15.setForeground(new java.awt.Color(102, 102, 102));
        jLabel15.setText("Tiene Control de Stock?");

        txtPrecioCompra.setAnchoDeBorde(1.0F);
        txtPrecioCompra.setDescripcion("Ingrese un Monto");
        txtPrecioCompra.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtPrecioCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrecioCompraActionPerformed(evt);
            }
        });
        txtPrecioCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioCompraKeyTyped(evt);
            }
        });

        jLabel16.setForeground(new java.awt.Color(102, 102, 102));
        jLabel16.setText("Precio Compra");

        txtStock_actual.setAnchoDeBorde(1.0F);
        txtStock_actual.setDescripcion("Ingrese un Monto");
        txtStock_actual.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtStock_actual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStock_actualActionPerformed(evt);
            }
        });
        txtStock_actual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtStock_actualKeyTyped(evt);
            }
        });

        jLabel17.setForeground(new java.awt.Color(102, 102, 102));
        jLabel17.setText("Stock Actual");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnProductoSimilar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboTiene_stock, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(cboUnidadMedida, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(43, 43, 43)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cboAfectacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cboAfectacionCompra, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtPrecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(6, 6, 6)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPrecioCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtStock_actual, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel11)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnProductoSimilar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboUnidadMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTiene_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtPrecioCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtStock_actual, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cboAfectacion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel14)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cboAfectacionCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed

        String codigo = txtCodigo.getText();
        if (codigo.equals("")) {
            codigo = (Metodos.GenerarCodigoUnico());
        }

        if (txtPrecio.getText().equalsIgnoreCase("")) {
            Metodos.MensajeAlerta("Ingrese un Precio Válido.");
            txtPrecio.requestFocusInWindow();
            return;
        }

        String descripcion = txtDescripcion.getText().toUpperCase();
        double precio1 = Double.parseDouble(txtPrecio.getText());
        String estado = cbxEstado.getSelectedItem().toString();
        String afectacion = cboAfectacion.getSelectedItem().toString();
        String tipo = cboTipo.getSelectedItem().toString();
        String unidad_medida = cboUnidadMedida.getSelectedItem().toString();
        String icbper = cboIcbper.getSelectedItem().toString();
        String tiene_stock = cboTiene_stock.getSelectedItem().toString();

        if (tiene_stock.equalsIgnoreCase("SI")) {

            if (txtPrecioCompra.getText().equalsIgnoreCase("")) {
                txtPrecioCompra.setText("0");
                return;
            }

            if (txtStock_actual.getText().equalsIgnoreCase("")) {
                txtStock_actual.requestFocusInWindow();
                Metodos.MensajeAlerta("Ingrese una Cantidad de Stock válida.");
                return;
            }

        } else {
            txtPrecioCompra.setText("0");
            txtStock_actual.setText("0");
        }

        double stock_actual = Double.parseDouble(txtStock_actual.getText());
        double precio_compra = Double.parseDouble(txtPrecioCompra.getText());
        String afectacion_compra = cboAfectacionCompra.getSelectedItem().toString();

        int incluye_igv = 0;
        if (rbtIncluyeIgv.isSelected()) {
            incluye_igv = 1;
        }

        if (descripcion.equals("")) {
            Metodos.MensajeAlerta("Ingrese una Descripción Válida.");
            txtDescripcion.requestFocusInWindow();
            return;
        }

        if (precio1 <= 0) {
            Metodos.MensajeAlerta("Ingrese un Precio Válido.");
            txtPrecio.requestFocusInWindow();
            return;
        }
        ///SI EL REGISTRO VIENE DE UN PRODUCTO COPIADO EL ID DE BUSQUEDA QUEDA EN CERO PARA UN REGISTRAR Y NO ACUALIZAR
        if (copiar == 1) {
            id_producto = 0;
        }
        ///Si el ID del producto es 0 REGISTRAMOS
        if (id_producto == 0) {

            try {
                String consulta = "Select codigo from producto where descripcion='" + descripcion + "' and estado='ACTIVO';";
                if (ProductoImpl.validar(consulta)) {
                    Metodos.MensajeAlerta("El código de producto " + descripcion + " ya existe.");
                    txtDescripcion.requestFocusInWindow();
                    return;
                }

                ObProducto obj = new ObProducto(0, codigo, descripcion, precio1, incluye_igv, tipo, afectacion, unidad_medida, estado, icbper, "NO", tiene_stock,
                        stock_actual, afectacion_compra, precio_compra);
                ProductoImpl.Registrar(obj);

                if (tiene_stock.equalsIgnoreCase("SI")) {
                    int id_maxProducto = ProductoImpl.obtenerId_Descripcion(descripcion);
                    ProductoImpl.Registrar_Kardex(id_maxProducto, stock_actual, 0, (id_maxProducto + " APERTURA DE STOCK "), "APERTURA",Fecha_Date.retorna_fecha_sistema());
                }

                procesado = 1;
                dispose();
            } catch (SQLException ex) {
                Logger.getLogger(JDialogProducto.class.getName()).log(Level.SEVERE, null, ex);
            }

            ///Si el ID del producto es DIFERENTE DE 0 ACTUALIZAMOS
        } else if (id_producto != 0) {

            try {
                String consulta = "Select codigo from producto where  descripcion='" + descripcion + "' and id='" + id_producto + "'  and estado='ACTIVO' ";
                if (!ProductoImpl.validar(consulta)) {
                    String consulta_reg = "Select codigo from producto where codigo='" + codigo + "' and descripcion='" + descripcion + "' and estado='ACTIVO' ";
                    if (ProductoImpl.validar(consulta_reg)) {
                        Metodos.MensajeAlerta("El código de producto " + descripcion + " ya existe.");
                        txtDescripcion.requestFocusInWindow();
                        return;
                    }
                }

                ObProducto obj = new ObProducto(id_producto, codigo, descripcion, precio1, incluye_igv, tipo, afectacion, unidad_medida, estado, icbper, "NO",
                        tiene_stock, stock_actual, afectacion_compra, precio_compra);
                ProductoImpl.Actualizar(obj);
                procesado = 1;
                dispose();
            } catch (Exception e) {
                Metodos.MensajeError("Error actualizando producto: " + e);
            }

        }

    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioActionPerformed

    private void txtPrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyTyped
        if (!Character.isDigit(evt.getKeyChar()) && evt.getKeyChar() != '.') {
            evt.consume();
        }
        if (evt.getKeyChar() == '.' && txtPrecio.getText().contains(".")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioKeyTyped

    private void cboAfectacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboAfectacionActionPerformed

        rbtIncluyeIgv.setEnabled(true);
        if (cboAfectacion.getSelectedIndex() == 2) {
            rbtIncluyeIgv.setEnabled(false);
            rbtIncluyeIgv.setSelected(false);
        } else if (cboAfectacion.getSelectedIndex() == 1) {
            rbtIncluyeIgv.setEnabled(false);
            rbtIncluyeIgv.setSelected(false);
        }
    }//GEN-LAST:event_cboAfectacionActionPerformed

    private void btnProductoSimilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProductoSimilarActionPerformed
        new JDialogBuscarProducto(null, true, "", "").setVisible(true);
        if (JDialogBuscarProducto.id != 0) {
            cargarProducto(JDialogBuscarProducto.id);
            txtCodigo.setText("");
        } else {
            Metodos.MensajeAlerta("Producto/Servicio no Seleccionado.");
        }
    }//GEN-LAST:event_btnProductoSimilarActionPerformed

    private void cboTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoActionPerformed
        if (id_producto == 0) {
            if (cboTipo.getSelectedIndex() == 0) {
                cboUnidadMedida.setSelectedIndex(0);
                cboIcbper.setSelectedItem("NO");
                cboIcbper.setEnabled(true);
            } else {
                cboUnidadMedida.setSelectedIndex(1);
                cboIcbper.setSelectedItem("NO");
                cboIcbper.setEnabled(false);
            }
        } else {
            if (copiar == 1) {
                if (cboTipo.getSelectedIndex() == 0) {
                    cboUnidadMedida.setSelectedIndex(0);
                    cboIcbper.setSelectedItem("NO");
                    cboIcbper.setEnabled(true);
                } else {
                    cboUnidadMedida.setSelectedIndex(1);
                    cboIcbper.setSelectedItem("NO");
                    cboIcbper.setEnabled(false);
                }
            }
        }
    }//GEN-LAST:event_cboTipoActionPerformed

    private void cboAfectacionCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboAfectacionCompraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboAfectacionCompraActionPerformed

    private void txtPrecioCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioCompraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioCompraActionPerformed

    private void txtPrecioCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioCompraKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrecioCompraKeyTyped

    private void txtStock_actualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStock_actualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStock_actualActionPerformed

    private void txtStock_actualKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStock_actualKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStock_actualKeyTyped

    private void rbtIncluyeIgvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtIncluyeIgvActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtIncluyeIgvActionPerformed

    private void cboTiene_stockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTiene_stockActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboTiene_stockActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogProducto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold> 

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogProducto dialog = new JDialogProducto(new javax.swing.JFrame(), true, 0, 0, "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnProductoSimilar;
    private javax.swing.JButton btnRegistrar;
    public static javax.swing.JComboBox<String> cboAfectacion;
    public static javax.swing.JComboBox<String> cboAfectacionCompra;
    private javax.swing.JComboBox<String> cboIcbper;
    private javax.swing.JComboBox<String> cboTiene_stock;
    private javax.swing.JComboBox<String> cboTipo;
    private javax.swing.JComboBox<String> cboUnidadMedida;
    private javax.swing.JComboBox<String> cbxEstado;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JCheckBox rbtIncluyeIgv;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCodigo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDescripcion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtPrecio;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtPrecioCompra;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtStock_actual;
    // End of variables declaration//GEN-END:variables
}
