/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 */
package factura_free.vista.modales.impresion;

import factura_free.clases.controlador.CompraImpl;
import factura_free.clases.controlador.ComprobantesImpl;
import factura_free.clases.controlador.PresupuestoImpl;
import factura_free.clases.controlador.Venta_internaImpl;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.vista.modales.auxiliares.Form_ClienteCorreo;
import javax.swing.ImageIcon;

public class Imp_Comprobantes extends javax.swing.JDialog {

    Fecha_Date fec = new Fecha_Date();
    public int idcliente = 0, idproveedor = 0;
    private String tipoDocumento = "", forma_pago = "Contado";

    public Imp_Comprobantes(java.awt.Frame parent, boolean modal, String id_comprobante, String tipoDocumento) {
        super(parent, modal);
        initComponents();
        this.tipoDocumento = tipoDocumento;

        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setTitle("Opciones de Impresión");

        cboFormato.setSelectedItem(Datos.getImpresion());
        txtDocumento.setText(id_comprobante);

        String nombreRazonSocial = "";
        String fecha = ""; 
        String correo_envio = "";
        if (tipoDocumento.equalsIgnoreCase("Presupuesto")) {
            PresupuestoImpl.buscarPresupuesto("Select * from presupuesto_view where id_presupuesto='" + id_comprobante + "'");
            // mostrando datos
            idcliente = PresupuestoImpl.getPresupuesto().getIdCliente();
            nombreRazonSocial = PresupuestoImpl.getPresupuesto().getCliente();
            fecha = PresupuestoImpl.getPresupuesto().getFecha(); 
            correo_envio = PresupuestoImpl.getPresupuesto().getCorreo_envios();

        } else if (tipoDocumento.equalsIgnoreCase("Venta_interna")) {
            Venta_internaImpl.buscarVenta_interna("Select * from venta_interna_view where id_venta_interna='" + id_comprobante + "'");
            // mostrando datos
            idcliente = Venta_internaImpl.getVenta_interna().getIdCliente();
            nombreRazonSocial = Venta_internaImpl.getVenta_interna().getCliente();
            fecha = Venta_internaImpl.getVenta_interna().getFecha(); 
            correo_envio = Venta_internaImpl.getVenta_interna().getCorreo_envios();

        } else if (tipoDocumento.equalsIgnoreCase("Compra")) {
            CompraImpl.buscarCompra("Select * from compra_view where id_compra='" + id_comprobante + "'");
            // mostrando datos
            idproveedor = CompraImpl.getCompra().getIdCliente();
            nombreRazonSocial = CompraImpl.getCompra().getCliente();
            fecha = CompraImpl.getCompra().getFecha(); 
            correo_envio = CompraImpl.getCompra().getCorreo_envios();

        }

        txtNombresCompleto.setText(nombreRazonSocial);
        txtFechaEmision.setDate(fec.StringADate(fec.convertirFecha(fecha)));
        txtCorreo.setText(correo_envio);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        LTABLA = new javax.swing.JLabel();
        lblIdEmpresa = new javax.swing.JLabel();
        lblcorreo = new javax.swing.JLabel();
        txtCorreo = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel4 = new javax.swing.JLabel();
        cboFormato = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cboImpresion = new javax.swing.JComboBox<>();
        txtFechaEmision = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        txtDocumento = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtNombresCompleto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        btnEnviar1 = new javax.swing.JButton();

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        LTABLA.setText("jLabel1");
        jPanel4.add(LTABLA);

        lblIdEmpresa.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        txtCorreo.setEditable(false);
        txtCorreo.setAnchoDeBorde(1.0F);
        txtCorreo.setDescripcion("Ingrese una descripción");
        txtCorreo.setEnabled(false);
        txtCorreo.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText("Opciones de Reporte *");

        cboFormato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ticket 80 mm" }));
        cboFormato.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboFormato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFormatoActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("N° Documento");

        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("Cliente");

        cboImpresion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "IMPRESIÓN DIRECTA (IMPRESORA POR DEFECTO)", "GENERAR VISTA PREVIA", "SELECCIONAR UNA IMPRESORA", "GENERAR UNA COPIA EN EL ESCRITORIO", "ENVIAR POR CORREO AL CLIENTE" }));
        cboImpresion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboImpresion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboImpresionActionPerformed(evt);
            }
        });

        txtFechaEmision.setEnabled(false);

        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Fecha");

        txtDocumento.setEditable(false);
        txtDocumento.setAnchoDeBorde(1.0F);
        txtDocumento.setDescripcion("Ingrese una descripción");
        txtDocumento.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        txtNombresCompleto.setEditable(false);
        txtNombresCompleto.setAnchoDeBorde(1.0F);
        txtNombresCompleto.setDescripcion("Descripción");
        txtNombresCompleto.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        btnEnviar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnEnviar1.setText("Generar Reporte");
        btnEnviar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEnviar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                .addGap(105, 105, 105)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(txtFechaEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombresCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(cboImpresion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEnviar1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaEmision, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombresCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboImpresion)
                    .addComponent(btnEnviar1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPlantillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlantillaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaActionPerformed

    private void txtPlantillaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlantillaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlantillaKeyReleased

    private void txtCodigoBarraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoBarraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraActionPerformed

    private void txtCodigoBarraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBarraKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoBarraKeyReleased

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed

    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtDocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDocumentoKeyReleased

    }//GEN-LAST:event_txtDocumentoKeyReleased

    private void cboImpresionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboImpresionActionPerformed

    }//GEN-LAST:event_cboImpresionActionPerformed

    private void cboFormatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboFormatoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboFormatoActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    public String leeFechaEmision() {
        return fec.mostrar_fecha(txtFechaEmision);
    }
    private void btnEnviar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviar1ActionPerformed

        String papel = cboFormato.getSelectedItem().toString();

        int valor = cboImpresion.getSelectedIndex();
        if (valor == 3 || valor == 4) {
            papel = "A4";

            ////SI LA OPCION SELECCIONADA ES LA OPCION DE ENVIO DE CORREO LLAMAMOS AL MODAL PARA VERIFICAR EL CORREO
            if (valor == 4) {
                //llamamos a la ventana de actualización de correos de cliente
                if (idcliente != 0) {
                    new Form_ClienteCorreo(null, true, idcliente, txtNombresCompleto.getText().toUpperCase(), txtCorreo.getText(), "cliente").setVisible(true);
                } else {
                    if (idproveedor != 0) {
                        new Form_ClienteCorreo(null, true, idproveedor, txtNombresCompleto.getText().toUpperCase(), txtCorreo.getText(), "proveedor").setVisible(true);
                    } else {
                        Metodos.MensajeError("Seleccione un Comprobante válido");
                    }
                }
            }
        }

        opciones_de_seleccion(papel, valor);
    }//GEN-LAST:event_btnEnviar1ActionPerformed

    public void opciones_de_seleccion(String papel, int valor) {
        ComprobantesImpl.comprobantes_impresion(tipoDocumento, txtDocumento.getText(), papel, valor, leeFechaEmision());
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        new Formulario_usarTipoImpresion(null, true).setVisible(true);
        if (!Formulario_usarTipoImpresion.formatoImpresion.equalsIgnoreCase("")) {
            cboFormato.setSelectedItem(Formulario_usarTipoImpresion.formatoImpresion);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Imp_Comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Imp_Comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Imp_Comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Imp_Comprobantes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold  
        //</editor-fold>   
        //</editor-fold>
        //</editor-fold>   

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Imp_Comprobantes dialog = new Imp_Comprobantes(new javax.swing.JFrame(), true, "", "");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LTABLA;
    private javax.swing.JButton btnEnviar1;
    private javax.swing.JComboBox<String> cboFormato;
    private javax.swing.JComboBox<String> cboImpresion;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lblIdEmpresa;
    public static javax.swing.JLabel lblcorreo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtCorreo;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtDocumento;
    private com.toedter.calendar.JDateChooser txtFechaEmision;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtNombresCompleto;
    // End of variables declaration//GEN-END:variables
}
