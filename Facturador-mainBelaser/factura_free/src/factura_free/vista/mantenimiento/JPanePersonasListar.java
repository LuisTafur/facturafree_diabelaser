package factura_free.vista.mantenimiento;

import factura_free.vista.modales.JDialogCliente;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import java.awt.Color;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import factura_free.clases.controlador.ClienteImpl;
import factura_free.clases.controlador.ProveedorImpl;
import factura_free.clases.modelo.ObCliente;
import factura_free.clases.modelo.ObProveedor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.modales.JDialogProveedor;  

public class JPanePersonasListar extends javax.swing.JPanel {

    DefaultTableModel modelo = null;
    ArrayList<ObCliente> lista_ObCliente = null;
    ArrayList<ObProveedor> lista_ObProveedor = null;

    public JPanePersonasListar() {
        initComponents();
        tblDetalle.setDefaultRenderer(Object.class, new ImgTabla());
        txtBusqueda.requestFocusInWindow();

        mostrar_Tabla();
        cargarClientes();

        txtBusqueda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {
                    cargarClientes();
                } else {
                    cargarProveedores();
                }
            }
        });
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn("#");
        modelo.addColumn("DOCUMENTO");
        modelo.addColumn("NOMBRES / RAZÓN SOCIAL");
        modelo.addColumn("DIRECCIÓN");
        modelo.addColumn("ESTADO");
        modelo.addColumn("");
        modelo.addColumn(""); 
        tblDetalle.setRowHeight(23);
        tblDetalle.setModel(modelo);
        tblDetalle.setBackground(Color.WHITE);
        tblDetalle.setAutoResizeMode(tblDetalle.AUTO_RESIZE_OFF);

        tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(30);
        tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(100);
        tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(300); 
        tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(350);
        tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(60);
        tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(28);
        tblDetalle.getColumnModel().getColumn(6).setPreferredWidth(28);
    }

    private void cargarClientes() {
        Metodos.LimpiarTabla(modelo);
        int cntCarga = Integer.parseInt(cboCantidadCarga.getSelectedItem().toString());
        lista_ObCliente = (ArrayList<ObCliente>) ClienteImpl.listarClientes("select * from cliente where "
                + "CONCAT(numeroDocumento,'-',nombreRazonSocial) like '%" + txtBusqueda.getText() + "%' "
                + "and estado='ACTIVO' order by nombreRazonSocial  limit 0," + cntCarga + ";");

        Object datos[] = new Object[10];
        for (int i = 0; i < lista_ObCliente.size(); i++) {
            datos[0] = (lista_ObCliente.get(i).getId()) + "";
            datos[1] = (lista_ObCliente.get(i).getTipoDocumento()) + " " + (lista_ObCliente.get(i).getNumeroDocumento());
            datos[2] = (lista_ObCliente.get(i).getNombreRazonSocial()) + "";
            datos[3] = (lista_ObCliente.get(i).getDireccion()) + "";
            datos[4] = (lista_ObCliente.get(i).getEstado()) + "";

            ImageIcon iconoModi = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificar = new ImageIcon(iconoModi.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificar = new JButton("", btnModificar);
            botonModificar.setName("btnModificar");
            botonModificar.setToolTipText("Modificar Registro");
            datos[5] = botonModificar;

            ImageIcon icono = new ImageIcon(getClass().getResource("/img/EliminarTabla_40px.png"));
            Icon btnEliminar = new ImageIcon(icono.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonEliminar = new JButton("", btnEliminar);
            botonEliminar.setName("btnEliminar");
            botonEliminar.setToolTipText("Eliminar Registro");
            datos[6] = botonEliminar; 

            modelo.addRow(datos);
        }
    }

    private void cargarProveedores() {
        Metodos.LimpiarTabla(modelo);
        int cntCarga = Integer.parseInt(cboCantidadCarga.getSelectedItem().toString());
        lista_ObProveedor = (ArrayList<ObProveedor>) ProveedorImpl.listarProveedors("select * from proveedor where "
                + "CONCAT(numeroDocumento,'-',nombreRazonSocial) like '%" + txtBusqueda.getText() + "%' "
                + "and estado='ACTIVO' order by nombreRazonSocial  limit 0," + cntCarga + ";");

        Object datos[] = new Object[8];
        for (int i = 0; i < lista_ObProveedor.size(); i++) {
            datos[0] = (lista_ObProveedor.get(i).getId()) + "";
            datos[1] = (lista_ObProveedor.get(i).getTipoDocumento()) + " " + (lista_ObProveedor.get(i).getNumeroDocumento());
            datos[2] = (lista_ObProveedor.get(i).getNombreRazonSocial()) + ""; 
            datos[3] = (lista_ObProveedor.get(i).getDireccion()) + "";
            datos[4] = (lista_ObProveedor.get(i).getEstado()) + "";

            ImageIcon iconoModi = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificar = new ImageIcon(iconoModi.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificar = new JButton("", btnModificar);
            botonModificar.setName("btnModificar");
            botonModificar.setToolTipText("Modificar Registro");
            datos[5] = botonModificar;

            ImageIcon icono = new ImageIcon(getClass().getResource("/img/EliminarTabla_40px.png"));
            Icon btnEliminar = new ImageIcon(icono.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonEliminar = new JButton("", btnEliminar);
            botonEliminar.setName("btnEliminar");
            botonEliminar.setToolTipText("Eliminar Registro");
            datos[6] = botonEliminar;
            modelo.addRow(datos);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNuevo = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        cboCantidadCarga = new javax.swing.JComboBox<>();
        txtBusqueda = new org.edisoncor.gui.textField.TextFieldRectBackground();
        cboTipoPersona = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        jLabel2.setText("Buscar:");

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalle.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblDetalle);

        jPanel1.add(jScrollPane1);

        cboCantidadCarga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCarga.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaActionPerformed(evt);
            }
        });

        txtBusqueda.setAnchoDeBorde(1.0F);
        txtBusqueda.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusqueda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        cboTipoPersona.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CLIENTES", "PROVEEDORES" }));
        cboTipoPersona.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTipoPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoPersonaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoPersona, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 692, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCarga, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1083, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cboCantidadCarga, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboTipoPersona, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {

            new JDialogCliente(null, true, 0, "TODOS").setVisible(true);
            if (JDialogCliente.procesado == 1) {
                cargarClientes();
            } else {
                txtBusqueda.requestFocus();
                AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
                alert.setVisible(true);
            }

        } else if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("PROVEEDORES")) {

            new JDialogProveedor(null, true, 0).setVisible(true);
            if (JDialogProveedor.procesado == 1) {
                cargarProveedores();
            } else {
                txtBusqueda.requestFocus();
                AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
                alert.setVisible(true);
            }

        }

    }//GEN-LAST:event_btnNuevoActionPerformed
    private void cargarCliente(int id_cliente, int fila) {
        ClienteImpl.buscarCliente("select * from cliente where id='" + id_cliente + "' ");
        String tipoDocumento = ClienteImpl.getCliente().getTipoDocumento();
        String numeroDocumento = ClienteImpl.getCliente().getNumeroDocumento();
        String nombreRazonSocial = ClienteImpl.getCliente().getNombreRazonSocial();
        String direccion = ClienteImpl.getCliente().getDireccion(); 
        String estado = ClienteImpl.getCliente().getEstado();

        tblDetalle.setValueAt(tipoDocumento + " " + numeroDocumento, fila, 1);
        tblDetalle.setValueAt(nombreRazonSocial, fila, 2); 
        tblDetalle.setValueAt(direccion, fila, 3);
        tblDetalle.setValueAt(estado, fila, 4);
    }

    private void cargarProveedor(int id_proveedor, int fila) {
        ProveedorImpl.buscarProveedor("select * from proveedor where id='" + id_proveedor + "' ");
        String tipoDocumento = ProveedorImpl.getProveedor().getTipoDocumento();
        String numeroDocumento = ProveedorImpl.getProveedor().getNumeroDocumento();
        String nombreRazonSocial = ProveedorImpl.getProveedor().getNombreRazonSocial();
        String direccion = ProveedorImpl.getProveedor().getDireccion(); 
        String estado = ProveedorImpl.getProveedor().getEstado();

        tblDetalle.setValueAt(tipoDocumento + " " + numeroDocumento, fila, 1);
        tblDetalle.setValueAt(nombreRazonSocial, fila, 2); 
        tblDetalle.setValueAt(direccion, fila, 3);
        tblDetalle.setValueAt(estado, fila, 4);
    }
    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked
        int fila = tblDetalle.getSelectedRow();
        int id_cliente = Integer.parseInt(String.valueOf(tblDetalle.getValueAt(fila, 0)));
        String cliente = String.valueOf(tblDetalle.getValueAt(fila, 2));

        int colum = tblDetalle.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetalle.getRowHeight();

        if (row < tblDetalle.getRowCount() && row >= 0 && colum < tblDetalle.getColumnCount() && colum >= 0) {
            Object value = tblDetalle.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("btnEliminar")) {

//                    if (JFramePrincipal.perfil.equalsIgnoreCase("ADMINISTRADOR")) {
                        ////VERIFICAMOS QUE NO SEA ELIMINADO EL CLIENTE PUBLICO VARIOS
                        if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {
                            if (id_cliente == 1) {
                                Metodos.MensajeError("El Cliente " + cliente + " NO PUEDE SER ELIMINADO");
                                return;
                            }

                            int opcion = JOptionPane.showConfirmDialog(null, "Realmente desea eliminar al Cliente " + cliente + "?", "Mensaje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (opcion == JOptionPane.OK_OPTION) {
                                ClienteImpl.eliminar(id_cliente);
                                txtBusqueda.requestFocus();
                                Eliminar();
                            } else {
                                txtBusqueda.requestFocus();
                                Metodos.MensajeAlerta("Operación Cancelada");
                            }

                        } else if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("PROVEEDORES")) {

                            ////VERIFICAMOS QUE NO SEA ELIMINADO EL CLIENTE PUBLICO VARIOS
                            if (id_cliente == 1) {
                                Metodos.MensajeError("El Proveedor " + cliente + " NO PUEDE SER ELIMINADO");
                                return;
                            }

                            int opcion = JOptionPane.showConfirmDialog(null, "Realmente desea eliminar al Proveedor " + cliente + "?", "Mensaje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (opcion == JOptionPane.OK_OPTION) {
                                ProveedorImpl.eliminar(id_cliente);
                                txtBusqueda.requestFocus();
                                Eliminar();
                            } else {
                                txtBusqueda.requestFocus();
                                Metodos.MensajeAlerta("Operación Cancelada");
                            }

                        }
//                    }

                } else if (boton.getName().equals("btnModificar")) {

//                    if (JFramePrincipal.perfil.equalsIgnoreCase("ADMINISTRADOR")) {
                        ////VERIFICAMOS QUE NO SEA ELIMINADO EL CLIENTE PUBLICO VARIOS
                        if (id_cliente == 1) {
                            Metodos.MensajeError("El Cliente " + cliente + " NO PUEDE SER MODIFICADO");
                            return;
                        }

                        if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {
                            new JDialogCliente(null, true, id_cliente, "TODOS").setVisible(true);
                            if (JDialogCliente.procesado == 1) {
                                cargarCliente(id_cliente, fila);
                            } else {
                                txtBusqueda.requestFocus();
                                Metodos.MensajeAlerta("Operación Cancelada");
                            }

                        } else if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("PROVEEDORES")) {

                            ////VERIFICAMOS QUE NO SEA ELIMINADO EL CLIENTE PUBLICO VARIOS
                            if (id_cliente == 1) {
                                Metodos.MensajeError("El Proveedor " + cliente + " NO PUEDE SER MODIFICADO");
                                return;
                            }

                            new JDialogProveedor(null, true, id_cliente).setVisible(true);
                            if (JDialogProveedor.procesado == 1) {
                                cargarProveedor(id_cliente, fila);
                            } else {
                                txtBusqueda.requestFocus();
                                Metodos.MensajeAlerta("Operación Cancelada");
                            }

                        }
////                    }

                }
            }
        }
    }//GEN-LAST:event_tblDetalleMouseClicked
    void Eliminar() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetalle.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetalle.getSelectedRow();//obtiene la fila 
            modelo.removeRow(fila);//remueve la fila obtenida 
        }
    }
    private void cboCantidadCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaActionPerformed

        if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {
            cargarClientes();
        } else if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("PROVEEDORES")) {
            cargarProveedores();
        }

    }//GEN-LAST:event_cboCantidadCargaActionPerformed

    private void cboTipoPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoPersonaActionPerformed
        mostrar_Tabla();

        if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("CLIENTES")) {
            cargarClientes();
        } else if (cboTipoPersona.getSelectedItem().toString().equalsIgnoreCase("PROVEEDORES")) {
            cargarProveedores();
        }
    }//GEN-LAST:event_cboTipoPersonaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cboCantidadCarga;
    private javax.swing.JComboBox<String> cboTipoPersona;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDetalle;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
