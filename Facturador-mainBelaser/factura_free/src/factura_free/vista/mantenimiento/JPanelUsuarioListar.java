package factura_free.vista.mantenimiento;

import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.UsuarioImpl;
import factura_free.clases.modelo.ObUsuario;
import factura_free.clases.principales.alternos.EncriparDatos;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.modales.JDialogUsuario;

public class JPanelUsuarioListar extends javax.swing.JPanel {

    DefaultTableModel modelo = null;
    ArrayList<ObUsuario> lista_ObUsuario = null;

    public static String rol;

    public JPanelUsuarioListar(String user_rol) {
        initComponents();
        rol = user_rol;
        cargarRol();
        tblDetalle.setDefaultRenderer(Object.class, new ImgTabla());
        txtBusqueda.requestFocusInWindow();

        mostrar_Tabla();
        cargarUsuarios();

        txtBusqueda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarUsuarios();
            }
        });
    }

    private void cargarRol() {
        if (rol.equalsIgnoreCase("USUARIO")) {
            btnNuevo.setEnabled(false);
        }
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn("#");
        modelo.addColumn("USUARIO");
        modelo.addColumn("ROL");
        modelo.addColumn("ESTADO");
        modelo.addColumn("");
        modelo.addColumn("");
        tblDetalle.setRowHeight(23);
        tblDetalle.setModel(modelo);
        tblDetalle.setBackground(Color.WHITE);
        tblDetalle.setAutoResizeMode(tblDetalle.AUTO_RESIZE_OFF);

        tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(30);
        tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(520);
        tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(120);
        tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(70);
        tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(28);
        tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(28); 
    }
 

    private void cargarUsuarios() {
        Metodos.LimpiarTabla(modelo);
        int cntCarga = Integer.parseInt(cboCantidadCarga.getSelectedItem().toString());
        String consulta = "select * from usuario where usuario like '%" + txtBusqueda.getText() + "%' order by usuario  limit 0," + cntCarga + ";";

        lista_ObUsuario = (ArrayList<ObUsuario>) UsuarioImpl.listarUsuarios(consulta);

        Object datos[] = new Object[6];
        for (int i = 0; i < lista_ObUsuario.size(); i++) {
            datos[0] = (lista_ObUsuario.get(i).getId()) + "";
            datos[1] = (lista_ObUsuario.get(i).getUsuario());
            datos[2] = (lista_ObUsuario.get(i).getRol()) + "";
            datos[3] = (lista_ObUsuario.get(i).getEstado()) + "";

            ImageIcon iconoModi = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificar = new ImageIcon(iconoModi.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificar = new JButton("", btnModificar);
            botonModificar.setName("btnModificar");
            botonModificar.setToolTipText("Modificar Registro");
            datos[4] = botonModificar;

            //BOTON NUEVO
            ImageIcon icono = new ImageIcon(getClass().getResource("/img/EliminarTabla_40px.png"));
            Icon btnEliminar = new ImageIcon(icono.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonEliminar = new JButton("", btnEliminar);
            botonEliminar.setName("btnEliminar");
            botonEliminar.setToolTipText("Eliminar Registro");
            datos[5] = botonEliminar;
            modelo.addRow(datos);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNuevo = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        cboCantidadCarga = new javax.swing.JComboBox<>();
        txtBusqueda = new org.edisoncor.gui.textField.TextFieldRectBackground();

        setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        jLabel2.setText("Buscar:");

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalle.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblDetalle);

        jPanel1.add(jScrollPane1);

        cboCantidadCarga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCarga.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaActionPerformed(evt);
            }
        });

        txtBusqueda.setAnchoDeBorde(1.0F);
        txtBusqueda.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusqueda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCarga, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(cboCantidadCarga)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        new JDialogUsuario(null, true, 0).setVisible(true);
        if (JDialogUsuario.procesado == 1) {
            cargarUsuarios();
        } else {
            txtBusqueda.requestFocus();
            AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
            alert.setVisible(true);
        }

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void cargarUsuario(int id_cliente, int fila) {
        UsuarioImpl.buscarUsuario(id_cliente);
        String codigo = UsuarioImpl.getUsuario().getUsuario();
        String descripcion = UsuarioImpl.getUsuario().getRol();
        String estado = UsuarioImpl.getUsuario().getEstado();

        tblDetalle.setValueAt(codigo, fila, 1);
        tblDetalle.setValueAt(descripcion, fila, 2);
        tblDetalle.setValueAt(estado, fila, 3);
    }

    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked
        int fila = tblDetalle.getSelectedRow();
        int id_usuario = Integer.parseInt(String.valueOf(tblDetalle.getValueAt(fila, 0)));
        String usuario = String.valueOf(tblDetalle.getValueAt(fila, 2));

        int colum = tblDetalle.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetalle.getRowHeight();

        if (row < tblDetalle.getRowCount() && row >= 0 && colum < tblDetalle.getColumnCount() && colum >= 0) {
            Object value = tblDetalle.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("btnEliminar")) {

                    int opcion = JOptionPane.showConfirmDialog(null, "Realmente desea eliminar el Usuario " + usuario + "?", "Mensaje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcion == JOptionPane.OK_OPTION) {
                        UsuarioImpl.eliminar(id_usuario);
                        txtBusqueda.requestFocus();
                        Eliminar();
                    } else {
                        txtBusqueda.requestFocus();
                        AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
                        alert.setVisible(true);
                    }

                } else if (boton.getName().equals("btnModificar")) {

                    new JDialogUsuario(null, true, id_usuario).setVisible(true);
                    if (JDialogUsuario.procesado == 1) {
                        cargarUsuario(id_usuario, fila);
                    } else {
                        txtBusqueda.requestFocus();
                        AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
                        alert.setVisible(true);
                    }

                }
            }
        }
    }//GEN-LAST:event_tblDetalleMouseClicked
    void Eliminar() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetalle.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetalle.getSelectedRow();//obtiene la fila 
            modelo.removeRow(fila);//remueve la fila obtenida 
        }
    }
    private void cboCantidadCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaActionPerformed

        cargarUsuarios();

    }//GEN-LAST:event_cboCantidadCargaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cboCantidadCarga;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDetalle;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
