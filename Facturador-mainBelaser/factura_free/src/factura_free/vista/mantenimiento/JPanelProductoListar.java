package factura_free.vista.mantenimiento;

import factura_free.vista.modales.JDialogProducto;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.ProductoImpl;
import factura_free.clases.modelo.ObProducto;
import factura_free.clases.principales.Catalogos;
import factura_free.clases.principales.alternos.Fecha_Date;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.modales.auxiliares.Form_CambioNumTPV;
import factura_free.vista.modales.auxiliares.Form_Producto_kardex;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JPanelProductoListar extends javax.swing.JPanel {

    DefaultTableModel modelo = null;
    ArrayList<ObProducto> lista_ObProducto = null;

    public static String rol;

    public JPanelProductoListar(String user_rol) {
        initComponents();
        rol = user_rol;
        cargarRol();
        tblDetalle.setDefaultRenderer(Object.class, new ImgTabla());
        txtBusqueda.requestFocusInWindow();

        mostrar_Tabla();
        cargarProductos();

        txtBusqueda.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarProductos();
            }
        });

    }

    private void cargarRol() {
        if (rol.equalsIgnoreCase("USUARIO")) {
            btnNuevo.setEnabled(false);
        }
    }

    public void mostrar_Tabla() {
        modelo = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelo.addColumn("#");
        modelo.addColumn("CÓDIGO");
        modelo.addColumn("PRODUCTO");
        modelo.addColumn("UNID.M");
        modelo.addColumn("P.VTA");
        modelo.addColumn("IGV");
        modelo.addColumn("P.COM");
        modelo.addColumn("STOCK");
        modelo.addColumn("ESTADO");
        modelo.addColumn("");
        modelo.addColumn("");
        modelo.addColumn("");
        modelo.addColumn("");
        tblDetalle.setRowHeight(23);
        tblDetalle.setModel(modelo);
        tblDetalle.setBackground(Color.WHITE);
        tblDetalle.setAutoResizeMode(tblDetalle.AUTO_RESIZE_OFF);

        tblDetalle.getColumnModel().getColumn(0).setPreferredWidth(0);
        tblDetalle.getColumnModel().getColumn(1).setPreferredWidth(85);
        tblDetalle.getColumnModel().getColumn(2).setPreferredWidth(400);
        tblDetalle.getColumnModel().getColumn(3).setPreferredWidth(80);
        tblDetalle.getColumnModel().getColumn(4).setPreferredWidth(55);
        tblDetalle.getColumnModel().getColumn(5).setPreferredWidth(40);
        tblDetalle.getColumnModel().getColumn(6).setPreferredWidth(55);
        tblDetalle.getColumnModel().getColumn(7).setPreferredWidth(65);
        tblDetalle.getColumnModel().getColumn(8).setPreferredWidth(60);
        tblDetalle.getColumnModel().getColumn(9).setPreferredWidth(28);
        tblDetalle.getColumnModel().getColumn(10).setPreferredWidth(28);
        tblDetalle.getColumnModel().getColumn(11).setPreferredWidth(28);
        tblDetalle.getColumnModel().getColumn(12).setPreferredWidth(28);
    }

    private void cargarProductos() {
        Metodos.LimpiarTabla(modelo);
        int cntCarga = Integer.parseInt(cboCantidadCarga.getSelectedItem().toString());
        String valorTipo = cboTipoRegistro.getSelectedItem().toString();

        String consulta = "";
        if (valorTipo.equalsIgnoreCase("TODOS")) {
            consulta = "select * from producto where CONCAT(codigo,'-',descripcion) like '%" + txtBusqueda.getText() + "%' and estado='ACTIVO' order by descripcion  limit 0," + cntCarga + ";";
        } else {
            consulta = "select * from producto where CONCAT(codigo,'-',descripcion) like '%" + txtBusqueda.getText() + "%' and estado='ACTIVO' and tipo='" + valorTipo + "' order by descripcion  limit 0," + cntCarga + ";";
        }
        lista_ObProducto = (ArrayList<ObProducto>) ProductoImpl.listarProductos(consulta);

        Object datos[] = new Object[13];
        for (int i = 0; i < lista_ObProducto.size(); i++) {
            datos[0] = (lista_ObProducto.get(i).getId()) + "";
            datos[1] = (lista_ObProducto.get(i).getCodigo());
            datos[2] = (lista_ObProducto.get(i).getDescripcion()) + "";
            datos[3] = (lista_ObProducto.get(i).getUnidad_medida()) + "";
            datos[4] = (lista_ObProducto.get(i).getPrecio()) + "";
            if (lista_ObProducto.get(i).getIcbper().equalsIgnoreCase("SI")) {
                datos[5] = "ICBPER";
            } else {
                datos[5] = Catalogos.tipoTributo("", lista_ObProducto.get(i).getAfectacion(), "", "")[3];
            }
            datos[6] = (lista_ObProducto.get(i).getPrecio_compra()) + "";
            if (lista_ObProducto.get(i).getTiene_stock().equalsIgnoreCase("SI")) {
                datos[7] = (lista_ObProducto.get(i).getStock_actual()) + "";
            } else {
                datos[7] = "ILIMITADO";
            }
            datos[8] = (lista_ObProducto.get(i).getEstado()) + "";

            ImageIcon iconoModi = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificar = new ImageIcon(iconoModi.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificar = new JButton("", btnModificar);
            botonModificar.setName("btnModificar");
            botonModificar.setToolTipText("Modificar Registro");
            datos[9] = botonModificar;

            ImageIcon iconoEliminar = new ImageIcon(getClass().getResource("/img/EliminarTabla_40px.png"));
            Icon btnEliminar = new ImageIcon(iconoEliminar.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonEliminar = new JButton("", btnEliminar);
            botonEliminar.setName("btnEliminar");
            botonEliminar.setToolTipText("Eliminar Registro");
            datos[10] = botonEliminar;

            ImageIcon iconoCopiar = new ImageIcon(getClass().getResource("/img/Check_16px.png"));
            Icon btnCopiar = new ImageIcon(iconoCopiar.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonCopiar = new JButton("", btnCopiar);
            botonCopiar.setName("botonCopiar");
            botonCopiar.setToolTipText("Copiar Registro");
            datos[11] = botonCopiar;

            ImageIcon iconoKardex = new ImageIcon(getClass().getResource("/img/stockGeneral_16px.png"));
            Icon btnKardex = new ImageIcon(iconoKardex.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonKardex = new JButton("", btnKardex);
            botonKardex.setName("botonKardex");
            botonKardex.setToolTipText("Ver Kardex");
            datos[12] = botonKardex;

            modelo.addRow(datos);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNuevo = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDetalle = new javax.swing.JTable();
        cboCantidadCarga = new javax.swing.JComboBox<>();
        cboTipoRegistro = new javax.swing.JComboBox<>();
        txtBusqueda = new org.edisoncor.gui.textField.TextFieldRectBackground();

        setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        jLabel2.setText("Buscar:");

        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalle.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalle.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalle.setFillsViewportHeight(true);
        tblDetalle.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblDetalle);

        jPanel1.add(jScrollPane1);

        cboCantidadCarga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCarga.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaActionPerformed(evt);
            }
        });

        cboTipoRegistro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TODOS", "PRODUCTO", "SERVICIO" }));
        cboTipoRegistro.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboTipoRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoRegistroActionPerformed(evt);
            }
        });

        txtBusqueda.setAnchoDeBorde(1.0F);
        txtBusqueda.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusqueda.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboTipoRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCarga, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboCantidadCarga)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cboTipoRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed

        new JDialogProducto(null, true, 0, 0, "").setVisible(true);
        if (JDialogProducto.procesado == 1) {
            cargarProductos();
        } else {
            txtBusqueda.requestFocus();
            AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
            alert.setVisible(true);
        }

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void cargarProducto(int id_producto, int fila) {
        ProductoImpl.buscarProducto(id_producto);
        String codigo = ProductoImpl.getProducto().getCodigo();
        String descripcion = ProductoImpl.getProducto().getDescripcion();
        String precio = "" + ProductoImpl.getProducto().getPrecio();
        String uniMedida = ProductoImpl.getProducto().getUnidad_medida();
        String afectacion = ProductoImpl.getProducto().getAfectacion();
        String icbper = ProductoImpl.getProducto().getIcbper();
        String precio_compra = "" + ProductoImpl.getProducto().getPrecio_compra();
        String tiene_stock = ProductoImpl.getProducto().getTiene_stock();
        String stock_actual = "" + ProductoImpl.getProducto().getStock_actual();
        String estado = ProductoImpl.getProducto().getEstado();

        tblDetalle.setValueAt(codigo, fila, 1);
        tblDetalle.setValueAt(descripcion, fila, 2);
        tblDetalle.setValueAt(uniMedida, fila, 3);
        tblDetalle.setValueAt(precio, fila, 4);
        if (icbper.equalsIgnoreCase("SI")) {
            tblDetalle.setValueAt("ICBPER", fila, 5);
        } else {
            tblDetalle.setValueAt(Catalogos.tipoTributo("", afectacion, "", "")[3], fila, 5);
        }
        tblDetalle.setValueAt(precio_compra, fila, 6);
        if (tiene_stock.equalsIgnoreCase("SI")) {
            tblDetalle.setValueAt(stock_actual, fila, 7);
        } else {
            tblDetalle.setValueAt("ILIMITADO", fila, 7);
        }
        tblDetalle.setValueAt(estado, fila, 8);
    }

    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked
        int fila = tblDetalle.getSelectedRow();
        int id_producto = Integer.parseInt(String.valueOf(tblDetalle.getValueAt(fila, 0)));
        String producto = String.valueOf(tblDetalle.getValueAt(fila, 2));
        String stock = String.valueOf(tblDetalle.getValueAt(fila, 7));

        int colum = tblDetalle.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetalle.getRowHeight();

        if (row < tblDetalle.getRowCount() && row >= 0 && colum < tblDetalle.getColumnCount() && colum >= 0) {
            Object value = tblDetalle.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("botonKardex")) {

                    if (stock.equalsIgnoreCase("ILIMITADO")) {
                        Metodos.MensajeError("El Producto no Cuenta con el Control de Stock");
                    } else {

                        int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                                new Object[]{" Ver Historial de Kardex ", " Recalcular Stock Actual ", " Agregar Cantidad ", " Disminuir Cantidad ", " Cancelar "}, " Enviar a SUNAT ");
                        if (seleccion == 0) {

                            Metodos.MensajeInformacion("Carga Exitosa de Kardex de Producto.");
                            new Form_Producto_kardex(null, true, id_producto, producto, stock).setVisible(true);

                        } else if (seleccion == 1) {

                            ProductoImpl.obtenerStockActual(id_producto);
                            cargarProducto(id_producto, fila);
                            Metodos.MensajeInformacion("Producto Recalculado de forma Correcta.");

                        } else if (seleccion == 2) {
                            new Form_CambioNumTPV(null, true).setVisible(true);
                            if (Form_CambioNumTPV.procesado == 0) {
                                AlertaInfo alerta = new AlertaInfo("Informe", "Cambio de Cantidad no Válida.");
                                alerta.setVisible(true);
                            } else {
                                try {
                                    ProductoImpl.Registrar_Kardex(id_producto, Form_CambioNumTPV.valorCambio, 0, (id_producto + " INGRESO EXTRA "),
                                            "INGRESO EXTRA", Fecha_Date.retorna_fecha_sistema());
                                    cargarProducto(id_producto, fila);
                                    Metodos.MensajeInformacion("Producto Recalculado de forma Correcta.");
                                } catch (SQLException ex) {
                                    Logger.getLogger(JPanelProductoListar.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        } else if (seleccion == 3) {
                            new Form_CambioNumTPV(null, true).setVisible(true);
                            if (Form_CambioNumTPV.procesado == 0) {
                                AlertaInfo alerta = new AlertaInfo("Informe", "Cambio de Cantidad no Válida.");
                                alerta.setVisible(true);
                            } else {
                                try {
                                    ProductoImpl.Registrar_Kardex(id_producto, 0, Form_CambioNumTPV.valorCambio, (id_producto + " SALIDA EXTRA "),
                                            "SALIDA EXTRA", Fecha_Date.retorna_fecha_sistema());
                                    cargarProducto(id_producto, fila);
                                    Metodos.MensajeInformacion("Producto Recalculado de forma Correcta.");
                                } catch (SQLException ex) {
                                    Logger.getLogger(JPanelProductoListar.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        } else if (seleccion == 4) {
                            Metodos.MensajeAlerta("Operación Cancelada.");
                        }
                    }

                } else if (boton.getName().equals("botonCopiar")) {

                    Metodos.MensajeInformacion("Producto Seleccionado para Registro Rápido.");
                    new JDialogProducto(null, true, id_producto, 1, "").setVisible(true);
                    if (JDialogProducto.procesado == 1) {
                        cargarProductos();
                    } else {
                        txtBusqueda.requestFocus();
                        Metodos.MensajeAlerta("Operación Cancelada.");
                    }

                } else if (boton.getName().equals("btnEliminar")) {

                    int opcion = JOptionPane.showConfirmDialog(null, "Realmente desea eliminar el Producto " + producto + "?", "Mensaje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (opcion == JOptionPane.OK_OPTION) {
                        ProductoImpl.eliminar(id_producto);
                        txtBusqueda.requestFocus();
                        Eliminar();
                    } else {
                        txtBusqueda.requestFocus();
                        Metodos.MensajeAlerta("Operación Cancelada.");
                    }

                } else if (boton.getName().equals("btnModificar")) {

                    new JDialogProducto(null, true, id_producto, 0, "").setVisible(true);
                    if (JDialogProducto.procesado == 1) {
                        cargarProducto(id_producto, fila);
                    } else {
                        txtBusqueda.requestFocus();
                        Metodos.MensajeAlerta("Operación Cancelada.");
                    }

                }
            }
        }
    }//GEN-LAST:event_tblDetalleMouseClicked
    void Eliminar() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetalle.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetalle.getSelectedRow();//obtiene la fila 
            modelo.removeRow(fila);//remueve la fila obtenida 
        }
    }
    private void cboCantidadCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaActionPerformed

        cargarProductos();

    }//GEN-LAST:event_cboCantidadCargaActionPerformed

    private void cboTipoRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoRegistroActionPerformed

        cargarProductos();

    }//GEN-LAST:event_cboTipoRegistroActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cboCantidadCarga;
    private javax.swing.JComboBox<String> cboTipoRegistro;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDetalle;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
