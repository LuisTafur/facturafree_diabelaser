package factura_free.vista.mantenimiento;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.Venta_internaImpl;
import factura_free.clases.modelo.ObVenta_internaView;
import factura_free.clases.principales.Datos;
import factura_free.vista.modales.auxiliares.Form_Motivo_anulacion;
import factura_free.vista.modales.auxiliares.Formulario_GenerarRepoFechas;
import factura_free.vista.modales.impresion.Imp_Comprobantes; 
import factura_free.vista.modales.paneles.JPanelVenta_interna;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;

public class JPanelVenta_internas extends javax.swing.JPanel {

    Fecha_Date fec = new Fecha_Date();
    DefaultTableModel dtmVenta_interna = null;
    ArrayList<ObVenta_internaView> lista_ObVenta_internaView = null;

    public JPanelVenta_internas() {
        initComponents();

        fec.capturaymuestrahoradelsistema(txtFechaInicialVenta_interna);
        fec.capturaymuestrahoradelsistema(txtFechaFinalVenta_interna);
        tblDetalleVenta_interna.setDefaultRenderer(Object.class, new ImgTabla());
        mostrar_TablaVenta_internas();

        txtBusquedaVenta_interna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarVenta_internas();
            }
        });

        txtFechaInicialVenta_interna.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarVenta_internas();
            }
        });

        txtFechaFinalVenta_interna.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarVenta_internas();
            }
        });

    }

    public void mostrar_TablaVenta_internas() {
        dtmVenta_interna = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        dtmVenta_interna.addColumn("EMISIÓN");
        dtmVenta_interna.addColumn("CLIENTE");
        dtmVenta_interna.addColumn("MONEDA");
        dtmVenta_interna.addColumn("TOTAL");
        dtmVenta_interna.addColumn("ESTADO");
        dtmVenta_interna.addColumn("");
        dtmVenta_interna.addColumn("");
        dtmVenta_interna.addColumn("");
        dtmVenta_interna.addColumn("");
        tblDetalleVenta_interna.setRowHeight(23);
        tblDetalleVenta_interna.setModel(dtmVenta_interna);
        tblDetalleVenta_interna.setBackground(Color.WHITE);
        tblDetalleVenta_interna.setAutoResizeMode(tblDetalleVenta_interna.AUTO_RESIZE_OFF);

        tblDetalleVenta_interna.getColumnModel().getColumn(0).setPreferredWidth(100);
        tblDetalleVenta_interna.getColumnModel().getColumn(1).setPreferredWidth(470);
        tblDetalleVenta_interna.getColumnModel().getColumn(2).setPreferredWidth(60);
        tblDetalleVenta_interna.getColumnModel().getColumn(3).setPreferredWidth(55);
        tblDetalleVenta_interna.getColumnModel().getColumn(4).setPreferredWidth(70);
        tblDetalleVenta_interna.getColumnModel().getColumn(5).setPreferredWidth(28);
        tblDetalleVenta_interna.getColumnModel().getColumn(6).setPreferredWidth(28);
        tblDetalleVenta_interna.getColumnModel().getColumn(7).setPreferredWidth(28);
        tblDetalleVenta_interna.getColumnModel().getColumn(8).setPreferredWidth(28);
    }

    public String leeFechaFinalVenta_interna() {
        return fec.mostrar_fecha(txtFechaFinalVenta_interna);
    }

    public String leeFechaInicialVenta_interna() {
        return fec.mostrar_fecha(txtFechaInicialVenta_interna);
    }

    private void cargarVenta_internas() {
        Metodos.LimpiarTabla(dtmVenta_interna);
        double montoTotalSoles = 0;
        double montoTotalDolares = 0;
        int cntCarga = Integer.parseInt(cboCantidadCargaVenta_interna.getSelectedItem().toString());
        String estado_comprobante = cboEstadoVenta_interna.getSelectedItem().toString();
        String addConsulta = "";
        if (!estado_comprobante.equalsIgnoreCase("TODOS")) {
            addConsulta = " and estado_comprobante='" + estado_comprobante + "' ";
        }

        String consulta = "select * from venta_interna_view where CONCAT(id_venta_interna,'-',cliente) like '%" + txtBusquedaVenta_interna.getText() + "%' and "
                + " fecha_emision>='" + leeFechaInicialVenta_interna() + "' and fecha_emision<='" + leeFechaFinalVenta_interna() + "' "
                + " " + addConsulta + " order by id_venta_interna desc limit 0," + cntCarga + ";";

        lista_ObVenta_internaView = (ArrayList<ObVenta_internaView>) Venta_internaImpl.listarVenta_internas(consulta);
        Object datos[] = new Object[15];
        for (int i = 0; i < lista_ObVenta_internaView.size(); i++) {
            datos[0] = (lista_ObVenta_internaView.get(i).getId()) + "";
            datos[1] = (lista_ObVenta_internaView.get(i).getCliente());
            datos[2] = (lista_ObVenta_internaView.get(i).getMoneda()) + "";
            datos[3] = (lista_ObVenta_internaView.get(i).getImporteTotal()) + "";
            datos[4] = (lista_ObVenta_internaView.get(i).getEstado_comprobante()) + ""; 

            if (lista_ObVenta_internaView.get(i).getEstado_comprobante().equalsIgnoreCase("GENERADO")
                    && lista_ObVenta_internaView.get(i).getComprobante_relacionado().equalsIgnoreCase("")) {
                if (lista_ObVenta_internaView.get(i).getMoneda().equalsIgnoreCase("PEN")) {
                    montoTotalSoles = montoTotalSoles + lista_ObVenta_internaView.get(i).getImporteTotal();
                } else {
                    montoTotalDolares = montoTotalDolares + lista_ObVenta_internaView.get(i).getImporteTotal();
                }
            }

            ImageIcon iconoModiVenta_interna = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificarVenta_interna = new ImageIcon(iconoModiVenta_interna.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificarVenta_interna = new JButton("", btnModificarVenta_interna);
            botonModificarVenta_interna.setName("btnModificarVenta_interna");
            botonModificarVenta_interna.setToolTipText("Modificar Registro");
            datos[5] = botonModificarVenta_interna;

            ImageIcon iconoVerVenta_interna = new ImageIcon(getClass().getResource("/img/VerTabla_40px.png"));
            Icon btnVerVenta_interna = new ImageIcon(iconoVerVenta_interna.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonVerVenta_interna = new JButton("", btnVerVenta_interna);
            botonVerVenta_interna.setName("botonVerVenta_interna");
            botonVerVenta_interna.setToolTipText("Ver Comprobante");
            datos[6] = botonVerVenta_interna;

            ImageIcon iconoAnularVenta_interna = new ImageIcon(getClass().getResource("/img/anular_16px.png"));
            Icon btnAnularVenta_interna = new ImageIcon(iconoAnularVenta_interna.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonAnularVenta_interna = new JButton("", btnAnularVenta_interna);
            botonAnularVenta_interna.setName("btnAnularVenta_interna");
            botonAnularVenta_interna.setToolTipText("Anular Comprobante");
            datos[7] = botonAnularVenta_interna;

            ImageIcon iconoImprimirVenta_interna = new ImageIcon(getClass().getResource("/img/Print_16px.png"));
            Icon btnImprimirVenta_interna = new ImageIcon(iconoImprimirVenta_interna.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonImprimirVenta_interna = new JButton("", btnImprimirVenta_interna);
            botonImprimirVenta_interna.setName("btnImprimirVenta_interna");
            botonImprimirVenta_interna.setToolTipText("Opciones de Impresión");
            datos[8] = botonImprimirVenta_interna;

            dtmVenta_interna.addRow(datos);
        }
        txtImporteTotaVenta_internaS.setText("S/." + Metodos.formatoDecimalMostrar(montoTotalSoles));
        txtImporteTotaVenta_internaD.setText("USD " + Metodos.formatoDecimalMostrar(montoTotalDolares));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TabVenta_interna = new javax.swing.JTabbedPane();
        jpnlFacturas = new javax.swing.JPanel();
        btnNuevaPresupuesto = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBusquedaVenta_interna = new org.edisoncor.gui.textField.TextFieldRectBackground();
        cboCantidadCargaVenta_interna = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtImporteTotaVenta_internaS = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtImporteTotaVenta_internaD = new javax.swing.JTextField();
        txtFechaFinalVenta_interna = new com.toedter.calendar.JDateChooser();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblDetalleVenta_interna = new javax.swing.JTable();
        cboEstadoVenta_interna = new javax.swing.JComboBox<>();
        txtFechaInicialVenta_interna = new com.toedter.calendar.JDateChooser();
        btnNotaVenta = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridLayout(1, 0));

        TabVenta_interna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jpnlFacturas.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevaPresupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevaPresupuesto.setText("Nuevo");
        btnNuevaPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaPresupuestoActionPerformed(evt);
            }
        });

        jLabel1.setText("Buscar:");

        txtBusquedaVenta_interna.setAnchoDeBorde(1.0F);
        txtBusquedaVenta_interna.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusquedaVenta_interna.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtBusquedaVenta_interna.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaVenta_internaKeyTyped(evt);
            }
        });

        cboCantidadCargaVenta_interna.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCargaVenta_interna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCargaVenta_interna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaVenta_internaActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setText("Importe Total:");

        txtImporteTotaVenta_internaS.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaVenta_internaS.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaVenta_internaS.setText("0.00");
        txtImporteTotaVenta_internaS.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaVenta_internaS, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtImporteTotaVenta_internaS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel14.setText("Importe Total:");

        txtImporteTotaVenta_internaD.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaVenta_internaD.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaVenta_internaD.setText("0.00");
        txtImporteTotaVenta_internaD.setEnabled(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaVenta_internaD, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtImporteTotaVenta_internaD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtFechaFinalVenta_interna.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jPanel7.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalleVenta_interna.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalleVenta_interna.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalleVenta_interna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalleVenta_interna.setFillsViewportHeight(true);
        tblDetalleVenta_interna.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalleVenta_interna.getTableHeader().setReorderingAllowed(false);
        tblDetalleVenta_interna.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleVenta_internaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tblDetalleVenta_internaMouseEntered(evt);
            }
        });
        jScrollPane7.setViewportView(tblDetalleVenta_interna);

        jPanel7.add(jScrollPane7);

        cboEstadoVenta_interna.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "GENERADO", "FACTURADO", "ANULADO", "TODOS" }));
        cboEstadoVenta_interna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboEstadoVenta_interna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEstadoVenta_internaActionPerformed(evt);
            }
        });

        txtFechaInicialVenta_interna.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnNotaVenta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNotaVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        btnNotaVenta.setText("GENERAR REPORTE");
        btnNotaVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNotaVenta.setFocusable(false);
        btnNotaVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnNotaVenta.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNotaVenta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNotaVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotaVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnlFacturasLayout = new javax.swing.GroupLayout(jpnlFacturas);
        jpnlFacturas.setLayout(jpnlFacturasLayout);
        jpnlFacturasLayout.setHorizontalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusquedaVenta_interna, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboEstadoVenta_interna, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaInicialVenta_interna, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFinalVenta_interna, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCargaVenta_interna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(btnNotaVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpnlFacturasLayout.setVerticalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cboCantidadCargaVenta_interna, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinalVenta_interna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaInicialVenta_interna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboEstadoVenta_interna, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBusquedaVenta_interna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(btnNotaVenta)
                        .addContainerGap())))
        );

        TabVenta_interna.addTab(".:: Notas de Venta ::.", jpnlFacturas);

        add(TabVenta_interna);
    }// </editor-fold>//GEN-END:initComponents

    private void cboEstadoVenta_internaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEstadoVenta_internaActionPerformed

        cargarVenta_internas();

    }//GEN-LAST:event_cboEstadoVenta_internaActionPerformed

    private void tblDetalleVenta_internaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleVenta_internaMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tblDetalleVenta_internaMouseEntered

    private void tblDetalleVenta_internaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleVenta_internaMouseClicked
        int fila = tblDetalleVenta_interna.getSelectedRow();
        if (fila == -1) {
            return;
        }
        String id = tblDetalleVenta_interna.getValueAt(fila, 0).toString();
        String estado = tblDetalleVenta_interna.getValueAt(fila, 4).toString();
        String comprobante_relacionado = tblDetalleVenta_interna.getValueAt(fila, 5).toString();
        int colum = tblDetalleVenta_interna.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetalleVenta_interna.getRowHeight();

        if (row < tblDetalleVenta_interna.getRowCount() && row >= 0 && colum < tblDetalleVenta_interna.getColumnCount() && colum >= 0) {
            Object value = tblDetalleVenta_interna.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("botonVerVenta_interna")) {

                    JPanelVenta_interna jpbn = new JPanelVenta_interna(id, 1, "", "");
                    Metodos.CambiarPanel(jpbn);

                } else if (boton.getName().equals("btnModificarVenta_interna")) {

                    ////evaluamos si ya tiene un comprobante relacionado  
                    if (comprobante_relacionado.equalsIgnoreCase("")) {
                        if (estado.equalsIgnoreCase("ANULADO")) {
                            Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                            recuperarVenta_interna(id);
                        } else {
                            JPanelVenta_interna jpbn = new JPanelVenta_interna(id, 0, "", "");
                            Metodos.CambiarPanel(jpbn);
                        }

                        ////si cuenta con n CPE RELACIONADO no puede editar
                    } else if (!comprobante_relacionado.equalsIgnoreCase("")) {
                        Metodos.MensajeInformacion("El comprobante seleccionado cuenta con un CPE.");
                    }

                } else  if (boton.getName().equals("btnAnularVenta_interna")) {

                    if (estado.equalsIgnoreCase("ANULADO")) {
                        Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                        recuperarVenta_interna(id);
                    } else {

                        ////evaluamos si ya tiene un comprobante relacionado  
                        if (comprobante_relacionado.equalsIgnoreCase("")) {
                            int seleccion = JOptionPane.showOptionDialog(null,
                                    "Se ha detectado que el Comprobante " + id + " se Generado de Forma Correcta. DESEAS ANULARLO???.\n"
                                    + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                                    "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                                    new Object[]{" Anular Nota de Venta ", " Cancelar Operación "}, " Cancelar Operación ");
                            if (seleccion == 0) {

                                String motivo_anulacion = Venta_internaImpl.obtenerDatosVenta_int("motivo_anulacion", "id", id);
                                new Form_Motivo_anulacion(null, true, motivo_anulacion).setVisible(true);
                                if (Form_Motivo_anulacion.valor == 1) {
                                    String descrip_Anulacion = "ANULACION - " + Form_Motivo_anulacion.motivo_anulacion;
                                    Venta_internaImpl.actualizarDatosVenta_int("motivo_anulacion", descrip_Anulacion, id);
                                    Venta_internaImpl.actualizarDatosVenta_int("estado_comprobante", "ANULADO", id);
 
                                        //////si en caso usa gestion de stock anulamos los item y retorna el stock
                                        Venta_internaImpl.anularVenta_internas_Det(id); 

                                    cargarVenta_interna(id, fila);
                                    Metodos.MensajeInformacion("Anulación de emisión de forma correcta.");
                                } else {
                                    Metodos.MensajeAlerta("Operación de Anulación Cancelada.");
                                }

                            } else if (seleccion == 1) {
                                Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
                            }
                            ////si cuenta con n CPE RELACIONADO no puede editar
                        } else if (!comprobante_relacionado.equalsIgnoreCase("")) {
                            Metodos.MensajeInformacion("El comprobante seleccionado cuenta con un CPE.");
                        }
                    }

                } else if (boton.getName().equals("btnImprimirVenta_interna")) {
                    try {
                        ////LLAMAMOS AL MODAL DE IMPRESIÓN
                        new Imp_Comprobantes(null, true, id, "Venta_interna").setVisible(true);
                    } catch (Exception e) {
                        Metodos.MensajeError("Error al generar vista de impresión.");
                    }
                }

            }
        }
    }//GEN-LAST:event_tblDetalleVenta_internaMouseClicked

    public void recuperarVenta_interna(String id) {

        int seleccion = JOptionPane.showOptionDialog(null,
                "Se ha detectado que el Comprobante " + id + " se encuentra Anulado.\n"
                + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Recuperar Nota de Venta ", " Cancelar Operación "}, " Cancelar Operación ");
        if (seleccion == 0) {

            JPanelVenta_interna jpbn = new JPanelVenta_interna("", 0, id, "");
            Metodos.CambiarPanel(jpbn);

        } else if (seleccion == 1) {
            Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
        }

    }
    private void cboCantidadCargaVenta_internaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaVenta_internaActionPerformed

        cargarVenta_internas();
    }//GEN-LAST:event_cboCantidadCargaVenta_internaActionPerformed

    private void txtBusquedaVenta_internaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaVenta_internaKeyTyped

    }//GEN-LAST:event_txtBusquedaVenta_internaKeyTyped

    private void btnNuevaPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaPresupuestoActionPerformed
        JPanelVenta_interna jpfn = new JPanelVenta_interna("", 0, "", "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnNuevaPresupuestoActionPerformed

    private void btnNotaVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotaVentaActionPerformed
        
        new Formulario_GenerarRepoFechas(null, true, "Ventas_internas").setVisible(true);
    }//GEN-LAST:event_btnNotaVentaActionPerformed

    private void cargarVenta_interna(String id_venta_interna, int fila) {
        Venta_internaImpl.buscarVenta_interna("select * from venta_interna_view where id_venta_interna='" + id_venta_interna + "' ");
        String id = Venta_internaImpl.getVenta_interna().getId();
        String cliente = Venta_internaImpl.getVenta_interna().getCliente();
        String moneda = Venta_internaImpl.getVenta_interna().getMoneda();
        String importe_total = "" + Venta_internaImpl.getVenta_interna().getImporteTotal();
        String estado_comprobante = Venta_internaImpl.getVenta_interna().getEstado_comprobante();

        tblDetalleVenta_interna.setValueAt(id, fila, 0);
        tblDetalleVenta_interna.setValueAt(cliente, fila, 1);
        tblDetalleVenta_interna.setValueAt(moneda, fila, 2);
        tblDetalleVenta_interna.setValueAt(importe_total, fila, 3);
        tblDetalleVenta_interna.setValueAt(estado_comprobante, fila, 4);

        String comboEstado = cboEstadoVenta_interna.getSelectedItem().toString();
        if (comboEstado.equalsIgnoreCase("GENERADO")) {
            if (estado_comprobante.equalsIgnoreCase("ANULADO") || estado_comprobante.equalsIgnoreCase("FACTURADO")) {
                quitar_listadoVenta_internas();
            }
        }
    }

    public void quitar_listadoVenta_internas() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetalleVenta_interna.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetalleVenta_interna.getSelectedRow();//obtiene la fila 
            dtmVenta_interna.removeRow(fila);//remueve la fila obtenida 
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane TabVenta_interna;
    private javax.swing.JButton btnNotaVenta;
    private javax.swing.JButton btnNuevaPresupuesto;
    private javax.swing.JComboBox<String> cboCantidadCargaVenta_interna;
    private javax.swing.JComboBox<String> cboEstadoVenta_interna;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPanel jpnlFacturas;
    private javax.swing.JTable tblDetalleVenta_interna;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusquedaVenta_interna;
    private com.toedter.calendar.JDateChooser txtFechaFinalVenta_interna;
    private com.toedter.calendar.JDateChooser txtFechaInicialVenta_interna;
    private javax.swing.JTextField txtImporteTotaVenta_internaD;
    private javax.swing.JTextField txtImporteTotaVenta_internaS;
    // End of variables declaration//GEN-END:variables
}
