package factura_free.vista.mantenimiento;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.CompraImpl;
import factura_free.clases.modelo.ObCompraView;
import factura_free.clases.principales.Datos;
import factura_free.vista.modales.auxiliares.Form_Motivo_anulacion;
import factura_free.vista.modales.impresion.Imp_Comprobantes;
import factura_free.vista.modales.paneles.JPanelCompra;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;

public class JPanelCompras extends javax.swing.JPanel {

    Fecha_Date fec = new Fecha_Date();
    DefaultTableModel dtmCompra = null;
    ArrayList<ObCompraView> lista_ObCompraView = null;

    public JPanelCompras() {
        initComponents();

        fec.capturaymuestrahoradelsistema(txtFechaInicialCompra);
        fec.capturaymuestrahoradelsistema(txtFechaFinalCompra);
        tblDetalleCompra.setDefaultRenderer(Object.class, new ImgTabla());
        mostrar_TablaCompras();

        txtBusquedaCompra.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarCompras();
            }
        });

        txtFechaInicialCompra.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarCompras();
            }
        });

        txtFechaFinalCompra.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarCompras();
            }
        });

    }

    public void mostrar_TablaCompras() {
        dtmCompra = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        dtmCompra.addColumn("EMISIÓN");
        dtmCompra.addColumn("CLIENTE");
        dtmCompra.addColumn("MONEDA");
        dtmCompra.addColumn("TOTAL");
        dtmCompra.addColumn("ESTADO");
        dtmCompra.addColumn("DOC. RELACIO.");
        dtmCompra.addColumn("");
        dtmCompra.addColumn("");
        dtmCompra.addColumn("");
        dtmCompra.addColumn("");
        tblDetalleCompra.setRowHeight(23);
        tblDetalleCompra.setModel(dtmCompra);
        tblDetalleCompra.setBackground(Color.WHITE);
        tblDetalleCompra.setAutoResizeMode(tblDetalleCompra.AUTO_RESIZE_OFF);

        tblDetalleCompra.getColumnModel().getColumn(0).setPreferredWidth(100);
        tblDetalleCompra.getColumnModel().getColumn(1).setPreferredWidth(480);
        tblDetalleCompra.getColumnModel().getColumn(2).setPreferredWidth(60);
        tblDetalleCompra.getColumnModel().getColumn(3).setPreferredWidth(55);
        tblDetalleCompra.getColumnModel().getColumn(4).setPreferredWidth(70);
        tblDetalleCompra.getColumnModel().getColumn(5).setPreferredWidth(100);
        tblDetalleCompra.getColumnModel().getColumn(6).setPreferredWidth(28);
        tblDetalleCompra.getColumnModel().getColumn(7).setPreferredWidth(28);
        tblDetalleCompra.getColumnModel().getColumn(8).setPreferredWidth(28);
        tblDetalleCompra.getColumnModel().getColumn(9).setPreferredWidth(28);
    }

    public String leeFechaFinalCompra() {
        return fec.mostrar_fecha(txtFechaFinalCompra);
    }

    public String leeFechaInicialCompra() {
        return fec.mostrar_fecha(txtFechaInicialCompra);
    }

    private void cargarCompras() {
        Metodos.LimpiarTabla(dtmCompra);
        double montoTotalSoles = 0;
        double montoTotalDolares = 0;
        int cntCarga = Integer.parseInt(cboCantidadCargaCompra.getSelectedItem().toString());
        String estado_comprobante = cboEstadoCompra.getSelectedItem().toString();
        String addConsulta = "";
        if (!estado_comprobante.equalsIgnoreCase("TODOS")) {
            addConsulta = " and estado_comprobante='" + estado_comprobante + "' ";
        }

        String consulta = "select * from compra_view where CONCAT(id_compra,'-',cliente) like '%" + txtBusquedaCompra.getText() + "%' and "
                + " fecha_emision>='" + leeFechaInicialCompra() + "' and fecha_emision<='" + leeFechaFinalCompra() + "' "
                + " " + addConsulta + " order by id_compra desc limit 0," + cntCarga + ";";

        lista_ObCompraView = (ArrayList<ObCompraView>) CompraImpl.listarCompras(consulta);
        Object datos[] = new Object[15];
        for (int i = 0; i < lista_ObCompraView.size(); i++) {
            datos[0] = (lista_ObCompraView.get(i).getId()) + "";
            datos[1] = (lista_ObCompraView.get(i).getCliente());
            datos[2] = (lista_ObCompraView.get(i).getMoneda()) + "";
            datos[3] = (lista_ObCompraView.get(i).getImporteTotal()) + "";
            datos[4] = (lista_ObCompraView.get(i).getEstado_comprobante()) + "";
            datos[5] = (lista_ObCompraView.get(i).getComprobante_relacionado()) + "";

            if (lista_ObCompraView.get(i).getEstado_comprobante().equalsIgnoreCase("GENERADO")
                    && lista_ObCompraView.get(i).getComprobante_relacionado().equalsIgnoreCase("")) {
                if (lista_ObCompraView.get(i).getMoneda().equalsIgnoreCase("PEN")) {
                    montoTotalSoles = montoTotalSoles + lista_ObCompraView.get(i).getImporteTotal();
                } else {
                    montoTotalDolares = montoTotalDolares + lista_ObCompraView.get(i).getImporteTotal();
                }
            }

            ImageIcon iconoModiCompra = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificarCompra = new ImageIcon(iconoModiCompra.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificarCompra = new JButton("", btnModificarCompra);
            botonModificarCompra.setName("btnModificarCompra");
            botonModificarCompra.setToolTipText("Modificar Registro");
            datos[6] = botonModificarCompra;

            ImageIcon iconoVerCompra = new ImageIcon(getClass().getResource("/img/VerTabla_40px.png"));
            Icon btnVerCompra = new ImageIcon(iconoVerCompra.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonVerCompra = new JButton("", btnVerCompra);
            botonVerCompra.setName("botonVerCompra");
            botonVerCompra.setToolTipText("Ver Comprobante");
            datos[7] = botonVerCompra;

            ImageIcon iconoAnularCompra = new ImageIcon(getClass().getResource("/img/anular_16px.png"));
            Icon btnAnularCompra = new ImageIcon(iconoAnularCompra.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonAnularCompra = new JButton("", btnAnularCompra);
            botonAnularCompra.setName("btnAnularCompra");
            botonAnularCompra.setToolTipText("Anular Comprobante");
            datos[8] = botonAnularCompra;

            ImageIcon iconoImprimirCompra = new ImageIcon(getClass().getResource("/img/Print_16px.png"));
            Icon btnImprimirCompra = new ImageIcon(iconoImprimirCompra.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonImprimirCompra = new JButton("", btnImprimirCompra);
            botonImprimirCompra.setName("btnImprimirCompra");
            botonImprimirCompra.setToolTipText("Opciones de Impresión");
            datos[9] = botonImprimirCompra;

            dtmCompra.addRow(datos);
        }
        txtImporteTotaCompraS.setText("S/." + Metodos.formatoDecimalMostrar(montoTotalSoles));
        txtImporteTotaCompraD.setText("USD " + Metodos.formatoDecimalMostrar(montoTotalDolares));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TabVenta_interna = new javax.swing.JTabbedPane();
        jpnlFacturas = new javax.swing.JPanel();
        btnNuevaPresupuesto = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBusquedaCompra = new org.edisoncor.gui.textField.TextFieldRectBackground();
        cboCantidadCargaCompra = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtImporteTotaCompraS = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtImporteTotaCompraD = new javax.swing.JTextField();
        txtFechaFinalCompra = new com.toedter.calendar.JDateChooser();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblDetalleCompra = new javax.swing.JTable();
        cboEstadoCompra = new javax.swing.JComboBox<>();
        txtFechaInicialCompra = new com.toedter.calendar.JDateChooser();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridLayout(1, 0));

        TabVenta_interna.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jpnlFacturas.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevaPresupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevaPresupuesto.setText("Nuevo");
        btnNuevaPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaPresupuestoActionPerformed(evt);
            }
        });

        jLabel1.setText("Buscar:");

        txtBusquedaCompra.setAnchoDeBorde(1.0F);
        txtBusquedaCompra.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusquedaCompra.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtBusquedaCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaCompraKeyTyped(evt);
            }
        });

        cboCantidadCargaCompra.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCargaCompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCargaCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaCompraActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setText("Importe Total:");

        txtImporteTotaCompraS.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaCompraS.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaCompraS.setText("0.00");
        txtImporteTotaCompraS.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaCompraS, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtImporteTotaCompraS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel14.setText("Importe Total:");

        txtImporteTotaCompraD.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaCompraD.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaCompraD.setText("0.00");
        txtImporteTotaCompraD.setEnabled(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaCompraD, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtImporteTotaCompraD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtFechaFinalCompra.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jPanel7.setLayout(new java.awt.GridLayout(1, 0));

        tblDetalleCompra.setForeground(new java.awt.Color(255, 255, 255));
        tblDetalleCompra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetalleCompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetalleCompra.setFillsViewportHeight(true);
        tblDetalleCompra.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetalleCompra.getTableHeader().setReorderingAllowed(false);
        tblDetalleCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleCompraMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tblDetalleCompraMouseEntered(evt);
            }
        });
        jScrollPane7.setViewportView(tblDetalleCompra);

        jPanel7.add(jScrollPane7);

        cboEstadoCompra.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "GENERADO", "ANULADO", "TODOS" }));
        cboEstadoCompra.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboEstadoCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEstadoCompraActionPerformed(evt);
            }
        });

        txtFechaInicialCompra.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout jpnlFacturasLayout = new javax.swing.GroupLayout(jpnlFacturas);
        jpnlFacturas.setLayout(jpnlFacturasLayout);
        jpnlFacturasLayout.setHorizontalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusquedaCompra, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboEstadoCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaInicialCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFinalCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCargaCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlFacturasLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpnlFacturasLayout.setVerticalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cboCantidadCargaCompra, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinalCompra, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaInicialCompra, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cboEstadoCompra, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBusquedaCompra, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        TabVenta_interna.addTab(".:: Compras ::.", jpnlFacturas);

        add(TabVenta_interna);
    }// </editor-fold>//GEN-END:initComponents

    private void cboEstadoCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEstadoCompraActionPerformed

        cargarCompras();

    }//GEN-LAST:event_cboEstadoCompraActionPerformed

    private void tblDetalleCompraMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleCompraMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tblDetalleCompraMouseEntered

    private void tblDetalleCompraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleCompraMouseClicked
        int fila = tblDetalleCompra.getSelectedRow();
        if (fila == -1) {
            return;
        }
        String id = tblDetalleCompra.getValueAt(fila, 0).toString();
        String estado = tblDetalleCompra.getValueAt(fila, 4).toString();
        int colum = tblDetalleCompra.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetalleCompra.getRowHeight();

        if (row < tblDetalleCompra.getRowCount() && row >= 0 && colum < tblDetalleCompra.getColumnCount() && colum >= 0) {
            Object value = tblDetalleCompra.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("botonVerCompra")) {

                    JPanelCompra jpbn = new JPanelCompra(id, 1, "");
                    Metodos.CambiarPanel(jpbn);

                } else if (boton.getName().equals("btnModificarCompra")) {

                    if (estado.equalsIgnoreCase("ANULADO")) {
                        Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                        recuperarCompra(id);
                    } else {
                        JPanelCompra jpbn = new JPanelCompra(id, 0, "");
                        Metodos.CambiarPanel(jpbn);
                    }

                } else if (boton.getName().equals("btnAnularCompra")) {

                    if (estado.equalsIgnoreCase("ANULADO")) {
                        Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                        recuperarCompra(id);
                    } else {

                        int seleccion = JOptionPane.showOptionDialog(null,
                                "Se ha detectado que el Comprobante " + id + " se Generado de Forma Correcta. DESEAS ANULARLO???.\n"
                                + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                                new Object[]{" Anular Compra ", " Cancelar Operación "}, " Cancelar Operación ");
                        if (seleccion == 0) {

                            String motivo_anulacion = CompraImpl.obtenerDatosCompra("motivo_anulacion", "id", id);
                            new Form_Motivo_anulacion(null, true, motivo_anulacion).setVisible(true);
                            if (Form_Motivo_anulacion.valor == 1) {
                                String descrip_Anulacion = "ANULACION - " + Form_Motivo_anulacion.motivo_anulacion;
                                CompraImpl.actualizarDatosCompra("motivo_anulacion", descrip_Anulacion, id);
                                CompraImpl.actualizarDatosCompra("estado_comprobante", "ANULADO", id);
 
                                    //////si en caso usa gestion de stock anulamos los item y retorna el stock
                                    CompraImpl.anularCompra_Det(id); 

                                cargarCompra(id, fila);
                                Metodos.MensajeInformacion("Anulación de emisión de forma correcta.");
                            } else {
                                Metodos.MensajeAlerta("Operación de Anulación Cancelada.");
                            }

                        } else if (seleccion == 1) {
                            Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
                        }

                    }

                } else if (boton.getName().equals("btnImprimirCompra")) {
                    try {
                        ////LLAMAMOS AL MODAL DE IMPRESIÓN
                        new Imp_Comprobantes(null, true, id, "Compra").setVisible(true);
                    } catch (Exception e) {
                        Metodos.MensajeError("Error al generar vista de impresión.");
                    }
                }

            }
        }
    }//GEN-LAST:event_tblDetalleCompraMouseClicked

    public void recuperarCompra(String id) {

        int seleccion = JOptionPane.showOptionDialog(null,
                "Se ha detectado que el Comprobante " + id + " se encuentra Anulado.\n"
                + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Recuperar Compra ", " Cancelar Operación "}, " Cancelar Operación ");
        if (seleccion == 0) {

            JPanelCompra jpbn = new JPanelCompra("", 0, id);
            Metodos.CambiarPanel(jpbn);

        } else if (seleccion == 1) {
            Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
        }

    }
    private void cboCantidadCargaCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaCompraActionPerformed

        cargarCompras();
    }//GEN-LAST:event_cboCantidadCargaCompraActionPerformed

    private void txtBusquedaCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaCompraKeyTyped

    }//GEN-LAST:event_txtBusquedaCompraKeyTyped

    private void btnNuevaPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaPresupuestoActionPerformed
        JPanelCompra jpfn = new JPanelCompra("", 0, "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnNuevaPresupuestoActionPerformed

    private void cargarCompra(String id_compra, int fila) {
        CompraImpl.buscarCompra("select * from compra_view where id_compra='" + id_compra + "' ");
        String id = CompraImpl.getCompra().getId();
        String cliente = CompraImpl.getCompra().getCliente();
        String moneda = CompraImpl.getCompra().getMoneda();
        String importe_total = "" + CompraImpl.getCompra().getImporteTotal();
        String estado_comprobante = CompraImpl.getCompra().getEstado_comprobante();

        tblDetalleCompra.setValueAt(id, fila, 0);
        tblDetalleCompra.setValueAt(cliente, fila, 1);
        tblDetalleCompra.setValueAt(moneda, fila, 2);
        tblDetalleCompra.setValueAt(importe_total, fila, 3);
        tblDetalleCompra.setValueAt(estado_comprobante, fila, 4);

        String comboEstado = cboEstadoCompra.getSelectedItem().toString();
        if (comboEstado.equalsIgnoreCase("GENERADO")) {
            if (estado_comprobante.equalsIgnoreCase("ANULADO") || estado_comprobante.equalsIgnoreCase("FACTURADO")) {
                quitar_listadoCompras();
            }
        }
    }

    public void quitar_listadoCompras() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetalleCompra.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetalleCompra.getSelectedRow();//obtiene la fila 
            dtmCompra.removeRow(fila);//remueve la fila obtenida 
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane TabVenta_interna;
    private javax.swing.JButton btnNuevaPresupuesto;
    private javax.swing.JComboBox<String> cboCantidadCargaCompra;
    private javax.swing.JComboBox<String> cboEstadoCompra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPanel jpnlFacturas;
    private javax.swing.JTable tblDetalleCompra;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusquedaCompra;
    private com.toedter.calendar.JDateChooser txtFechaFinalCompra;
    private com.toedter.calendar.JDateChooser txtFechaInicialCompra;
    private javax.swing.JTextField txtImporteTotaCompraD;
    private javax.swing.JTextField txtImporteTotaCompraS;
    // End of variables declaration//GEN-END:variables
}
