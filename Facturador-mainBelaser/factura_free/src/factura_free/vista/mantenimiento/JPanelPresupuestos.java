package factura_free.vista.mantenimiento;

import factura_free.clases.principales.alternos.Fecha_Date;
import factura_free.clases.principales.alternos.ImgTabla;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.controlador.PresupuestoImpl;
import factura_free.clases.modelo.ObPresupuestoView;
import factura_free.vista.modales.auxiliares.Form_Motivo_anulacion;
import factura_free.vista.modales.auxiliares.Formulario_GenerarRepoFechas;
import factura_free.vista.modales.impresion.Imp_Comprobantes;
import factura_free.vista.modales.paneles.JPanelPresupuesto;
import factura_free.vista.modales.paneles.JPanelVenta_interna;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;

public class JPanelPresupuestos extends javax.swing.JPanel {

    Fecha_Date fec = new Fecha_Date();
    DefaultTableModel dtmPresupuesto = null;
    ArrayList<ObPresupuestoView> lista_ObPresupuestoView = null;

    public JPanelPresupuestos() {
        initComponents();

        fec.capturaymuestrahoradelsistema(txtFechaInicialPresupuesto);
        fec.capturaymuestrahoradelsistema(txtFechaFinalPresupuesto);
        tblDetallePresupuesto.setDefaultRenderer(Object.class, new ImgTabla());
        mostrar_TablaPresupuestos();

        txtBusquedaPresupuesto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cargarPresupuestos();
            }
        });

        txtFechaInicialPresupuesto.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarPresupuestos();
            }
        });

        txtFechaFinalPresupuesto.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                cargarPresupuestos();
            }
        });

    }

    public void mostrar_TablaPresupuestos() {
        dtmPresupuesto = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        dtmPresupuesto.addColumn("EMISIÓN");
        dtmPresupuesto.addColumn("VENCIMIENTO");
        dtmPresupuesto.addColumn("CLIENTE");
        dtmPresupuesto.addColumn("MONEDA");
        dtmPresupuesto.addColumn("TOTAL");
        dtmPresupuesto.addColumn("ESTADO");
        dtmPresupuesto.addColumn("CPE RELACIO.");
        dtmPresupuesto.addColumn("");
        dtmPresupuesto.addColumn("");
        dtmPresupuesto.addColumn("");
        dtmPresupuesto.addColumn("");
        dtmPresupuesto.addColumn("");
        tblDetallePresupuesto.setRowHeight(23);
        tblDetallePresupuesto.setModel(dtmPresupuesto);
        tblDetallePresupuesto.setBackground(Color.WHITE);
        tblDetallePresupuesto.setAutoResizeMode(tblDetallePresupuesto.AUTO_RESIZE_OFF);

        tblDetallePresupuesto.getColumnModel().getColumn(0).setPreferredWidth(95);
        tblDetallePresupuesto.getColumnModel().getColumn(1).setPreferredWidth(90);
        tblDetallePresupuesto.getColumnModel().getColumn(2).setPreferredWidth(400);
        tblDetallePresupuesto.getColumnModel().getColumn(3).setPreferredWidth(60);
        tblDetallePresupuesto.getColumnModel().getColumn(4).setPreferredWidth(55);
        tblDetallePresupuesto.getColumnModel().getColumn(5).setPreferredWidth(65);
        tblDetallePresupuesto.getColumnModel().getColumn(6).setPreferredWidth(100);
        tblDetallePresupuesto.getColumnModel().getColumn(7).setPreferredWidth(28);
        tblDetallePresupuesto.getColumnModel().getColumn(8).setPreferredWidth(28);
        tblDetallePresupuesto.getColumnModel().getColumn(9).setPreferredWidth(28);
        tblDetallePresupuesto.getColumnModel().getColumn(10).setPreferredWidth(28);
        tblDetallePresupuesto.getColumnModel().getColumn(11).setPreferredWidth(28);
    }

    public String leeFechaFinalPresupuesto() {
        return fec.mostrar_fecha(txtFechaFinalPresupuesto);
    }

    public String leeFechaInicialPresupuesto() {
        return fec.mostrar_fecha(txtFechaInicialPresupuesto);
    }

    private void cargarPresupuestos() {
        Metodos.LimpiarTabla(dtmPresupuesto);
        double montoTotalSoles = 0;
        double montoTotalDolares = 0;
        int cntCarga = Integer.parseInt(cboCantidadCargaPresupuesto.getSelectedItem().toString());
        String estado_comprobante = cboEstadoPresupuesto.getSelectedItem().toString();
        String addConsulta = "";
        if (!estado_comprobante.equalsIgnoreCase("TODOS")) {
            addConsulta = " and estado_comprobante='" + estado_comprobante + "' ";
        }

        String consulta = "select * from presupuesto_view where CONCAT(id_presupuesto,'-',cliente) like '%" + txtBusquedaPresupuesto.getText() + "%' and "
                + " fecha_emision>='" + leeFechaInicialPresupuesto() + "' and fecha_emision<='" + leeFechaFinalPresupuesto() + "' "
                + " " + addConsulta + " order by id_presupuesto desc limit 0," + cntCarga + ";";

        lista_ObPresupuestoView = (ArrayList<ObPresupuestoView>) PresupuestoImpl.listarPresupuestos(consulta);
        Object datos[] = new Object[15];
        for (int i = 0; i < lista_ObPresupuestoView.size(); i++) {
            datos[0] = (lista_ObPresupuestoView.get(i).getId()) + "";
            datos[1] = (lista_ObPresupuestoView.get(i).getFechaVencimiento()) + "";
            datos[2] = (lista_ObPresupuestoView.get(i).getCliente());
            datos[3] = (lista_ObPresupuestoView.get(i).getMoneda()) + "";
            datos[4] = (lista_ObPresupuestoView.get(i).getImporteTotal()) + "";
            datos[5] = (lista_ObPresupuestoView.get(i).getEstado_comprobante()) + "";
            datos[6] = (lista_ObPresupuestoView.get(i).getComprobante_relacionado()) + "";

            if (!lista_ObPresupuestoView.get(i).getEstado_comprobante().equalsIgnoreCase("ANULADO")) {
                if (lista_ObPresupuestoView.get(i).getMoneda().equalsIgnoreCase("PEN")) {
                    montoTotalSoles = montoTotalSoles + lista_ObPresupuestoView.get(i).getImporteTotal();
                } else {
                    montoTotalDolares = montoTotalDolares + lista_ObPresupuestoView.get(i).getImporteTotal();
                }
            }

            ImageIcon iconoModiPresupuesto = new ImageIcon(getClass().getResource("/img/EditarTabla_40px.png"));
            Icon btnModificarPresupuesto = new ImageIcon(iconoModiPresupuesto.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonModificarPresupuesto = new JButton("", btnModificarPresupuesto);
            botonModificarPresupuesto.setName("btnModificarPresupuesto");
            botonModificarPresupuesto.setToolTipText("Modificar Registro");
            datos[7] = botonModificarPresupuesto;

            ImageIcon iconoVerPresupuesto = new ImageIcon(getClass().getResource("/img/VerTabla_40px.png"));
            Icon btnVerPresupuesto = new ImageIcon(iconoVerPresupuesto.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonVerPresupuesto = new JButton("", btnVerPresupuesto);
            botonVerPresupuesto.setName("botonVerPresupuesto");
            botonVerPresupuesto.setToolTipText("Ver Comprobante");
            datos[8] = botonVerPresupuesto;

            ImageIcon iconoAnularPresupuesto = new ImageIcon(getClass().getResource("/img/anular_16px.png"));
            Icon btnAnularPresupuesto = new ImageIcon(iconoAnularPresupuesto.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonAnularPresupuesto = new JButton("", btnAnularPresupuesto);
            botonAnularPresupuesto.setName("btnAnularPresupuesto");
            botonAnularPresupuesto.setToolTipText("Anular Comprobante");
            datos[9] = botonAnularPresupuesto;

            ImageIcon iconoImprimirPresupuesto = new ImageIcon(getClass().getResource("/img/Print_16px.png"));
            Icon btnImprimirPresupuesto = new ImageIcon(iconoImprimirPresupuesto.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonImprimirPresupuesto = new JButton("", btnImprimirPresupuesto);
            botonImprimirPresupuesto.setName("btnImprimirPresupuesto");
            botonImprimirPresupuesto.setToolTipText("Opciones de Impresión");
            datos[10] = botonImprimirPresupuesto;

            ImageIcon iconoSunatPresupuesto = new ImageIcon(getClass().getResource("/img/Check_16px.png"));
            Icon btnSunatPresupuesto = new ImageIcon(iconoSunatPresupuesto.getImage().getScaledInstance(14, 14, Image.SCALE_DEFAULT));
            JButton botonSunatPresupuesto = new JButton("", btnSunatPresupuesto);
            botonSunatPresupuesto.setName("btnSunatPresupuesto");
            botonSunatPresupuesto.setToolTipText("Procesos Venta");
            datos[11] = botonSunatPresupuesto;

            dtmPresupuesto.addRow(datos);
        }
        txtImporteTotaPresupuestoS.setText("S/." + Metodos.formatoDecimalMostrar(montoTotalSoles));
        txtImporteTotaPresupuestoD.setText("USD " + Metodos.formatoDecimalMostrar(montoTotalDolares));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TabPresupuesto = new javax.swing.JTabbedPane();
        jpnlFacturas = new javax.swing.JPanel();
        btnNuevaPresupuesto = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBusquedaPresupuesto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        cboCantidadCargaPresupuesto = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txtImporteTotaPresupuestoS = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtImporteTotaPresupuestoD = new javax.swing.JTextField();
        txtFechaFinalPresupuesto = new com.toedter.calendar.JDateChooser();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblDetallePresupuesto = new javax.swing.JTable();
        cboEstadoPresupuesto = new javax.swing.JComboBox<>();
        txtFechaInicialPresupuesto = new com.toedter.calendar.JDateChooser();
        btnNotaVenta = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridLayout(1, 0));

        TabPresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jpnlFacturas.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevaPresupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/nuevo_16px.png"))); // NOI18N
        btnNuevaPresupuesto.setText("Nuevo");
        btnNuevaPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaPresupuestoActionPerformed(evt);
            }
        });

        jLabel1.setText("Buscar:");

        txtBusquedaPresupuesto.setAnchoDeBorde(1.0F);
        txtBusquedaPresupuesto.setDescripcion("Ingrese una descripción y PRESIONE ENTER");
        txtBusquedaPresupuesto.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txtBusquedaPresupuesto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBusquedaPresupuestoKeyTyped(evt);
            }
        });

        cboCantidadCargaPresupuesto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "100", "300", "500", "1000", "2500", "5000", "10000" }));
        cboCantidadCargaPresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboCantidadCargaPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCantidadCargaPresupuestoActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setText("Importe Total:");

        txtImporteTotaPresupuestoS.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaPresupuestoS.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaPresupuestoS.setText("0.00");
        txtImporteTotaPresupuestoS.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaPresupuestoS, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtImporteTotaPresupuestoS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel14.setText("Importe Total:");

        txtImporteTotaPresupuestoD.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtImporteTotaPresupuestoD.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtImporteTotaPresupuestoD.setText("0.00");
        txtImporteTotaPresupuestoD.setEnabled(false);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addGap(18, 18, 18)
                .addComponent(txtImporteTotaPresupuestoD, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtImporteTotaPresupuestoD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        txtFechaFinalPresupuesto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jPanel7.setLayout(new java.awt.GridLayout(1, 0));

        tblDetallePresupuesto.setForeground(new java.awt.Color(255, 255, 255));
        tblDetallePresupuesto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblDetallePresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblDetallePresupuesto.setFillsViewportHeight(true);
        tblDetallePresupuesto.setGridColor(new java.awt.Color(247, 247, 247));
        tblDetallePresupuesto.getTableHeader().setReorderingAllowed(false);
        tblDetallePresupuesto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetallePresupuestoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tblDetallePresupuestoMouseEntered(evt);
            }
        });
        jScrollPane7.setViewportView(tblDetallePresupuesto);

        jPanel7.add(jScrollPane7);

        cboEstadoPresupuesto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PENDIENTE", "GENERADO", "ANULADO", "TODOS" }));
        cboEstadoPresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cboEstadoPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEstadoPresupuestoActionPerformed(evt);
            }
        });

        txtFechaInicialPresupuesto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnNotaVenta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNotaVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        btnNotaVenta.setText("GENERAR REPORTE");
        btnNotaVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNotaVenta.setFocusable(false);
        btnNotaVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnNotaVenta.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNotaVenta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNotaVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotaVentaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnlFacturasLayout = new javax.swing.GroupLayout(jpnlFacturas);
        jpnlFacturas.setLayout(jpnlFacturasLayout);
        jpnlFacturasLayout.setHorizontalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusquedaPresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboEstadoPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaInicialPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtFechaFinalPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCantidadCargaPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(btnNotaVenta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jpnlFacturasLayout.setVerticalGroup(
            jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnlFacturasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cboCantidadCargaPresupuesto, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtFechaFinalPresupuesto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtBusquedaPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cboEstadoPresupuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnNuevaPresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFechaInicialPresupuesto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnlFacturasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnlFacturasLayout.createSequentialGroup()
                        .addComponent(btnNotaVenta)
                        .addContainerGap())))
        );

        TabPresupuesto.addTab(".:: Presupuestos ::.", jpnlFacturas);

        add(TabPresupuesto);
    }// </editor-fold>//GEN-END:initComponents

    private void cboEstadoPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEstadoPresupuestoActionPerformed

        cargarPresupuestos();
    }//GEN-LAST:event_cboEstadoPresupuestoActionPerformed

    private void tblDetallePresupuestoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetallePresupuestoMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tblDetallePresupuestoMouseEntered

    private void tblDetallePresupuestoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetallePresupuestoMouseClicked
        int fila = tblDetallePresupuesto.getSelectedRow();
        if (fila == -1) {
            return;
        }
        String id = tblDetallePresupuesto.getValueAt(fila, 0).toString();
        String estado = tblDetallePresupuesto.getValueAt(fila, 5).toString();
        String comprobante_relacionado = tblDetallePresupuesto.getValueAt(fila, 6).toString();
        int colum = tblDetallePresupuesto.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / tblDetallePresupuesto.getRowHeight();

        if (row < tblDetallePresupuesto.getRowCount() && row >= 0 && colum < tblDetallePresupuesto.getColumnCount() && colum >= 0) {
            Object value = tblDetallePresupuesto.getValueAt(row, colum);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("botonVerPresupuesto")) {

                    JPanelPresupuesto jpbn = new JPanelPresupuesto(id, 1, "");
                    Metodos.CambiarPanel(jpbn);

                } else if (boton.getName().equals("btnModificarPresupuesto")) {

                    ////evaluamos si ya tiene un comprobante relacionado  
                    if (comprobante_relacionado.equalsIgnoreCase("")) {
                        if (estado.equalsIgnoreCase("ANULADO")) {
                            Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                            recuperarPresupuesto(id);
                        } else {
                            JPanelPresupuesto jpbn = new JPanelPresupuesto(id, 0, "");
                            Metodos.CambiarPanel(jpbn);
                        }

                        ////si cuenta con n CPE RELACIONADO no puede editar
                    } else if (!comprobante_relacionado.equalsIgnoreCase("")) {
                        Metodos.MensajeInformacion("El comprobante seleccionado cuenta con un CPE.");
                    }

                } else if (boton.getName().equals("btnSunatPresupuesto")) {

                    if (estado.equalsIgnoreCase("ANULADO")) {
                        Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                        recuperarPresupuesto(id);
                    } else {

                        ////evaluamos si ya tiene un comprobante relacionado  
                        if (comprobante_relacionado.equalsIgnoreCase("")) {
                            ///SI EL COMPROBANTE NO TIENE DOCUMENTO CPE RELACIONADO, MOSTRAMOS AL CLIENTE QUE LO GENERE
                            int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                                    "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                                    new Object[]{" Generar Nota de Venta ", " Cancelar Operación "}, " Cancelar Operación ");
                            if (seleccion == 0) {
                                ///si ya tiene un comprobante relacionado mostraos la opción de ver su comprobante
                                Metodos.MensajeInformacion("Emisión de Nota de Venta Aperturada.");
                                JPanelVenta_interna jpbn = new JPanelVenta_interna("", 0, "", id);
                                Metodos.CambiarPanel(jpbn);

                            } else if (seleccion == 1) {
                                Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
                            }

                        } else if (!comprobante_relacionado.equalsIgnoreCase("")) {
                            Metodos.MensajeInformacion("Emisión de Nota de Venta Aperturada.");
                            JPanelVenta_interna jpbn = new JPanelVenta_interna(comprobante_relacionado, 1, "", "");
                            Metodos.CambiarPanel(jpbn);

                        }
                    }

                } else if (boton.getName().equals("btnAnularPresupuesto")) {

                    if (estado.equalsIgnoreCase("ANULADO")) {
                        Metodos.MensajeAlerta("El comprobante seleccionado se encuentra anulado.");
                        recuperarPresupuesto(id);
                    } else {

                        ////evaluamos si ya tiene un comprobante relacionado  
                        if (comprobante_relacionado.equalsIgnoreCase("")) {
                            int seleccion = JOptionPane.showOptionDialog(null,
                                    "Se ha detectado que el Comprobante " + id + " se Generado de Forma Correcta. DESEAS ANULARLO???.\n"
                                    + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                                    "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                                    JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                                    new Object[]{" Anular Presupuesto ", " Cancelar Operación "}, " Cancelar Operación ");
                            if (seleccion == 0) {

                                String motivo_anulacion = PresupuestoImpl.obtenerDatosPresupuesto("motivo_anulacion", "id", id);
                                new Form_Motivo_anulacion(null, true, motivo_anulacion).setVisible(true);
                                if (Form_Motivo_anulacion.valor == 1) {
                                    String descrip_Anulacion = "ANULACION - " + Form_Motivo_anulacion.motivo_anulacion;
                                    PresupuestoImpl.actualizarDatosPresupuesto("motivo_anulacion", descrip_Anulacion, id);
                                    PresupuestoImpl.actualizarDatosPresupuesto("estado_comprobante", "ANULADO", id);
                                    cargarPresupuesto(id, fila);
                                    Metodos.MensajeInformacion("Anulación de emisión de forma correcta.");
                                } else {
                                    Metodos.MensajeAlerta("Operación de Anulación Cancelada.");
                                }

                            } else if (seleccion == 1) {
                                Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
                            }
                            ////si cuenta con n CPE RELACIONADO no puede editar
                        } else if (!comprobante_relacionado.equalsIgnoreCase("")) {
                            Metodos.MensajeInformacion("El comprobante seleccionado cuenta con un CPE.");
                        }
                    }

                } else if (boton.getName().equals("btnImprimirPresupuesto")) {
                    try {
                        ////LLAMAMOS AL MODAL DE IMPRESIÓN
                        new Imp_Comprobantes(null, true, id, "Presupuesto").setVisible(true);
                    } catch (Exception e) {
                        Metodos.MensajeError("Error al generar vista de impresión.");
                    }
                }

            }
        }
    }//GEN-LAST:event_tblDetallePresupuestoMouseClicked

    public void recuperarPresupuesto(String id) {

        int seleccion = JOptionPane.showOptionDialog(null,
                "Se ha detectado que el Comprobante " + id + " se encuentra Anulado.\n"
                + "Seleccione una de las opciones mostradas para poder realizar el proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Recuperar Presupuesto ", " Cancelar Operación "}, " Cancelar Operación ");
        if (seleccion == 0) {

            JPanelPresupuesto jpbn = new JPanelPresupuesto("", 0, id);
            Metodos.CambiarPanel(jpbn);

        } else if (seleccion == 1) {
            Metodos.MensajeAlerta("Solicitud de Anulación Cancelada.");
        }

    }
    private void cboCantidadCargaPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCantidadCargaPresupuestoActionPerformed
        if (TabPresupuesto.getSelectedIndex() == 1) {
            cargarPresupuestos();
        }
    }//GEN-LAST:event_cboCantidadCargaPresupuestoActionPerformed

    private void txtBusquedaPresupuestoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaPresupuestoKeyTyped

    }//GEN-LAST:event_txtBusquedaPresupuestoKeyTyped

    private void btnNuevaPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaPresupuestoActionPerformed
        JPanelPresupuesto jpfn = new JPanelPresupuesto("", 0, "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnNuevaPresupuestoActionPerformed

    private void btnNotaVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotaVentaActionPerformed

        new Formulario_GenerarRepoFechas(null, true, "Presupuestos").setVisible(true);
    }//GEN-LAST:event_btnNotaVentaActionPerformed

    private void cargarPresupuesto(String id_presupuesto, int fila) {
        PresupuestoImpl.buscarPresupuesto("select * from presupuesto_view where id_presupuesto='" + id_presupuesto + "' ");
        String id = PresupuestoImpl.getPresupuesto().getId();
        String fecha_vencimiento = PresupuestoImpl.getPresupuesto().getFechaVencimiento();
        String cliente = PresupuestoImpl.getPresupuesto().getCliente();
        String moneda = PresupuestoImpl.getPresupuesto().getMoneda();
        String importe_total = "" + PresupuestoImpl.getPresupuesto().getImporteTotal();
        String estado = "" + PresupuestoImpl.getPresupuesto().getEstado_comprobante();
        String relacionado = "" + PresupuestoImpl.getPresupuesto().getComprobante_relacionado();

        tblDetallePresupuesto.setValueAt(id, fila, 0);
        tblDetallePresupuesto.setValueAt(fecha_vencimiento, fila, 1);
        tblDetallePresupuesto.setValueAt(cliente, fila, 2);
        tblDetallePresupuesto.setValueAt(moneda, fila, 3);
        tblDetallePresupuesto.setValueAt(importe_total, fila, 4);
        tblDetallePresupuesto.setValueAt(estado, fila, 5);
        tblDetallePresupuesto.setValueAt(relacionado, fila, 6);

        String comboEstado = cboEstadoPresupuesto.getSelectedItem().toString();
        if (comboEstado.equalsIgnoreCase("PEDIENTE")) {
            if (estado.equalsIgnoreCase("ANULADO") || estado.equalsIgnoreCase("GENERADO")) {
                quitar_listadoPresupuestos();
            }
        }
    }

    public void quitar_listadoPresupuestos() {
        //obtiene el numero de filas seleccionadas
        int filas = tblDetallePresupuesto.getSelectedRowCount();
        if (filas == 0) {//si no elije ninguna fila
            JOptionPane.showMessageDialog(null, "Deve selecionar una fila o mas");
        } else {//cuando si seleciona 
            int fila = tblDetallePresupuesto.getSelectedRow();//obtiene la fila 
            dtmPresupuesto.removeRow(fila);//remueve la fila obtenida 
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane TabPresupuesto;
    private javax.swing.JButton btnNotaVenta;
    private javax.swing.JButton btnNuevaPresupuesto;
    private javax.swing.JComboBox<String> cboCantidadCargaPresupuesto;
    private javax.swing.JComboBox<String> cboEstadoPresupuesto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPanel jpnlFacturas;
    private javax.swing.JTable tblDetallePresupuesto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBusquedaPresupuesto;
    private com.toedter.calendar.JDateChooser txtFechaFinalPresupuesto;
    private com.toedter.calendar.JDateChooser txtFechaInicialPresupuesto;
    private javax.swing.JTextField txtImporteTotaPresupuestoD;
    private javax.swing.JTextField txtImporteTotaPresupuestoS;
    // End of variables declaration//GEN-END:variables
}
