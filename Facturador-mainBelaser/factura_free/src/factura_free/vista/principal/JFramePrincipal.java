package factura_free.vista.principal;
 
import factura_free.clases.controlador.ConfigImpl;  
import factura_free.vista.modales.paneles.JPanelInicio;
import factura_free.vista.modales.paneles.JPanelConfigurar;
import factura_free.clases.principales.alternos.Metodos; 
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import factura_free.vista.alertas.AlertaInfo;
import factura_free.vista.mantenimiento.JPanePersonasListar;
import factura_free.vista.mantenimiento.JPanelCompras;
import factura_free.vista.mantenimiento.JPanelPresupuestos;
import factura_free.vista.mantenimiento.JPanelProductoListar; 
import factura_free.vista.mantenimiento.JPanelUsuarioListar;
import factura_free.vista.mantenimiento.JPanelVenta_internas;
import factura_free.vista.modales.auxiliares.Form_Backup;
import factura_free.vista.modales.auxiliares.Formulario_ExportarDatos;
import factura_free.vista.modales.auxiliares.Formulario_GenerarRepoFechas;
import factura_free.vista.modales.auxiliares.Formulario_GenerarRepoFechasCliente;
import factura_free.vista.modales.auxiliares.Formulario_serverDatos;
import factura_free.vista.modales.paneles.JPanelCompra;
import factura_free.vista.modales.paneles.JPanelPresupuesto;
import factura_free.vista.modales.paneles.JPanelVenta_interna;
import factura_free.vista.modales.paneles.importar.JPanelImportarClientes;
import factura_free.vista.modales.paneles.importar.JPanelImportarProductos;
import factura_free.vista.modales.paneles.importar.JPanelImportarProveedores;
import javax.swing.ImageIcon;

public class JFramePrincipal extends javax.swing.JFrame {

    public static String rol, usuario;
    public static int id_usuario = 0;
    java.util.Timer timerProductoOnline = null;

    public JFramePrincipal(String user_rol, int id_usuario, String usuario) {
        initComponents();
        this.rol = user_rol;
        this.id_usuario = id_usuario;
        this.usuario = usuario;

        this.setSize(1100, 700);
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
        JPanelInicio jpi = new JPanelInicio(rol);
        Metodos.CambiarPanel(jpi);
        cargarRol(); 

        ConfigImpl.buscarConfig("select * from config where id = '1';");
        String Truc = ConfigImpl.getConfig().getRuc();
        String TrazonSocial = ConfigImpl.getConfig().getRazonSocial();
        String Tdireccion = ConfigImpl.getConfig().getDireccion();
        String Ttelefono = ConfigImpl.getConfig().getTelefono();
        String Tcorreo = ConfigImpl.getConfig().getCorreo();
        String Tweb = ConfigImpl.getConfig().getWeb();
        String Timpresion = ConfigImpl.getConfig().getImpresion(); 
        String Tusa_cnt_flotante = ConfigImpl.getConfig().getUsa_cnt_flotante();
        String Tproducto_igv = ConfigImpl.getConfig().getProducto_igv();
        String Tserv_datos = ConfigImpl.getConfig().getServ_datos();
        String Tcorreo_aplicacion = ConfigImpl.getConfig().getCorreo_aplicacion();
        String Tpass_aplicacion = ConfigImpl.getConfig().getPass_aplicacion();
        String Tlogo = ConfigImpl.getConfig().getLogo(); 
        String Temail_contador = ConfigImpl.getConfig().getEmail_contador();  
        String Timpre_directa = ConfigImpl.getConfig().getImpre_directa();
        Metodos.ConfigurarVentana(this, "Sistema de Facturación Electrónica - " + Truc + " - " + TrazonSocial);

        ////CARGANDO DATOS
        lblRucEmpresa.setText(Truc);
        lblRazonSocial.setText(TrazonSocial);
        lblDireccion.setText(Tdireccion);
        telefono.setText(Ttelefono);
        correo.setText(Tcorreo);
        web.setText(Tweb);
        impresion.setText(Timpresion); 
        usa_cnt_flotante.setText(Tusa_cnt_flotante);
        producto_igv.setText(Tproducto_igv);
        serv_datos.setText(Tserv_datos);
        correo_aplicacion.setText(Tcorreo_aplicacion);
        pass_aplicacion.setText(Tpass_aplicacion);
        logo.setText(Tlogo); 
        email_contador.setText(Temail_contador);  
        impre_directa.setText(Timpre_directa); 

        cerrar();

    }

    private void cargarRol() {
        if (rol.equalsIgnoreCase("USUARIO")) {
            jmiUsuarios.setEnabled(false);
        }
    }

    public void cerrar() {
        try {
            this.setDefaultCloseOperation(JFramePrincipal.DO_NOTHING_ON_CLOSE);
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent w) {
                    confirmarSalida();
                }
            });
            this.setVisible(true);
        } catch (Exception e) {
            System.err.println("error en " + e);
        }
    }

    public void confirmarSalida() {

        int seleccion = JOptionPane.showOptionDialog(null, "Seleccione una de las opciones para su proceso correspondiente",
                "Opciones", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,// null para icono por defecto.
                new Object[]{" Salir del Sistema ", " Cancelar Operación "}, " Salir del Sistema ");

        if (seleccion == 0) {

            int opcion = JOptionPane.showConfirmDialog(null,
                    "¿Desea cerrar la ventana de trabajo?\n"
                    + "Recuerde que esta Opción cerrará el Sistema", "Mensaje", JOptionPane.OK_CANCEL_OPTION);
            if (opcion == JOptionPane.OK_OPTION) {

                new Form_Backup(this, true).setVisible(true);
                System.exit(-1);
            } else {
                AlertaInfo alert = new AlertaInfo("Mensaje", "Operación Cancelada");
                alert.setVisible(true);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        telefono = new javax.swing.JLabel();
        correo = new javax.swing.JLabel();
        web = new javax.swing.JLabel();
        impresion = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        lblRazonSocial = new javax.swing.JLabel();
        lblRucEmpresa = new javax.swing.JLabel();
        usa_cnt_flotante = new javax.swing.JLabel();
        serv_datos = new javax.swing.JLabel();
        producto_igv = new javax.swing.JLabel();
        pass_aplicacion = new javax.swing.JLabel();
        correo_aplicacion = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        email_contador = new javax.swing.JLabel();
        impre_directa = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        AccesoClientes = new javax.swing.JButton();
        AccesoProductos = new javax.swing.JButton();
        AccesoPresupuesto = new javax.swing.JButton();
        AccesoVentas_internas = new javax.swing.JButton();
        AccesoCompras = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        AccesoCompras1 = new javax.swing.JButton();
        jbtnAbrirBandeja1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnNotaVenta1 = new javax.swing.JButton();
        btnPresupuesto = new javax.swing.JButton();
        btnNotaVenta = new javax.swing.JButton();
        jpnlPrincipal = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menArchivo = new javax.swing.JMenu();
        jmiInicio = new javax.swing.JMenuItem();
        jmiProductos = new javax.swing.JMenuItem();
        jmiClientes = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jmiCerrarSesion = new javax.swing.JMenuItem();
        menArchivo1 = new javax.swing.JMenu();
        MenuPresupuestos = new javax.swing.JMenuItem();
        MenuVentas_internas = new javax.swing.JMenuItem();
        menArchivo2 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jmiConfigurar4 = new javax.swing.JMenuItem();
        jmiConfigurar5 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jmiConfigurar7 = new javax.swing.JMenuItem();
        jmiConfigurar8 = new javax.swing.JMenuItem();
        jmenAdministrar = new javax.swing.JMenu();
        jmiConfigurar = new javax.swing.JMenuItem();
        jmiConfigurar1 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        jmiConfigurar9 = new javax.swing.JMenuItem();
        jmiConfigurar10 = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jmiConfigurar11 = new javax.swing.JMenuItem();
        jmiConfigurar12 = new javax.swing.JMenuItem();
        ImportarProveedor = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jmiUsuarios = new javax.swing.JMenuItem();

        telefono.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        telefono.setForeground(new java.awt.Color(255, 255, 255));
        telefono.setText("00000000000");

        correo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        correo.setForeground(new java.awt.Color(255, 255, 255));
        correo.setText("00000000000");

        web.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        web.setForeground(new java.awt.Color(255, 255, 255));
        web.setText("00000000000");

        impresion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        impresion.setForeground(new java.awt.Color(255, 255, 255));
        impresion.setText("00000000000");

        lblDireccion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        lblDireccion.setForeground(new java.awt.Color(255, 255, 255));
        lblDireccion.setText("DIRECCION");

        lblRazonSocial.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        lblRazonSocial.setForeground(new java.awt.Color(255, 255, 255));
        lblRazonSocial.setText("RAZON SOCIAL");

        lblRucEmpresa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        lblRucEmpresa.setForeground(new java.awt.Color(255, 255, 255));
        lblRucEmpresa.setText("00000000000");

        usa_cnt_flotante.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        usa_cnt_flotante.setForeground(new java.awt.Color(255, 255, 255));
        usa_cnt_flotante.setText("00000000000");

        serv_datos.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        serv_datos.setForeground(new java.awt.Color(255, 255, 255));
        serv_datos.setText("00000000000");

        producto_igv.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        producto_igv.setForeground(new java.awt.Color(255, 255, 255));
        producto_igv.setText("00000000000");

        pass_aplicacion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        pass_aplicacion.setForeground(new java.awt.Color(255, 255, 255));
        pass_aplicacion.setText("00000000000");

        correo_aplicacion.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        correo_aplicacion.setForeground(new java.awt.Color(255, 255, 255));
        correo_aplicacion.setText("00000000000");

        logo.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        logo.setForeground(new java.awt.Color(255, 255, 255));
        logo.setText("00000000000");

        email_contador.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        email_contador.setForeground(new java.awt.Color(255, 255, 255));
        email_contador.setText("00000000000");

        impre_directa.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        impre_directa.setForeground(new java.awt.Color(255, 255, 255));
        impre_directa.setText("00000000000");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.GridLayout(1, 0));

        AccesoClientes.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/colaboradores_16px.png"))); // NOI18N
        AccesoClientes.setText("PERSONAS");
        AccesoClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoClientes.setFocusable(false);
        AccesoClientes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoClientes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoClientesActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoClientes);

        AccesoProductos.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/stockGeneral_16px.png"))); // NOI18N
        AccesoProductos.setText("PRODUCTOS");
        AccesoProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoProductos.setFocusable(false);
        AccesoProductos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoProductos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoProductosActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoProductos);

        AccesoPresupuesto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoPresupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        AccesoPresupuesto.setText("COTIZACIÓN");
        AccesoPresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoPresupuesto.setFocusable(false);
        AccesoPresupuesto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoPresupuesto.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoPresupuestoActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoPresupuesto);

        AccesoVentas_internas.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoVentas_internas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        AccesoVentas_internas.setText("N. DE VENTA");
        AccesoVentas_internas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoVentas_internas.setFocusable(false);
        AccesoVentas_internas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoVentas_internas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoVentas_internas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoVentas_internasActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoVentas_internas);

        AccesoCompras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        AccesoCompras.setText("COMPRAS");
        AccesoCompras.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoCompras.setFocusable(false);
        AccesoCompras.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoCompras.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoCompras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoComprasActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoCompras);
        jPanel1.add(jLabel1);
        jPanel1.add(jLabel2);
        jPanel1.add(jLabel3);

        AccesoCompras1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AccesoCompras1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        AccesoCompras1.setText("INICIO");
        AccesoCompras1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        AccesoCompras1.setFocusable(false);
        AccesoCompras1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        AccesoCompras1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        AccesoCompras1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AccesoCompras1ActionPerformed(evt);
            }
        });
        jPanel1.add(AccesoCompras1);

        jbtnAbrirBandeja1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jbtnAbrirBandeja1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        jbtnAbrirBandeja1.setText("SALIR");
        jbtnAbrirBandeja1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbtnAbrirBandeja1.setFocusable(false);
        jbtnAbrirBandeja1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbtnAbrirBandeja1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbtnAbrirBandeja1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnAbrirBandeja1ActionPerformed(evt);
            }
        });
        jPanel1.add(jbtnAbrirBandeja1);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        btnNotaVenta1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNotaVenta1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        btnNotaVenta1.setText("COMPRAS ");
        btnNotaVenta1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNotaVenta1.setFocusable(false);
        btnNotaVenta1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnNotaVenta1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNotaVenta1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNotaVenta1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotaVenta1ActionPerformed(evt);
            }
        });
        jPanel2.add(btnNotaVenta1);

        btnPresupuesto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPresupuesto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        btnPresupuesto.setText("COTIZACIÓN");
        btnPresupuesto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPresupuesto.setFocusable(false);
        btnPresupuesto.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnPresupuesto.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnPresupuesto.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPresupuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPresupuestoActionPerformed(evt);
            }
        });
        jPanel2.add(btnPresupuesto);

        btnNotaVenta.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnNotaVenta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        btnNotaVenta.setText("N. DE VENTA");
        btnNotaVenta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNotaVenta.setFocusable(false);
        btnNotaVenta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnNotaVenta.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNotaVenta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnNotaVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotaVentaActionPerformed(evt);
            }
        });
        jPanel2.add(btnNotaVenta);

        getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

        jpnlPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        jpnlPrincipal.setLayout(new java.awt.GridLayout(1, 0));
        getContentPane().add(jpnlPrincipal, java.awt.BorderLayout.CENTER);

        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N

        menArchivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        menArchivo.setText("Inicio  ");
        menArchivo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        menArchivo.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);

        jmiInicio.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        jmiInicio.setText("Inicio");
        jmiInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiInicioActionPerformed(evt);
            }
        });
        menArchivo.add(jmiInicio);

        jmiProductos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/stockGeneral_16px.png"))); // NOI18N
        jmiProductos.setText("Productos");
        jmiProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiProductosActionPerformed(evt);
            }
        });
        menArchivo.add(jmiProductos);

        jmiClientes.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/colaboradores_16px.png"))); // NOI18N
        jmiClientes.setText("Clientes & Proveedores");
        jmiClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiClientesActionPerformed(evt);
            }
        });
        menArchivo.add(jmiClientes);
        menArchivo.add(jSeparator1);

        jmiCerrarSesion.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiCerrarSesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        jmiCerrarSesion.setText("Cerrar sesión");
        jmiCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiCerrarSesionActionPerformed(evt);
            }
        });
        menArchivo.add(jmiCerrarSesion);

        jMenuBar1.add(menArchivo);

        menArchivo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        menArchivo1.setText("Facturación  ");
        menArchivo1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        menArchivo1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);

        MenuPresupuestos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        MenuPresupuestos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        MenuPresupuestos.setText("Presupuestos (Cotizaciones)");
        MenuPresupuestos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuPresupuestosActionPerformed(evt);
            }
        });
        menArchivo1.add(MenuPresupuestos);

        MenuVentas_internas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        MenuVentas_internas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        MenuVentas_internas.setText("Notas de Ventas ");
        MenuVentas_internas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuVentas_internasActionPerformed(evt);
            }
        });
        menArchivo1.add(MenuVentas_internas);

        jMenuBar1.add(menArchivo1);

        menArchivo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        menArchivo2.setText("Reportes  ");
        menArchivo2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        menArchivo2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jMenu1.setText("Reportes Principales");

        jmiConfigurar4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar4.setText("Generar Reporte de Notas de Venta");
        jmiConfigurar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar4ActionPerformed(evt);
            }
        });
        jMenu1.add(jmiConfigurar4);

        jmiConfigurar5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar5.setText("Generar Reporte de Presupuestos");
        jmiConfigurar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar5ActionPerformed(evt);
            }
        });
        jMenu1.add(jmiConfigurar5);

        menArchivo2.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jMenu2.setText("Reportes Principales por Cliente");

        jmiConfigurar7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar7.setText("Generar Reporte de Notas de Venta por Cliente");
        jmiConfigurar7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar7ActionPerformed(evt);
            }
        });
        jMenu2.add(jmiConfigurar7);

        jmiConfigurar8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar8.setText("Generar Reporte de Presupuestos por Cliente");
        jmiConfigurar8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar8ActionPerformed(evt);
            }
        });
        jMenu2.add(jmiConfigurar8);

        menArchivo2.add(jMenu2);

        jMenuBar1.add(menArchivo2);

        jmenAdministrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Home_16px.png"))); // NOI18N
        jmenAdministrar.setText(" Administrar  ");
        jmenAdministrar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jmiConfigurar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        jmiConfigurar.setText("Configuración");
        jmiConfigurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurarActionPerformed(evt);
            }
        });
        jmenAdministrar.add(jmiConfigurar);

        jmiConfigurar1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/servers_16.png"))); // NOI18N
        jmiConfigurar1.setText("Cambiar servidor de Datos");
        jmiConfigurar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar1ActionPerformed(evt);
            }
        });
        jmenAdministrar.add(jmiConfigurar1);
        jmenAdministrar.add(jSeparator5);

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Check_16px.png"))); // NOI18N
        jMenu3.setText("Exportar Datos Masivos");

        jmiConfigurar9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar9.setText("Exportar Datos de Clientes");
        jmiConfigurar9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar9ActionPerformed(evt);
            }
        });
        jMenu3.add(jmiConfigurar9);

        jmiConfigurar10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar10.setText("Exportar Datos de Productos");
        jmiConfigurar10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar10ActionPerformed(evt);
            }
        });
        jMenu3.add(jmiConfigurar10);

        jmenAdministrar.add(jMenu3);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jMenu4.setText("Importar Datos Masivos");

        jmiConfigurar11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar11.setText("Importar Datos de Clientes");
        jmiConfigurar11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar11ActionPerformed(evt);
            }
        });
        jMenu4.add(jmiConfigurar11);

        jmiConfigurar12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiConfigurar12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        jmiConfigurar12.setText("Importar Datos de Productos");
        jmiConfigurar12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiConfigurar12ActionPerformed(evt);
            }
        });
        jMenu4.add(jmiConfigurar12);

        ImportarProveedor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        ImportarProveedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        ImportarProveedor.setText("Importar Datos de Proveedores");
        ImportarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImportarProveedorActionPerformed(evt);
            }
        });
        jMenu4.add(ImportarProveedor);

        jmenAdministrar.add(jMenu4);
        jmenAdministrar.add(jSeparator2);

        jmiUsuarios.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jmiUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/colaboradores_16px.png"))); // NOI18N
        jmiUsuarios.setText("Usuarios");
        jmiUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiUsuariosActionPerformed(evt);
            }
        });
        jmenAdministrar.add(jmiUsuarios);

        jMenuBar1.add(jmenAdministrar);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jmiInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiInicioActionPerformed
        JPanelInicio jpi = new JPanelInicio(rol);
        Metodos.CambiarPanel(jpi);
    }//GEN-LAST:event_jmiInicioActionPerformed

    private void jmiClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiClientesActionPerformed
        JPanePersonasListar jpcl = new JPanePersonasListar();
        Metodos.CambiarPanel(jpcl);
    }//GEN-LAST:event_jmiClientesActionPerformed

    private void jmiConfigurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurarActionPerformed
        JPanelConfigurar jpc = new JPanelConfigurar();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_jmiConfigurarActionPerformed

    private void jmiProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiProductosActionPerformed
        JPanelProductoListar jppl = new JPanelProductoListar(rol);
        Metodos.CambiarPanel(jppl);
    }//GEN-LAST:event_jmiProductosActionPerformed

    private void jmiUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiUsuariosActionPerformed
        JPanelUsuarioListar jpul = new JPanelUsuarioListar(rol);
        Metodos.CambiarPanel(jpul);
    }//GEN-LAST:event_jmiUsuariosActionPerformed

    private void jmiCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiCerrarSesionActionPerformed

        confirmarSalida();
    }//GEN-LAST:event_jmiCerrarSesionActionPerformed

    private void jbtnAbrirBandeja1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnAbrirBandeja1ActionPerformed

        confirmarSalida();
    }//GEN-LAST:event_jbtnAbrirBandeja1ActionPerformed

    private void AccesoProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoProductosActionPerformed
        JPanelProductoListar jppl = new JPanelProductoListar(rol);
        Metodos.CambiarPanel(jppl);
    }//GEN-LAST:event_AccesoProductosActionPerformed

    private void AccesoClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoClientesActionPerformed
        JPanePersonasListar jpcl = new JPanePersonasListar();
        Metodos.CambiarPanel(jpcl);
    }//GEN-LAST:event_AccesoClientesActionPerformed

    private void jmiConfigurar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar1ActionPerformed

        new Formulario_serverDatos(null, true).setVisible(true);
        if (Formulario_serverDatos.serv_datos.equalsIgnoreCase("")) {
            Metodos.MensajeAlerta("Servidor de Datos sin Cambios");
        }

    }//GEN-LAST:event_jmiConfigurar1ActionPerformed

    private void btnPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPresupuestoActionPerformed
        JPanelPresupuesto jpfn = new JPanelPresupuesto("", 0, "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnPresupuestoActionPerformed

    private void AccesoPresupuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoPresupuestoActionPerformed
        JPanelPresupuestos jpc = new JPanelPresupuestos();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_AccesoPresupuestoActionPerformed

    private void AccesoVentas_internasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoVentas_internasActionPerformed
        JPanelVenta_internas jpc = new JPanelVenta_internas();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_AccesoVentas_internasActionPerformed

    private void btnNotaVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotaVentaActionPerformed
        JPanelVenta_interna jpfn = new JPanelVenta_interna("", 0, "", "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnNotaVentaActionPerformed

    private void MenuPresupuestosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuPresupuestosActionPerformed
        JPanelPresupuestos jpc = new JPanelPresupuestos();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_MenuPresupuestosActionPerformed

    private void MenuVentas_internasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuVentas_internasActionPerformed
        JPanelVenta_internas jpc = new JPanelVenta_internas();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_MenuVentas_internasActionPerformed

    private void jmiConfigurar8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar8ActionPerformed

        new Formulario_GenerarRepoFechasCliente(null, true, "Presupuestos").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar8ActionPerformed

    private void jmiConfigurar7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar7ActionPerformed

        new Formulario_GenerarRepoFechasCliente(null, true, "Ventas_internas").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar7ActionPerformed

    private void jmiConfigurar9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar9ActionPerformed

        new Formulario_ExportarDatos(null, true, "Clientes").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar9ActionPerformed

    private void jmiConfigurar10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar10ActionPerformed

        new Formulario_ExportarDatos(null, true, "Productos").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar10ActionPerformed

    private void jmiConfigurar11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar11ActionPerformed
        JPanelImportarClientes jpfn = new JPanelImportarClientes();
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_jmiConfigurar11ActionPerformed

    private void jmiConfigurar12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar12ActionPerformed
        JPanelImportarProductos jpfn = new JPanelImportarProductos();
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_jmiConfigurar12ActionPerformed

    private void AccesoComprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoComprasActionPerformed
        JPanelCompras jpc = new JPanelCompras();
        Metodos.CambiarPanel(jpc);
    }//GEN-LAST:event_AccesoComprasActionPerformed

    private void ImportarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImportarProveedorActionPerformed
        JPanelImportarProveedores jpfn = new JPanelImportarProveedores();
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_ImportarProveedorActionPerformed

    private void jmiConfigurar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar5ActionPerformed

        new Formulario_GenerarRepoFechas(null, true, "Presupuestos").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar5ActionPerformed

    private void jmiConfigurar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiConfigurar4ActionPerformed

        new Formulario_GenerarRepoFechas(null, true, "Ventas_internas").setVisible(true);
    }//GEN-LAST:event_jmiConfigurar4ActionPerformed

    private void AccesoCompras1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AccesoCompras1ActionPerformed
        JPanelInicio jpi = new JPanelInicio(rol);
        Metodos.CambiarPanel(jpi);
    }//GEN-LAST:event_AccesoCompras1ActionPerformed

    private void btnNotaVenta1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotaVenta1ActionPerformed
        JPanelCompra jpfn = new JPanelCompra("", 0, "");
        Metodos.CambiarPanel(jpfn);
    }//GEN-LAST:event_btnNotaVenta1ActionPerformed
     

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFramePrincipal(rol, id_usuario, usuario).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AccesoClientes;
    private javax.swing.JButton AccesoCompras;
    private javax.swing.JButton AccesoCompras1;
    private javax.swing.JButton AccesoPresupuesto;
    private javax.swing.JButton AccesoProductos;
    private javax.swing.JButton AccesoVentas_internas;
    private javax.swing.JMenuItem ImportarProveedor;
    private javax.swing.JMenuItem MenuPresupuestos;
    private javax.swing.JMenuItem MenuVentas_internas;
    private javax.swing.JButton btnNotaVenta;
    private javax.swing.JButton btnNotaVenta1;
    private javax.swing.JButton btnPresupuesto;
    public static javax.swing.JLabel correo;
    public static javax.swing.JLabel correo_aplicacion;
    public static javax.swing.JLabel email_contador;
    public static javax.swing.JLabel impre_directa;
    public static javax.swing.JLabel impresion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JButton jbtnAbrirBandeja1;
    private javax.swing.JMenu jmenAdministrar;
    private javax.swing.JMenuItem jmiCerrarSesion;
    private javax.swing.JMenuItem jmiClientes;
    private javax.swing.JMenuItem jmiConfigurar;
    private javax.swing.JMenuItem jmiConfigurar1;
    private javax.swing.JMenuItem jmiConfigurar10;
    private javax.swing.JMenuItem jmiConfigurar11;
    private javax.swing.JMenuItem jmiConfigurar12;
    private javax.swing.JMenuItem jmiConfigurar4;
    private javax.swing.JMenuItem jmiConfigurar5;
    private javax.swing.JMenuItem jmiConfigurar7;
    private javax.swing.JMenuItem jmiConfigurar8;
    private javax.swing.JMenuItem jmiConfigurar9;
    private javax.swing.JMenuItem jmiInicio;
    private javax.swing.JMenuItem jmiProductos;
    private javax.swing.JMenuItem jmiUsuarios;
    public static javax.swing.JPanel jpnlPrincipal;
    public static javax.swing.JLabel lblDireccion;
    public static javax.swing.JLabel lblRazonSocial;
    public static javax.swing.JLabel lblRucEmpresa;
    public static javax.swing.JLabel logo;
    private javax.swing.JMenu menArchivo;
    private javax.swing.JMenu menArchivo1;
    private javax.swing.JMenu menArchivo2;
    public static javax.swing.JLabel pass_aplicacion;
    public static javax.swing.JLabel producto_igv;
    public static javax.swing.JLabel serv_datos;
    public static javax.swing.JLabel telefono;
    public static javax.swing.JLabel usa_cnt_flotante;
    public static javax.swing.JLabel web;
    // End of variables declaration//GEN-END:variables
}
