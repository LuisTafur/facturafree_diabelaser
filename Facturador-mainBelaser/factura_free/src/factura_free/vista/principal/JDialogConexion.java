package factura_free.vista.principal;

import factura_free.clases.controlador.ConfigImpl;
import factura_free.clases.principales.Conectar;
import factura_free.clases.principales.Datos;
import factura_free.clases.principales.alternos.EncriparDatos;
import factura_free.clases.principales.alternos.Metodos;
import factura_free.clases.principales.reindexar.ReindexadoBD;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class JDialogConexion extends javax.swing.JDialog {

    EncriparDatos encriptar = new EncriparDatos();

    public JDialogConexion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        configurarVentana();
        cargarEnv();
    }

    private void configurarVentana() {
        //posiciono el frame al centro de la pantalla
        this.setLocationRelativeTo(null);
        //desactiva el cambio de tamaño de la ventana
        this.setResizable(false);
        //asigno titulo a mostrar del frame
        this.setTitle("Datos de Conexión");
        setIconImage(new ImageIcon(getClass().getResource("/img/cpe.png")).getImage());
    }

    private void cargarEnv() {
        String host = Datos.getHost();
        String puerto = Datos.getPuerto();
        String baseDatos = Datos.getBaseDatos();
        String usuario = Datos.getUsuario();
        String contrasena = Datos.getContrasena();
        String ubicacion = Datos.getUbicacionDistrito();

        txtHost.setText((host));
        txtPuerto.setText((puerto));
        txtBaseDatos.setText((baseDatos));
        txtUsuario.setText((usuario));
        txtContrasena.setText((contrasena));
        txtUbicacion.setText((ubicacion));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtUbicacion = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel12 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnProbar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtHost = new org.edisoncor.gui.textField.TextFieldRectBackground();
        txtPuerto = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel8 = new javax.swing.JLabel();
        txtBaseDatos = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel9 = new javax.swing.JLabel();
        txtUsuario = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel10 = new javax.swing.JLabel();
        txtContrasena = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel11 = new javax.swing.JLabel();
        btnProbar1 = new javax.swing.JButton();

        txtUbicacion.setAnchoDeBorde(1.0F);
        txtUbicacion.setDescripcion("Ingrese una descripción");
        txtUbicacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel12.setForeground(new java.awt.Color(102, 102, 102));
        jLabel12.setText("Ubicación de Local (Distrito) *");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/guardar_16px.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir_16px.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnProbar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnProbar.setText("Probar");
        btnProbar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProbarActionPerformed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Servidor *");

        txtHost.setAnchoDeBorde(1.0F);
        txtHost.setDescripcion("Ingrese una descripción");
        txtHost.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtPuerto.setAnchoDeBorde(1.0F);
        txtPuerto.setDescripcion("Ingrese una descripción");
        txtPuerto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Puerto *");

        txtBaseDatos.setAnchoDeBorde(1.0F);
        txtBaseDatos.setDescripcion("Ingrese una descripción");
        txtBaseDatos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Base datos *");

        txtUsuario.setAnchoDeBorde(1.0F);
        txtUsuario.setDescripcion("Ingrese una descripción");
        txtUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel10.setForeground(new java.awt.Color(102, 102, 102));
        jLabel10.setText("Usuario *");

        txtContrasena.setAnchoDeBorde(1.0F);
        txtContrasena.setDescripcion("Ingrese una descripción");
        txtContrasena.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel11.setForeground(new java.awt.Color(102, 102, 102));
        jLabel11.setText("Contraseña *");

        btnProbar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Online_16px.png"))); // NOI18N
        btnProbar1.setText("Crear Base de Datos");
        btnProbar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProbar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtHost, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPuerto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBaseDatos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtContrasena, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProbar, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnProbar1)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnProbar1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtHost, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBaseDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnProbar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel4);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        String host = txtHost.getText();
        String puerto = txtPuerto.getText();
        String baseDatos = txtBaseDatos.getText();
        String usuario = txtUsuario.getText();
        String contrasena = txtContrasena.getText();
        String ubicacion = txtUbicacion.getText();

        try {
            ConfigImpl.ActualizarENV(encriptar.Encriptar(host), encriptar.Encriptar(puerto), encriptar.Encriptar(baseDatos), encriptar.Encriptar(usuario), 
                    encriptar.Encriptar(contrasena), encriptar.Encriptar(ubicacion));
            dispose();
        } catch (Exception e) {
            System.out.println("Error guardando: " + e);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnProbarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProbarActionPerformed
        try {
            String host = txtHost.getText();
            String puerto = txtPuerto.getText();
            String baseDatos = txtBaseDatos.getText();
            String usuario = txtUsuario.getText();
            String contrasena = txtContrasena.getText();

            Conectar c = new Conectar();
            c.getConexionTest(host, puerto, baseDatos, usuario, contrasena);

        } catch (Exception e) {
            Metodos.MensajeError("Error: " + e);
        }
    }//GEN-LAST:event_btnProbarActionPerformed

    private void btnProbar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProbar1ActionPerformed
        Conectar.reindexadoBDFacturaFree();

        txtHost.setText("localhost");
        txtPuerto.setText("3306");
        txtBaseDatos.setText("factura_free");
        txtUsuario.setText("root");
        txtContrasena.setText("");

        String host = txtHost.getText();
        String puerto = txtPuerto.getText();
        String baseDatos = txtBaseDatos.getText();
        String usuario = txtUsuario.getText();
        String contrasena = txtContrasena.getText();
        String ubicacion = txtUbicacion.getText();

        try {
            ConfigImpl.ActualizarENV(encriptar.Encriptar(host), encriptar.Encriptar(puerto), encriptar.Encriptar(baseDatos), encriptar.Encriptar(usuario), 
                    encriptar.Encriptar(contrasena),encriptar.Encriptar(ubicacion));

            JOptionPane.showMessageDialog(null, "Esta función permite mantener actualizada la Base de Datos del sistema.\n"
                    + "Se recomienda aplicar cuando se realiza la actualización del sistema. Si tiene dudas comuníquese al 924-899-800.\n"
                    + "Precione ACEPTAR para continuar con el proceso solicitado.", "Mensaje", JOptionPane.INFORMATION_MESSAGE);

            ReindexadoBD.reindexarBase_de_Datos();
            Metodos.MensajeInformacion("Poceso Exitoso. Cerraremos el sistema por su seguridad");
            System.exit(-1);
        } catch (Exception e) {
            System.out.println("Error guardando: " + e);
        }
    }//GEN-LAST:event_btnProbar1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogConexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogConexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogConexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogConexion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogConexion dialog = new JDialogConexion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnProbar;
    private javax.swing.JButton btnProbar1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtBaseDatos;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtContrasena;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtHost;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtPuerto;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtUbicacion;
    private org.edisoncor.gui.textField.TextFieldRectBackground txtUsuario;
    // End of variables declaration//GEN-END:variables
}
